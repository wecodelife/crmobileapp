import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CoachTrainingSessionLocation extends StatefulWidget {
  final String locationAddress;
  final String locationName;
  final bool rated;
  final bool liked;
  final String startTime1;
  final String startTime2;
  final String endTime1;
  final String endTime2;
  final String likes;
  final String bookmarks;
  final String distance;
  final String time;

  CoachTrainingSessionLocation(
      {this.locationName,
      this.locationAddress,
      this.rated,
      this.liked,
      this.likes,
      this.time,
      this.bookmarks,
      this.distance,
      this.endTime1,
      this.endTime2,
      this.startTime1,
      this.startTime2});

  @override
  _CoachTrainingSessionLocationState createState() =>
      _CoachTrainingSessionLocationState();
}

class _CoachTrainingSessionLocationState
    extends State<CoachTrainingSessionLocation> {
  bool rated;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Icon(
              Icons.location_on_outlined,
              size: 16,
              color: Colors.white,
            ),
            SizedBox(
              width: 2,
            ),
            Container(
                width: screenWidth(context, dividedBy: 3),
                child: Text(
                  widget.locationName,
                  style: TextStyle(
                      fontSize: screenWidth(context, dividedBy: 33),
                      fontFamily: 'OpenSansRegular',
                      color: Colors.white),
                )),
            Spacer(
              flex: 1,
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: screenWidth(context, dividedBy: 4),
              child: Text(widget.locationAddress,
                  style: TextStyle(
                      fontSize: screenWidth(context, dividedBy: 40),
                      fontFamily: 'OpenSansRegular',
                      color: Colors.white),
                  overflow: TextOverflow.visible),
            ),
            SizedBox(
              width: screenWidth(context, dividedBy: 9),
            ),
            Column(
              children: [
                Row(
                  children: [
                    widget.rated == true
                        ? GestureDetector(
                            onTap: () {},
                            child: Icon(
                              Icons.star_rate_rounded,
                              size: 16,
                              color: Constants.kitGradients[0],
                            ))
                        : Icon(
                            Icons.star_rate_rounded,
                            color: Constants.kitGradients[1],
                            size: 18,
                          ),
                    Container(
                      width: screenWidth(context, dividedBy: 18),
                      child: Text(widget.likes,
                          style: TextStyle(
                              fontSize: screenWidth(context, dividedBy: 40),
                              fontFamily: 'OpenSansRegular',
                              color: Colors.white),
                          overflow: TextOverflow.ellipsis),
                    ),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 100),
                    ),
                    Container(
                      width: 2,
                      height: 17,
                      color: Colors.white,
                    ),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 80),
                    ),
                    widget.liked == true
                        ? GestureDetector(
                            onTap: () {},
                            child: Image.asset(
                              "assets/icons/people_icon.png",
                              color: Constants.kitGradients[0],
                            ))
                        : Image.asset(
                            "assets/icons/people_icon.png",
                            color: Constants.kitGradients[0],
                          ),
                    SizedBox(
                      width: 3,
                    ),
                    Container(
                      width: screenWidth(context, dividedBy: 15),
                      child: Text(widget.bookmarks,
                          style: TextStyle(
                              fontSize: screenWidth(context, dividedBy: 40),
                              fontFamily: 'OpenSansRegular',
                              color: Colors.white),
                          overflow: TextOverflow.ellipsis),
                    ),
                    Container(
                      width: 2,
                      height: 17,
                      color: Colors.white,
                    ),
                    SizedBox(
                      width: 3,
                    ),
                    Container(
                      width: screenWidth(context, dividedBy: 12),
                      child: Text(widget.time,
                          style: TextStyle(
                              fontSize: screenWidth(context, dividedBy: 40),
                              fontFamily: 'OpenSansRegular',
                              color: Colors.white),
                          overflow: TextOverflow.ellipsis),
                    ),
                    Icon(
                      Icons.circle,
                      size: 8,
                      color: Colors.red,
                    ),
                    SizedBox(
                      width: 3,
                    ),
                    Container(
                      width: screenWidth(context, dividedBy: 9),
                      child: Text(widget.distance + " Km",
                          style: TextStyle(
                              fontSize: screenWidth(context, dividedBy: 40),
                              fontFamily: 'OpenSansRegular',
                              color: Colors.white),
                          overflow: TextOverflow.ellipsis),
                    ),
                  ],
                ),
                Container(
                  width: screenWidth(context, dividedBy: 2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 5,
                      ),
                      Container(
                        // width: screenWidth(context, dividedBy: 4),
                        child: Text(
                            "M-F:" +
                                widget.startTime1 +
                                "-" +
                                widget.startTime2,
                            style: TextStyle(
                                fontSize: 11,
                                fontFamily: 'OpenSansRegular',
                                color: Colors.white),
                            overflow: TextOverflow.ellipsis),
                      ),
                      Spacer(
                        flex: 1,
                      ),
                      Container(
                        // width: screenWidth(context, dividedBy: 4),
                        child: Text(
                            " S-Su:" + widget.endTime1 + "-" + widget.endTime2,
                            style: TextStyle(
                                fontSize: 11,
                                fontFamily: 'OpenSansRegular',
                                color: Colors.white),
                            overflow: TextOverflow.ellipsis),
                      ),
                      Spacer(
                        flex: 2,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }
}
