import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
class BottomContainer extends StatefulWidget {
  @override
  _BottomContainerState createState() => _BottomContainerState();
}

class _BottomContainerState extends State<BottomContainer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context,dividedBy: 90),
      width: screenWidth(context,dividedBy:2),

      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(40)
      ),

    );
  }
}
