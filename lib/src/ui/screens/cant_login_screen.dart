import 'package:app_template/src/ui/screens/forget_email_screen.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/designation_selection_box.dart';
import 'package:app_template/src/ui/widgets/forget_password_screen.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class CantLoginScreen extends StatefulWidget {
  @override
  _OnBoarding5State createState() => _OnBoarding5State();
}

class _OnBoarding5State extends State<CantLoginScreen> {
  bool password = false;
  bool username = false;
  // bool businessman = false;
  // bool allSelected = false;
  bool disabled = true;
  bool loading = false;
  int id;

  didUpdate() {
    if (password == true || username == true) {
      disabled = false;
    } else {
      disabled = true;
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: loading == false
          ? Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      TextProgressIndicator(
                        heading: "LET'S GET YOU BACK INTO YOUR ACCOUNT",
                        descriptionText:
                            "No problem we will help get you back into your account.",
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 8),
                      ),
                      DesignationSelectionBox(
                        descriptionText:
                            "I remember my password but I cant remember what my username is.",
                        heading: "I FORGOT MY USERNAME",
                        onPressed: () {
                          setState(() {
                            didUpdate();
                            username = !username;
                            password = false;
                          });
                        },
                        selected: username,
                      ),
                      DesignationSelectionBox(
                        descriptionText:
                            "I remember my username but I cant remember what my password is.",
                        heading: "I FORGOT MY PASSWORD",
                        onPressed: () {
                          setState(() {
                            password = !password;
                            username = false;
                            didUpdate();
                          });
                        },
                        selected: password,
                      ),
                      // DesignationSelectionBox(
                      //   descriptionText:
                      //       "A Club, Organization, or Facility looking to find members or rent out facilities.",
                      //   heading: "I AM A BUSINESSMAN",
                      //   onPressed: () {
                      //     setState(() {
                      //       id = 3;
                      //       businessman = !businessman;
                      //       parent = false;
                      //       coach = false;
                      //       allSelected = false;
                      //       didUpdate();
                      //     });
                      //   },
                      //   selected: businessman,
                      // ),
                      // DesignationSelectionBox(
                      //   descriptionText: "Perfect Blend of Work and Play",
                      //   heading: "I AM A COACH/REF & PARENT/ATHLETE",
                      //   onPressed: () {
                      //     setState(() {
                      //       id = 4;
                      //       allSelected = !allSelected;
                      //       parent = false;
                      //       businessman = false;
                      //       coach = false;
                      //       didUpdate();
                      //     });
                      //   },
                      //   selected: allSelected,
                      // ),
                    ],
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 8),
                ),
                BuildButton(
                  title: "CANCEL",
                  onPressed: () {},
                  transparent: true,
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 40),
                ),
                BuildButton(
                  title: "NEXT",
                  onPressed: () {
                    didUpdate();
                    username == true
                        ? push(context, ForgetEmailScreen())
                        : push(context, ForgetPasswordScreen());
                  },
                  disabled: disabled,
                )
              ],
            )
          : Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }
}
