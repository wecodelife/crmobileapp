// To parse this JSON data, do
//
//     final addReferenceRequest = addReferenceRequestFromJson(jsonString);

import 'dart:convert';

AddReferenceRequest addReferenceRequestFromJson(String str) =>
    AddReferenceRequest.fromJson(json.decode(str));

String addReferenceRequestToJson(AddReferenceRequest data) =>
    json.encode(data.toJson());

class AddReferenceRequest {
  AddReferenceRequest({
    this.trainer,
    this.name,
    this.contactNumber,
    this.address,
    this.address2,
    this.city,
    this.state,
    this.zip,
  });

  final int trainer;
  final String name;
  final String contactNumber;
  final String address;
  final String address2;
  final String city;
  final int state;
  final int zip;

  factory AddReferenceRequest.fromJson(Map<String, dynamic> json) =>
      AddReferenceRequest(
        trainer: json["trainer"] == null ? null : json["trainer"],
        name: json["name"] == null ? null : json["name"],
        contactNumber:
            json["contact_number"] == null ? null : json["contact_number"],
        address: json["address"] == null ? null : json["address"],
        address2: json["address_2"] == null ? null : json["address_2"],
        city: json["city"] == null ? null : json["city"],
        state: json["state"] == null ? null : json["state"],
        zip: json["zip"] == null ? null : json["zip"],
      );

  Map<String, dynamic> toJson() => {
        "trainer": trainer == null ? null : trainer,
        "name": name == null ? null : name,
        "contact_number": contactNumber == null ? null : contactNumber,
        "address": address == null ? null : address,
        "address_2": address2 == null ? null : address2,
        "city": city == null ? null : city,
        "state": state == null ? null : state,
        "zip": zip == null ? null : zip,
      };
}
