// To parse this JSON data, do
//
//     final getAgeRangeResponse = getAgeRangeResponseFromJson(jsonString);

import 'dart:convert';

GetAgeRangeResponse getAgeRangeResponseFromJson(String str) =>
    GetAgeRangeResponse.fromJson(json.decode(str));

String getAgeRangeResponseToJson(GetAgeRangeResponse data) =>
    json.encode(data.toJson());

class GetAgeRangeResponse {
  GetAgeRangeResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final List<DatumAge> data;

  factory GetAgeRangeResponse.fromJson(Map<String, dynamic> json) =>
      GetAgeRangeResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null
            ? null
            : List<DatumAge>.from(
                json["data"].map((x) => DatumAge.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DatumAge {
  DatumAge({
    this.id,
    this.value,
    this.order,
  });

  final int id;
  final String value;
  final int order;

  factory DatumAge.fromJson(Map<String, dynamic> json) => DatumAge(
        id: json["id"] == null ? null : json["id"],
        value: json["value"] == null ? null : json["value"],
        order: json["order"] == null ? null : json["order"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "value": value == null ? null : value,
        "order": order == null ? null : order,
      };
}
