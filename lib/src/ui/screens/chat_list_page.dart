import 'package:app_template/src/ui/screens/chat_page.dart';
import 'package:app_template/src/ui/screens/new_chat.dart';
import 'package:app_template/src/ui/widgets/appbar_cr.dart';
import 'package:app_template/src/ui/widgets/bottom_navigation_bar_widget.dart';
import 'package:app_template/src/ui/widgets/chat_list_tile.dart';
import 'package:app_template/src/ui/widgets/search_bar.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class ChatListPage extends StatefulWidget {
  @override
  _ChatListPageState createState() => _ChatListPageState();
}

class _ChatListPageState extends State<ChatListPage> {
  TextEditingController searchTextEditingController =
      new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        actions: [
          AppBarCr(
            leftTextYes: true,
            leftText: "Edit",
            onTapLeftIcon: () {},
            onTapRightIcon: () {
              push(context, NewChat());
            },
            rightTextYes: false,
            rightIcon: "assets/icons/icon_plus.svg",
            pageTitle: "CHAT",
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationWidget(),
      body: Container(
        height: screenHeight(context, dividedBy: 1),
        width: screenWidth(context, dividedBy: 1),
        child: ListView(
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 20)),
              child: SearchBar(
                hintText: "Start typing to search..",
                textEditingController: searchTextEditingController,
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),
            Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 3),
              child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: 1,
                  itemBuilder: (BuildContext cntxt, int index) {
                    return GestureDetector(
                      onTap: () {
                        push(context, ChatPage());
                      },
                      child: ChatListTile(
                        imageName: "assets/images/user_image.png",
                        time: "5.30pm",
                        name: "DanSmith",
                        phoneNumber: "4567898765",
                      ),
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }
}
