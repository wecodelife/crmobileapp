import 'package:app_template/src/ui/screens/search_feilds.dart';
import 'package:app_template/src/ui/widgets/bottom_navigation_bar_widget.dart';
import 'package:app_template/src/ui/widgets/home_fliter_tile.dart';
import 'package:app_template/src/ui/widgets/home_listview_header.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/rating_bottom_sheet.dart';
import 'package:app_template/src/ui/widgets/search_bar.dart';
import 'package:app_template/src/ui/widgets/search_item_tile.dart';
import 'package:app_template/src/ui/widgets/search_tile_trainers.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class SearchItems extends StatefulWidget {
  @override
  _SearchItemsState createState() => _SearchItemsState();
}

class _SearchItemsState extends State<SearchItems> {
  TextEditingController searchTextEditingController =
      new TextEditingController();

  List<String> ratings = ["4.5+", "4+", "3+"];
  List<String> groups = ["Camp", "Clinic", "Groups<5", "Teams 5+"];
  List<String> price = ["\u0024", "\u0024\u0024"];
  List<String> gender = ["Male", "Female"];
  List<String> time = [
    "Any",
    "Early AM",
    "Morning",
    "Afternoon",
    "Evening",
    "Late"
  ];
  List<String> week = [
    "Any",
    "Today",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
  ];
  List<String> distance = [
    "Any",
    "<10 Miles",
    "<25 Miles",
    "<50 Miles",
    "<100 Miles"
  ];
  List<String> facilities = ["Any", "Coach Reserves", "My Home"];
  List<bool> color = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];

  List<String> filters = [
    "Over 4.5",
    "Groups",
    "\u0024, \u0024\u0024",
    "Gender",
    "Any Time of Day",
    "Day of Week",
    "Under 10 Miles",
    "Remote",
    "Facilities"
  ];
  int filterIndex;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationWidget(),
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: SingleChildScrollView(
        child: Container(
          height: screenHeight(context, dividedBy: 1),
          width: screenWidth(context, dividedBy: 1),
          child: Column(
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 40),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30)),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Constants.kitGradients[1])),
                  child: SearchBar(
                    hintText: "Type to start Search...",
                    textEditingController: searchTextEditingController,
                    rightIcon: true,
                    onTap: () {
                      push(context, SearchFields());
                    },
                  ),
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 80),
              ),
              Container(
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 15),
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: 9,
                  itemBuilder: (BuildContext context, int index) {
                    return HomeFilterTile(
                      label: filters[index],
                      colorChange: color[index],
                      onPressed: () {
                        displayBottomSheet(context);
                        setState(() {
                          filterIndex = index;
                        });
                      },
                    );
                  },
                ),
              ),
              HomeListViewHeader(
                title: "GOODS/ITEMS (5)",
              ),
              Container(
                height: screenHeight(context, dividedBy: 3),
                width: screenWidth(context, dividedBy: 1),
                child: ListView.builder(
                    itemCount: 5,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext cntxt, int index) {
                      return SearchItemTile();
                    }),
              ),
              HomeListViewHeader(
                title: "TRAINERS (4)",
              ),
              Container(
                height: screenHeight(context, dividedBy: 3),
                width: screenWidth(context, dividedBy: 1),
                child: ListView.builder(
                    itemCount: 4,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext cntxt, int index) {
                      return SearchTrainerTile();
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void displayBottomSheet(context) {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return filterIndex == 0
            ? BottomSheetHome(
                height: 3,
                title: "Ratings",
                label: ratings,
                onClicked: (value) {
                  setState(() {
                    color[0] = value;
                  });
                },
              )
            : filterIndex == 1
                ? BottomSheetHome(
                    height: 2.5,
                    title: "Groups",
                    label: groups,
                    onClicked: (value) {
                      setState(() {
                        color[1] = value;
                      });
                    },
                  )
                : filterIndex == 2
                    ? BottomSheetHome(
                        height: 3,
                        title: "Cost",
                        label: price,
                        onClicked: (value) {
                          setState(() {
                            color[2] = value;
                          });
                        },
                      )
                    : filterIndex == 3
                        ? BottomSheetHome(
                            height: 3,
                            title: "Gender",
                            label: gender,
                            onClicked: (value) {
                              setState(() {
                                color[3] = value;
                              });
                            },
                          )
                        : filterIndex == 4
                            ? BottomSheetHome(
                                height: 1.5,
                                title: "Time of Day",
                                label: time,
                                onClicked: (value) {
                                  setState(() {
                                    color[4] = value;
                                  });
                                },
                              )
                            : filterIndex == 5
                                ? BottomSheetHome(
                                    height: 1.2,
                                    title: "Day Of Week",
                                    label: week,
                                    onClicked: (value) {
                                      setState(() {
                                        color[5] = value;
                                      });
                                    },
                                  )
                                : filterIndex == 6
                                    ? BottomSheetHome(
                                        height: 2,
                                        title: "Distance",
                                        label: distance,
                                        onClicked: (value) {
                                          setState(() {
                                            color[6] = value;
                                          });
                                        },
                                      )
                                    : filterIndex == 7
                                        ? BottomSheetHome(
                                            height: 4,
                                            title: "Remote",
                                            onClicked: (value) {
                                              setState(() {
                                                color[7] = value;
                                              });
                                            },
                                          )
                                        : BottomSheetHome(
                                            height: 2.8,
                                            title: "Facilities",
                                            label: facilities,
                                            onClicked: (value) {
                                              setState(() {
                                                color[8] = value;
                                              });
                                            },
                                          );
      },
    );
  }
}
