import 'package:app_template/material/pickers/date_range_picker_dialog.dart';
import 'package:app_template/src/ui/screens/chat_list_page.dart';
import 'package:app_template/src/ui/screens/home_screen.dart';
import 'package:app_template/src/ui/screens/profile_page.dart';
import 'package:app_template/src/ui/screens/search_athletes.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BottomNavigationWidget extends StatefulWidget {
  final bool coach;
  final String image;
  final String sessionName;
  final Widget navigatePageName;
  BottomNavigationWidget(
      {this.coach, this.sessionName, this.image, this.navigatePageName});
  @override
  _BottomNavigationWidgetState createState() => _BottomNavigationWidgetState();
}

class _BottomNavigationWidgetState extends State<BottomNavigationWidget> {
  Future<Null> selectDateRange(BuildContext context) async {
    DateTimeRange picked = await showDatesRangePicker(
        context: context,
        // initialDateRange: DateTimeRange(
        //   start: DateTime.now().add(Duration(days: 0)),
        //   end: DateTime.now().add(Duration(days: 1)),
        // ),

        firstDate: DateTime.now(),
        lastDate: DateTime(DateTime.now().year + 5),
        helpText: "Select_Date_Range",
        cancelText: "CANCEL",
        confirmText: "OK",
        saveText: "SAVE",
        errorFormatText: "Invalid_format",
        errorInvalidText: "Out_of_range",
        errorInvalidRangeText: "Invalid_range",
        fieldStartHintText: "Start_Date",
        sessionName: widget.sessionName,
        fieldEndLabelText: "End_Date");

    // if (picked != null) {
    //   state.didChange(picked);
    // }
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      backgroundColor: Colors.white,
      unselectedFontSize: 1,
      showUnselectedLabels: false,
      showSelectedLabels: false,
      selectedLabelStyle: TextStyle(fontSize: 1),
      unselectedLabelStyle: TextStyle(fontSize: 1),
      items: [
        BottomNavigationBarItem(
            backgroundColor: Colors.black,
            icon: GestureDetector(
              onTap: () {
                if (widget.coach == true) {
                  push(context, widget.navigatePageName);
                } else {
                  push(context, HomePage());
                }
              },
              // child: ImageIcon(
              //   AssetImage("assets/icons/basket_icon.png"),
              //   size: 40,
              //   color: Constants.kitGradients[0],
              // ),
              child: widget.coach == true
                  ? Image.asset(widget.image)
                  : Icon(
                      Icons.home_rounded,
                      color: Constants.kitGradients[0],
                    ),
            ),
            label: ""),
        BottomNavigationBarItem(
            icon: GestureDetector(
              onTap: () {
                push(
                  context,
                  ChatListPage(),
                );
              },
              child: ImageIcon(
                AssetImage("assets/icons/chat_icon.png"),
                size: 40,
                color: Constants.kitGradients[0],
              ),
            ),
            label: ""),
        BottomNavigationBarItem(
            icon: GestureDetector(
              onTap: () {
                push(context, SearchAthletes());
              },
              child: ImageIcon(
                AssetImage("assets/icons/magnify_glass_icon.png"),
                size: 40,
                color: Constants.kitGradients[0],
              ),
            ),
            label: ""),
        BottomNavigationBarItem(
            icon: GestureDetector(
              onTap: () {
                selectDateRange(context);
              },
              child: ImageIcon(
                AssetImage("assets/icons/schedule_icon.png"),
                size: 40,
                color: Constants.kitGradients[0],
              ),
            ),
            label: ""),
        BottomNavigationBarItem(
          icon: GestureDetector(
            onTap: () {
              push(context, ProfilePage());
            },
            child: ImageIcon(
              AssetImage("assets/icons/profile_icon.png"),
              size: 40,
              color: Constants.kitGradients[0],
            ),
          ),
          label: "",
        ),
      ],
    );
  }
}
