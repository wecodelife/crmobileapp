// To parse this JSON data, do
//
//     final uploadVideoRequest = uploadVideoRequestFromJson(jsonString);

import 'dart:convert';

UploadVideoRequest uploadVideoRequestFromJson(String str) =>
    UploadVideoRequest.fromJson(json.decode(str));

String uploadVideoRequestToJson(UploadVideoRequest data) =>
    json.encode(data.toJson());

class UploadVideoRequest {
  UploadVideoRequest({
    this.video,
  });

  final dynamic video;

  factory UploadVideoRequest.fromJson(Map<String, dynamic> json) =>
      UploadVideoRequest(
        video: json["video"] == null ? null : json["video"],
      );

  Map<String, dynamic> toJson() => {
        "video": video == null ? null : video,
      };
}
