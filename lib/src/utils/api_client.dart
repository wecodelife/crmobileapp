import 'dart:async';

import 'package:app_template/src/models/add_athlete_request_model.dart';
import 'package:app_template/src/models/add_authorized_contact_request_model.dart';
import 'package:app_template/src/models/add_banking_info_request.dart';
import 'package:app_template/src/models/add_business_banking_info_request.dart';
import 'package:app_template/src/models/add_coach_request.dart';
import 'package:app_template/src/models/add_emergency_contact_request_model.dart';
import 'package:app_template/src/models/add_facility_rate_request.dart';
import 'package:app_template/src/models/add_facility_request.dart';
import 'package:app_template/src/models/add_parent_request_model.dart';
import 'package:app_template/src/models/add_reference_request.dart';
import 'package:app_template/src/models/book_zoom_meeting_request.dart';
import 'package:app_template/src/models/get_timeslots_request.dart';
import 'package:app_template/src/models/header_model.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/update_sports_request.dart';
import 'package:app_template/src/models/update_user_type_request_model.dart';
import 'package:app_template/src/models/upload_image_request_model.dart';
import 'package:app_template/src/models/upload_video_request.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:dio/dio.dart';

import 'object_factory.dart';

enum AccessMode { READ, WRITE }
HeaderModel authHeaderModel = new HeaderModel();

void setAuthHeaderModel({var accessMode, String xReferenceId}) {
  if (ObjectFactory().appHive != null &&
      ObjectFactory().appHive.getToken() != null &&
      ObjectFactory().appHive.getToken().trim().length > 0)
    authHeaderModel.authorization = "Bearer " +
        // "eyJraWQiOiJsSEJLdjdRZ2daRFcwM3lMdWRORU1jV3VGVFhmYUpuYWZSb21FMERVcmhrPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiJkZTk4YmRhMy05MWJiLTQ3YzktOWIzNC1kM2ZlZDVhOGY5MTgiLCJhdWQiOiIzNG0zNDhwY2phOXMzZTY2bnZyZmpvNjFscCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJldmVudF9pZCI6IjE0YWJlNjgwLWRlOWMtNDk2YS1hMTY3LTNlMzYzNzZjZDA0YSIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjE5MDcxMzk1LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0yLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMl81YzRWa0l4WFciLCJjb2duaXRvOnVzZXJuYW1lIjoiZGlzb24iLCJleHAiOjE2MTkwNzQ5OTUsImlhdCI6MTYxOTA3MTM5NSwiZW1haWwiOiJkaXNvbnNoaWJ1MjAxNkBnbWFpbC5jb20ifQ.nwN_JkYtW3btf8HEp6SZUi4vCFyDkDqx4w83A_Unv7lwqNFvwpqH9z0Tg_5pSbVjfCy8hoj2TLFvwca4MPnJbovtcO_2zLOWT444AIbTxAbQZ_IetEjgPIV7mbEmtetqh3ajEH5dnJD1vxwubCwKfJIsN27nK2kbbXVvn1_-Wgms3bzQ2atmuQmBXosG4oinBv8HKl3Rt6lEeZWBwQfGFHwBLsTjlYArfOmlnF0D2yaF6MLadJ_LCh9tX8qmcVEvBzgXvQZ8tZM0IG1rlFu0KgO-eXKAYnx8nLW1m8kfW7CJ4YKiqhF7ehgsSoEB-856_63Y6mGsqOFqMz_qPU2iWg";
        ObjectFactory().appHive.getToken();
  // authHeaderModel.xAccessMode = accessMode.toString();
  // authHeaderModel.xUdid = "";
  // authHeaderModel.xRequestTime = "";
  // authHeaderModel.xReferenceId = xReferenceId;
  // authHeaderModel.xSession = "";
  // if (ObjectFactory().appHive != null &&
  //     ObjectFactory().appHive.getXUser() != null &&
  //     ObjectFactory().appHive.getXUser().trim().length > 0)
  //   authHeaderModel.xUser = ObjectFactory().appHive.getXUser();
  // if (ObjectFactory().appHive != null &&
  //     ObjectFactory().appHive.getUserId() != null &&
  //     ObjectFactory().appHive.getUserId().trim().length > 0)
  //   authHeaderModel.xUserId = ObjectFactory().appHive.getUserId();
  // print("Token "+authHeaderModel.xTenantId);
  print("print" + authHeaderModel.authorization);
}

class ApiClient {
  HeaderModel loginModel = new HeaderModel();

  ///  user login
  Future<Response> loginRequest(LoginRequest loginRequest) {
    print(loginRequest.toString());

    return ObjectFactory()
        .appDio
        .loginPost(url: Urls.baseUrl, data: loginRequest, header: loginModel);
  }

  Future<Response> getBasicUserDetails() {
    setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.getBasicDetailsUrl, header: authHeaderModel);
  }

  Future<Response> getUserInvitationTypes() {
    // setAuthHeaderModel();
    return ObjectFactory()
        .appDio
        .get(url: Urls.getUserInvitationType, header: loginModel);
  }

  Future<Response> updateUserType(
      UpdateUserTypeRequestModel updateUserTypeRequestModel) {
    print(
        "ghgg ${updateUserTypeRequestModel.invitationType + updateUserTypeRequestModel.userType.toString()}");
    setAuthHeaderModel();
    return ObjectFactory().appDio.post(
        url: Urls.updateUserType,
        header: authHeaderModel,
        data: updateUserTypeRequestModel);
  }

  Future<Response> addParent(AddParentRequestModel addParentRequestModel) {
    print(
        "ghgg ${addParentRequestModel.name + addParentRequestModel.address.toString() + addParentRequestModel.email.toString() + addParentRequestModel.sports.toString() + addParentRequestModel.profPic.toString()}");
    setAuthHeaderModel();
    print(Urls.addParent);
    return ObjectFactory().appDio.post(
        url: Urls.addParent,
        header: authHeaderModel,
        data: addParentRequestModel);
  }

  Future<Response> uploadImage(UploadImageRequest uploadImageRequest) async {
    FormData formDataUploadProfileImage = FormData.fromMap({
      if (uploadImageRequest.image != null)
        "image": await MultipartFile.fromFile(uploadImageRequest.image.path,
            filename: uploadImageRequest.image.path.split('/').last),
    });
    // print(
    //     "ghgg ${updateUserTypeRequestModel.invitationType + updateUserTypeRequestModel.userType.toString()}");
    setAuthHeaderModel();
    return ObjectFactory().appDio.loginPost(
        url: Urls.uploadImage,
        header: loginModel,
        data: formDataUploadProfileImage);
  }

  Future<Response> getSports() async {
    // print(
    //     "ghgg ${updateUserTypeRequestModel.invitationType + updateUserTypeRequestModel.userType.toString()}");
    setAuthHeaderModel();
    return ObjectFactory().appDio.get(url: Urls.getSports, header: loginModel);
  }

  Future<Response> addEmergencyContact(
      AddEmergencyContactRequestModel addEmergencyContactRequestModel) {
    print(
        "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    print(Urls.addParent);
    return ObjectFactory().appDio.post(
        url: Urls.addEmergencyContact,
        header: authHeaderModel,
        data: [addEmergencyContactRequestModel]);
  }

  Future<Response> addAuthorizedContact(
      AddAuthorizedContactRequestModel addAuthorizedContactRequestModel) {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory().appDio.post(
        url: Urls.addAuthorizedContact,
        header: authHeaderModel,
        data: [addAuthorizedContactRequestModel]);
  }

  Future<Response> addAthlete(AddAthleteRequestModel addAthleteRequestModel) {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory().appDio.post(
        url: Urls.addAthlete,
        header: authHeaderModel,
        data: [addAthleteRequestModel]);
  }

  Future<Response> addFacility(AddFacilityRequest addFacilityRequest) {
    print("asdsd" +
        addFacilityRequest.businessHoursTo +
        addFacilityRequest.businessHoursFrom);
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(addFacilityRequest.businessHours +
    //     addFacilityRequest.address +
    //     addFacilityRequest.name +
    //     addFacilityRequest.email +
    //     addFacilityRequest.phone +
    //     addFacilityRequest.images.toString() +
    //     addFacilityRequest.sports.toString() +
    //     addFacilityRequest.facilityType.toString() +
    //     addFacilityRequest.lat.toString() +
    //     addFacilityRequest.lng.toString());
    // print(Urls.addParent);
    return ObjectFactory().appDio.post(
        url: Urls.addFacility,
        header: authHeaderModel,
        data: addFacilityRequest);
  }

  Future<Response> getFacility() {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory()
        .appDio
        .get(url: Urls.getFacility, header: authHeaderModel);
  }

  Future<Response> getFacilityType() {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory()
        .appDio
        .get(url: Urls.getFacilityType, header: authHeaderModel);
  }

  Future<Response> getProfile() {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory()
        .appDio
        .get(url: Urls.getProfile, header: authHeaderModel);
  }

  Future<Response> getAthleteList() {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory()
        .appDio
        .get(url: Urls.getAthleteList, header: authHeaderModel);
  }

  Future<Response> deleteAthlete(int id) {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory().appDio.delete(
        url: Urls.deleteAthlete + id.toString(), header: authHeaderModel);
  }

  Future<Response> updateSports(UpdateSportsRequest updateSportsRequest) {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory().appDio.post(
        url: Urls.updateSports,
        data: updateSportsRequest,
        header: authHeaderModel);
  }

  Future<Response> getAgeRange() {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory()
        .appDio
        .get(url: Urls.ageRange, header: authHeaderModel);
  }

  Future<Response> getSessionIncrement() {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory()
        .appDio
        .get(url: Urls.sessionIncrement, header: authHeaderModel);
  }

  Future<Response> uploadVideo(UploadVideoRequest uploadVideoRequest) async {
    FormData formDataUploadVideo = FormData.fromMap({
      if (uploadVideoRequest.video != null)
        "video": await MultipartFile.fromFile(uploadVideoRequest.video.path,
            filename: uploadVideoRequest.video.path.split('/').last),
    });
    // print(
    //     "ghgg ${updateUserTypeRequestModel.invitationType + updateUserTypeRequestModel.userType.toString()}");
    setAuthHeaderModel();
    return ObjectFactory().appDio.post(
        url: Urls.uploadVideo,
        header: authHeaderModel,
        data: formDataUploadVideo);
  }

  Future<Response> getLevelOfEducation() {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory()
        .appDio
        .get(url: Urls.levelOfEducationUrl, header: authHeaderModel);
  }

  Future<Response> getRateByView() {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory()
        .appDio
        .get(url: Urls.rateByViewUrl, header: authHeaderModel);
  }

  Future<Response> addCoach(AddCoachRequest addCoachRequest) {
    // print("ccsc"+addCoachRequest.+addCoachRequest.profPic.toString());
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory().appDio.post(
        url: Urls.addCoachUrl, header: authHeaderModel, data: addCoachRequest);
  }

  Future<Response> getLevelOfPlay() {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory()
        .appDio
        .get(url: Urls.getLevelOfPlayUrl, header: authHeaderModel);
  }

  Future<Response> addReference(AddReferenceRequest addReferenceRequest) {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory().appDio.post(
        url: Urls.addReferenceUrl,
        header: authHeaderModel,
        data: addReferenceRequest);
  }

  Future<Response> getStates() {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory()
        .appDio
        .get(url: Urls.getStateUrl, header: authHeaderModel);
  }

  Future<Response> addBankingInfo(AddBankingInfoRequest addBankingInfoRequest) {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory().appDio.post(
        url: Urls.addBankingInfoUrl,
        header: authHeaderModel,
        data: addBankingInfoRequest);
  }

  Future<Response> getPaymentMethods() {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory()
        .appDio
        .get(url: Urls.getPaymentMethodUrl, header: authHeaderModel);
  }

  Future<Response> getLeaseRates() {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory()
        .appDio
        .get(url: Urls.getLeaseRateUrl, header: authHeaderModel);
  }

  Future<Response> getBanks() {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory()
        .appDio
        .get(url: Urls.getBankUrl, header: authHeaderModel);
  }

  Future<Response> addFacilityRate(
      AddFacilityRateRequest addFacilityRateRequest) {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory().appDio.post(
        url: Urls.addFacilityRateUrl,
        header: authHeaderModel,
        data: addFacilityRateRequest);
  }

  Future<Response> addBusinessBankInfo(
      AddBusinessBankingInfo addBusinessBankingInfo) {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory().appDio.post(
        url: Urls.addBusinessBankingInfoUrl,
        header: authHeaderModel,
        data: addBusinessBankingInfo);
  }

  Future<Response> getTrainerList() {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory()
        .appDio
        .get(url: Urls.trainerListUrl, header: authHeaderModel);
  }

  Future<Response> getTimeSlots(GetTimeSlotsRequest getTimeSlotsRequest) {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory().appDio.post(
        url: Urls.getTimeSlotsUrl,
        header: authHeaderModel,
        data: getTimeSlotsRequest);
  }

  Future<Response> bookZoomMeeting(
      BookZoomMeetingRequest bookZoomMeetingRequest) {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory().appDio.post(
        url: Urls.bookZoomMeetingUrl,
        header: authHeaderModel,
        data: bookZoomMeetingRequest);
  }

  Future<Response> addOnBoardingStatus() {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory().appDio.post(
        url: Urls.addOnBoardingStatus,
        header: authHeaderModel);
  }

  Future<Response> getBillingAddress() {
    // print(
    //     "ghgg ${addEmergencyContactRequestModel.phoneNumber + addEmergencyContactRequestModel.parent.toString() + addEmergencyContactRequestModel.email}");
    setAuthHeaderModel();
    // print(Urls.addParent);
    return ObjectFactory().appDio.get(
        url: Urls.getBillingAddressUrl,
        header: authHeaderModel);
  }
}
