import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class PaymentDetailsBox extends StatefulWidget {
  final String details;
  final String imageName;
  final Function onPressed;
  final int index;
  final int count;

  PaymentDetailsBox(
      {this.count, this.index, this.details, this.imageName, this.onPressed});
  @override
  _PaymentDetailsBoxState createState() => _PaymentDetailsBoxState();
}

class _PaymentDetailsBoxState extends State<PaymentDetailsBox> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Container(
        height: screenHeight(context, dividedBy: 10),
        width: screenWidth(context, dividedBy: 1.2),
        decoration: BoxDecoration(
          color: Constants.kitGradients[5],
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
              color: widget.count == widget.index
                  ? Constants.kitGradients[1]
                  : Colors.transparent),
        ),
        child: Row(
          children: [
            Spacer(
              flex: 1,
            ),
            Container(
                height: screenHeight(context, dividedBy: 20),
                width: screenWidth(context, dividedBy: 12),
                child: Image.network(widget.imageName)),
            Spacer(
              flex: 1,
            ),
            Text(
              widget.details,
              style: TextStyle(
                  color: Colors.white, fontSize: 17, fontFamily: "AvenirBook"),
            ),
            Spacer(
              flex: 3,
            ),
            Container(
                width: screenWidth(context, dividedBy: 17),
                height: screenHeight(context, dividedBy: 20),
                child: widget.index == widget.count
                    ? Icon(
                        Icons.check_circle,
                        color: Constants.kitGradients[1],
                        size: 20,
                      )
                    : Container()),
            Spacer(
              flex: 1,
            )
          ],
        ),
      ),
    );
  }
}
