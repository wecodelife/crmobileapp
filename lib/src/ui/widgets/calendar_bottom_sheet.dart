import 'package:app_template/src/ui/widgets/calender_timeline.dart';
import 'package:flutter/cupertino.dart';

class CalenderBottomSheet extends StatefulWidget {
  final Function onPressed;
  CalenderBottomSheet({this.onPressed});
  @override
  _CalenderBottomSheetState createState() => _CalenderBottomSheetState();
}

class _CalenderBottomSheetState extends State<CalenderBottomSheet> {
  List<DateTime> timeList = [
    DateTime.now(),
    DateTime.now().add(Duration(hours: 1)),
    DateTime.now().add(Duration(hours: 2)),
    DateTime.now().add(Duration(hours: 3)),
    DateTime.now().add(Duration(hours: 4)),
    DateTime.now().add(Duration(hours: 5)),
    DateTime.now().add(Duration(hours: 6)),
    DateTime.now().add(Duration(hours: 7)),
    DateTime.now().add(Duration(hours: 8)),
    DateTime.now().add(Duration(hours: 9)),
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView.builder(
            itemCount: 9,
            itemBuilder: (BuildContext ctxt, int index) {
              return GestureDetector(
                onTap: () {
                  widget.onPressed();
                },
                child: CalendarTimeLine(
                  startTime: timeList[index],
                  endTime: timeList[index + 1],
                  eventEndTime: DateTime.now().add(Duration(hours: 3)),
                  eventStartTime: DateTime.now(),
                ),
              );
            }),
      ),
    );
  }
}
