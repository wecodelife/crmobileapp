// To parse this JSON data, do
//
//     final basicUserDetailsResponseModel = basicUserDetailsResponseModelFromJson(jsonString);

import 'dart:convert';

BasicUserDetailsResponseModel basicUserDetailsResponseModelFromJson(
        String str) =>
    BasicUserDetailsResponseModel.fromJson(json.decode(str));

String basicUserDetailsResponseModelToJson(
        BasicUserDetailsResponseModel data) =>
    json.encode(data.toJson());

class BasicUserDetailsResponseModel {
  BasicUserDetailsResponseModel({
    this.cognitoId,
    this.password,
    this.lastLogin,
    this.isSuperuser,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.username,
    this.firstName,
    this.lastName,
    this.email,
    this.isStaff,
    this.isActive,
    this.userType,
    this.dateJoined,
    this.isOnBoardingComplete,
    this.invitationType,
    this.groups,
    this.userPermissions,
  });

  final String cognitoId;
  final String password;
  final dynamic lastLogin;
  final bool isSuperuser;
  final bool isDeleted;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String username;
  final String firstName;
  final String lastName;
  final String email;
  final bool isStaff;
  final bool isActive;
  final int userType;
  final DateTime dateJoined;
  final bool isOnBoardingComplete;
  final int invitationType;
  final List<dynamic> groups;
  final List<dynamic> userPermissions;

  factory BasicUserDetailsResponseModel.fromJson(Map<String, dynamic> json) =>
      BasicUserDetailsResponseModel(
        cognitoId: json["cognito_id"] == null ? null : json["cognito_id"],
        password: json["password"] == null ? null : json["password"],
        lastLogin: json["last_login"],
        isSuperuser: json["is_superuser"] == null ? null : json["is_superuser"],
        isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        username: json["username"] == null ? null : json["username"],
        firstName: json["first_name"] == null ? null : json["first_name"],
        lastName: json["last_name"] == null ? null : json["last_name"],
        email: json["email"] == null ? null : json["email"],
        isStaff: json["is_staff"] == null ? null : json["is_staff"],
        isActive: json["is_active"] == null ? null : json["is_active"],
        userType: json["user_type"] == null ? null : json["user_type"],
        dateJoined: json["date_joined"] == null
            ? null
            : DateTime.parse(json["date_joined"]),
        isOnBoardingComplete: json["is_on_boarding_complete"] == null
            ? null
            : json["is_on_boarding_complete"],
        invitationType:
            json["invitation_type"] == null ? null : json["invitation_type"],
        groups: json["groups"] == null
            ? null
            : List<dynamic>.from(json["groups"].map((x) => x)),
        userPermissions: json["user_permissions"] == null
            ? null
            : List<dynamic>.from(json["user_permissions"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "cognito_id": cognitoId == null ? null : cognitoId,
        "password": password == null ? null : password,
        "last_login": lastLogin,
        "is_superuser": isSuperuser == null ? null : isSuperuser,
        "is_deleted": isDeleted == null ? null : isDeleted,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "username": username == null ? null : username,
        "first_name": firstName == null ? null : firstName,
        "last_name": lastName == null ? null : lastName,
        "email": email == null ? null : email,
        "is_staff": isStaff == null ? null : isStaff,
        "is_active": isActive == null ? null : isActive,
        "user_type": userType == null ? null : userType,
        "date_joined": dateJoined == null ? null : dateJoined.toIso8601String(),
        "is_on_boarding_complete":
            isOnBoardingComplete == null ? null : isOnBoardingComplete,
        "invitation_type": invitationType == null ? null : invitationType,
        "groups":
            groups == null ? null : List<dynamic>.from(groups.map((x) => x)),
        "user_permissions": userPermissions == null
            ? null
            : List<dynamic>.from(userPermissions.map((x) => x)),
      };
}
