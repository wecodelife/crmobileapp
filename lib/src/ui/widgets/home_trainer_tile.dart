import 'package:app_template/src/models/get_trainer_list_response.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeTrainerTile extends StatefulWidget {
  final bool hasIcon;
  final String name;
  final String imageName;
  final Function onPressed;
  final List<TrainerSport> sports;
  HomeTrainerTile(
      {this.hasIcon, this.onPressed, this.name, this.imageName, this.sports});
  @override
  _HomeTrainerTileState createState() => _HomeTrainerTileState();
}

class _HomeTrainerTileState extends State<HomeTrainerTile> {
  @override
  void initState() {
    print(widget.name);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          child: Container(
            width: screenWidth(context, dividedBy: 3),
            height: screenHeight(context, dividedBy: 3.8),
            decoration: BoxDecoration(
                color: Constants.kitGradients[5],
                borderRadius: BorderRadius.all(Radius.circular(5))),
            child: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      width: screenWidth(context, dividedBy: 3),
                      height: screenHeight(context, dividedBy: 5),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/images/dummy_bg.jpg"),
                              fit: BoxFit.fill)),
                      child: widget.imageName != null
                          ? CachedNetworkImage(
                              fit: BoxFit.fill,
                              imageUrl: Urls.baseUrl + widget.imageName,
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                width: screenWidth(context, dividedBy: 1),
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: imageProvider,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              placeholder: (context, url) => Center(
                                heightFactor: 1,
                                widthFactor: 1,
                                child: SizedBox(
                                  height: 16,
                                  width: 16,
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation(
                                        Constants.kitGradients[0]),
                                    strokeWidth: 2,
                                  ),
                                ),
                              ),
                            )
                          : Image.asset(
                              "assets/images/dummy_bg.jpg",
                              fit: BoxFit.cover,
                            ),
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 100),
                        ),
                        Row(
                          children: [
                            Spacer(flex: 1),
                            SvgPicture.asset("assets/icons/online_icon.svg",
                                height: screenWidth(context, dividedBy: 25),
                                width: screenWidth(context, dividedBy: 25)),
                            Spacer(flex: 10),
                            SvgPicture.asset("assets/icons/bookmark_icon.svg",
                                height: screenWidth(context, dividedBy: 25),
                                width: screenWidth(context, dividedBy: 25)),
                            Spacer(flex: 1),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                Container(
                  width: screenWidth(context, dividedBy: 3),
                  height: screenHeight(context, dividedBy: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Spacer(
                        flex: 1,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: screenWidth(context, dividedBy: 60),
                          ),
                          Text(
                            widget.name,
                            // "Coach Name",
                            style: TextStyle(
                                color: Constants.kitGradients[0],
                                fontSize: 14,
                                fontFamily: 'OpenSSansRegular'),
                          ),
                        ],
                      ),
                      Spacer(
                        flex: 1,
                      ),
                      // Row(
                      //   children: [
                      //     Row(
                      //       children: [
                      //         SizedBox(
                      //           width: screenWidth(context, dividedBy: 60),
                      //         ),
                      //         Container(
                      //             height: screenHeight(context, dividedBy: 40),
                      //             width: screenWidth(context, dividedBy: 20),
                      //             child: SvgPicture.asset(
                      //                 "assets/icons/soccer_icon.svg")),
                      //         SizedBox(
                      //           width: 4,
                      //         )
                      //       ],
                      //     ),
                      //     Text(
                      //       "Soccer",
                      //       style: TextStyle(
                      //           color: Constants.kitGradients[0],
                      //           fontSize: 12,
                      //           fontFamily: 'OpenSSansRegular'),
                      //     ),
                      //   ],
                      // ),
                      Container(
                        width: screenWidth(context, dividedBy: 1),
                        height: screenHeight(context, dividedBy: 30),
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: widget.sports.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Row(
                              children: [
                                SizedBox(
                                  width: 5,
                                ),
                                Container(
                                    height:
                                        screenHeight(context, dividedBy: 40),
                                    width: screenWidth(context, dividedBy: 20),
                                    child: SvgPicture.string(
                                        widget.sports[index].sport.icon
                                        //   widget.sports[index].
                                        )),
                                SizedBox(
                                  width: 4,
                                ),
                                Text(
                                  // "Football",
                                  widget.sports[index].sport.name,
                                  style: TextStyle(
                                      color: Constants.kitGradients[0],
                                      fontSize: 12,
                                      fontFamily: 'OpenSSansRegular'),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            );
                          },
                        ),
                      ),
                      Spacer(
                        flex: 1,
                      ),
                      Row(
                        children: [
                          Spacer(
                            flex: 10,
                          ),
                          Icon(
                            Icons.phone_outlined,
                            size: 20,
                            color: Constants.kitGradients[1],
                          ),
                          SizedBox(
                            width: screenWidth(context, dividedBy: 20),
                          ),
                          Icon(
                            Icons.mail_outline,
                            size: 20,
                            color: Constants.kitGradients[1],
                          ),
                          Spacer(
                            flex: 1,
                          ),
                        ],
                      ),
                      Spacer(
                        flex: 1,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
