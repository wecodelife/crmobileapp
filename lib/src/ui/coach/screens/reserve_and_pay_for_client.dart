import 'package:app_template/src/models/add_coach_request.dart';
import 'package:app_template/src/ui/coach/screens/background_information.dart';
import 'package:app_template/src/ui/screens/on_boarding_parent_athlete_22.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class ReserveAndPayForClient extends StatefulWidget {
  final List<TrainerSport> sports;
  ReserveAndPayForClient({this.sports});
  @override
  _ReserveAndPayForClientState createState() => _ReserveAndPayForClientState();
}

class _ReserveAndPayForClientState extends State<ReserveAndPayForClient> {
  bool yes;
  bool transparentYes = true;
  bool transparentNo = true;
  bool disabled = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[6],
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 20)),
        child: Column(
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            TextProgressIndicator(
              heading: "WILL YOUR  CLIENTS  RESERVE & PAY FOR THE FACILITIES?",
              descriptionText:
                  "If not you will have to choose the facilities for yourself.",
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            OnBoardingProgressIndicator(
              width: 2.2,
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 5),
            ),
            BuildButton(
              title: "YES THEY WILL CHOOSE AND PAY",
              transparent: transparentYes,
              onPressed: () {
                setState(
                  () {
                    yes = true;
                    transparentYes = false;
                    transparentNo = true;
                    disabled = false;
                  },
                );
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            BuildButton(
              title: "NO  I WILL CHOOSE AND PAY",
              transparent: transparentNo,
              onPressed: () {
                setState(
                  () {
                    yes = false;
                    transparentYes = true;
                    transparentNo = false;
                    disabled = false;
                  },
                );
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 3.4),
            ),
            BuildButton(
              title: "NEXT",
              disabled: disabled,
              onPressed: () {
                ObjectFactory().appHive.putClientsReservePay(value: yes);
                if (yes == true) {
                  push(
                      context,
                      BackgroundInformation(
                        sports: widget.sports,
                      ));
                } else {
                  push(
                      context,
                      OnBoardingParentAthlete22(
                        sessionName: "coach",
                        sports: widget.sports,
                      ));
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
