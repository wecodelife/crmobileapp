import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeFacilityMainTile extends StatefulWidget {
  final Function onPressed;
  final String name;
  final String address;
  final String fromTime;
  final String toTime;
  final String imgUrl;
  HomeFacilityMainTile(
      {this.onPressed,
      this.name,
      this.fromTime,
      this.toTime,
      this.imgUrl,
      this.address});
  @override
  _HomeFacilityMainTileState createState() => _HomeFacilityMainTileState();
}

class _HomeFacilityMainTileState extends State<HomeFacilityMainTile> {
  bool rated = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Container(
        width: screenWidth(context, dividedBy: 1),
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: 8.0,
          ),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 70),
                  ),
                  ClipRRect(
                    child: Container(
                        width: screenWidth(context, dividedBy: 4.5),
                        height: screenHeight(context, dividedBy: 10),
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("assets/images/dummy_bg.jpg"),
                                fit: BoxFit.fill)),
                        child: widget.imgUrl != null
                            ? CachedNetworkImage(
                                fit: BoxFit.fill,
                                imageUrl: Urls.baseUrl + widget.imgUrl,
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  width: screenWidth(context, dividedBy: 1),
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                                placeholder: (context, url) => Center(
                                  heightFactor: 1,
                                  widthFactor: 1,
                                  child: SizedBox(
                                    height: 16,
                                    width: 16,
                                    child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation(
                                          Constants.kitGradients[0]),
                                      strokeWidth: 2,
                                    ),
                                  ),
                                ),
                              )
                            : Image.asset(
                                "assets/images/dummy_bg.jpg",
                                fit: BoxFit.cover,
                              )
                        // Image.network(
                        //   "https://image.shutterstock.com/image-photo/business-woman-drawing-global"
                        //   "-structure-260nw-1006041130.jpg",
                        //   fit: BoxFit.cover,
                        // ),
                        ),
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 60),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 100)),
                    child: Container(
                      width: screenWidth(context, dividedBy: 1.6),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                  width: screenWidth(context, dividedBy: 3),
                                  child: Text(
                                    widget.name,
                                    // " YMCA",
                                    style: TextStyle(
                                        fontSize: 13,
                                        fontFamily: 'OpenSansRegular',
                                        color: Colors.white),
                                  )),
                              Spacer(
                                flex: 1,
                              ),
                              Container(
                                  width: screenWidth(context, dividedBy: 19),
                                  height: screenHeight(context, dividedBy: 40),
                                  child: SvgPicture.asset(
                                      "assets/icons/arrow_button.svg"))
                            ],
                          ),
                          SizedBox(
                            height: 2.0,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Icon(
                                Icons.location_on_outlined,
                                size: 16,
                                color: Colors.white,
                              ),
                              SizedBox(
                                width: 2,
                              ),
                              Container(
                                width: screenWidth(context, dividedBy: 2),
                                child: Text(widget.address,
                                    // "76A Eighth Avenue, Pittsburgh",
                                    style: TextStyle(
                                        fontSize: 13,
                                        fontFamily: 'OpenSansRegular',
                                        color: Colors.white),
                                    overflow: TextOverflow.ellipsis),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 120),
                          ),
                          Row(
                            children: [
                              rated == true
                                  ? GestureDetector(
                                      onTap: () {
                                        rated = !rated;
                                      },
                                      child: Icon(
                                        Icons.star_rate_rounded,
                                        size: 16,
                                        color: Constants.kitGradients[0],
                                      ))
                                  : Icon(
                                      Icons.star_rate_rounded,
                                      color: Constants.kitGradients[1],
                                      size: 18,
                                    ),
                              Container(
                                width: screenWidth(context, dividedBy: 18),
                                child: Text("4.7",
                                    style: TextStyle(
                                        fontSize: 13,
                                        fontFamily: 'OpenSansRegular',
                                        color: Colors.white),
                                    overflow: TextOverflow.ellipsis),
                              ),
                              SizedBox(
                                width: screenWidth(context, dividedBy: 100),
                              ),
                              Container(
                                width: 2,
                                height: 17,
                                color: Colors.white,
                              ),
                              SizedBox(
                                width: screenWidth(context, dividedBy: 80),
                              ),
                              rated == true
                                  ? GestureDetector(
                                      onTap: () {
                                        rated = !rated;
                                      },
                                      child: Image.asset(
                                        "assets/icons/people_icon.png",
                                        color: Constants.kitGradients[0],
                                      ))
                                  : Image.asset(
                                      "assets/icons/people_icon.png",
                                      color: Constants.kitGradients[0],
                                    ),
                              SizedBox(
                                width: screenWidth(context, dividedBy: 50),
                              ),
                              Container(
                                width: screenWidth(context, dividedBy: 15),
                                child: Text("4.7",
                                    style: TextStyle(
                                        fontSize: 13,
                                        fontFamily: 'OpenSansRegular',
                                        color: Colors.white),
                                    overflow: TextOverflow.ellipsis),
                              ),
                              SizedBox(
                                width: screenWidth(context, dividedBy: 120),
                              ),
                              Container(
                                width: screenWidth(context, dividedBy: 9),
                                child: Text("30" + "Min",
                                    style: TextStyle(
                                        fontSize: 13,
                                        fontFamily: 'OpenSansRegular',
                                        color: Colors.white),
                                    overflow: TextOverflow.ellipsis),
                              ),
                              Icon(
                                Icons.circle,
                                size: 8,
                                color: Colors.red,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Container(
                                width: screenWidth(context, dividedBy: 9),
                                child: Text("0.4 " + "Km",
                                    style: TextStyle(
                                        fontSize: 13,
                                        fontFamily: 'OpenSansRegular',
                                        color: Colors.white),
                                    overflow: TextOverflow.ellipsis),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 120),
                          ),
                          // Row(
                          //   children: [
                          //     SizedBox(
                          //       width: 5,
                          //     ),
                          //     Container(
                          //       width: screenWidth(context, dividedBy: 3.8),
                          //       child: Text("M-F:" "9AM -6AM",
                          //           style: TextStyle(
                          //               fontSize: 13,
                          //               fontFamily: 'OpenSansRegular',
                          //               color: Colors.white),
                          //           overflow: TextOverflow.ellipsis),
                          //     ),
                          //     Container(
                          //       width: screenWidth(context, dividedBy: 4),
                          //       child: Text("S-Su:" "9AM -6AM",
                          //           style: TextStyle(
                          //               fontSize: 13,
                          //               fontFamily: 'OpenSansRegular',
                          //               color: Colors.white),
                          //           overflow: TextOverflow.ellipsis),
                          //     ),
                          //   ],
                          // ),
                          Container(
                            width: screenWidth(context, dividedBy: 1.6),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(
                                  width: 5,
                                ),
                                Container(
                                  // width: screenWidth(context, dividedBy: 4),
                                  child: Text(widget.fromTime,
                                      // "M-F:" "9AM -6AM",
                                      style: TextStyle(
                                          fontSize: 11,
                                          fontFamily: 'OpenSansRegular',
                                          color: Colors.white),
                                      overflow: TextOverflow.ellipsis),
                                ),
                                Spacer(
                                  flex: 1,
                                ),
                                Container(
                                  // width: screenWidth(context, dividedBy: 4),
                                  child: Text(widget.toTime,
                                      // " S-Su:" "9AM -6AM",
                                      style: TextStyle(
                                          fontSize: 11,
                                          fontFamily: 'OpenSansRegular',
                                          color: Colors.white),
                                      overflow: TextOverflow.ellipsis),
                                ),
                                Spacer(
                                  flex: 1,
                                ),
                                Icon(
                                  Icons.phone_outlined,
                                  size: 20,
                                  color: Constants.kitGradients[1],
                                ),
                                Spacer(
                                  flex: 1,
                                ),
                                Icon(
                                  Icons.mail_outline,
                                  size: 20,
                                  color: Constants.kitGradients[1],
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 100),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
