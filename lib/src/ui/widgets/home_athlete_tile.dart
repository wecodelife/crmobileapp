import 'package:app_template/src/models/get_athlete_list_response.dart';
import 'package:app_template/src/models/is_athlete_selected.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeAthleteTile extends StatefulWidget {
  final int index;
  final bool onClick;
  final int selectedIndex;
  final String imgUrl;
  final String name;
  final Function onPressed;
  final List<Sport> sports;
  final ValueChanged<IsAthleteSelected> sportsList;
  HomeAthleteTile(
      {this.index,
      this.imgUrl,
      this.sports,
      this.name,
      this.onClick,
      this.sportsList,
      this.selectedIndex,
      this.onPressed});
  @override
  _HomeAthleteTileState createState() => _HomeAthleteTileState();
}

class _HomeAthleteTileState extends State<HomeAthleteTile> {
  // bool onClick = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
          onTap: () {
            widget.onPressed();
            setState(() {
              // onClick = !onClick;
              // // if (widget.selectedIndex == widget.index) {}
              // widget.sportsList(IsAthleteSelected(
              //     isSelected: onClick,
              //     // widget.selectedIndex == widget.index ? onClick : false,
              //     sports: widget.sports));
            });
          },
          child: Row(
            children: [
              widget.index == 0
                  ? GestureDetector(
                      onTap: () {
                        // push(
                        //     context,
                        //     OnBoardingParentAthlete11(
                        //       athleteCount: 1,
                        //     ));
                      },
                      child: Column(
                        children: [
                          Container(
                            height: screenHeight(context, dividedBy: 6),
                            width: screenWidth(context, dividedBy: 1.8),
                            child: Center(
                              child: SvgPicture.asset(
                                "assets/images/add_icon.svg",
                                fit: BoxFit.fitHeight,
                              ),
                            ),
                          ),
                          Text(
                            "Add Athlete",
                            style: TextStyle(
                                color: Constants.kitGradients[0],
                                fontSize: 14,
                                fontFamily: 'OpenSSansRegular'),
                          ),
                        ],
                      ),
                    )
                  : Container(),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    child: Container(
                        height: screenHeight(context, dividedBy: 6),
                        width: screenWidth(context, dividedBy: 1.8),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: widget.selectedIndex == widget.index
                                    ? widget.onClick == true
                                        ? Constants.kitGradients[1]
                                        : Colors.transparent
                                    : Colors.transparent,
                                width: 3),
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            image: DecorationImage(
                                image: AssetImage("assets/images/dummy_bg.jpg"),
                                fit: BoxFit.fill)),
                        child: widget.imgUrl != null
                            ? CachedNetworkImage(
                                fit: BoxFit.fill,
                                imageUrl: widget.imgUrl,
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  width: screenWidth(context, dividedBy: 1),
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                                placeholder: (context, url) => Center(
                                  heightFactor: 1,
                                  widthFactor: 1,
                                  child: SizedBox(
                                    height: 16,
                                    width: 16,
                                    child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation(
                                          Constants.kitGradients[0]),
                                      strokeWidth: 2,
                                    ),
                                  ),
                                ),
                              )
                            : Image.asset(
                                "assets/images/dummy_bg.jpg",
                                fit: BoxFit.cover,
                              )
                        // Image.network(
                        //   "https://image.shutterstock.com/image-photo/business-woman-drawing-global"
                        //   "-structure-260nw-1006041130.jpg",
                        //   fit: BoxFit.cover,
                        // ),
                        ),
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 2),
                    height: screenHeight(context, dividedBy: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Spacer(
                          flex: 1,
                        ),
                        Text(
                          // "Dan Smith",
                          widget.name,
                          style: TextStyle(
                              color: Constants.kitGradients[0],
                              fontSize: 14,
                              fontFamily: 'OpenSSansRegular'),
                        ),
                        Spacer(
                          flex: 1,
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 1),
                          height: screenHeight(context, dividedBy: 30),
                          child: ListView.builder(
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            itemCount: widget.sports.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Row(
                                children: [
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Container(
                                      height:
                                          screenHeight(context, dividedBy: 40),
                                      width:
                                          screenWidth(context, dividedBy: 20),
                                      child: SvgPicture.string(
                                          // "assets/icons/basketball.svg"
                                          widget.sports[index].icon)),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Text(
                                    // "Football",
                                    widget.sports[index].name,
                                    style: TextStyle(
                                        color: Constants.kitGradients[0],
                                        fontSize: 12,
                                        fontFamily: 'OpenSSansRegular'),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ],
                              );
                            },
                          ),
                        ),
                        Spacer(
                          flex: 1,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
