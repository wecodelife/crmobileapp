import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/add_coach_request.dart';
import 'package:app_template/src/models/get_facility_response.dart';
import 'package:app_template/src/ui/coach/screens/background_information.dart';
import 'package:app_template/src/ui/screens/on_boarding_parent_Athlete_23.dart';
import 'package:app_template/src/ui/screens/onboardingAthlete25.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/search_bar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OnBoardingParentAthlete22 extends StatefulWidget {
  final String sessionName;
  final List<TrainerSport> sports;
  OnBoardingParentAthlete22({this.sessionName, this.sports});
  @override
  _OnBoardingParentAthlete22State createState() =>
      _OnBoardingParentAthlete22State();
}

class _OnBoardingParentAthlete22State extends State<OnBoardingParentAthlete22> {
  bool rated = false;
  TextEditingController searchTextEditingController =
      new TextEditingController();
  UserBloc userBloc = UserBloc();
  @override
  void initState() {
    userBloc.getFacility();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 20)),
        child: Column(
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            TextProgressIndicator(
              heading: "LETS SElECT SOME PREFERRED TRAINING FACILITIES",
              descriptionText:
                  "Find some facilities, search some facilities, invite facilities, or add your own.",
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            OnBoardingProgressIndicator(
              width: 1.4,
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            SearchBar(
              textEditingController: searchTextEditingController,
              hintText: "Search for Facilities",
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            Expanded(
              child: StreamBuilder<GetFacilityResponse>(
                  stream: userBloc.getFacilityResponse,
                  builder: (context, snapshot) {
                    return snapshot.hasData
                        ? ListView.builder(
                            itemCount: snapshot.data.data.length,
                            // itemCount: 2,
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            itemBuilder: (BuildContext context, int index) {
                              return Card(
                                color: Constants.kitGradients[5],
                                child: Container(
                                  width: screenWidth(context, dividedBy: 1),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                      vertical: 8.0,
                                    ),
                                    child: Column(
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              width: screenWidth(context,
                                                  dividedBy: 70),
                                            ),
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              child: Container(
                                                width: screenWidth(context,
                                                    dividedBy: 4.5),
                                                height: screenHeight(context,
                                                    dividedBy: 8),
                                                color: Colors.red,
                                                child: snapshot.data.data[index]
                                                            .images.length >
                                                        0
                                                    ? CachedNetworkImage(
                                                        fit: BoxFit.fill,
                                                        imageUrl: Urls.baseUrl +
                                                            snapshot
                                                                .data
                                                                .data[index]
                                                                .images[0]
                                                                .imageUrl,
                                                        imageBuilder: (context,
                                                                imageProvider) =>
                                                            Container(
                                                          width: screenWidth(
                                                              context,
                                                              dividedBy: 1),
                                                          decoration:
                                                              BoxDecoration(
                                                            image:
                                                                DecorationImage(
                                                              image:
                                                                  imageProvider,
                                                              fit: BoxFit.fill,
                                                            ),
                                                          ),
                                                        ),
                                                        placeholder:
                                                            (context, url) =>
                                                                Center(
                                                          heightFactor: 1,
                                                          widthFactor: 1,
                                                          child: SizedBox(
                                                            height: 16,
                                                            width: 16,
                                                            child:
                                                                CircularProgressIndicator(
                                                              valueColor:
                                                                  AlwaysStoppedAnimation(
                                                                      Constants
                                                                          .kitGradients[0]),
                                                              strokeWidth: 2,
                                                            ),
                                                          ),
                                                        ),
                                                      )
                                                    : Image.asset(
                                                        "assets/images/dummy_bg.jpg",
                                                        fit: BoxFit.cover,
                                                      ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: screenWidth(context,
                                                  dividedBy: 60),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: screenWidth(
                                                      context,
                                                      dividedBy: 100)),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: [
                                                      Container(
                                                          width: screenWidth(
                                                              context,
                                                              dividedBy: 3),
                                                          child: Text(
                                                            snapshot
                                                                .data
                                                                .data[index]
                                                                .name,
                                                            // "YMCA",
                                                            style: TextStyle(
                                                                fontSize: 13,
                                                                fontFamily:
                                                                    'OpenSansRegular',
                                                                color: Colors
                                                                    .white),
                                                          )),
                                                      SizedBox(
                                                        width: screenWidth(
                                                            context,
                                                            dividedBy: 5),
                                                      ),
                                                      Container(
                                                          width: screenWidth(
                                                              context,
                                                              dividedBy: 19),
                                                          height: screenHeight(
                                                              context,
                                                              dividedBy: 40),
                                                          child: SvgPicture.asset(
                                                              "assets/icons/arrow_button.svg"))
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 2.0,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: [
                                                      Icon(
                                                        Icons
                                                            .location_on_outlined,
                                                        size: 16,
                                                        color: Colors.white,
                                                      ),
                                                      SizedBox(
                                                        width: 2,
                                                      ),
                                                      Container(
                                                        width: screenWidth(
                                                            context,
                                                            dividedBy: 2),
                                                        child: Text(
                                                            // "76A Eighth Avenue, Pittsburgh",
                                                            snapshot
                                                                .data
                                                                .data[index]
                                                                .address,
                                                            style: TextStyle(
                                                                fontSize: 13,
                                                                fontFamily:
                                                                    'OpenSansRegular',
                                                                color: Colors
                                                                    .white),
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: screenHeight(
                                                        context,
                                                        dividedBy: 120),
                                                  ),
                                                  Row(
                                                    children: [
                                                      rated == true
                                                          ? GestureDetector(
                                                              onTap: () {
                                                                rated = !rated;
                                                              },
                                                              child: Icon(
                                                                Icons
                                                                    .star_rate_rounded,
                                                                size: 16,
                                                                color: Constants
                                                                    .kitGradients[0],
                                                              ))
                                                          : Icon(
                                                              Icons
                                                                  .star_rate_rounded,
                                                              color:
                                                                  Colors.white,
                                                              size: 18,
                                                            ),
                                                      Container(
                                                        width: screenWidth(
                                                            context,
                                                            dividedBy: 18),
                                                        child: Text("4.7",
                                                            style: TextStyle(
                                                                fontSize: 13,
                                                                fontFamily:
                                                                    'OpenSansRegular',
                                                                color: Colors
                                                                    .white),
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis),
                                                      ),
                                                      SizedBox(
                                                        width: screenWidth(
                                                            context,
                                                            dividedBy: 100),
                                                      ),
                                                      Container(
                                                        width: 2,
                                                        height: 17,
                                                        color: Colors.white,
                                                      ),
                                                      SizedBox(
                                                        width: screenWidth(
                                                            context,
                                                            dividedBy: 80),
                                                      ),
                                                      rated == true
                                                          ? GestureDetector(
                                                              onTap: () {
                                                                rated = !rated;
                                                              },
                                                              child:
                                                                  Image.asset(
                                                                "assets/icons/people_icon.png",
                                                                color: Constants
                                                                    .kitGradients[0],
                                                              ))
                                                          : Image.asset(
                                                              "assets/icons/people_icon.png",
                                                              color: Constants
                                                                  .kitGradients[0],
                                                            ),
                                                      SizedBox(
                                                        width: screenWidth(
                                                            context,
                                                            dividedBy: 50),
                                                      ),
                                                      Container(
                                                        width: screenWidth(
                                                            context,
                                                            dividedBy: 15),
                                                        child: Text("4.7",
                                                            style: TextStyle(
                                                                fontSize: 13,
                                                                fontFamily:
                                                                    'OpenSansRegular',
                                                                color: Colors
                                                                    .white),
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis),
                                                      ),
                                                      SizedBox(
                                                        width: screenWidth(
                                                            context,
                                                            dividedBy: 120),
                                                      ),
                                                      Container(
                                                        width: screenWidth(
                                                            context,
                                                            dividedBy: 9),
                                                        child: Text(
                                                            "30" + "Min",
                                                            style: TextStyle(
                                                                fontSize: 13,
                                                                fontFamily:
                                                                    'OpenSansRegular',
                                                                color: Colors
                                                                    .white),
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis),
                                                      ),
                                                      Icon(
                                                        Icons.circle,
                                                        size: 8,
                                                        color: Colors.red,
                                                      ),
                                                      SizedBox(
                                                        width: 5,
                                                      ),
                                                      Container(
                                                        width: screenWidth(
                                                            context,
                                                            dividedBy: 9),
                                                        child: Text(
                                                            "0.4 " + "Km",
                                                            style: TextStyle(
                                                                fontSize: 13,
                                                                fontFamily:
                                                                    'OpenSansRegular',
                                                                color: Colors
                                                                    .white),
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: screenHeight(
                                                        context,
                                                        dividedBy: 120),
                                                  ),
                                                  Row(
                                                    children: [
                                                      SizedBox(
                                                        width: 5,
                                                      ),
                                                      Container(
                                                        width: screenWidth(
                                                            context,
                                                            dividedBy: 3.5),
                                                        child: Text(
                                                            // "M-F:" "9AM -6AM",
                                                            snapshot
                                                                .data
                                                                .data[index]
                                                                .businessHoursFrom,
                                                            style: TextStyle(
                                                                fontSize: 13,
                                                                fontFamily:
                                                                    'OpenSansRegular',
                                                                color: Colors
                                                                    .white),
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis),
                                                      ),
                                                      Container(
                                                        width: screenWidth(
                                                            context,
                                                            dividedBy: 3.5),
                                                        child: Text(
                                                            // "S-Su:" "9AM -6AM",
                                                            snapshot
                                                                .data
                                                                .data[index]
                                                                .businessHoursTo,
                                                            style: TextStyle(
                                                                fontSize: 13,
                                                                fontFamily:
                                                                    'OpenSansRegular',
                                                                color: Colors
                                                                    .white),
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: screenHeight(context,
                                              dividedBy: 100),
                                        ),
                                        Row(
                                          children: [
                                            SizedBox(
                                              width: screenWidth(context,
                                                  dividedBy: 70),
                                            ),
                                            Container(
                                              width: screenWidth(context,
                                                  dividedBy: 3.8),
                                              height: screenHeight(context,
                                                  dividedBy: 30),
                                              child: ListView.builder(
                                                  itemCount: snapshot
                                                      .data
                                                      .data[index]
                                                      .sports
                                                      .length,
                                                  shrinkWrap: true,
                                                  scrollDirection:
                                                      Axis.horizontal,
                                                  itemBuilder:
                                                      (BuildContext context,
                                                          int indx) {
                                                    return Container(
                                                        height: screenHeight(
                                                            context,
                                                            dividedBy: 70),
                                                        width: screenWidth(
                                                            context,
                                                            dividedBy: 15),
                                                        child: SvgPicture.asset(
                                                          "assets/icons/basket_ball_icon.svg",
                                                          fit: BoxFit.contain,
                                                        ));
                                                  }),
                                            ),
                                            SizedBox(
                                              width: screenWidth(context,
                                                  dividedBy: 2.3),
                                            ),
                                            Icon(
                                              Icons.phone_outlined,
                                              size: 20,
                                              color: Constants.kitGradients[1],
                                            ),
                                            SizedBox(
                                              width: screenWidth(context,
                                                  dividedBy: 25),
                                            ),
                                            Icon(
                                              Icons.mail_outline,
                                              size: 20,
                                              color: Constants.kitGradients[1],
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            })
                        : Container(
                            width: screenWidth(context, dividedBy: 1),
                            height: screenHeight(context, dividedBy: 1),
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          );
                  }),
            ),
            BuildButton(
              title: "ADD YOUR OWN FACILITY",
              onPressed: () {
                push(
                    context,
                    OnBoardingParentAthlete23(
                      sports: widget.sports,
                      SessionName: widget.sessionName,
                    ));
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            BuildButton(
              title: "NEXT",
              disabled: false,
              onPressed: () {
                if (widget.sessionName == "coach") {
                  push(
                      context,
                      BackgroundInformation(
                        sports: widget.sports,
                      ));
                } else {
                  push(context, OnBoardingAthlete25());
                }
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 70),
            )
          ],
        ),
      ),
    );
  }
}
