import 'dart:async';

import 'package:app_template/src/models/add_athlete_request_model.dart';
import 'package:app_template/src/models/add_athlete_response.dart';
import 'package:app_template/src/models/add_authorized_contact_request_model.dart';
import 'package:app_template/src/models/add_authorized_contact_response_model.dart';
import 'package:app_template/src/models/add_banking_info_request.dart';
import 'package:app_template/src/models/add_banking_info_response.dart';
import 'package:app_template/src/models/add_business_banking_info_request.dart';
import 'package:app_template/src/models/add_business_banking_info_response.dart';
import 'package:app_template/src/models/add_coach_request.dart';
import 'package:app_template/src/models/add_coach_response.dart';
import 'package:app_template/src/models/add_emergency_contact_request_model.dart';
import 'package:app_template/src/models/add_emergency_contact_response_model.dart';
import 'package:app_template/src/models/add_facility_rate_request.dart';
import 'package:app_template/src/models/add_facility_rate_response.dart';
import 'package:app_template/src/models/add_facility_request.dart';
import 'package:app_template/src/models/add_facility_response.dart';
import 'package:app_template/src/models/add_onboarding_status_response.dart';
import 'package:app_template/src/models/add_parent_request_model.dart';
import 'package:app_template/src/models/add_parent_response_model.dart';
import 'package:app_template/src/models/add_reference_request.dart';
import 'package:app_template/src/models/add_reference_response.dart';
import 'package:app_template/src/models/basic_user_details_response.dart';
import 'package:app_template/src/models/book_zoom_meeting_request.dart';
import 'package:app_template/src/models/book_zoom_meeting_response.dart';
import 'package:app_template/src/models/delete_athlete_response.dart';
import 'package:app_template/src/models/get_age_range_response.dart';
import 'package:app_template/src/models/get_athlete_list_response.dart';
import 'package:app_template/src/models/get_bank_response.dart';
import 'package:app_template/src/models/get_billing_address_response.dart';
import 'package:app_template/src/models/get_facility_response.dart';
import 'package:app_template/src/models/get_facility_type_response.dart';
import 'package:app_template/src/models/get_lease_response.dart';
import 'package:app_template/src/models/get_level_of_education_response.dart';
import 'package:app_template/src/models/get_level_of_play_response.dart';
import 'package:app_template/src/models/get_payment_method_response.dart';
import 'package:app_template/src/models/get_profile_response.dart';
import 'package:app_template/src/models/get_rate_by_view.dart';
import 'package:app_template/src/models/get_session_increment_response.dart';
import 'package:app_template/src/models/get_sports_response_model.dart';
import 'package:app_template/src/models/get_states_response.dart';
import 'package:app_template/src/models/get_timeslots_request.dart';
import 'package:app_template/src/models/get_timeslots_response.dart';
import 'package:app_template/src/models/get_trainer_list_response.dart';
import 'package:app_template/src/models/invitation_type_response_model.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/login_response_model.dart';
import 'package:app_template/src/models/state.dart';
import 'package:app_template/src/models/update_sports_request.dart';
import 'package:app_template/src/models/update_sports_response.dart';
import 'package:app_template/src/models/update_user_type_request_model.dart';
import 'package:app_template/src/models/update_user_type_response_model.dart';
import 'package:app_template/src/models/upload_image_request_model.dart';
import 'package:app_template/src/models/upload_image_response_model.dart';
import 'package:app_template/src/models/upload_video_request.dart';
import 'package:app_template/src/models/upload_video_response.dart';
import 'package:app_template/src/utils/object_factory.dart';

class UserApiProvider {
  Future<State> loginCall(LoginRequest loginRequest) async {
    final response = await ObjectFactory().apiClient.loginRequest(loginRequest);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<LoginResponse>.success(
          LoginResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getBasicUserDetails() async {
    final response = await ObjectFactory().apiClient.getBasicUserDetails();
    print(response.toString());
    if (response.statusCode == 200) {
      return State<BasicUserDetailsResponseModel>.success(
          BasicUserDetailsResponseModel.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getUserInvitationType() async {
    final response = await ObjectFactory().apiClient.getUserInvitationTypes();
    print(response.toString());
    if (response.statusCode == 200) {
      return State<InvitationTypeResponseModel>.success(
          InvitationTypeResponseModel.fromJson(response.data));
    } else
      return null;
  }

  Future<State> updateUserType(
      UpdateUserTypeRequestModel updateUserTypeRequestModel) async {
    final response = await ObjectFactory()
        .apiClient
        .updateUserType(updateUserTypeRequestModel);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<UpdateUserTypeResponseModel>.success(
          UpdateUserTypeResponseModel.fromJson(response.data));
    } else
      return null;
  }

  Future<State> addParent(AddParentRequestModel addParentRequestModel) async {
    final response =
        await ObjectFactory().apiClient.addParent(addParentRequestModel);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<AddParentResponseModel>.success(
          AddParentResponseModel.fromJson(response.data));
    } else
      return null;
  }

  Future<State> uploadImage(UploadImageRequest uploadImageRequest) async {
    final response =
        await ObjectFactory().apiClient.uploadImage(uploadImageRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<UploadImageResponse>.success(
          UploadImageResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getSports() async {
    final response = await ObjectFactory().apiClient.getSports();
    print(response.toString());
    if (response.statusCode == 200) {
      return State<GetSportsResponseModel>.success(
          GetSportsResponseModel.fromJson(response.data));
    } else
      return null;
  }

  Future<State> addEmergencyContact(
      AddEmergencyContactRequestModel addEmergencyContactRequestModel) async {
    final response = await ObjectFactory()
        .apiClient
        .addEmergencyContact(addEmergencyContactRequestModel);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<AddEmergencyContactResponseModel>.success(
          AddEmergencyContactResponseModel.fromJson(response.data));
    } else
      return null;
  }

  Future<State> addAuthorizedContact(
      AddAuthorizedContactRequestModel addAuthorizedContactRequestModel) async {
    final response = await ObjectFactory()
        .apiClient
        .addAuthorizedContact(addAuthorizedContactRequestModel);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<AddAuthorizedContactResponseModel>.success(
          AddAuthorizedContactResponseModel.fromJson(response.data));
    } else
      return null;
  }

  Future<State> addAthlete(
      AddAthleteRequestModel addAthleteRequestModel) async {
    final response =
        await ObjectFactory().apiClient.addAthlete(addAthleteRequestModel);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<AddAthleteResponseModel>.success(
          AddAthleteResponseModel.fromJson(response.data));
    } else
      return null;
  }

  Future<State> addFacility(AddFacilityRequest addFacilityRequest) async {
    final response =
        await ObjectFactory().apiClient.addFacility(addFacilityRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<AddFacilityResponse>.success(
          AddFacilityResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getFacility() async {
    final response = await ObjectFactory().apiClient.getFacility();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetFacilityResponse>.success(
          GetFacilityResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getFacilityType() async {
    final response = await ObjectFactory().apiClient.getFacilityType();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetFacilityTypeResponse>.success(
          GetFacilityTypeResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getProfile() async {
    final response = await ObjectFactory().apiClient.getProfile();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetProfileResponse>.success(
          GetProfileResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getAthleteList() async {
    final response = await ObjectFactory().apiClient.getAthleteList();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetAthleteListResponse>.success(
          GetAthleteListResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> deleteAthlete(int id) async {
    final response = await ObjectFactory().apiClient.deleteAthlete(id);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<DeleteAthleteResponse>.success(
          DeleteAthleteResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> updateSports(UpdateSportsRequest updateSportsRequest) async {
    final response =
        await ObjectFactory().apiClient.updateSports(updateSportsRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<UpdateSportsResponse>.success(
          UpdateSportsResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getAgeRange() async {
    final response = await ObjectFactory().apiClient.getAgeRange();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetAgeRangeResponse>.success(
          GetAgeRangeResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getSessionIncrement() async {
    final response = await ObjectFactory().apiClient.getSessionIncrement();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetSessionIncrementResponse>.success(
          GetSessionIncrementResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> uploadVideo(UploadVideoRequest uploadVideoRequest) async {
    final response =
        await ObjectFactory().apiClient.uploadVideo(uploadVideoRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<UploadVideoResponse>.success(
          UploadVideoResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getLevelOfEducation() async {
    final response = await ObjectFactory().apiClient.getLevelOfEducation();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetLevelOfEducationResponse>.success(
          GetLevelOfEducationResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getRateByView() async {
    final response = await ObjectFactory().apiClient.getRateByView();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetRateByViewResponse>.success(
          GetRateByViewResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> addCoach(AddCoachRequest addCoachRequest) async {
    final response = await ObjectFactory().apiClient.addCoach(addCoachRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<AddCoachResponse>.success(
          AddCoachResponse.fromJson(response.data));
    } else
      return State<AddCoachResponse>.error(
          AddCoachResponse.fromJson(response.data));
  }

  Future<State> getLevelOfPlay() async {
    final response = await ObjectFactory().apiClient.getLevelOfPlay();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetLevelOfPlayResponse>.success(
          GetLevelOfPlayResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> addReference(AddReferenceRequest addReferenceRequest) async {
    final response =
        await ObjectFactory().apiClient.addReference(addReferenceRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<AddReferenceResponse>.success(
          AddReferenceResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getStates() async {
    final response = await ObjectFactory().apiClient.getStates();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetStatesResponse>.success(
          GetStatesResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> addBankingInfo(
      AddBankingInfoRequest addBankingInfoRequest) async {
    final response =
        await ObjectFactory().apiClient.addBankingInfo(addBankingInfoRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<AddBankingInfoResponse>.success(
          AddBankingInfoResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getPaymentMethods() async {
    final response = await ObjectFactory().apiClient.getPaymentMethods();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetPaymentMethodResponse>.success(
          GetPaymentMethodResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getLeaseRates() async {
    final response = await ObjectFactory().apiClient.getLeaseRates();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetLeaseRateResponse>.success(
          GetLeaseRateResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getBanks() async {
    final response = await ObjectFactory().apiClient.getBanks();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetBankResponse>.success(
          GetBankResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> addFacilityRate(
      AddFacilityRateRequest addFacilityRateRequest) async {
    final response =
        await ObjectFactory().apiClient.addFacilityRate(addFacilityRateRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<AddFacilityRateResponse>.success(
          AddFacilityRateResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> addBusinessBankInfo(
      AddBusinessBankingInfo addBusinessBankingInfo) async {
    final response = await ObjectFactory()
        .apiClient
        .addBusinessBankInfo(addBusinessBankingInfo);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<AddBusinessBankingInfoResponse>.success(
          AddBusinessBankingInfoResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getTrainerList() async {
    final response = await ObjectFactory().apiClient.getTrainerList();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetTrainerListResponse>.success(
          GetTrainerListResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getTimeSlots(GetTimeSlotsRequest getTimeSlotsRequest) async {
    final response =
        await ObjectFactory().apiClient.getTimeSlots(getTimeSlotsRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetTimeSlotsResponse>.success(
          GetTimeSlotsResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> bookZoomMeeting(
      BookZoomMeetingRequest bookZoomMeetingRequest) async {
    final response =
        await ObjectFactory().apiClient.bookZoomMeeting(bookZoomMeetingRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<BookZoomMeetingResponse>.success(
          BookZoomMeetingResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> addOnBoardingStatus() async {
    final response = await ObjectFactory().apiClient.addOnBoardingStatus();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<AddOnBoardStatusResponse>.success(
          AddOnBoardStatusResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getBillingAddress() async {
    final response = await ObjectFactory().apiClient.getBillingAddress();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<GetBillingAddressResponse>.success(
          GetBillingAddressResponse.fromJson(response.data));
    } else
      return null;
  }
}
