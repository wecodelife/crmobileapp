import 'dart:io';

import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_athlete_list_response.dart';
import 'package:app_template/src/ui/screens/edit_sport_page.dart';
import 'package:app_template/src/ui/screens/profile_page.dart';
import 'package:app_template/src/ui/widgets/appbar_cr.dart';
import 'package:app_template/src/ui/widgets/bottom_navigation_bar_widget.dart';
import 'package:app_template/src/ui/widgets/information_widget.dart';
import 'package:app_template/src/ui/widgets/list_tile_add_sports.dart';
import 'package:app_template/src/ui/widgets/select_button.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class UserDetails extends StatefulWidget {
  final Datum athleteData;
  UserDetails({this.athleteData});
  @override
  _UserDetailsState createState() => _UserDetailsState();
}

class _UserDetailsState extends State<UserDetails> {
  final picker = ImagePicker();
  UserBloc userBloc = UserBloc();
  File imagePicked;
  File galleryPhoto;
  bool selectPhoto = false;
  List<File> playerPhotosList = [];
  List<int> sportList = [];
  List<String> sports = [
    "Basketball",
    "Volleyball",
    "Football",
    "Swimming",
    "Baseball",
    "Hockey",
    "Soccer",
    "Tennis"
  ];
  List<String> sportsIcon = [
    "assets/icons/basketball.svg",
    "assets/icons/volleyball.svg",
    "assets/icons/basket_ball_icon.svg",
    "assets/icons/swimming.svg",
    "assets/icons/baseball.svg",
    "assets/icons/hockey.svg",
    "assets/icons/soccer.svg",
    "assets/icons/tennis.svg"
  ];

  Future getImageGallery() async {
    final pickedFile =
        await picker.getImage(source: ImageSource.gallery, imageQuality: 25);
    if (pickedFile != null) {
      setState(() {
        imagePicked = File(pickedFile.path);
        playerPhotosList.add(imagePicked);
      });
    }
  }

  Future getImageCamera() async {
    final pickedFile =
        await picker.getImage(source: ImageSource.camera, imageQuality: 25);
    if (pickedFile != null) {
      setState(() {
        imagePicked = File(pickedFile.path);
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    sportsList();
    super.initState();
    userBloc.deleteAthleteResponse.listen((event) {});
  }

  void sportsList() {
    for (int i = 0; i < widget.athleteData.sports.length; i++) {
      sportList.add(widget.athleteData.sports[i].id);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: Container(),
          actions: [
            AppBarCr(
              pageTitle: "USERS",
              rightTextYes: true,
              rightText: "Done",
              leftIcon: "assets/icons/back_button.svg",
              onTapRightIcon: () {
                pop(context);
              },
              onTapLeftIcon: () {
                push(context, ProfilePage());
              },
            ),
          ],
        ),
        bottomNavigationBar: BottomNavigationWidget(),
        body: ListView(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 20)),
              child: Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {});
                      bottomSheetAddPhotos(context);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 70),
                        ),
                        Stack(
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: Container(
                                  width: screenWidth(context, dividedBy: 2),
                                  height: screenWidth(context, dividedBy: 2),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle),
                                  child: Image(
                                    image: AssetImage(
                                        "assets/images/user_image.png"),
                                  )),
                            ),
                            Column(
                              children: [
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 5.4),
                                ),
                                Row(
                                  children: [
                                    SizedBox(
                                      width:
                                          screenWidth(context, dividedBy: 2.9),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        bottomSheetAddPhotos(context);
                                      },
                                      child: CircleAvatar(
                                        backgroundColor:
                                            Constants.kitGradients[1],
                                        radius:
                                            screenWidth(context, dividedBy: 20),
                                        child: Icon(
                                          Icons.camera_alt_rounded,
                                          color: Colors.white,
                                          size: screenWidth(context,
                                              dividedBy: 15),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 20),
                  ),
                  InformationWidget(
                    label: "UserName",
                    text: widget.athleteData.name,
                  ),
                  Divider(
                    thickness: 2.0,
                    color: Colors.white12,
                  ),
                  InformationWidget(
                    label: "Gender",
                    text: widget.athleteData.gender == 1 ? "Male" : "Female",
                  ),
                  Divider(
                    thickness: 2.0,
                    color: Colors.white24,
                  ),
                  InformationWidget(
                    label: "Date of Birth",
                    text: "26/11/2020",
                  ),
                  Divider(
                    thickness: 2.0,
                    color: Colors.white24,
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 100),
                  ),
                  Row(
                    children: [
                      Text(
                        "Sports",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontFamily: "OpenSansRegular",
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          push(
                              context,
                              EditSportsPage(
                                icon: sportsIcon,
                                label: sports,
                                sportId: sportList,
                                athleteId: widget.athleteData.id,
                              ));
                        },
                        child: Container(
                            height: screenHeight(context, dividedBy: 7),
                            width: screenWidth(context, dividedBy: 5),
                            child: Column(
                              children: [
                                Icon(
                                  Icons.add,
                                  color: Constants.kitGradients[1],
                                  size: 60,
                                ),
                                Text(
                                  "Add",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'OpenSansRegular',
                                  ),
                                )
                              ],
                            )),
                      ),
                      Container(
                        width: screenWidth(context, dividedBy: 1.45),
                        height: screenHeight(context, dividedBy: 8),
                        child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: widget.athleteData.sports.length,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (BuildContext context, int index) {
                              return ListTileAddSports(
                                image: "assets/icons/basket_ball_icon.svg",
                                label: widget.athleteData.sports[index].name,
                              );
                            }),
                      ),
                    ],
                  ),
                  // Container(
                  //   width: screenWidth(context, dividedBy: 1.12),
                  //   height: screenHeight(context, dividedBy: 8),
                  //   child: ListView.builder(
                  //       shrinkWrap: true,
                  //       itemCount: playerPhotosList.length == 0
                  //           ? 1
                  //           : playerPhotosList.length,
                  //       scrollDirection: Axis.horizontal,
                  //       itemBuilder: (BuildContext context, int index) {
                  //         return index == 0
                  //             ? GestureDetector(
                  //                 onTap: () {
                  //                   bottomSheetAddPhotos(context);
                  //                 },
                  //                 child: Container(
                  //                     height:
                  //                         screenHeight(context, dividedBy: 8),
                  //                     width: screenWidth(context, dividedBy: 4),
                  //                     decoration: BoxDecoration(
                  //                       borderRadius: BorderRadius.circular(10),
                  //                       color: Colors.white,
                  //                     ),
                  //                     child: Column(
                  //                       children: [
                  //                         Icon(
                  //                           Icons.add,
                  //                           color: Constants.kitGradients[1],
                  //                           size: 60,
                  //                         ),
                  //                         Text(
                  //                           "Add File",
                  //                           style: TextStyle(
                  //                             color: Colors.black,
                  //                             fontFamily: 'OpenSansRegular',
                  //                           ),
                  //                         )
                  //                       ],
                  //                     )),
                  //               )
                  //             : ListTileAddImages(
                  //                 imageName: playerPhotosList[index - 1],
                  //               );
                  //       }),
                  // ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  SelectButton(
                    title: "Delete User",
                    onPressed: () {
                      userBloc.deleteAthlete(id: widget.athleteData.id);
                    },
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 14),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void bottomSheetAddPhotos(context) {
    showModalBottomSheet<dynamic>(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (BuildContext context, setState) {
            return Container(
                color: Colors.white,
                height: screenHeight(context, dividedBy: 4),
                width: screenWidth(context, dividedBy: 1),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          right: screenWidth(context, dividedBy: 10)),
                      child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Cancel",
                            style: TextStyle(
                              color: Constants.kitGradients[1],
                              fontFamily: "Poppins",
                              fontSize: 14,
                            ),
                          )),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                            onTap: () {
                              getImageCamera();
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.camera,
                              size: 50,
                              color: Constants.kitGradients[1],
                            )),
                        GestureDetector(
                            onTap: () {
                              getImageGallery();
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.image,
                              size: 50,
                              color: Constants.kitGradients[1],
                            )),
                      ],
                    ),
                  ],
                ));
          });
        });
  }
}
