import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CoachProfileAssociation extends StatefulWidget {
  final String imageName;
  final String heading;
  final String subHeading;

  CoachProfileAssociation({
    this.subHeading,
    this.imageName,
    this.heading,
  });
  @override
  _CoachProfileAssociationState createState() =>
      _CoachProfileAssociationState();
}

class _CoachProfileAssociationState extends State<CoachProfileAssociation> {
  bool bookMarks = false;
  bool rated = false;
  bool leagues = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                width: screenWidth(context, dividedBy: 70),
              ),
              Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 80),
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 6),
                    height: screenWidth(context, dividedBy: 5.5),
                    color: Colors.red,
                    child: SvgPicture.asset(
                      widget.imageName,
                      fit: BoxFit.cover,
                    ),
                  )
                ],
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 30),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 120),
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: screenWidth(context, dividedBy: 1.46),
                          child: Text(
                            widget.heading,
                            style: TextStyle(
                                fontSize: 13,
                                color: Colors.white,
                                fontFamily: 'OpenSansSemiBold'),
                          ),
                        ),
                      ]),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 100),
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 1.7),
                    child: Center(
                      child: Text(
                        widget.subHeading,
                        style: TextStyle(
                            fontSize: screenWidth(context, dividedBy: 37),
                            color: Colors.white,
                            fontFamily: 'OpenSansSemiBold'),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 100),
                  ),
                ],
              ),
            ]),
        SizedBox(height: screenHeight(context, dividedBy: 100))
      ],
    );
  }
}
