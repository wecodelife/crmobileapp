import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CalendarTimeLine extends StatefulWidget {
  final DateTime startTime;
  final DateTime eventStartTime;
  final DateTime eventEndTime;
  final DateTime endTime;
  CalendarTimeLine(
      {this.startTime, this.endTime, this.eventStartTime, this.eventEndTime});
  @override
  _CalendarTimeLineState createState() => _CalendarTimeLineState();
}

class _CalendarTimeLineState extends State<CalendarTimeLine> {
  bool _eventTimeCheck() {
    if (widget.startTime.isAtSameMomentAs(widget.eventStartTime) ||
        widget.endTime.isAfter(widget.eventStartTime)) {
      if (widget.endTime.isAtSameMomentAs(widget.eventEndTime) ||
          widget.endTime.isBefore(widget.eventEndTime)) {
        print("true");
        return true;
      } else
        print("false");

      return false;
    } else
      print("false");

    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              DateFormat('hh:mm a').format(widget.startTime),
              style: TextStyle(color: Colors.white),
            ),
            Container(
              width: screenWidth(context, dividedBy: 1.3),
              height: 2,
              color: Colors.white,
            )
          ],
        ),
        Stack(
          children: [
            Container(
              height: screenHeight(context, dividedBy: 14),
              width: screenWidth(context, dividedBy: 1),
              color: _eventTimeCheck() ? Colors.yellow : Colors.transparent,
            ),
            Container(
              height: screenHeight(context, dividedBy: 14),
              width: screenWidth(context, dividedBy: 1),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  ListView.builder(
                      shrinkWrap: true,
                      itemCount: 45,
                      itemBuilder: (BuildContext ctxt, int index) {
                        return SizedBox(
                          child: Container(
                            color: Colors.red,
                          ),
                          height: screenHeight(context, dividedBy: 900),
                        );
                      }),
                  Text(DateFormat('hh:mm a').format(DateTime.now()) +
                      "-------------------------------")
                ],
              ),
            ),
            // Positioned(
            //   child: Text(DateFormat('hh:mm a').format(DateTime.now()) +
            //       "-------------------------------"),
            //   height: screenHeight(context, dividedBy: 10),
            // )
          ],
        ),
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceAround,
        //   children: [
        //     Text(
        //       widget.endTime,
        //       style: TextStyle(color: Colors.white),
        //     ),
        //     Container(
        //       width: screenWidth(context, dividedBy: 1.3),
        //       height: 2,
        //       color: Colors.white,
        //     )
        //   ],
        // ),
      ],
    );
  }
}
