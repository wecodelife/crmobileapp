import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/add_parent_request_model.dart';
import 'package:app_template/src/models/get_sports_response_model.dart';
import 'package:app_template/src/ui/screens/gal.dart';
import 'package:app_template/src/ui/screens/onboarding_parent_athelete_9.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/drop_down_cr_app.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/list_tile_select_sports.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class OnBoardingParentAthlete6 extends StatefulWidget {
  final int imageId;
  OnBoardingParentAthlete6({this.imageId});
  @override
  _OnBoardingParentAthlete6State createState() =>
      _OnBoardingParentAthlete6State();
}

class _OnBoardingParentAthlete6State extends State<OnBoardingParentAthlete6> {
  TextEditingController nameTextEditingController = new TextEditingController();
  TextEditingController addressTextEditingController =
      new TextEditingController();
  TextEditingController yourEmailTextEditingController =
      new TextEditingController();
  TextEditingController phoneNumberTextEditingController =
      new TextEditingController();
  List<String> genderList = ["Male", "Female"];
  List<String> designationList = ["Yes", "No"];
  String genderDropDownValue;
  String designationDropDownValue;
  DateTime selectedDate;
  UserBloc userBloc = UserBloc();
  int count = 0;
  bool disabled;
  bool isValid;
  bool loading = false;
  bool onClicked;
  List<int> selectedSports = [];
  List<String> sports = [
    "Basketball",
    "Volleyball",
    "Football",
    "Swimming",
    "Baseball",
    "Hockey",
    "Soccer",
    "Tennis"
  ];
  List<String> sportsIcon = [
    "assets/icons/basketball.svg",
    "assets/icons/volleyball.svg",
    "assets/icons/basket_ball_icon.svg",
    "assets/icons/swimming.svg",
    "assets/icons/baseball.svg",
    "assets/icons/hockey.svg",
    "assets/icons/soccer.svg",
    "assets/icons/tennis.svg"
  ];
  // DateTime currentDate=DateTime.now();
  Future<String> addItems(int value) async {
    if (onClicked == true) {
      incrementCounter();
      setState(() {
        // selectedSports.add(snapshot
        //     .data.data[index].id);
        selectedSports.add(value);
      });
      return "fhfhgf" + selectedSports.toString();
    } else {
      decrementCounter();
      selectedSports.remove(value);
      return "gfh" + selectedSports.toString();
    }
  }

  String formattedDate;
  _selectDate(BuildContext context) async {
    selectedDate = DateTime.now();
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        formattedDate = DateFormat('dd/MM/yyyy').format(selectedDate);
        print(selectedDate);
      });
  }

  didValuesUpdate() {
    if (nameTextEditingController.text != "" &&
        yourEmailTextEditingController.text != "" &&
        addressTextEditingController.text != "" &&
        phoneNumberTextEditingController.text != "" &&
        genderDropDownValue != "" &&
        designationDropDownValue != "" &&
        selectedDate != null &&
        count > 0 &&
        genderDropDownValue != "") {
      if (isValid =
          EmailValidator.validate(yourEmailTextEditingController.text)) {
        setState(() {
          disabled = false;
        });
      } else {
        showToast("Enter a Valid Email.");
      }
    } else {
      showToast("Enter all the Details");
    }
  }

  void incrementCounter() {
    count = count + 1;
  }

  void decrementCounter() {
    count = count - 1;
  }

  @override
  void initState() {
    formattedDate = (DateFormat('dd/mm/yyyy').format(DateTime.now()));
    disabled = true;

    print(selectedDate);
    userBloc.getSports();
    userBloc.addParentResponse.listen((event) {
      print("fdhdh");
      setState(() {
        loading = false;
      });
      if (event.status == 200 || event.status == 201) {
        ObjectFactory().appHive.putParentId(value: event.data.id);
        ObjectFactory().appHive.putParentName(value: event.data.name);
        ObjectFactory().appHive.putParentEmail(value: event.data.email);
        ObjectFactory().appHive.putParentNumber(value: event.data.phoneNumber);
        push(
            context,
            OnBoardingParentAthlete9(
              parentId: event.data.id,
            ));
      }
    });

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: loading == true
          ? Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : ListView(
              children: [
                Container(
                  height: screenHeight(context, dividedBy: 1.28),
                  child: ListView(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      TextProgressIndicator(
                        heading: "LETS GET SOME ACCOUNT DETAILS SET UP",
                        descriptionText:
                            "Here you add a profile picture, name, address, email, spouse, and kids.",
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: screenWidth(context, dividedBy: 20),
                          ),
                          OnBoardingProgressIndicator(
                            width: 3.5,
                            progressIndicator: true,
                          ),
                          SizedBox(
                            width: screenWidth(context, dividedBy: 20),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      // ObjectFactory()
                      //             .appHive
                      //             .getImagePath(key: "parentImage") ==
                      //         null
                      //     ?
                      Center(
                        child: Text(
                          "Add a Profile Picture",
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'OpenSansRegular',
                              fontSize: 14),
                        ),
                      ),
                      // : Container(),
                      // ObjectFactory()
                      //             .appHive
                      //             .getImagePath(key: "parentImage") ==
                      //         null
                      //     ?
                      GestureDetector(
                        child: Icon(
                          Icons.add_circle,
                          size: 80,
                          color: Theme.of(context).buttonColor,
                        ),
                        onTap: () {
                          push(
                              context,
                              GalleryScreen(
                                imageKey: "parentImage",
                              ));
                        },
                      ),
                      // : Container(
                      //     child: CircleAvatar(
                      //       radius: screenWidth(context, dividedBy: 10),
                      //       backgroundImage: NetworkImage(
                      //         ObjectFactory().appHive.getImagePath(
                      //                     key: "parentImage") ==
                      //                 null
                      //             ? Urls.baseUrl +
                      //                 ObjectFactory()
                      //                     .appHive
                      //                     .getImagePath(key: "parentImage")
                      //             : "https://www.foodwest.fi/wp-content/uploads/2016/11/blank-profile-picture-973460_1280.png",
                      //       ),
                      //     ),
                      //   ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: screenWidth(context, dividedBy: 12)),
                        child: Column(
                          children: [
                            FormFeildUserDetails(
                              labelText: "Name",
                              textEditingController: nameTextEditingController,
                              onValueChanged: (name) {},
                            ),
                            FormFeildUserDetails(
                              labelText: "Your Address",
                              textEditingController:
                                  addressTextEditingController,
                              onValueChanged: (yourAddress) {},
                            ),
                            FormFeildUserDetails(
                              labelText: "Your Email",
                              textEditingController:
                                  yourEmailTextEditingController,
                              onValueChanged: (yourEmail) {},
                            ),
                            FormFeildUserDetails(
                              labelText: "PhoneNumber",
                              textEditingController:
                                  phoneNumberTextEditingController,
                              onValueChanged: (phoneNumber) {},
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 30),
                            ),
                            Row(
                              children: [
                                Text(
                                  "Gender",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'OpenSansRegular',
                                      fontSize: 16),
                                ),
                              ],
                            ),
                            Container(
                              width: screenWidth(context, dividedBy: 1.1),
                              child: DropDownCrApp(
                                title: "Select",
                                dropDownValue: genderDropDownValue,
                                onClicked: (value) {
                                  setState(() {
                                    genderDropDownValue = value;
                                  });
                                },
                                dropDownList: genderList,
                              ),
                            ),
                            Row(
                              children: [
                                Text(
                                  "Date of Birth",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'OpenSansRegular',
                                      fontSize: 16),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 60),
                            ),
                            Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      formattedDate,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontFamily: 'OpenSansRegular'),
                                    ),
                                    GestureDetector(
                                        onTap: () {
                                          _selectDate(context);
                                        },
                                        child: Icon(
                                          Icons.calendar_today,
                                          color: Colors.white,
                                        )),
                                  ],
                                ),
                                Divider(
                                  thickness: 2,
                                  color: Colors.white30,
                                )
                              ],
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 60),
                            ),
                            Row(
                              children: [
                                Text(
                                  "Select Your Sports",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'OpenSansRegular',
                                      fontSize: 16),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 60),
                            ),
                            Container(
                              height: screenHeight(context, dividedBy: 5.5),
                              width: screenWidth(context, dividedBy: 1.07),
                              child: StreamBuilder<GetSportsResponseModel>(
                                  stream: userBloc.getSportsResponse,
                                  builder: (context, snapshot) {
                                    return snapshot.hasData
                                        ? ListView.builder(
                                            shrinkWrap: true,
                                            scrollDirection: Axis.horizontal,
                                            itemCount:
                                                snapshot.data.data.length,
                                            itemBuilder: (BuildContext context,
                                                int index) {
                                              print(snapshot
                                                  .data.data[index].name);
                                              return ListTileSelectSports(
                                                label: snapshot
                                                    .data.data[index].name,
                                                icon: snapshot
                                                    .data.data[index].icon,
                                                onValueChanged:
                                                    (onClick) async {
                                                  onClicked = onClick;
                                                  await addItems(snapshot
                                                      .data.data[index].id);

                                                  print(selectedSports
                                                      .toString());
                                                },
                                              );
                                            },
                                          )
                                        : Container();
                                  }),
                            ),
                            Row(
                              children: [
                                Text(
                                  "Do you want a coach or Ref?",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'OpenSansRegular',
                                      fontSize: 16),
                                ),
                              ],
                            ),
                            Container(
                              width: screenWidth(context, dividedBy: 1.1),
                              child: DropDownCrApp(
                                title: "Select",
                                dropDownValue: designationDropDownValue,
                                onClicked: (value) {
                                  setState(() {
                                    designationDropDownValue = value;
                                  });
                                },
                                dropDownList: designationList,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 70),
                ),
                Row(
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 30),
                    ),
                    BuildButton(
                      title: "NEXT",
                      disabled: false,
                      onPressed: () {
                        setState(() {
                          loading = true;
                        });
                        userBloc.addParent(
                            addParentRequestModel: AddParentRequestModel(
                                email: yourEmailTextEditingController.text,
                                name: nameTextEditingController.text,
                                phoneNumber:
                                    phoneNumberTextEditingController.text,
                                address: addressTextEditingController.text,
                                dob: selectedDate,
                                gender: genderDropDownValue == "Male" ? 1 : 2,
                                profPic: ObjectFactory()
                                            .appHive
                                            .getImagePath(key: "parentImage") !=
                                        null
                                    ? int.parse(ObjectFactory()
                                        .appHive
                                        .getImageId("parentImageid"))
                                    : null,
                                trainerChooseFacilities: null,
                                trainingAtHome: null,
                                user: ObjectFactory().appHive.getUserId(),
                                isCoachOrRef: designationDropDownValue == "Yes"
                                    ? true
                                    : false,
                                sports: selectedSports));
                      },
                    ),
                  ],
                ),
              ],
            ),
    );
  }
}
