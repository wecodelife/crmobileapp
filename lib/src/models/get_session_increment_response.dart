// To parse this JSON data, do
//
//     final getSessionIncrementResponse = getSessionIncrementResponseFromJson(jsonString);

import 'dart:convert';

GetSessionIncrementResponse getSessionIncrementResponseFromJson(String str) =>
    GetSessionIncrementResponse.fromJson(json.decode(str));

String getSessionIncrementResponseToJson(GetSessionIncrementResponse data) =>
    json.encode(data.toJson());

class GetSessionIncrementResponse {
  GetSessionIncrementResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final List<DatumSession> data;

  factory GetSessionIncrementResponse.fromJson(Map<String, dynamic> json) =>
      GetSessionIncrementResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null
            ? null
            : List<DatumSession>.from(
                json["data"].map((x) => DatumSession.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DatumSession {
  DatumSession({
    this.id,
    this.name,
    this.order,
  });

  final int id;
  final String name;
  final int order;

  factory DatumSession.fromJson(Map<String, dynamic> json) => DatumSession(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        order: json["order"] == null ? null : json["order"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "order": order == null ? null : order,
      };
}
