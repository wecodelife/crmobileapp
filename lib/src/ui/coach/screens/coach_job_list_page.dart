import 'package:app_template/material/pickers/date_range_picker_dialog.dart';
import 'package:app_template/src/ui/buisness/screens/bottomsheet_events.dart';
import 'package:app_template/src/ui/coach/widgets/coachBottomNavigationBar.dart';
import 'package:app_template/src/ui/coach/widgets/coachJobListTile.dart';
import 'package:app_template/src/ui/coach/widgets/coach_training_session_bottom_sheet.dart';
import 'package:app_template/src/ui/coach/widgets/pending_jobs_bottom_sheets.dart';
import 'package:app_template/src/ui/widgets/button.dart';
import 'package:app_template/src/ui/widgets/home_fliter_tile.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/rating_bottom_sheet.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class CoachJobListPage extends StatefulWidget {
  @override
  _CoachJobListPageState createState() => _CoachJobListPageState();
}

class _CoachJobListPageState extends State<CoachJobListPage> {
  List<String> dayType = ["Today", "Week", "Month"];
  List<bool> borderType = [false, true, true];
  List<String> filters = [
    "All Sports",
    "Time",
    "\$,\$\$",
  ];
  List<String> price = ["\u0024", "\u0024\u0024"];
  List<String> titles = [
    "FootBall Field",
    "FootBall Field",
    "Soccer Field",
    "Soccer Field"
  ];
  List<String> time = [
    "Any",
    "Early AM",
    "Morning",
    "Afternoon",
    "Evening",
    "Late"
  ];
  List<String> subtitles = ["Field A", "Field C", "Field B", "Field D"];
  List<String> images = [
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png"
  ];
  List<String> icons = [
    " assets/icons/soccer_icon.svg",
    " assets/icons/volleyball.svg",
    " assets/icons/soccer_icon.svg",
    " assets/icons/volleyball.svg",
  ];

  int count = 1;
  int fcount = 1;
  int filterIndex;
  List<String> sports = [
    "Basketball",
    "Volleyball",
    "Football",
    "Swimming",
    "Baseball",
    "Hockey",
    "Soccer",
    "Tennis"
  ];
  List<bool> color = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];

  Future<Null> selectDateRange(BuildContext context) async {
    DateTimeRange picked = await showDatesRangePicker(
        context: context,
        // initialDateRange: DateTimeRange(
        //   start: DateTime.now().add(Duration(days: 0)),
        //   end: DateTime.now().add(Duration(days: 1)),
        // ),

        firstDate: DateTime.now(),
        lastDate: DateTime(DateTime.now().year + 5),
        helpText: "Select_Date_Range",
        cancelText: "CANCEL",
        confirmText: "OK",
        saveText: "SAVE",
        bottomNavigationWidget: CoachBottomNavigationWidget(),
        errorFormatText: "Invalid_format",
        errorInvalidText: "Out_of_range",
        errorInvalidRangeText: "Invalid_range",
        fieldStartHintText: "Start_Date",
        sessionName: "business",
        fieldEndLabelText: "End_Date");

    // if (picked != null) {
    //   state.didChange(picked);
    // }
  }

  FToast fToast;

  @override
  void initState() {
    fToast = FToast();
    fToast.init(context);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: CoachBottomNavigationWidget(),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[6],
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: SingleChildScrollView(
        child: Container(
          //height: screenHeight(context, dividedBy: 1),
          width: screenWidth(context, dividedBy: 1),
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 35)),
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),

                //Buttons
                Container(
                  //width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 25),
                  alignment: Alignment.center,
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: dayType.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        margin: EdgeInsets.symmetric(horizontal: 7),
                        child: CRButton(
                          buttonName: dayType[index],
                          buttonWidth: 100,
                          buttonBackgroundColor: Color(0xff3F4A40),
                          buttonBorder: borderType[index],
                          borderRadius: 0,
                          textColor: Colors.white,
                          onPressed: () {},
                        ),
                      );
                    },
                  ),
                ),

                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),

                //Filters
                Container(
                  //width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 15),
                  alignment: Alignment.center,
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: dayType.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        // margin: EdgeInsets.symmetric(horizontal: 7),
                        child: HomeFilterTile(
                          label: filters[index],
                          colorChange: color[index],
                          onPressed: () {
                            filterIndex = index;
                            displayBottomSheet(context);
                          },
                          index: index,
                        ),
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),

                //Expansion Activities
                Column(
                  children: [
                    Row(
                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        fcount == 1
                            ? GestureDetector(
                                onTap: () {
                                  setState(() {
                                    fcount = 2;
                                  });
                                },
                                child: Icon(
                                  Icons.keyboard_arrow_down,
                                  size: 24,
                                  color: Colors.white,
                                ),
                              )
                            : GestureDetector(
                                onTap: () {
                                  setState(() {
                                    fcount = 1;
                                  });
                                },
                                child: Icon(
                                  Icons.keyboard_arrow_up,
                                  size: 24,
                                  color: Colors.white,
                                ),
                              ),
                        Container(
                          width: screenWidth(context, dividedBy: 3),
                          child: Text(
                            "Pending Jobs" + "(" + "1" ")",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'OpenSansSemiBold',
                                fontSize: screenWidth(context, dividedBy: 28)),
                          ),
                        ),
                        Container(
                          width: screenWidth(
                            context,
                            dividedBy: 2,
                          ),
                          height: 2,
                          color: Color(0xff3F4A40),
                        )
                      ],
                    ),
                  ],
                ),
                // SizedBox(
                //   height: screenHeight(context, dividedBy: 40),
                // ),
                fcount == 2
                    ? Container(
                        //flex: 2,
                        child: ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: 1,
                            itemBuilder: (BuildContext context, int index) {
                              return CoachJobListTile(
                                locationHeading: "YMCA",
                                address: "76A Eighth Ave. Pittsburgh PA, 15211",
                                contact: "Greth Smith",
                                date: "12/10/2020",
                                startTime: "9:00AM",
                                sport: "Soccer",
                                duration: "5 hours",
                                amount: "100",
                                onPressed: () {
                                  rescheduleModalBottomSheet(context);
                                },
                              );
                            }),
                      )
                    : Container(),
                Divider(
                  color: Color(0xff3F4A40),
                ),

                //Expansion Facilities

                Column(
                  children: [
                    Row(
                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        count == 1
                            ? GestureDetector(
                                onTap: () {
                                  setState(() {
                                    count = 2;
                                  });
                                },
                                child: Icon(
                                  Icons.keyboard_arrow_down,
                                  size: 24,
                                  color: Colors.white,
                                ),
                              )
                            : GestureDetector(
                                onTap: () {
                                  setState(() {
                                    count = 1;
                                  });
                                },
                                child: Icon(
                                  Icons.keyboard_arrow_up,
                                  size: 24,
                                  color: Colors.white,
                                ),
                              ),
                        Container(
                          width: screenWidth(context, dividedBy: 2.5),
                          child: Text(
                            "Current Jobs " + "(" + "6" ")",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'OpenSansSemiBold',
                                fontSize: screenWidth(context, dividedBy: 28)),
                          ),
                        ),
                        Container(
                          width: screenWidth(
                            context,
                            dividedBy: 2.3,
                          ),
                          height: 2,
                          color: Color(0xff3F4A40),
                        )
                      ],
                    ),
                  ],
                ),
                count == 2
                    ? Container(
                        //flex: 4,
                        child: ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: 3,
                            itemBuilder: (BuildContext context, int index) {
                              return CoachJobListTile(
                                locationHeading: "YMCA",
                                address: "76A Eighth Ave. Pittsburgh PA, 15211",
                                contact: "Greth Smith",
                                date: "12/10/2020",
                                startTime: "9:00AM",
                                sport: "Soccer",
                                duration: "5 hours",
                                amount: "100",
                                onPressed: () {
                                  rescheduleCurrentJobsBottomSheet(context);
                                },
                              );
                            }),
                      )
                    : Container(),
                // SizedBox(
                //   height: screenHeight(context, dividedBy: 40),
                // ),
                Divider(
                  color: Color(0xff3F4A40),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void displayBottomSheet(context) {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return filterIndex == 0
            ? BottomSheetHome(
                height: 1.5,
                title: "Sports",
                label: sports,
                onClicked: (value) {
                  setState(() {
                    color[4] = value;
                  });
                },
              )
            : filterIndex == 1
                ? BottomSheetHome(
                    height: 1.5,
                    title: "Time of Day",
                    label: time,
                    onClicked: (value) {
                      setState(() {
                        color[4] = value;
                      });
                    },
                  )
                : BottomSheetHome(
                    height: 3,
                    title: "Cost",
                    label: price,
                    onClicked: (value) {
                      setState(() {
                        color[2] = value;
                      });
                    },
                  );
      },
    );
  }

  void rescheduleModalBottomSheet(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: true,
        context: context,
        builder: (builder) {
          return new Container(
              height: screenHeight(context, dividedBy: 1.7),
              decoration: new BoxDecoration(
                  color: Constants.kitGradients[7],
                  borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(10.0),
                      topRight: const Radius.circular(10.0))),
              child: BottomSheetPendingJobs(
                name: "Greth Smith",
                phoneNumber: "6878789798",
                buttonName3: "RESCHEDULE",
                startTime: "9 AM",
                endTime: "10 AM",
                email: "GrethSmith@gmail.com",
                duration: "1 Hour",
                amount: "10",
                sportImage: "assets/icons/soccer_icon.svg",
                buttonName1: "DECLINE",
                buttonName2: "ACCEPT",
                sportName: "Football",
                date: "12/10/2020",
                locationAddress: "76A Eighth Avenue Pittsburgh PA, 15211",
                locationName: "YMCA",
                rated: true,
                bookmarks: "137",
                distance: "0.4",
                endTime1: "9AM",
                endTime2: "9AM",
                liked: true,
                likes: "4.7",
                startTime1: "9AM",
                startTime2: "9AM",
                time: "30Min",
                profileImageUrl: "https://via.placeholder.com/150/92c952",
                onPressedReschedule: () {
                  coachEventModalBottomSheet(context);
                },
                onPressedDecline: () {
                  pop(context);
                },
                onPressedAccept: () {
                  pop(context);
                },
              ));
        });
  }

  void coachEventModalBottomSheet(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: true,
        context: context,
        builder: (builder) {
          return new Container(
              height: screenHeight(context, dividedBy: 1.17),
              decoration: new BoxDecoration(
                  color: Constants.kitGradients[7],
                  borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(10.0),
                      topRight: const Radius.circular(10.0))),
              child: EventsBottomSheet(
                onPressed: () {
                  showToastCompleted(
                      context, fToast, "Updated event has been sent");
                  push(context, CoachJobListPage());
                },
              ));
        });
  }

  void rescheduleCurrentJobsBottomSheet(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: true,
        context: context,
        builder: (builder) {
          return new Container(
              height: screenHeight(context, dividedBy: 1.7),
              decoration: new BoxDecoration(
                  color: Constants.kitGradients[7],
                  borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(10.0),
                      topRight: const Radius.circular(10.0))),
              child: CoachTrainingSessionBottomSheet(
                name: "Greth Smith",
                phoneNumber: "6878789798",
                startTime: "9 AM",
                endTime: "10 AM",
                email: "GrethSmith@gmail.com",
                duration: "1 Hour",
                amount: "10",
                sportImage: "assets/icons/soccer_icon.svg",
                cancelButtonTrue: true,
                buttonName1: "RESCHEDULE",
                buttonName2: "NO SHOW",
                sportName: "Football",
                date: "12/10/2020",
                locationAddress: "76A Eighth Avenue Pittsburgh PA, 15211",
                locationName: "YMCA",
                rated: true,
                bookmarks: "137",
                distance: "0.4",
                endTime1: "9AM",
                endTime2: "9AM",
                liked: true,
                likes: "4.7",
                startTime1: "9AM",
                startTime2: "9AM",
                time: "30Min",
                profileImageUrl: "https://via.placeholder.com/150/92c952",
                onPressedCancelButton: () {
                  showBookingAlert(
                      context: context,
                      icon: "assets/icons/exclamation.svg",
                      message:
                          "A \u002415 cancelation fee will be automatically withdrawn from your account for the facility. Are you sure you want to do this?",
                      onPressedCancelBooking: () {
                        eventCancelledAlertBox(
                            context: context,
                            icon: "assets/icons/circle_right_icon.svg",
                            onPressed: () {
                              push(context, CoachJobListPage());
                            },
                            price: "15");
                      },
                      onPressedNeverMind: () {
                        pop(context);
                      });
                },
                onPressedRescheduleButton: () {
                  coachEventModalBottomSheet(context);
                },
                onPressedNoShowButton: () {},
              ));
        });
  }
}
