import 'package:app_template/src/models/get_facility_type_response.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DropDownCoachRefAppFacility extends StatefulWidget {
  List<Datums> dropDownList;
  String title;
  Datums dropDownValue;
  ValueChanged<Datums> onClicked;
  DropDownCoachRefAppFacility(
      {this.title, this.dropDownList, this.dropDownValue, this.onClicked});
  @override
  _DropDownCoachRefAppFacilityState createState() =>
      _DropDownCoachRefAppFacilityState();
}

Datums dropdownValue;

class _DropDownCoachRefAppFacilityState
    extends State<DropDownCoachRefAppFacility> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 10),
      width: screenWidth(context, dividedBy: 1.23),
      child: DropdownButton<Datums>(
        underline: Divider(
          thickness: 1.0,
          color: Constants.kitGradients[0],
        ),
        icon: Row(
          children: [
            Icon(
              Icons.arrow_drop_down,
              size: 30,
              color: Constants.kitGradients[0],
            ),
          ],
        ),
        iconSize: 20,
        dropdownColor: Colors.black,
        value:
            widget.dropDownValue == null ? dropdownValue : widget.dropDownValue,
        style: TextStyle(fontSize: 16, color: Constants.kitGradients[0]),
        items: widget.dropDownList
            .map<DropdownMenuItem<Datums>>(
                (Datums value) => DropdownMenuItem<Datums>(
                    value: value,
                    child: Container(
                        width: screenWidth(context, dividedBy: 1.34),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              value.name,
                              style: TextStyle(fontFamily: 'OpenSansRegular'),
                            ),
                          ],
                        ))))
            .toList(),
        onChanged: (selectedValue) {
          setState(() {
            widget.dropDownValue = selectedValue;
            print(widget.dropDownValue);
          });
          widget.onClicked(selectedValue);
        },
        hint: Column(
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),
            Text(
              widget.title,
              style: TextStyle(fontSize: 18, color: Constants.kitGradients[0]),
            ),
          ],
        ),
      ),
    );
  }
}
