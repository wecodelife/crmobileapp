import 'package:app_template/src/ui/widgets/rating_widget.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProfileAssociation extends StatefulWidget {
  final String imageName;
  final String heading;
  final String subHeading;
  final String location;
  final String rating;
  final String leagueNumbers;
  final bool plusIcon;

  ProfileAssociation(
      {this.subHeading,
      this.imageName,
      this.heading,
      this.rating,
      this.location,
      this.leagueNumbers,
      this.plusIcon});
  @override
  _ProfileAssociationState createState() => _ProfileAssociationState();
}

class _ProfileAssociationState extends State<ProfileAssociation> {
  bool bookMarks = false;
  bool rated = false;
  bool leagues = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                width: screenWidth(context, dividedBy: 30),
              ),
              Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 6),
                    height: screenWidth(context, dividedBy: 6.4),
                    child: SvgPicture.asset(widget.imageName),
                  )
                ],
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 30),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 120),
                  ),
                  Row(children: [
                    Container(
                      width: screenWidth(context, dividedBy: 1.6),
                      child: Row(
                        children: [
                          Text(
                            widget.heading,
                            style: TextStyle(
                                fontSize: 13,
                                color: Colors.white,
                                fontFamily: 'OpenSansSemiBold'),
                          ),
                          Spacer(
                            flex: 5,
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                bookMarks = !bookMarks;
                              });
                            },
                            child: Icon(
                                bookMarks == true
                                    ? Icons.bookmark
                                    : Icons.bookmark_border,
                                size: 20,
                                color: Constants.kitGradients[1]),
                          ),
                        ],
                      ),
                    ),
                  ]),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 100),
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 1.7),
                    child: Center(
                      child: Text(
                        widget.subHeading,
                        style: TextStyle(
                            fontSize: screenWidth(context, dividedBy: 34),
                            color: Colors.white,
                            fontFamily: 'OpenSansSemiBold'),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 100),
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 1.6),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Icon(
                          Icons.location_on,
                          color: Constants.kitGradients[1],
                          size: 16,
                        ),
                        Spacer(
                          flex: 1,
                        ),
                        Text(
                          widget.location,
                          style: TextStyle(
                              fontFamily: 'OpenSansRegular',
                              fontSize: screenWidth(context, dividedBy: 40),
                              color: Constants.kitGradients[1],
                              decoration: TextDecoration.underline),
                        ),
                        Spacer(
                          flex: 1,
                        ),
                        Container(
                          color: Colors.white,
                          height: screenWidth(context, dividedBy: 40),
                          width: 1,
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: screenWidth(context, dividedBy: 70),
                            ),
                            GestureDetector(
                                onTap: () {
                                  setState(() {
                                    leagues = !leagues;
                                  });
                                },
                                child: Container(
                                  width: screenWidth(context, dividedBy: 20),
                                  child: leagues == true
                                      ? Image.asset(
                                          "assets/icons/people_icon.png",
                                          color: Constants.kitGradients[1],
                                        )
                                      : Image.asset(
                                          "assets/icons/people_icon.png",
                                          color: Colors.white,
                                        ),
                                )),
                            SizedBox(
                              width: screenWidth(context, dividedBy: 70),
                            ),
                            Container(
                              width: screenWidth(context, dividedBy: 18),
                              child: Text("45",
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontFamily: 'OpenSansRegular',
                                      color: Colors.white),
                                  overflow: TextOverflow.ellipsis),
                            ),
                            SizedBox(
                              width: screenWidth(context, dividedBy: 100),
                            ),
                            Container(
                              color: Colors.white,
                              height: screenWidth(context, dividedBy: 40),
                              width: 1,
                            ),
                            SizedBox(
                              width: screenWidth(context, dividedBy: 100),
                            )
                          ],
                        ),
                        RatingWidget(
                          icon: Icons.star,
                          heading: "4.7",
                          onPressed: () {
                            setState(() {
                              rated = !rated;
                            });
                          },
                          rated: rated,
                          divider: true,
                        ),
                        Spacer(
                          flex: 6,
                        ),
                        widget.plusIcon != true
                            ? Icon(
                                Icons.phone_outlined,
                                size: 18,
                                color: Constants.kitGradients[1],
                              )
                            : Container(),
                        widget.plusIcon != true
                            ? Spacer(
                                flex: 3,
                              )
                            : Spacer(
                                flex: 1,
                              ),
                        widget.plusIcon != true
                            ? Icon(
                                Icons.mail_outline,
                                size: 18,
                                color: Constants.kitGradients[1],
                              )
                            : Icon(
                                Icons.add_circle,
                                size: 20,
                                color: Constants.kitGradients[1],
                              )
                      ],
                    ),
                  )
                ],
              ),
            ]),
        SizedBox(height: screenHeight(context, dividedBy: 100))
      ],
    );
  }
}
