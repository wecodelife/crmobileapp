// To parse this JSON data, do
//
//     final addEmergencyContactResponseModel = addEmergencyContactResponseModelFromJson(jsonString);

import 'dart:convert';

AddEmergencyContactResponseModel addEmergencyContactResponseModelFromJson(
        String str) =>
    AddEmergencyContactResponseModel.fromJson(json.decode(str));

String addEmergencyContactResponseModelToJson(
        AddEmergencyContactResponseModel data) =>
    json.encode(data.toJson());

class AddEmergencyContactResponseModel {
  AddEmergencyContactResponseModel({
    this.message,
    this.data,
    this.status,
  });

  final String message;
  final List<Datum> data;
  final int status;

  factory AddEmergencyContactResponseModel.fromJson(
          Map<String, dynamic> json) =>
      AddEmergencyContactResponseModel(
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
        "status": status == null ? null : status,
      };
}

class Datum {
  Datum({
    this.id,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.email,
    this.phoneNumber,
    this.parent,
  });

  final int id;
  final bool isActive;
  final bool isDeleted;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String email;
  final String phoneNumber;
  final int parent;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        isActive: json["is_active"] == null ? null : json["is_active"],
        isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        email: json["email"] == null ? null : json["email"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        parent: json["parent"] == null ? null : json["parent"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "is_active": isActive == null ? null : isActive,
        "is_deleted": isDeleted == null ? null : isDeleted,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "email": email == null ? null : email,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "parent": parent == null ? null : parent,
      };
}
