import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class CRButton extends StatefulWidget {
  final Function onPressed;
  final double borderRadius;
  final Color buttonBackgroundColor;
  final String buttonName;
  final bool buttonBorder;
  final double buttonWidth;
  final Color textColor;
  final bool disabled;

  CRButton(
      {this.onPressed,
      this.borderRadius,
      this.buttonBackgroundColor,
      this.buttonName,
      this.buttonBorder,
      this.buttonWidth,
      this.textColor,
      this.disabled});

  @override
  _CRButtonState createState() => _CRButtonState();
}

class _CRButtonState extends State<CRButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.buttonWidth,
      height: screenHeight(context, dividedBy: 25),
      child: RaisedButton(
        onPressed: () {
          widget.onPressed();
        },
        color: widget.buttonBorder == true
            ? Colors.transparent
            : widget.buttonBackgroundColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(widget.borderRadius),
            side: widget.buttonBorder == true
                ? BorderSide(color: Color(0xffFFFFFF))
                : BorderSide.none),
        child: Center(
            child: Text(
          widget.buttonName,
          style: TextStyle(
              fontFamily: 'MultiBold', fontSize: 13.0, color: widget.textColor),
        )),
      ),
    );
  }
}
