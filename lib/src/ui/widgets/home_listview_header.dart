import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeListViewHeader extends StatefulWidget {
  final String title;
  final String itemNumber;
  final bool hasBackIcon;
  final bool hasEditIcon;
  final Function onPressedEditIcon;
  final Function onPressedBackIcon;
  HomeListViewHeader(
      {this.title,
      this.hasBackIcon,
      this.hasEditIcon,
      this.itemNumber,
      this.onPressedEditIcon,
      this.onPressedBackIcon});
  @override
  _HomeListViewHeaderState createState() => _HomeListViewHeaderState();
}

class _HomeListViewHeaderState extends State<HomeListViewHeader> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: screenWidth(context, dividedBy: 20),
        ),
        Text(
          widget.title,
          style: TextStyle(
              fontSize: 17,
              color: Constants.kitGradients[1],
              fontFamily: "Oswald",
              fontWeight: FontWeight.bold),
        ),
        SizedBox(
          width: screenWidth(context, dividedBy: 20),
        ),
        if (widget.hasEditIcon == true)
          GestureDetector(
            onTap: () {
              widget.onPressedEditIcon();
            },
            child: SvgPicture.asset("assets/icons/edit_icon.svg",
                height: screenWidth(context, dividedBy: 25),
                width: screenWidth(context, dividedBy: 25)),
          ),
        Spacer(
          flex: 13,
        ),
        if (widget.hasBackIcon == true)
          GestureDetector(
            onTap: () {
              widget.onPressedBackIcon();
            },
            child: SvgPicture.asset("assets/icons/square_back_icon.svg",
                height: screenWidth(context, dividedBy: 20),
                width: screenWidth(context, dividedBy: 20)),
          ),
        if (widget.itemNumber != null)
          Text(
            widget.itemNumber,
            style: TextStyle(
                fontSize: 14,
                color: Constants.kitGradients[0],
                fontFamily: "OpenSansRegular",
                fontWeight: FontWeight.normal),
          ),
        SizedBox(
          width: screenWidth(context, dividedBy: 20),
        ),
      ],
    );
  }
}
