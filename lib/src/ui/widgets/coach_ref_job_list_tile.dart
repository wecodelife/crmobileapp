import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/ui/widgets/summary_widget_email.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:app_template/src/ui/widgets/facility_soccer_filed_tile.dart';

class CoachRefJobListTile extends StatefulWidget {
  final String contact;
  final String date;
  final String startTime;
  final String sport;
  final String duration;
  final String rate;

  CoachRefJobListTile(
      {this.contact,
      this.date,
      this.duration,
      this.rate,
      this.sport,
      this.startTime});
  @override
  _CoachRefJobListTileState createState() => _CoachRefJobListTileState();
}

class _CoachRefJobListTileState extends State<CoachRefJobListTile> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            //push(context, ProfileDoc2());
          },
          child: Container(
            height: screenHeight(context, dividedBy: 2.45),
            width: screenWidth(context, dividedBy: 1),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    bottomLeft: Radius.circular(10)),
                color: Constants.kitGradients[5]),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: screenHeight(context, dividedBy: 2.5),
                  width: screenWidth(context, dividedBy: 50),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        bottomLeft: Radius.circular(10)),
                    color: Constants.kitGradients[12],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      //horizontal: screenWidth(context, dividedBy: 40),
                      vertical: screenHeight(context, dividedBy: 30)),
                  child: Container(
                    width: screenWidth(context, dividedBy: 1.15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: screenWidth(context, dividedBy: 1),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(
                                  horizontal:
                                      screenWidth(context, dividedBy: 30),
                                ),
                                width: screenWidth(context, dividedBy: 1.7),
                                child: Text(
                                  "Soccer Camp (8/10 Athletes)",
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontFamily: 'OpenSansSemiBold',
                                      color: Colors.white),
                                ),
                              ),
                              Container(
                                  //width: screenWidth(context, dividedBy: 3.5),
                                  child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Container(
                                      width:
                                          screenWidth(context, dividedBy: 19),
                                      height:
                                          screenHeight(context, dividedBy: 40),
                                      child: SvgPicture.asset(
                                          "assets/icons/call.svg")),
                                  SizedBox(
                                    width: screenWidth(context, dividedBy: 50),
                                  ),
                                  Container(
                                      width:
                                          screenWidth(context, dividedBy: 19),
                                      height:
                                          screenHeight(context, dividedBy: 40),
                                      child: SvgPicture.asset(
                                          "assets/icons/email.svg"))
                                ],
                              )),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: screenWidth(context, dividedBy: 30),
                            ),
                            SummaryTileEmail(
                              heading: "Contact",
                              subheading: widget.contact,
                              width: 3.8,
                            ),
                            SummaryTileEmail(
                              heading: "Date",
                              subheading: widget.date,
                              width: 3.8,
                            ),
                            SummaryTileEmail(
                                heading: "Start Time",
                                subheading: widget.startTime,
                                width: 3.7),
                          ],
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: screenWidth(context, dividedBy: 30),
                            ),
                            SummaryTileEmail(
                              icon: true,
                              image: "assets/icons/soccer_icon.svg",
                              heading: "Sport",
                              subheading: widget.sport,
                              width: 3.8,
                            ),
                            SummaryTileEmail(
                              heading: "Duration",
                              subheading: widget.duration,
                              width: 3.8,
                            ),
                            SummaryTileEmail(
                              heading: "Rate",
                              subheading: "\$" + widget.rate,
                              width: 3.8,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 40),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                            horizontal: screenWidth(context, dividedBy: 30),
                          ),
                          width: screenWidth(context, dividedBy: 3),
                          child: Text(
                            "FACILITIES" + "(" + "1" ")",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'OpenSansSemiBold',
                                fontSize: screenWidth(context, dividedBy: 28)),
                          ),
                        ),
                        FacilitySoccerFieldTile(
                          title: "Soccer Field",
                          image: "assets/images/soccer_field.png",
                          subtitle: "Field B",
                          icon: "assets/icons/soccer_icon.svg",
                          hasIcon: true,
                          hasButton: false,
                          hasPrice: false,
                          onPressed: () {},
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 40),
        )
      ],
    );
  }
}
