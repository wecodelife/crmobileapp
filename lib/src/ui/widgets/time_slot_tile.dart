import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class TimeSlotTile extends StatefulWidget {
  bool selected;
  bool deselected;
  String time;
  TimeSlotTile({this.selected, this.deselected, this.time});

  @override
  _TimeSlotTileState createState() => _TimeSlotTileState();
}

class _TimeSlotTileState extends State<TimeSlotTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 30),
      width: screenWidth(context, dividedBy: 5),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: widget.selected == true
              ? Constants.kitGradients[1]
              : Colors.transparent,
          border: Border.all(color: Constants.kitGradients[1], width: 1)),
      child: Center(
        child: Text(
          widget.time,
          style: TextStyle(
              color: widget.deselected == false ? Colors.white : Colors.grey,
              fontFamily: "SFProText-Regular"),
        ),
      ),
    );
  }
}
