import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class ProfileIconWidget extends StatefulWidget {
  final double radius;
  final double textPadding;
  final String phoneNumber;
  final String imageName;
  final String userName;
  final double fontSizeHeading;
  final double fontSizedPhoneNUmber;
  final double imagePadding;
  ProfileIconWidget(
      {this.radius,
      this.textPadding,
      this.imageName,
      this.phoneNumber,
      this.userName,
      this.fontSizedPhoneNUmber,
      this.imagePadding,
      this.fontSizeHeading});
  @override
  _ProfileIconWidgetState createState() => _ProfileIconWidgetState();
}

class _ProfileIconWidgetState extends State<ProfileIconWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.transparent,
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(
              vertical: screenHeight(context, dividedBy: 120),
              horizontal: screenWidth(context, dividedBy: 30)),
          child: Row(
            children: [
              Column(
                children: [
                  SizedBox(
                    height:
                        screenHeight(context, dividedBy: widget.imagePadding),
                  ),
                  CircleAvatar(
                    radius: screenWidth(context, dividedBy: widget.radius),
                    backgroundImage: NetworkImage(widget.imageName),
                  ),
                ],
              ),
              Spacer(
                flex: 1,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.userName,
                    style: TextStyle(
                        fontSize: widget.fontSizeHeading,
                        color: Colors.white,
                        fontFamily: 'OpenSansSemiBold'),
                  ),
                  SizedBox(
                    height:
                        screenHeight(context, dividedBy: widget.textPadding),
                  ),
                  Text(
                    widget.phoneNumber,
                    style: TextStyle(
                        fontSize: widget.fontSizedPhoneNUmber,
                        color: Colors.white,
                        fontFamily: 'OpenSansRegular'),
                  ),
                ],
              ),
              Spacer(
                flex: 14,
              )
            ],
          ),
        ));
  }
}
