import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class AppBarTimeSlot extends StatefulWidget {
  final Function pageNavigation;
  final String pageTitle;
  final bool icon;
  final bool rightIcon;
  final Function onTapRightIcon;
  AppBarTimeSlot(
      {this.pageNavigation,
      this.pageTitle,
      this.icon,
      this.rightIcon,
      this.onTapRightIcon});
  @override
  _AppBarTimeSlotState createState() => _AppBarTimeSlotState();
}

class _AppBarTimeSlotState extends State<AppBarTimeSlot> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: screenHeight(context, dividedBy: 100),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
                onTap: () {
                  widget.pageNavigation();
                },
                child: widget.icon == true
                    ? Container(
                        width: screenWidth(context, dividedBy: 12),
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: screenWidth(context, dividedBy: 30)),
                          child: Icon(
                            Icons.arrow_back,
                            size: screenWidth(context, dividedBy: 13),
                            color: Constants.kitGradients[0],
                          ),
                        ),
                      )
                    : Container(
                        width: screenWidth(context, dividedBy: 12),
                      )),
            Spacer(
              flex: 5,
            ),
            Container(
              width: screenWidth(context, dividedBy: 1.8),
              child: Center(
                child: Text(
                  widget.pageTitle,
                  style: TextStyle(
                      color: Constants.kitGradients[1],
                      fontFamily: 'OswaldBold',
                      fontSize: screenWidth(context, dividedBy: 22)),
                ),
              ),
            ),
            Spacer(
              flex: 4,
            ),
            widget.rightIcon == true
                ? GestureDetector(
                    onTap: () {
                      widget.onTapRightIcon();
                    },
                    child: Container(
                      width: screenWidth(context, dividedBy: 12),
                      child: Center(
                        child: Icon(
                          Icons.close,
                          size: screenWidth(context, dividedBy: 13),
                          color: Constants.kitGradients[0],
                        ),
                      ),
                    ),
                  )
                : Container(
                    width: screenWidth(context, dividedBy: 12),
                  ),
            Spacer(
              flex: 2,
            )
          ],
        ),
      ],
    );
  }
}
