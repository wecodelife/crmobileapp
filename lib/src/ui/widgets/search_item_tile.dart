import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SearchItemTile extends StatefulWidget {
  final bool hasIcon;
  final bool length;
  final bool index;
  final String rating;
  final Function onPressed;
  SearchItemTile(
      {this.hasIcon, this.length, this.index, this.rating, this.onPressed});
  @override
  _SearchItemTileState createState() => _SearchItemTileState();
}

class _SearchItemTileState extends State<SearchItemTile> {
  bool bookmarked = false;
  bool rated = false;
  int count = 827;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        child: Container(
          width: screenWidth(context, dividedBy: 3.5),
          height: screenHeight(context, dividedBy: 4),
          decoration: BoxDecoration(
              color: Constants.kitGradients[5],
              borderRadius: BorderRadius.all(Radius.circular(5))),
          child: Column(
            children: [
              Stack(
                children: [
                  Container(
                    width: screenWidth(context, dividedBy: 3.5),
                    height: screenHeight(context, dividedBy: 6),
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/images/dummy_bg.jpg"),
                            fit: BoxFit.fill)),
                    child: Image.network(
                      "https://image.shutterstock.com/image-photo/business-woman-drawing-global"
                      "-structure-260nw-1006041130.jpg",
                      fit: BoxFit.cover,
                    ),
                  ),
                  Column(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 100),
                      ),
                      Row(
                        children: [
                          // Spacer(flex: 1),
                          // SvgPicture.asset("assets/icons/online_icon.svg",
                          //     height: screenWidth(context, dividedBy: 25),
                          //     width: screenWidth(context, dividedBy: 25)),
                          Spacer(flex: 10),
                          GestureDetector(
                              onTap: () {
                                setState(() {
                                  bookmarked = !bookmarked;
                                });
                              },
                              child: Icon(
                                  bookmarked == true
                                      ? Icons.favorite
                                      : Icons.favorite_border_outlined,
                                  color: Constants.kitGradients[1],
                                  size: 20)),
                          Spacer(flex: 1),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              Container(
                width: screenWidth(context, dividedBy: 3.5),
                height: screenHeight(context, dividedBy: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Spacer(
                      flex: 3,
                    ),
                    Text(
                      " Shot Trainer",
                      style: TextStyle(
                          color: Constants.kitGradients[0],
                          fontSize: 15,
                          fontFamily: 'OpenSSansRegular'),
                    ),
                    Spacer(
                      flex: 10,
                    ),
                    Row(
                      children: [
                        Spacer(
                          flex: 1,
                        ),
                        Text(
                          "\u0024299.99",
                          style: TextStyle(
                              color: Constants.kitGradients[0],
                              fontSize: 15,
                              fontFamily: 'OpenSSansRegular'),
                        ),
                        Spacer(
                          flex: 5,
                        ),
                        Container(
                            width: screenWidth(context, dividedBy: 19),
                            height: screenHeight(context, dividedBy: 40),
                            child: SvgPicture.asset(
                                "assets/icons/add_rounded_icon.svg")),
                        Spacer(
                          flex: 1,
                        ),
                      ],
                    ),
                    Spacer(
                      flex: 2,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
