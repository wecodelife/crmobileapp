import 'dart:async';

import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GoogleMaps extends StatefulWidget {
  @override
  _GoogleMapsState createState() => _GoogleMapsState();
}

class _GoogleMapsState extends State<GoogleMaps> {
  Completer<GoogleMapController> _controller = Completer();

  final Set<Marker> _markers = {};
  LatLng lastLocation;
  Position hotelLocation;

  LatLng _center = LatLng(9.9312, 76.2673);
  @override
  void initState() {
    setState(() {
      _markers.add(Marker(
        // This marker id can be anything that uniquely identifies each marker.
        markerId: MarkerId(_center.toString()),
        position: _center,
      ));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: screenHeight(context, dividedBy: 3.3),
          width: screenWidth(context, dividedBy: 1),
          color: Colors.white,
          child: GoogleMap(
            initialCameraPosition: CameraPosition(
              target: _center,
              zoom: 11.0,
            ),
            markers: _markers,
          ),
        ),
        Container(
          width: screenWidth(context, dividedBy: 1),
          height: screenHeight(context, dividedBy: 19),
          color: Constants.kitGradients[1],
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(
                  top: screenHeight(context, dividedBy: 200),
                  left: screenWidth(context, dividedBy: 20),
                ),
                child: Text(
                  _center.toString(),
                  style:
                      TextStyle(fontSize: 16, color: Constants.kitGradients[0]),
                  overflow: TextOverflow.clip,
                ),
              ),
              GestureDetector(
                child: Padding(
                  padding: EdgeInsets.only(
                      left: screenWidth(context, dividedBy: 20)),
                  child: Text(
                    "Get direction",
                    style: TextStyle(
                        fontSize: 10,
                        color: Constants.kitGradients[1],
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w600),
                    overflow: TextOverflow.clip,
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
