import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class BottomSheetButton extends StatefulWidget {
  final String label;
  final Function onPressed;
  BottomSheetButton({this.label, this.onPressed});
  @override
  _BottomSheetButtonState createState() => _BottomSheetButtonState();
}

class _BottomSheetButtonState extends State<BottomSheetButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 25),
      width: screenWidth(context, dividedBy: 4),
      decoration: BoxDecoration(
          border: Border.all(color: Constants.kitGradients[1]),
          color: widget.label == "Yes"
              ? Constants.kitGradients[6]
              : Constants.kitGradients[1]),
      child: FlatButton(
        onPressed: () {
          widget.onPressed();
        },
        child: Center(
            child: Text(
          widget.label,
          style: TextStyle(
              fontSize: 15,
              fontFamily: "OpenSansRegular",
              color: Constants.kitGradients[0]),
        )),
      ),
    );
  }
}
