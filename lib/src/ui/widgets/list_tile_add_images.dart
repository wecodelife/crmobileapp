import 'dart:io';

import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class ListTileAddImages extends StatefulWidget {
  final File imageName;
  ListTileAddImages({this.imageName});
  @override
  _ListTileAddImagesState createState() => _ListTileAddImagesState();
}

class _ListTileAddImagesState extends State<ListTileAddImages> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: screenWidth(context, dividedBy: 20),
        ),
        ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Container(
            width: screenWidth(context, dividedBy: 3.8),
            height: screenHeight(context, dividedBy: 8),
            color: Colors.white24,
            child: Image.file(
              widget.imageName,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ],
    );
  }
}
