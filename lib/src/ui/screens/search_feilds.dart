import 'package:app_template/src/ui/screens/search_football_teams.dart';
import 'package:app_template/src/ui/widgets/bottom_navigation_bar_widget.dart';
import 'package:app_template/src/ui/widgets/home_facility_main_tile.dart';
import 'package:app_template/src/ui/widgets/home_listview_header.dart';
import 'package:app_template/src/ui/widgets/list_tile_select_sports.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/search_bar.dart';
import 'package:app_template/src/ui/widgets/search_page_search_bar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class SearchFields extends StatefulWidget {
  @override
  _SearchFieldsState createState() => _SearchFieldsState();
}

class _SearchFieldsState extends State<SearchFields> {
  TextEditingController searchTextEditingController =
      new TextEditingController();
  TextEditingController locationTextEditingController =
      new TextEditingController();
  bool onClicked = false;
  List<String> ratings = ["4.5+", "4+", "3+"];
  List<String> groups = ["Camp", "Clinic", "Groups<5", "Teams 5+"];
  List<String> price = ["\u0024", "\u0024\u0024"];
  List<String> gender = ["Male", "Female"];
  List<String> time = [
    "Any",
    "Early AM",
    "Morning",
    "Afternoon",
    "Evening",
    "Late"
  ];
  List<String> week = [
    "Any",
    "Today",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
  ];
  List<String> distance = [
    "Any",
    "<10 Miles",
    "<25 Miles",
    "<50 Miles",
    "<100 Miles"
  ];
  List<String> facilities = ["Any", "Coach Reserves", "My Home"];
  List<bool> color = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];

  List<String> filters = [
    "Over 4.5",
    "Groups",
    "\u0024, \u0024\u0024",
    "Gender",
    "Any Time of Day",
    "Day of Week",
    "Under 10 Miles",
    "Remote",
    "Facilities"
  ];
  List<String> sports = [
    "Basketball",
    "Volleyball",
    "Football",
    "Swimming",
    "Baseball",
    "Hockey",
    "Soccer",
    "Tennis"
  ];
  List<String> sportsIcon = [
    "assets/icons/basketball.svg",
    "assets/icons/volleyball.svg",
    "assets/icons/basket_ball_icon.svg",
    "assets/icons/swimming.svg",
    "assets/icons/baseball.svg",
    "assets/icons/hockey.svg",
    "assets/icons/soccer.svg",
    "assets/icons/tennis.svg"
  ];
  int filterIndex;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      bottomNavigationBar: BottomNavigationWidget(),
      body: SingleChildScrollView(
        child: Container(
          height: screenHeight(context, dividedBy: 1),
          width: screenWidth(context, dividedBy: 1),
          child: Column(
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30)),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Constants.kitGradients[1])),
                  child: SearchBar(
                    hintText: "Type to start Search...",
                    textEditingController: searchTextEditingController,
                    rightIcon: true,
                    onTap: () {
                      push(context, SearchFootballTeams());
                    },
                  ),
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              Container(
                height: screenHeight(context, dividedBy: 7),
                width: screenWidth(context, dividedBy: 1.08),
                child: ListView.builder(
                    itemCount: sportsIcon.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext cntxt, int index) {
                      return ListTileSelectSports(
                        label: sports[index],
                        icon: sportsIcon[index],
                        onValueChanged: (value) {},
                      );
                    }),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30)),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Constants.kitGradients[1])),
                  child: SearchBarSearchPage(
                    textEditingController: locationTextEditingController,
                    rightIcon: true,
                    hintText: "",
                    leftIcon: "assets/icons/location_icon.svg",
                  ),
                ),
              ),
              SizedBox(
                height: 7,
              ),
              Expanded(
                flex: 1,
                child: ListView.builder(
                    itemCount: 3,
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemBuilder: (BuildContext cntxt, int index) {
                      return Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: screenWidth(context, dividedBy: 30)),
                        child: Column(
                          children: [
                            index == 0
                                ? HomeListViewHeader(
                                    title: "Facilities",
                                  )
                                : Container(),
                            index == 0
                                ? SizedBox(
                                    height: 10,
                                  )
                                : Container(),
                            index == 0
                                ? Container(
                                    height:
                                        screenHeight(context, dividedBy: 3.5),
                                    width: screenWidth(context, dividedBy: 1),
                                    child: Image.network(
                                      "https://miro.medium.com/max/4064/1*qYUvh-EtES8dtgKiBRiLsA.png",
                                      fit: BoxFit.cover,
                                    ),
                                  )
                                : Container(),
                            index == 0
                                ? SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 40),
                                  )
                                : Container(),
                            Column(
                              children: [
                                Container(
                                  child: HomeFacilityMainTile(),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Constants.kitGradients[5],
                                  ),
                                ),
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 40),
                                )
                              ],
                            )
                          ],
                        ),
                      );
                    }),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 90),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
