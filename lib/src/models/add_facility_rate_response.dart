// To parse this JSON data, do
//
//     final addFacilityRateResponse = addFacilityRateResponseFromJson(jsonString);

import 'dart:convert';

AddFacilityRateResponse addFacilityRateResponseFromJson(String str) =>
    AddFacilityRateResponse.fromJson(json.decode(str));

String addFacilityRateResponseToJson(AddFacilityRateResponse data) =>
    json.encode(data.toJson());

class AddFacilityRateResponse {
  AddFacilityRateResponse({
    this.message,
    this.data,
    this.status,
  });

  final String message;
  final Data data;
  final int status;

  factory AddFacilityRateResponse.fromJson(Map<String, dynamic> json) =>
      AddFacilityRateResponse(
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "data": data == null ? null : data.toJson(),
        "status": status == null ? null : status,
      };
}

class Data {
  Data({
    this.id,
    this.facility,
    this.leaseRateType,
    this.rateAmount,
    this.feesApplicable,
    this.discountsApplicable,
  });

  final int id;
  final int facility;
  final int leaseRateType;
  final String rateAmount;
  final bool feesApplicable;
  final bool discountsApplicable;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"] == null ? null : json["id"],
        facility: json["facility"] == null ? null : json["facility"],
        leaseRateType:
            json["lease_rate_type"] == null ? null : json["lease_rate_type"],
        rateAmount: json["rate_amount"] == null ? null : json["rate_amount"],
        feesApplicable:
            json["fees_applicable"] == null ? null : json["fees_applicable"],
        discountsApplicable: json["discounts_applicable"] == null
            ? null
            : json["discounts_applicable"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "facility": facility == null ? null : facility,
        "lease_rate_type": leaseRateType == null ? null : leaseRateType,
        "rate_amount": rateAmount == null ? null : rateAmount,
        "fees_applicable": feesApplicable == null ? null : feesApplicable,
        "discounts_applicable":
            discountsApplicable == null ? null : discountsApplicable,
      };
}
