// To parse this JSON data, do
//
//     final addBankingInfoResponse = addBankingInfoResponseFromJson(jsonString);

import 'dart:convert';

AddBankingInfoResponse addBankingInfoResponseFromJson(String str) =>
    AddBankingInfoResponse.fromJson(json.decode(str));

String addBankingInfoResponseToJson(AddBankingInfoResponse data) =>
    json.encode(data.toJson());

class AddBankingInfoResponse {
  AddBankingInfoResponse({
    this.id,
    this.trainer,
    this.bank,
    this.routingNumber,
    this.accountNumber,
    this.debitCardOpted,
  });

  final int id;
  final int trainer;
  final int bank;
  final String routingNumber;
  final String accountNumber;
  final bool debitCardOpted;

  factory AddBankingInfoResponse.fromJson(Map<String, dynamic> json) =>
      AddBankingInfoResponse(
        id: json["id"] == null ? null : json["id"],
        trainer: json["trainer"] == null ? null : json["trainer"],
        bank: json["bank"] == null ? null : json["bank"],
        routingNumber:
            json["routing_number"] == null ? null : json["routing_number"],
        accountNumber:
            json["account_number"] == null ? null : json["account_number"],
        debitCardOpted:
            json["debit_card_opted"] == null ? null : json["debit_card_opted"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "trainer": trainer == null ? null : trainer,
        "bank": bank == null ? null : bank,
        "routing_number": routingNumber == null ? null : routingNumber,
        "account_number": accountNumber == null ? null : accountNumber,
        "debit_card_opted": debitCardOpted == null ? null : debitCardOpted,
      };
}
