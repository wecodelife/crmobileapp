import 'package:app_template/src/models/get_athlete_list_response.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserListTile extends StatefulWidget {
  final Function onPressed;
  final Datum athleteData;
  UserListTile({this.onPressed, this.athleteData});
  @override
  _UserListTileState createState() => _UserListTileState();
}

class _UserListTileState extends State<UserListTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: 20),
        ),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Constants.kitGradients[5],
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(
                vertical: screenHeight(context, dividedBy: 120),
                horizontal: screenWidth(context, dividedBy: 30)),
            child: Row(
              children: [
                CircleAvatar(
                  radius: screenWidth(context, dividedBy: 10),
                  backgroundImage: NetworkImage(widget.athleteData.profPic !=
                          null
                      ? Urls.baseUrl + widget.athleteData.profPic.imageFile
                      : "https://i.pinimg.com/474x/bc/d4/ac/bcd4ac32cc7d3f98b5e54bde37d6b09e.jpg"),
                ),
                Spacer(
                  flex: 1,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.athleteData.name,
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                          fontFamily: 'OpenSansSemiBold'),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 150),
                    ),
                    Text(
                      "+1 (454) 3421 1432",
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                          fontFamily: 'OpenSansRegular'),
                    ),
                  ],
                ),
                Spacer(
                  flex: 10,
                ),
                Icon(
                  Icons.arrow_forward_ios_outlined,
                  color: Colors.white,
                  size: 16,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
