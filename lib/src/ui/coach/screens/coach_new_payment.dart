import 'package:app_template/src/ui/screens/home_screen.dart';
import 'package:app_template/src/ui/widgets/appbar_cr.dart';
import 'package:app_template/src/ui/widgets/bottom_navigation_bar_widget.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/login_text_box.dart';
import 'package:app_template/src/ui/widgets/password_text_feild.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CoachNewPayment extends StatefulWidget {
  @override
  _CoachNewPaymentState createState() => _CoachNewPaymentState();
}

class _CoachNewPaymentState extends State<CoachNewPayment> {
  TextEditingController usernameTextEditingController =
      new TextEditingController();
  TextEditingController passwordTextEditingController =
      new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: BottomNavigationWidget(
        coach: true,
        navigatePageName: HomePage(),
        image: "assets/icons/group_image.png",
      ),
      appBar: AppBar(
        backgroundColor: Colors.black,
        actions: [
          AppBarCr(
            pageTitle: "NEW PAYMENT",
            leftIcon: "assets/icons/back_button.svg",
            noRightIcon: true,
            rightTextYes: false,
            onTapLeftIcon: () {
              pop(context);
            },
          ),
        ],
      ),
      body: Column(
        children: [
          Container(
              height: screenHeight(context, dividedBy: 8),
              child:
                  Center(child: SvgPicture.asset("assets/icons/stripe.svg"))),
          SizedBox(
            height: screenHeight(context, dividedBy: 50),
          ),
          LoginTextBox(
            label: "Email",
            textEditingController: usernameTextEditingController,
          ),
          SizedBox(
            height: 8,
          ),
          PasswordTextField(
            label: "Password",
            textEditingController: passwordTextEditingController,
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 2.2),
          ),
          BuildButton(
            title: "Connect",
            onPressed: () {},
            disabled: false,
          )
        ],
      ),
    );
  }
}
