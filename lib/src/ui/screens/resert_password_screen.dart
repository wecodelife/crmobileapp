import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/newPasswordTextFeild.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class ResetPasswordScreen extends StatefulWidget {
  final String username;
  ResetPasswordScreen({this.username});
  @override
  _ResetPasswordScreenState createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {
  bool loading = false;
  int id;
  bool disabled = true;
  TextEditingController newPasswordTextEditingController =
      new TextEditingController();
  TextEditingController verifyPasswordTextEditingController =
      new TextEditingController();
  TextEditingController confirmationCodeTextEditingController =
      new TextEditingController();

  void passwordEquals() {
    if (newPasswordTextEditingController.text ==
        verifyPasswordTextEditingController.text) {
    } else {
      showToast("The passwords you have entered doesn't match.");
    }
  }

  void didUpdate() {
    if (newPasswordTextEditingController.text != "" &&
        verifyPasswordTextEditingController.text != "" &&
        confirmationCodeTextEditingController.text != "") {
      setState(() {
        disabled = false;
      });
    } else {
      disabled = true;
    }
  }

  Future<void> _resetPassword() async {
    try {
      UpdatePasswordResult res = await Amplify.Auth.confirmPassword(
          username: widget.username,
          newPassword: newPasswordTextEditingController.text,
          confirmationCode: confirmationCodeTextEditingController.text);

      setState(() {
        // print(res.);
      });
    } on AuthException catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: loading == false
          ? SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 20)),
                    child: Column(
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                        TextProgressIndicator(
                          heading: "LET'S GET YOU BACK INTO YOUR ACCOUNT",
                          descriptionText: "Enter in your new password.",
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 4),
                        ),
                        NewPasswordTextFeild(
                          labelText: "New Password *",
                          textEditingController:
                              newPasswordTextEditingController,
                          onValueChanged: (emailValue) {
                            didUpdate();
                            // emailCheck();
                          },
                        ),
                        NewPasswordTextFeild(
                          labelText: "Verify Password *",
                          textEditingController:
                              verifyPasswordTextEditingController,
                          onValueChanged: (emailValue) {
                            didUpdate();
                            // emailCheck();
                          },
                        ),
                        FormFeildUserDetails(
                          onValueChanged: (value) {
                            didUpdate();
                          },
                          textEditingController:
                              confirmationCodeTextEditingController,
                          labelText: "Confirmation Code *",
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 4),
                  ),
                  // BuildButton(
                  //   title: "CANCEL",
                  //   onPressed: () {},
                  //   transparent: true,
                  // ),
                  // SizedBox(
                  //   height: screenHeight(context, dividedBy: 40),
                  // ),
                  BuildButton(
                      title: "SAVE PASSWORD",
                      onPressed: () {
                        passwordEquals();
                        _resetPassword();
                      },
                      disabled: disabled)
                ],
              ),
            )
          : Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }
}
