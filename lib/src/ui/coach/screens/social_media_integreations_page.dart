import 'package:app_template/src/ui/coach/screens/banking_information.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SocialMediaIntegrationPage extends StatefulWidget {
  @override
  _SocialMediaIntegrationPageState createState() =>
      _SocialMediaIntegrationPageState();
}

class _SocialMediaIntegrationPageState
    extends State<SocialMediaIntegrationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[6],
          actions: [OnBoardingAppBar()],
        ),
        body: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 17)),
              child: Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                  TextProgressIndicator(
                    heading: "LETS SETUP SOME INTEGRATIONS",
                    descriptionText:
                        "Set up integrations with your profile to better your experience!",
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                  OnBoardingProgressIndicator(
                    width: 1.3,
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 20),
                  ),
                  SvgPicture.asset(
                    "assets/icons/google_calendar.svg",
                    height: screenHeight(context, dividedBy: 14),
                    width: screenWidth(context, dividedBy: 6),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                  Center(
                    child: Text(
                      "GOOGLE CALENDAR",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'OswaldBold',
                        fontSize: 16,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  SvgPicture.asset(
                    "assets/icons/facebook_icon.svg",
                    height: screenHeight(context, dividedBy: 14),
                    width: screenWidth(context, dividedBy: 6),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                  Center(
                    child: Text(
                      "FACEBOOK",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'OswaldBold',
                        fontSize: 16,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  SvgPicture.asset(
                    "assets/icons/instagram_icon.svg",
                    height: screenHeight(context, dividedBy: 14),
                    width: screenWidth(context, dividedBy: 6),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                  Center(
                    child: Text(
                      "INSTAGRAM",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'OswaldBold',
                        fontSize: 16,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  SvgPicture.asset(
                    "assets/icons/linked_in_icon.svg",
                    height: screenHeight(context, dividedBy: 14),
                    width: screenWidth(context, dividedBy: 6),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                  Center(
                    child: Text(
                      "LINKED IN",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'OswaldBold',
                        fontSize: 16,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 20),
            ),
            BuildButton(
              onPressed: () {
                push(context, BankingInformation());
              },
              title: "I'll Do this Later",
            )
          ],
        ));
  }
}
