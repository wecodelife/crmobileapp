import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/add_reference_request.dart';
import 'package:app_template/src/ui/coach/widgets/white_border_button.dart';
import 'package:app_template/src/ui/widgets/drop_down_cr_app.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/time_slot_app_bar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class BottomSheetAddReferences extends StatefulWidget {
  final Function onClicked;
  final List<String> stateList;
  final List<int> stateListId;
  BottomSheetAddReferences({this.onClicked, this.stateList, this.stateListId});
  @override
  _BottomSheetAddReferencesState createState() =>
      _BottomSheetAddReferencesState();
}

class _BottomSheetAddReferencesState extends State<BottomSheetAddReferences> {
  TextEditingController nameTextEditingController = new TextEditingController();
  TextEditingController numberTextEditingController =
      new TextEditingController();
  TextEditingController addressTextEditingController =
      new TextEditingController();
  TextEditingController buildingTextEditingController =
      new TextEditingController();
  TextEditingController cityTextEditingController = new TextEditingController();
  TextEditingController zipTextEditingController = new TextEditingController();
  String state = "";
  int stateId;
  bool loading = false;
  // List<String> stateList = ["Kerala", "Tamil"];
  List<String> stateList = [];
  List<int> stateListId = [];
  UserBloc userBloc = new UserBloc();
  @override
  void initState() {
    userBloc.addReferenceResponse.listen((event) {
      pop(context);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: screenHeight(context, dividedBy: 60),
          ),
          AppBarTimeSlot(
            pageTitle: "REFERENCES",
            rightIcon: true,
            onTapRightIcon: () {
              pop(context);
            },
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 60),
          ),
          WhiteBorderButton(
            title: "ADD REFERENCES",
            onPressed: () {},
            transparent: true,
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 60),
          ),
          Container(
            height: screenHeight(context, dividedBy: 2.3),
            color: Constants.kitGradients[6],
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 15)),
              child: ListView(
                children: [
                  Container(
                    height: screenHeight(context, dividedBy: 15),
                    child: FormFeildUserDetails(
                      textEditingController: nameTextEditingController,
                      labelText: "Name",
                      onValueChanged: (value) {},
                    ),
                  ),
                  Container(
                    height: screenHeight(context, dividedBy: 15),
                    child: FormFeildUserDetails(
                      textEditingController: numberTextEditingController,
                      labelText: "",
                      onValueChanged: (value) {},
                    ),
                  ),
                  Container(
                    height: screenHeight(context, dividedBy: 15),
                    child: FormFeildUserDetails(
                      textEditingController: addressTextEditingController,
                      labelText: "Address",
                      onValueChanged: (value) {},
                    ),
                  ),
                  Container(
                    height: screenHeight(context, dividedBy: 15),
                    child: FormFeildUserDetails(
                      textEditingController: buildingTextEditingController,
                      labelText: "Apt,Suit,Unit,Building",
                      onValueChanged: (value) {},
                    ),
                  ),
                  Container(
                    height: screenHeight(context, dividedBy: 15),
                    child: FormFeildUserDetails(
                      textEditingController: cityTextEditingController,
                      labelText: "City",
                      onValueChanged: (value) {},
                    ),
                  ),
                  Container(
                    height: screenHeight(context, dividedBy: 12),
                    child: DropDownCrApp(
                      title: "State",
                      dropDownValue: state,
                      onClicked: (value) {
                        setState(() {
                          state = value;
                          stateId = widget
                              .stateListId[widget.stateList.indexOf(value)];
                        });
                        print(stateId.toString());
                      },
                      dropDownList: widget.stateList,
                    ),
                  ),
                  Container(
                    height: screenHeight(context, dividedBy: 15),
                    child: FormFeildUserDetails(
                      textEditingController: zipTextEditingController,
                      labelText: "ZIP",
                      onValueChanged: (value) {},
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          WhiteBorderButton(
            title: "Submit",
            transparent: true,
            onPressed: () {
              if (zipTextEditingController.text != null &&
                  nameTextEditingController != null &&
                  numberTextEditingController != null &&
                  addressTextEditingController != null &&
                  buildingTextEditingController.text != null &&
                  zipTextEditingController.text != null) {
                // pop(context);
                // AddReferenceRequest(
                //     address: addressTextEditingController.text,
                //     name: nameTextEditingController.text,
                //     address2: addressTextEditingController.text,
                //     city: cityTextEditingController.text,
                //     contactNumber: numberTextEditingController.text,
                //     state: 6,
                //     trainer: 18,
                //     zip: int.parse(zipTextEditingController.text));
                // widget.onClicked();
                // setState(() {
                //   loading = true;
                // });
                userBloc.addReference(
                    addReferenceRequest: AddReferenceRequest(
                        address: addressTextEditingController.text,
                        name: nameTextEditingController.text,
                        address2: addressTextEditingController.text,
                        city: cityTextEditingController.text,
                        contactNumber: numberTextEditingController.text,
                        state: 6,
                        trainer: 18,
                        zip: int.parse(zipTextEditingController.text)));
              }
            },
          )
        ],
      ),
      height: screenHeight(context, dividedBy: 1.5),
    );
  }
}
