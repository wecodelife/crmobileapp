import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_sports_response_model.dart';
import 'package:app_template/src/ui/widgets/appbar_cr.dart';
import 'package:app_template/src/ui/widgets/search_bar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class EditSportsPage extends StatefulWidget {
  final List<String> label;
  final List<String> icon;
  final List<int> sportId;
  final int athleteId;
  EditSportsPage({this.label, this.icon, this.sportId, this.athleteId});
  @override
  _EditSportsPageState createState() => _EditSportsPageState();
}

class _EditSportsPageState extends State<EditSportsPage> {
  TextEditingController searchTextEditingController =
      new TextEditingController();
  UserBloc userBloc = UserBloc();
  List<int> sportsList = [];
  // List<String> icons = [
  //   "assets/icons/soccer_icon.svg",
  //   "assets/icons/basket_icon.png",
  //   "assets/icons/soccer_icon.svg",
  //   "assets/icons/basket_icon.png",
  //   "assets/icons/soccer_icon.svg",
  //   "assets/icons/basket_icon.png",
  //   "assets/icons/soccer_icon.svg",
  //   "assets/icons/basket_icon.png",
  // ];
  // List<String> title = [
  //   "Basketball",
  //   "Volleyball",
  //   "Football",
  //   "Swimming",
  //   "Baseball",
  //   "Hockey",
  //   "Soccer",
  //   "Tennis"
  // ];
  @override
  void initState() {
    // TODO: implement initState
    sportsList = widget.sportId;
    super.initState();
    userBloc.getSports();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Constants.kitGradients[3],
          appBar: AppBar(
            leading: Container(),
            actions: [
              AppBarCr(
                pageTitle: "SPORTS",
                rightTextYes: true,
                rightText: "Done",
                onTapRightIcon: () {
                  // push(context, HomePage());
                  print(ObjectFactory().appHive.getParentId().toString());
                  // userBloc.updateSports(
                  //     updateSportsRequest: UpdateSportsRequest(
                  //   sports: sportsList,
                  //   athlete: int.parse(ObjectFactory().appHive.getParentId()),
                  // ));
                  // pop(context);
                },
                leftIcon: "assets/icons/back_button.svg",
              ),
            ],
          ),
          body: Container(
              child: Column(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(8),
                child: SearchBar(
                  textEditingController: searchTextEditingController,
                  hintText: "Start typing to search..",
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 1.5),
                //color:Colors.yellow,
                child: StreamBuilder<GetSportsResponseModel>(
                    stream: userBloc.getSportsResponse,
                    builder: (context, snapshot) {
                      return snapshot.hasData
                          ? GridView.builder(
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 3,
                                      crossAxisSpacing: 10.0,
                                      mainAxisSpacing: 8.0),
                              itemCount: snapshot.data.data.length,
                              itemBuilder: (BuildContext context, int index) {
                                return GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      if (sportsList.contains(
                                          snapshot.data.data[index].id)) {
                                        sportsList.remove(
                                            snapshot.data.data[index].id);
                                      } else {
                                        sportsList
                                            .add(snapshot.data.data[index].id);
                                      }
                                    });
                                  },
                                  child: Container(
                                    // margin: EdgeInsets.symmetric(horizontal: 6, vertical: 5),
                                    width: screenWidth(context, dividedBy: 1),
                                    height:
                                        screenHeight(context, dividedBy: 10),
                                    // height: 300,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Constants.kitGradients[1]),
                                      color: Constants.kitGradients[6],
                                      // color: Colors.blue,
                                      borderRadius: BorderRadius.circular(7),
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          // padding: EdgeInsets.only(bottom: 10),
                                          width: screenWidth(context,
                                              dividedBy: 6),
                                          height: screenHeight(context,
                                              dividedBy: 12),
                                          child: SvgPicture.string(
                                            snapshot.data.data[index].icon,
                                            color: Colors.white,
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                        SizedBox(
                                          height: screenHeight(context,
                                              dividedBy: 50),
                                        ),
                                        Text(
                                          snapshot.data.data[index].name,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              },
                            )
                          : Container(
                              width: screenWidth(context, dividedBy: 1),
                              height: screenHeight(context, dividedBy: 1.5),
                              child: Center(
                                child: CircularProgressIndicator(),
                              ),
                            );
                    }),
              )
            ],
          ))),
    );
  }
}
