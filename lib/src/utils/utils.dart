import 'package:app_template/src/ui/buisness/screens/alert_box.dart';
import 'package:app_template/src/ui/buisness/screens/alert_box_event_cancelled.dart';
import 'package:app_template/src/ui/widgets/cancel_alert_box.dart';
import 'package:app_template/src/ui/widgets/otp_message.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

///it contain common functions
class Utils {}

Size screenSize(BuildContext context) {
  return MediaQuery.of(context).size;
}

double screenHeight(BuildContext context, {double dividedBy = 1}) {
  return screenSize(context).height / dividedBy;
}

double screenWidth(BuildContext context, {double dividedBy = 1}) {
  return screenSize(context).width / dividedBy;
}

Future<dynamic> push(BuildContext context, Widget route) {
  return Navigator.push(
      context, MaterialPageRoute(builder: (context) => route));
}

void pop(BuildContext context) {
  return Navigator.pop(context);
}

// Future<dynamic> pushAndRemoveUntil(
//     BuildContext context, Widget route, bool goBack) {
//   return Navigator.pushAndRemoveUntil(context,
//       MaterialPageRoute(builder: (context) => route), (route) => goBack);
// }

Future<dynamic> pushAndReplacement(BuildContext context, Widget route) {
  return Navigator.pushReplacement(
      context, MaterialPageRoute(builder: (context) => route));
}

///common toast
void showToast(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
  );
}

void cancelAlertBox(
    {context,
    msg,
    text1,
    text2,
    route,
    double insetPadding,
    double contentPadding,
    double titlePadding}) {
  showGeneralDialog(
      context: context,
      barrierDismissible: false,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.black.withOpacity(0.8),
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext buildContext, Animation animation,
          Animation secondaryAnimation) {
        return CancelAlertBox(
          title: msg,
          text1: text1,
          text2: text2,
          route: route,
          contentPadding: contentPadding,
          titlePadding: titlePadding,
          insetPadding: insetPadding,
        );
      });
}

void showAlert(context, String msg) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text(msg),
//        content: new Text("Alert Dialog body"),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("OK"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

void showBookingAlert(
    {context,
    String icon,
    String message,
    Function onPressedNeverMind,
    Function onPressedCancelBooking}) {
  // flutter defined function
  showDialog(
    useSafeArea: false,
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return Dialog(
        insetPadding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: 20),
        ),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        child: new AlertBox(
          icon: icon,
          message: message,
          onPressedCancelBooking: onPressedCancelBooking,
          onPressedNeverMind: onPressedNeverMind,
        ),
        // ],
      );
    },
  );
}

void eventCancelledAlertBox({
  context,
  String icon,
  String price,
  Function onPressed,
}) {
  // flutter defined function
  showDialog(
    useSafeArea: false,
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return Dialog(
          insetPadding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 20),
          ),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          child: new AlertBoxEventCancelled(
            onPressed: onPressed,
            icon: icon,
            price: price,
          )
          // ],
          );
    },
  );
}

void otpAlertBox({context, title, route, stayOnPage, onPressed}) {
  showGeneralDialog(
      context: context,
      barrierDismissible: false,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.transparent,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext buildContext, Animation animation,
          Animation secondaryAnimation) {
        return OtpMessage(
          title: title,
          route: route,
          stayOnPage: stayOnPage,
          onPressed: onPressed,
        );
      });
}

///common toast
void showToastLong(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
  );
}

void showToastConnection(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
  );
}

void showToastCompleted(
  context,
  fToast,
  msg,
) {
  Widget toast = Container(
      height: screenHeight(context, dividedBy: 1.6),
      width: screenWidth(context, dividedBy: 1.2),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(30)),
      child: Column(
        children: [
          Container(
            height: screenHeight(context, dividedBy: 10),
            decoration: BoxDecoration(
                color: Constants.kitGradients[1],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20))),
            child: Center(
                child: Icon(
              Icons.check_circle_outline,
              size: 40,
              color: Colors.white,
            )),
          ),
          Container(
            decoration: BoxDecoration(
                color: Constants.kitGradients[5],
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20))),
            height: screenHeight(context, dividedBy: 10),
            child: Center(
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 15)),
                child: Text(
                  msg,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontFamily: 'OpenSansSemiBold',
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          )
        ],
      ));
  fToast.showToast(
    child: toast,
    gravity: ToastGravity.SNACKBAR,
    toastDuration: Duration(seconds: 2),
  );
}
