import 'package:app_template/src/models/chat_message_model.dart';
import 'package:app_template/src/ui/widgets/bottom_navigation_bar_widget.dart';
import 'package:app_template/src/ui/widgets/chat_screen_textfeild.dart';
import 'package:app_template/src/ui/widgets/new_chat_search_bar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_chat_bubble/bubble_type.dart';
import 'package:flutter_chat_bubble/chat_bubble.dart';
import 'package:flutter_chat_bubble/clippers/chat_bubble_clipper_3.dart';

class NewChat extends StatefulWidget {
  @override
  _NewChatState createState() => _NewChatState();
}

class _NewChatState extends State<NewChat> {
  TextEditingController receiverTextEditingController =
      new TextEditingController();
  List<ChatMessage> messages = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndDocked,
      floatingActionButton: Padding(
        padding: EdgeInsets.only(bottom: screenHeight(context, dividedBy: 20)),
        child: Container(
          child: ChatScreenTextField(
            textEditingController: receiverTextEditingController,
            hintText: "    Type Message...",
          ),
        ),
      ),
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[3],
        toolbarHeight: screenHeight(context, dividedBy: 12),
        centerTitle: true,
        title: Container(
          child: Column(
            children: [
              Text(
                "New Message",
                style: TextStyle(
                    fontSize: 17,
                    color: Constants.kitGradients[1],
                    fontFamily: 'OpenSansRegular'),
              ),
            ],
          ),
        ),
        actions: [
          Row(
            children: [
              Text(
                "Cancel",
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.white,
                    fontFamily: 'OpenSansRegular'),
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 40),
              )
            ],
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationWidget(),
      body: ListView(
        children: [
          Container(
              height: screenHeight(context, dividedBy: 14),
              width: screenWidth(context, dividedBy: 1),
              color: Constants.kitGradients[5],
              child: NewChatSearchBar(
                hintText: "   To:",
                onValueChanged: (value) {},
              )),
          Container(
              height: screenHeight(context, dividedBy: 1),
              child: ListView.builder(
                itemCount: messages.length,
                itemBuilder: (BuildContext cntxt, int index) {
                  print(messages[index].messageType);
                  return messages[index].messageType == "receiver"
                      ? getReceiverView(
                          ChatBubbleClipper3(type: BubbleType.receiverBubble),
                          context)
                      : getSenderView(
                          ChatBubbleClipper3(type: BubbleType.sendBubble),
                          context);
                },
              )),
        ],
      ),
    );
  }

  getSenderView(CustomClipper clipper, BuildContext context) => ChatBubble(
        clipper: clipper,
        alignment: Alignment.topRight,
        margin: EdgeInsets.only(top: 20),
        backGroundColor: Colors.white,
        child: Container(
          constraints: BoxConstraints(
            maxWidth: screenWidth(context, dividedBy: 1.4),
          ),
          child: Text(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            style: TextStyle(color: Colors.black),
          ),
        ),
      );

  getReceiverView(CustomClipper clipper, BuildContext context) => ChatBubble(
        clipper: clipper,
        shadowColor: Constants.kitGradients[5],
        backGroundColor: Constants.kitGradients[5],
        margin: EdgeInsets.only(top: 20),
        child: Container(
          constraints: BoxConstraints(
            maxWidth: screenWidth(context, dividedBy: 1.4),
          ),
          child: Text(
            "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
            style: TextStyle(color: Colors.black),
          ),
        ),
      );
}
