import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CoachDetailsCupertinoTile extends StatefulWidget {
  final int coachCurrentIndex;
  final String imageName;
  final String iconName;
  final double sizedBoxWidth;
  final bool icon1;
  final String iconName1;
  CoachDetailsCupertinoTile({
    this.sizedBoxWidth,
    this.iconName1,
    this.coachCurrentIndex,
    this.icon1,
    this.imageName,
    this.iconName,
  });

  @override
  _CoachDetailsCupertinoTileState createState() =>
      _CoachDetailsCupertinoTileState();
}

class _CoachDetailsCupertinoTileState extends State<CoachDetailsCupertinoTile> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: screenWidth(context, dividedBy: 1),
          height: screenHeight(context, dividedBy: 2.6),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/dummy_bg.jpg"),
                  fit: BoxFit.fill)),
          child: widget.imageName != null
              ? CachedNetworkImage(
                  fit: BoxFit.fill,
                  imageUrl: Urls.baseUrl + widget.imageName,
                  imageBuilder: (context, imageProvider) => Container(
                    width: screenWidth(context, dividedBy: 1),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  placeholder: (context, url) => Center(
                    heightFactor: 1,
                    widthFactor: 1,
                    child: SizedBox(
                      height: 16,
                      width: 16,
                      child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation(Constants.kitGradients[0]),
                        strokeWidth: 2,
                      ),
                    ),
                  ),
                )
              : Image.asset(
                  "assets/images/dummy_bg.jpg",
                  fit: BoxFit.cover,
                ),
        ),
        Container(
          width: screenWidth(context, dividedBy: 1),
          height: screenHeight(context, dividedBy: 2.6),
          // decoration: BoxDecoration(
          //     image: DecorationImage(
          //   image: AssetImage(
          //     // widget.imageName,
          //   ),
          //   fit: BoxFit.fill,
          // )),
          child: Column(
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 40),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 50)),
                child: Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back,
                        size: 25,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      width:
                          screenWidth(context, dividedBy: widget.sizedBoxWidth),
                    ),
                    widget.icon1 == true
                        ? Column(
                            children: [
                              SizedBox(
                                height: screenHeight(context, dividedBy: 80),
                              ),
                              Container(
                                  width: screenWidth(context, dividedBy: 22),
                                  child: SvgPicture.asset(widget.iconName1)),
                            ],
                          )
                        : Container(),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 20),
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 80),
                        ),
                        Container(
                            width: screenWidth(context, dividedBy: 22),
                            child: SvgPicture.asset(widget.iconName)),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        // Positioned(
        //   top: screenHeight(context, dividedBy: 3.2),
        //   left: screenWidth(context, dividedBy: 2.5),
        //   child: Row(
        //     children: [
        //       DotsIndicator(
        //         dotsCount: 3,
        //         position: widget.coachCurrentIndex.toDouble(),
        //         axis: Axis.horizontal,
        //         decorator: DotsDecorator(
        //             activeColor: Constants.kitGradients[1],
        //             color: Colors.white.withOpacity(.40),
        //             shape: CircleBorder(),
        //             size: Size(10, 10)),
        //       ),
        //     ],
        //   ),
        // ),
      ],
    );
  }
}
