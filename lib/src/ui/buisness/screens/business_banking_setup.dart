import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/add_business_banking_info_request.dart';
import 'package:app_template/src/models/get_payment_method_response.dart';
import 'package:app_template/src/ui/buisness/screens/business_finish_page.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/card_bottom_sheet.dart';
import 'package:app_template/src/ui/widgets/drop_down_cr_app.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class BankingSetUp extends StatefulWidget {
  final int facilityId;
  BankingSetUp({this.facilityId});
  @override
  _BankingSetUpState createState() => _BankingSetUpState();
}

class _BankingSetUpState extends State<BankingSetUp> {
  String paymentType;
  String bankName;
  int bankNameId;
  bool loading = true;
  List<int> bankListId = [];
  List<String> bankList = [];
  List<int> selectedCards = [];
  List<String> selectedCardsName = [];
  List<DatumPayment> paymentMethods = [];
  List<bool> cardValue = [false, false, false, false];
  UserBloc userBloc = UserBloc();
  TextEditingController routingNumberTextEditingController =
      new TextEditingController();
  TextEditingController paymentTextEditingController =
      new TextEditingController();
  TextEditingController accountNumberTextEditingController =
      new TextEditingController();
  @override
  void initState() {
    userBloc.getPaymentMethods();
    userBloc.getPaymentMethodResponse.listen((event) {
      setState(() {
        paymentMethods = event.data;
      });
    });
    userBloc.getBanks();
    userBloc.getBankResponse.listen((event) {
      for (int i = 0; i < event.data.length; i++) {
        bankList.add(event.data[i].name);
        bankListId.add(event.data[i].id);
      }
      setState(() {
        loading = false;
      });
    });
    userBloc.addBusinessBankingInfoResponse.listen((event) {
      setState(() {
        loading = false;
      });
      push(context, BusinessFinishPage());
    });
    super.initState();
  }

  void getPaymentValues() {
    for (int i = 0; i < selectedCardsName.length; i++) {
      setState(() {
        paymentTextEditingController.text =
            paymentTextEditingController.text + selectedCardsName[i] + ", ";
      });
    }
  }

  // var _items = cards
  //     .map<MultiSelectItem<String>>((String value) => MultiSelectItem<String>(value, value))
  //     .toList();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[6],
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: loading == true
          ? Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 12)),
              child: ListView(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  TextProgressIndicator(
                    heading: "LETS GET YOUR BANKING  SET UP",
                    descriptionText:
                        "Set up how people can pay you and where payments will be deposited to.",
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  OnBoardingProgressIndicator(
                    width: 1.3,
                    progressIndicator: true,
                  ),
                  // SizedBox(
                  //   height: screenHeight(context, dividedBy: 20),
                  // ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 20),
                  ),
                  // Row(
                  //   children: [
                  //     Text(
                  //       "Accepted Payment Methods",
                  //       style: TextStyle(
                  //           color: Colors.white,
                  //           fontFamily: 'OpenSansRegular',
                  //           fontSize: 16),
                  //     ),
                  //   ],
                  // ),
                  FormFeildUserDetails(
                    textEditingController: paymentTextEditingController,
                    labelText: "Accepted Payment Methods",
                    readOnly: true,
                    onPressed: () {
                      displayBottomSheet(context);
                    },
                    //labelText: "Rate Amount",
                  ),
                  // GestureDetector(
                  //   onTap: () {
                  //     displayBottomSheet(context);
                  //   },
                  //   child: Container(
                  //     height: screenHeight(context, dividedBy: 11),
                  //     color: Constants.kitGradients[1],
                  //     // child: DropDownCrApp(
                  //     //   title: "Select",
                  //     //   dropDownList: ["UPI", "DEBIT CARD", "CREDIT CARD"],
                  //     //   dropDownValue: bankName,
                  //     //   onClicked: (value) {
                  //     //     setState(() {
                  //     //       bankName = value;
                  //     //     });
                  //     //   },
                  //     // ),
                  //   ),
                  // ),
                  // MultiSelectDialogField(
                  //   items: cards
                  //       .map<MultiSelectItem<String>>(
                  //           (String value) => MultiSelectItem<String>(value, value))
                  //       .toList(),
                  //   title: Text("Animals"),
                  //   selectedColor: Colors.blue,
                  //   decoration: BoxDecoration(
                  //     color: Colors.blue.withOpacity(0.1),
                  //     borderRadius: BorderRadius.all(Radius.circular(40)),
                  //     border: Border.all(
                  //       color: Colors.blue,
                  //       width: 2,
                  //     ),
                  //   ),
                  //   buttonIcon: Icon(
                  //     Icons.pets,
                  //     color: Colors.blue,
                  //   ),
                  //   buttonText: Text(
                  //     "Favorite Animals",
                  //     style: TextStyle(
                  //       color: Colors.blue[800],
                  //       fontSize: 16,
                  //     ),
                  //   ),
                  //   onConfirm: (results) {
                  //     //_selectedAnimals = results;
                  //   },
                  // ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  Row(
                    children: [
                      Text(
                        "DEPOSITS CAN BE MADE TO:",
                        style: TextStyle(
                            color: Constants.kitGradients[1],
                            fontFamily: 'OpenSansRegular',
                            fontSize: 16),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  Row(
                    children: [
                      Text(
                        "Bank Name",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'OpenSansRegular',
                            fontSize: 16),
                      ),
                    ],
                  ),
                  Container(
                    height: screenHeight(context, dividedBy: 11),
                    child: DropDownCrApp(
                      title: "Select",
                      dropDownList: bankList,
                      dropDownValue: bankName,
                      onClicked: (value) {
                        setState(() {
                          bankName = value;
                          bankNameId = bankListId[bankList.indexOf(value)];
                        });
                      },
                    ),
                  ),

                  FormFeildUserDetails(
                    labelText: "Routing Numbers",
                    textEditingController: routingNumberTextEditingController,

                    //labelText: "Rate Amount",
                  ),

                  FormFeildUserDetails(
                    textEditingController: accountNumberTextEditingController,
                    labelText: "Account Number",
                    //labelText: "Rate Amount",
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 9),
                  ),
                  BuildButton(
                    title: "Next",
                    disabled: false,
                    onPressed: () {
                      setState(() {
                        loading = true;
                      });
                      userBloc.addBusinessBankingInfo(
                          addBusinessBankingInfo: AddBusinessBankingInfo(
                              routingNumber:
                                  routingNumberTextEditingController.text,
                              accountNumber:
                                  accountNumberTextEditingController.text,
                              bank: bankNameId,
                              facility: widget.facilityId,
                              paymentMethods: selectedCards));
                    },
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                ],
              ),
            ),
    );
  }

  void displayBottomSheet(context) {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return BottomSheetCard(
          height: 2,
          title: "Payment Methods",
          label: paymentMethods,
          onClicked: (value) {
            setState(() {
              selectedCards = value.id;
              selectedCardsName = value.name;
              getPaymentValues();
              // cards[0] = value;
            });
            print(selectedCards.toString());
          },
        );
      },
    );
  }
}
