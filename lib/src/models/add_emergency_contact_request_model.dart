// To parse this JSON data, do
//
//     final addEmergencyContactRequestModel = addEmergencyContactRequestModelFromJson(jsonString);

import 'dart:convert';

List<AddEmergencyContactRequestModel> addEmergencyContactRequestModelFromJson(
        String str) =>
    List<AddEmergencyContactRequestModel>.from(json
        .decode(str)
        .map((x) => AddEmergencyContactRequestModel.fromJson(x)));

String addEmergencyContactRequestModelToJson(
        List<AddEmergencyContactRequestModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AddEmergencyContactRequestModel {
  AddEmergencyContactRequestModel({
    this.parent,
    this.email,
    this.phoneNumber,
  });

  final int parent;
  final String email;
  final String phoneNumber;

  factory AddEmergencyContactRequestModel.fromJson(Map<String, dynamic> json) =>
      AddEmergencyContactRequestModel(
        parent: json["parent"] == null ? null : json["parent"],
        email: json["email"] == null ? null : json["email"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
      );

  Map<String, dynamic> toJson() => {
        "parent": parent == null ? null : parent,
        "email": email == null ? null : email,
        "phone_number": phoneNumber == null ? null : phoneNumber,
      };
}
