import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class FormFeildPassword extends StatefulWidget {
  String labelText;
  bool readOnly;
  Function onPressed;
  ValueChanged onChanged;
  TextEditingController textEditingController;
  FormFeildPassword({this.labelText, this.textEditingController,this.onPressed,this.readOnly,this.onChanged});
  @override
  _FormFeildPasswordState createState() => _FormFeildPasswordState();
}

class _FormFeildPasswordState extends State<FormFeildPassword> {
  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged:(val){
        widget.onChanged(val);

      },
      obscuringCharacter: '*',
      obscureText: true,
      controller: widget.textEditingController,
      autofocus: false,
      readOnly: widget.readOnly ?? false,
      style: TextStyle(
        color: Constants.kitGradients[0],
        fontFamily: 'MuliSemiBold',
        fontSize: 16,),
      cursorColor: Constants.kitGradients[0],
      decoration: InputDecoration(
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Constants.kitGradients[0]),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Constants.kitGradients[0]),
        ),
        labelText: widget.labelText,
        labelStyle: TextStyle(
          color: Constants.kitGradients[0],
          fontFamily: 'MuliSemiBold',
          fontSize: 16,
        ),
      ),
    );
  }
}
