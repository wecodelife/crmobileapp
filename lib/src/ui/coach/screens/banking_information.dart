import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/add_banking_info_request.dart';
import 'package:app_template/src/ui/screens/on_boarding_athlete_finished_page.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/check_box.dart';
import 'package:app_template/src/ui/widgets/drop_down_cr_app.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BankingInformation extends StatefulWidget {
  @override
  _BankingInformationState createState() => _BankingInformationState();
}

class _BankingInformationState extends State<BankingInformation> {
  String dropDownValue = "";
  int dropDownValueId;
  List<String> dropDownList = [];
  List<int> dropDownListId = [];
  TextEditingController routingTextEditingController =
      new TextEditingController();
  TextEditingController accNumberTextEditingController =
      new TextEditingController();
  bool disabled = true;
  UserBloc userBloc = new UserBloc();
  bool checked = false;
  bool loading = true;
  void didUpdate() {
    if (dropDownValue != "" &&
        routingTextEditingController.text != null &&
        accNumberTextEditingController.text != null &&
        checked == true) {
      setState(() {
        disabled = false;
      });
    } else {
      setState(() {
        disabled = true;
      });
    }
  }

  @override
  void initState() {
    // dropDownList = ["SBI", "SIB"];
    userBloc.getBanks();
    userBloc.getBankResponse.listen((event) {
      for (int i = 0; i < event.data.length; i++) {
        dropDownList.add(event.data[i].name);
        dropDownListId.add(event.data[i].id);
      }
      setState(() {
        loading = false;
      });
    });
    userBloc.addBankingInfoResponse.listen((event) {
      setState(() {
        loading = false;
      });
      push(context, OnBoardingAthleteFinished());
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[6],
          actions: [
            OnBoardingAppBar(),
          ],
        ),
        body: loading == true
            ? Container(
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 1),
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              )
            : Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 12)),
                    child: Column(
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 60),
                        ),
                        TextProgressIndicator(
                          heading: "LETS SET UP YOUR BANKING INFORMATION",
                          descriptionText:
                              "Set up your banking information so you can get paid",
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 60),
                        ),
                        OnBoardingProgressIndicator(
                          width: 1.3,
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                        Row(
                          children: [
                            Text(
                              "Name",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'OpenSansRegular',
                                  fontSize: 14),
                            ),
                          ],
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 1),
                          height: screenHeight(context, dividedBy: 10),
                          child: DropDownCrApp(
                            title: "Select",
                            dropDownValue: dropDownValue,
                            dropDownList: dropDownList,
                            onClicked: (value) {
                              setState(() {
                                didUpdate();
                                dropDownValue = value;
                                dropDownValueId =
                                    dropDownListId[dropDownList.indexOf(value)];
                              });
                            },
                          ),
                        ),
                        FormFeildUserDetails(
                          labelText: "Routing Number",
                          textEditingController: routingTextEditingController,
                          onValueChanged: (value) {
                            didUpdate();
                          },
                        ),
                        FormFeildUserDetails(
                          labelText: "Account Number",
                          textEditingController: accNumberTextEditingController,
                          onValueChanged: (value) {
                            didUpdate();
                          },
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 40),
                        ),
                        Row(
                          children: [
                            CheckBox(
                              checked: checked,
                              onTap: () {
                                setState(() {
                                  checked = !checked;
                                  didUpdate();
                                });
                              },
                            ),
                            SizedBox(
                              width: screenWidth(context, dividedBy: 50),
                            ),
                            RichText(
                              text: TextSpan(
                                text: 'Send me a Debit Card ',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 13,
                                    fontFamily: "OpenSansRegular"),
                                children: <TextSpan>[
                                  TextSpan(
                                      text: "",
                                      style: TextStyle(
                                          fontSize: 13,
                                          fontFamily: "OpenSansRegular",
                                          color: Colors.white,
                                          decoration:
                                              TextDecoration.underline)),
                                  TextSpan(
                                      text: "",
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontFamily: "OpenSansRegular",
                                          color: Colors.white)),
                                ],
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 3.7),
                  ),
                  BuildButton(
                    title: "NEXT",
                    onPressed: () {
                      setState(() {
                        loading = true;
                      });
                      userBloc.addBankingInfo(
                          addBankingInfoRequest: AddBankingInfoRequest(
                              trainer: ObjectFactory().appHive.getCoachId(),
                              accountNumber:
                                  accNumberTextEditingController.text,
                              debitCardOpted: checked,
                              bank: dropDownValueId,
                              routingNumber:
                                  routingTextEditingController.text));
                    },
                    disabled: disabled,
                  )
                ],
              ));
  }
}
