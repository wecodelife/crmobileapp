// To parse this JSON data, do
//
//     final getTimeSlotsResponse = getTimeSlotsResponseFromJson(jsonString);

import 'dart:convert';

GetTimeSlotsResponse getTimeSlotsResponseFromJson(String str) =>
    GetTimeSlotsResponse.fromJson(json.decode(str));

String getTimeSlotsResponseToJson(GetTimeSlotsResponse data) =>
    json.encode(data.toJson());

class GetTimeSlotsResponse {
  GetTimeSlotsResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final Data data;

  factory GetTimeSlotsResponse.fromJson(Map<String, dynamic> json) =>
      GetTimeSlotsResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
        "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    this.date,
    this.availableSlots,
  });

  final String date;
  final List<String> availableSlots;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        date: json["date"] == null ? null : json["date"],
        availableSlots: json["available_slots"] == null
            ? null
            : List<String>.from(json["available_slots"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "date": date == null ? null : date,
        "available_slots": availableSlots == null
            ? null
            : List<dynamic>.from(availableSlots.map((x) => x)),
      };
}
