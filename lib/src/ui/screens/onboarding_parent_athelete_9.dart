import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/add_emergency_contact_request_model.dart';
import 'package:app_template/src/ui/screens/onboaring_parent_athlete_8.dart';
import 'package:app_template/src/ui/widgets/add_phone_number_button.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class OnBoardingParentAthlete9 extends StatefulWidget {
  final List<AddEmergencyContactRequestModel> emergencyContactList;
  final int parentId;
  OnBoardingParentAthlete9({this.emergencyContactList, this.parentId});
  @override
  _OnBoardingParentAthlete9State createState() =>
      _OnBoardingParentAthlete9State();
}

class _OnBoardingParentAthlete9State extends State<OnBoardingParentAthlete9> {
  TextEditingController emailTexEditingController = new TextEditingController();
  TextEditingController phoneNumberTexEditingController =
      new TextEditingController();
  List<AddEmergencyContactRequestModel> emergencyContactList = [];
  bool disabled = true;
  bool reload = false;
  bool loading = false;
  UserBloc userBloc = UserBloc();
  @override
  void initState() {
    super.initState();
    print(widget.parentId.toString() + " id");
    if (widget.emergencyContactList != null) {
      emergencyContactList = widget.emergencyContactList;
    }
    userBloc.addEmergencyContactResponse.listen((event) {
      setState(() {
        loading = false;
      });
      if (event.status == 200 || event.status == 201) {
        reload == true
            ? push(context, OnBoardingParentAthlete9(parentId: widget.parentId))
            : push(
                context,
                OnBoardingParentAthlete8(
                  parentId: widget.parentId,
                ));
      }
    });
  }

  Future addToList() async {
    emergencyContactList.add(AddEmergencyContactRequestModel(
        phoneNumber: phoneNumberTexEditingController.text,
        email: emailTexEditingController.text,
        parent: widget.parentId));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: loading == true
          ? Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 12)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 40),
                      ),
                      TextProgressIndicator(
                        heading: "ADD SOME EMERGENCY CONTACT",
                        descriptionText:
                            "Set up emergency contacts incase anything happens, coaches will know who to call.",
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      OnBoardingProgressIndicator(
                        width: 2.9,
                      ),
                      Column(
                        children: [
                          SizedBox(
                            height: screenHeight(context, dividedBy: 40),
                          ),
                          FormFeildUserDetails(
                            textEditingController: emailTexEditingController,
                            labelText: "Their Email",
                            onValueChanged: (email) {},
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 40),
                          ),
                          FormFeildUserDetails(
                            textEditingController:
                                phoneNumberTexEditingController,
                            labelText: "Their PhoneNumber",
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 40),
                          ),
                          AddPhoneNumberButton(onPressed: () {
                            setState(() {
                              loading = true;
                              reload = true;
                            });
                            userBloc.addEmergencyContact(
                                addEmergencyContactRequestModel:
                                    AddEmergencyContactRequestModel(
                                        parent: widget.parentId,
                                        email: emailTexEditingController.text,
                                        phoneNumber:
                                            phoneNumberTexEditingController
                                                .text));
                          })
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 2.8),
                      ),
                      BuildButton(
                        title: "NEXT",
                        disabled: false,
                        onPressed: () {
                          setState(() {
                            loading = true;
                            reload = false;
                          });
                          userBloc.addEmergencyContact(
                              addEmergencyContactRequestModel:
                                  AddEmergencyContactRequestModel(
                                      parent: widget.parentId,
                                      email: emailTexEditingController.text,
                                      phoneNumber:
                                          phoneNumberTexEditingController
                                              .text));
                        },
                      )
                    ],
                  ),
                )
              ],
            ),
    );
  }
}
