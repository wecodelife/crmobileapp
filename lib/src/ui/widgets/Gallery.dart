import 'dart:io';
import 'dart:typed_data';

import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:photo_manager/photo_manager.dart';

class InvertedCircleClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    return new Path()
      ..addOval(new Rect.fromCircle(
          center: new Offset(size.width / 2, size.height / 2),
          radius: size.height * 0.5))
      ..addRect(new Rect.fromLTWH(0.0, 0.0, size.width, size.height))
      ..fillType = PathFillType.evenOdd;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class Gallery extends StatefulWidget {
  final Widget title;
  final Widget footer;
  ValueChanged image;

  Gallery({this.title, this.footer, this.image});
  @override
  _GalleryState createState() => _GalleryState();
}

class _GalleryState extends State<Gallery> {
  // This will hold all the assets we fetched
  List<AssetEntity> assets = [];
  AssetEntity selectedAsset;
  List<bool> selected = [];
  File imageSelected;

  @override
  void initState() {
    _fetchAssets();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: screenHeight(context, dividedBy: 3),
          child: Stack(children: <Widget>[
            selectedAsset != null
                ? FutureBuilder<File>(
                    future: selectedAsset.originFile,
                    builder: (_, snapshot) {
                      final bytes = snapshot.data;
                      // If we have no data, display a spinner
                      if (bytes == null) return CircularProgressIndicator();
                      // If there's data, display it as an image
                      return Stack(
                        children: [
                          // Wrap the image in a Positioned.fill to fill the space
                          Positioned.fill(
                            child: Container(
                                child: Image.file(
                              bytes,
                              filterQuality: FilterQuality.high,
                              fit: BoxFit.cover,
                            )),
                          ),

                          // Display a Play icon if the asset is a video
                        ],
                      );
                    },
                  )
                : Container(),
            ClipPath(
              clipper: new InvertedCircleClipper(),
              child: new Container(
                color: new Color.fromRGBO(0, 0, 0, 0.3),
              ),
            ),
          ]),
        ),
        widget.title != null ? widget.title : Container(),
        Container(
          width: MediaQuery.of(context).size.width,
          height: screenHeight(context, dividedBy: 2.5),
          child: GridView.builder(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              // A grid view with 3 items per row
              crossAxisCount: 4,
            ),
            itemCount: assets.length,
            itemBuilder: (_, index) {
              return assetThumbnail(context, assets[index], index);
            },
          ),
        ),
        widget.footer != null ? widget.footer : Container(),
      ],
    );
  }

  Widget assetThumbnail(BuildContext context, AssetEntity asset, int index) {
    // We're using a FutureBuilder since thumbData is a future
    return FutureBuilder<Uint8List>(
      future: asset.thumbData,
      builder: (_, snapshot) {
        final bytes = snapshot.data;
        // If we have no data, display a spinner
        if (bytes == null) return CircularProgressIndicator();
        // If there's data, display it as an image
        return Stack(
          children: [
            // Wrap the image in a Positioned.fill to fill the space
            Positioned.fill(
              child: GestureDetector(
                  child: selected[index] == false
                      ? Container(
                          child: Image.memory(
                          bytes,
                          fit: BoxFit.contain,
                          filterQuality: FilterQuality.none,
                        ))
                      : Container(
                          child: Image.memory(
                          bytes,
                          fit: BoxFit.cover,
                          filterQuality: FilterQuality.none,
                          color: Colors.blue.withOpacity(.5),
                          colorBlendMode: BlendMode.darken,
                          width: 100,
                          height: 100,
                        )),
                  onTap: () async {
                    if (index != -1) {
                      imageSelected = await asset.file;
                      for (int i = 0; i < selected.length; i++) {
                        setState(() {
                          selected[i] = false;
                        });
                      }
                      setState(() {
                        selectedAsset = asset;
                        selected[index] = true;
                      });
                      print(imageSelected);
                      widget.image(imageSelected);
                    }
                  }),
            ),
            // Display a Play icon if the asset is a video
            if (asset.type == AssetType.video)
              Center(
                child: Container(
                  color: Colors.blue,
                  child: Icon(
                    Icons.play_arrow,
                    color: Colors.white,
                  ),
                ),
              ),
          ],
        );
      },
    );
  }

  _fetchAssets() async {
    // Set onlyAll to true, to fetch only the 'Recent' album
    // which contains all the photos/videos in the storage
    final albums = await PhotoManager.getAssetPathList(
      onlyAll: true,
    );
    final recentAlbum = albums.first;

    // Now that we got the album, fetch all the assets it contains
    final recentAssets = await recentAlbum.getAssetListRange(
      start: 0, // start at index 0
      end: 1000000, // end at a very big index (to get all the assets)
    );

    // Update the state and notify UI
    setState(() => assets = recentAssets);
    for (int i = 0; i < assets.length; i++) {
      setState(() {
        selected.add(false);
      });
    }
    setState(() {
      selectedAsset = assets[0];
    });
  }
}

//AssetEntity selectedAsset;
