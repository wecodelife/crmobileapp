// To parse this JSON data, do
//
//     final getLevelOfPlayResponse = getLevelOfPlayResponseFromJson(jsonString);

import 'dart:convert';

GetLevelOfPlayResponse getLevelOfPlayResponseFromJson(String str) =>
    GetLevelOfPlayResponse.fromJson(json.decode(str));

String getLevelOfPlayResponseToJson(GetLevelOfPlayResponse data) =>
    json.encode(data.toJson());

class GetLevelOfPlayResponse {
  GetLevelOfPlayResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final List<Datum> data;

  factory GetLevelOfPlayResponse.fromJson(Map<String, dynamic> json) =>
      GetLevelOfPlayResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.value,
    this.order,
  });

  final int id;
  final String value;
  final int order;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        value: json["value"] == null ? null : json["value"],
        order: json["order"] == null ? null : json["order"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "value": value == null ? null : value,
        "order": order == null ? null : order,
      };
}
