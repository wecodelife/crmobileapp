// To parse this JSON data, do
//
//     final getSportsResponseModel = getSportsResponseModelFromJson(jsonString);

import 'dart:convert';

GetSportsResponseModel getSportsResponseModelFromJson(String str) => GetSportsResponseModel.fromJson(json.decode(str));

String getSportsResponseModelToJson(GetSportsResponseModel data) => json.encode(data.toJson());

class GetSportsResponseModel {
  GetSportsResponseModel({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final List<Datum> data;

  factory GetSportsResponseModel.fromJson(Map<String, dynamic> json) => GetSportsResponseModel(
    message: json["message"] == null ? null : json["message"],
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "message": message == null ? null : message,
    "status": status == null ? null : status,
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.name,
    this.icon,
    this.iconFile,
    this.description,
  });

  final int id;
  final bool isActive;
  final bool isDeleted;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String name;
  final String icon;
  final dynamic iconFile;
  final String description;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    name: json["name"] == null ? null : json["name"],
    icon: json["icon"] == null ? null : json["icon"],
    iconFile: json["iconFile"],
    description: json["description"] == null ? null : json["description"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "is_active": isActive == null ? null : isActive,
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "name": name == null ? null : name,
    "icon": icon == null ? null : icon,
    "iconFile": iconFile,
    "description": description == null ? null : description,
  };
}
