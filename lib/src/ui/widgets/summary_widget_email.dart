import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SummaryTileEmail extends StatefulWidget {
  final String heading;
  final String subheading;
  final double width;
  final String image;
  final bool icon;
  SummaryTileEmail(
      {this.heading, this.subheading, this.width, this.image, this.icon});
  @override
  _SummaryTileEmailState createState() => _SummaryTileEmailState();
}

class _SummaryTileEmailState extends State<SummaryTileEmail> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: screenHeight(context, dividedBy: 40),
        ),
        Container(
          width: screenWidth(context, dividedBy: widget.width),
          height: screenHeight(context, dividedBy: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                widget.heading,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                    fontFamily: 'OpenSSansRegular'),
              ),
              Row(
                children: [
                  widget.icon == true
                      ? Row(
                          children: [
                            SizedBox(
                              width: 5,
                            ),
                            Container(
                                height: screenHeight(context, dividedBy: 40),
                                width: screenWidth(context, dividedBy: 20),
                                child: SvgPicture.asset(widget.image)),
                            SizedBox(
                              width: 4,
                            )
                          ],
                        )
                      : Container(),
                  Text(
                    widget.subheading,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontFamily: 'OpenSSansRegular'),
                  ),
                ],
              )
            ],
          ),
        ),
      ],
    );
  }
}
