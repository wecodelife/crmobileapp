// To parse this JSON data, do
//
//     final getStatesResponse = getStatesResponseFromJson(jsonString);

import 'dart:convert';

GetStatesResponse getStatesResponseFromJson(String str) =>
    GetStatesResponse.fromJson(json.decode(str));

String getStatesResponseToJson(GetStatesResponse data) =>
    json.encode(data.toJson());

class GetStatesResponse {
  GetStatesResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final List<Datum> data;

  factory GetStatesResponse.fromJson(Map<String, dynamic> json) =>
      GetStatesResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.name,
  });

  final int id;
  final String name;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
      };
}
