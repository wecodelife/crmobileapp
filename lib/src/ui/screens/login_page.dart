import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify.dart';
import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/services/amplifyconfiguration.dart';
import 'package:app_template/src/ui/coach/screens/onboarding_coach%20_%202.dart';
import 'package:app_template/src/ui/screens/cant_login_screen.dart';
import 'package:app_template/src/ui/screens/home_screen.dart';
import 'package:app_template/src/ui/screens/on_boarding_parent_athelete_2.dart';
import 'package:app_template/src/ui/screens/onboarding4.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/login_text_box.dart';
import 'package:app_template/src/ui/widgets/password_text_feild.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController usernameTextEditingController =
      new TextEditingController();
  TextEditingController passwordTextEditingController =
      new TextEditingController();
  String isSignUpComplete = "";
  bool loginNotPossible;
  bool _amplifyConfigured = false;
  bool loading = false;
  UserBloc userBloc = new UserBloc();
  Future<void> _signIn() async {
    print(usernameTextEditingController.text);
    print(passwordTextEditingController.text);
    print(isSignUpComplete);
    try {
      if (isSignUpComplete != "true") {
        SignInResult res = await Amplify.Auth.signIn(
            username: usernameTextEditingController.text,
            password: passwordTextEditingController.text);
      }
      // print(rest.);
      CognitoAuthSession rest = await Amplify.Auth.fetchAuthSession(
          options: CognitoSessionOptions(getAWSCredentials: true));
      setState(() {
        ObjectFactory().appHive.putIsSignIn(rest.isSignedIn.toString());
        ObjectFactory().appHive.putToken(token: rest.userPoolTokens.idToken);
        userBloc.getBasicUserDetails();
        print(rest.isSignedIn.toString());

        print(rest.userPoolTokens.idToken);
      });
    } on AuthException catch (e) {
      // showToast(e.underlyingException);
      // if (e.underlyingException.contains("Incorrect username or password")) {
      //   setState(() {
      //     loading = false;
      //     loginNotPossible = true;
      //   });
      // }
      print("authe" + e.message.toString());
      print("signedin");
    }
  }

  void _configureAmplify() async {
    if (!mounted) return;
    Amplify.addPlugin(AmplifyAuthCognito());
    try {
      await Amplify.configure(amplifyconfig);
    } on AmplifyAlreadyConfiguredException {
      print("Amplify was already configured. Was the app restarted?");
    }
    try {
      setState(() {
        _amplifyConfigured = true;
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    isSignUpComplete = ObjectFactory().appHive.getIsSignIn() != null
        ? ObjectFactory().appHive.getIsSignIn()
        : "false";
    _configureAmplify();
    userBloc.basicUserDetailsResponse.listen((event) {
      setState(() {
        loading = false;
      });
      if (event.isOnBoardingComplete == true) {
        push(
            context,
            HomePage(
              userType: event.userType,
            ));
      } else if (event.userType == 1) {
        push(context, OnBoardingCoach2());
      } else if (event.userType == 2) {
        push(context, OnBoardingParentAthlete2());
      } else if (event.userType == 3) {
        push(context, OnBoardingCoach2());
      }

      ObjectFactory().appHive.putUserId(userId: event.cognitoId);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: loading == true
          ? Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : SingleChildScrollView(
              child: Container(
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 1),
                decoration: BoxDecoration(
                    image: DecorationImage(
                  image: AssetImage('assets/images/login_page_bg.png'),
                  fit: BoxFit.cover,
                )),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 10),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: screenWidth(context, dividedBy: 7.0),
                          height: 1.0,
                          decoration: BoxDecoration(
                              color: Theme.of(context).accentColor,
                              borderRadius: BorderRadius.circular(0.3)),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12.0),
                          child: Container(
                              width: 20.0,
                              height: 20.0,
                              child: SvgPicture.asset(
                                  "assets/icons/whistle_icon.svg")),
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 7.0),
                          height: 1.0,
                          decoration: BoxDecoration(
                              color: Theme.of(context).accentColor,
                              borderRadius: BorderRadius.circular(0.3)),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 100),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'COACHES & REFS',
                          style: TextStyle(
                              fontFamily: 'Delicato',
                              fontSize: 43.0,
                              color: Theme.of(context).accentColor,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 100),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'ESTD.',
                          style: TextStyle(
                              fontFamily: 'SofiaProRegular',
                              fontSize: 9.0,
                              color: Theme.of(context).accentColor,
                              fontWeight: FontWeight.bold),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12.0),
                          child: Container(
                            height: screenHeight(context, dividedBy: 20),
                            width: 1.0,
                            decoration: BoxDecoration(
                                color: Theme.of(context).accentColor,
                                borderRadius: BorderRadius.circular(0.25)),
                          ),
                        ),
                        Text(
                          'LONG LIVE SPORTS',
                          style: TextStyle(
                              fontFamily: 'SofiaProRegular',
                              fontSize: 14.0,
                              color: Theme.of(context).accentColor,
                              fontWeight: FontWeight.bold),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12.0),
                          child: Container(
                            height: screenHeight(context, dividedBy: 20),
                            width: 1.0,
                            decoration: BoxDecoration(
                                color: Theme.of(context).accentColor,
                                borderRadius: BorderRadius.circular(0.25)),
                          ),
                        ),
                        Text(
                          'MMXX',
                          style: TextStyle(
                              fontFamily: 'SofiaProRegular',
                              fontSize: 9.0,
                              color: Theme.of(context).accentColor,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    loginNotPossible == true
                        ? Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal:
                                    screenWidth(context, dividedBy: 20)),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 25),
                                ),
                                Container(
                                  height: screenHeight(context, dividedBy: 9),
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.red),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: screenWidth(context,
                                            dividedBy: 40)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "Your username or password was incorrect.Try again or tap can't login.",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 25),
                                ),
                              ],
                            ),
                          )
                        : SizedBox(
                            height: screenHeight(context, dividedBy: 7),
                          ),
                    LoginTextBox(
                      label: "Username",
                      textEditingController: usernameTextEditingController,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    PasswordTextField(
                      label: "Password",
                      textEditingController: passwordTextEditingController,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: Container(
                        child: GestureDetector(
                          onTap: () {
                            push(context, CantLoginScreen());
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                "Can't Log in?",
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Constants.kitGradients[1]),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 5),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        BuildButton(
                          transparent: true,
                          title: "BECOME A MEMBER",
                          onPressed: () {
                            push(context, OnBoarding4());
                          },
                        )
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 100),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        BuildButton(
                          title: "LOG IN",
                          onPressed: () {
                            setState(() {
                              loading = true;
                            });
                            _signIn();
                          },
                        )
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 20),
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
