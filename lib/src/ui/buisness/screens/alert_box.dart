import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AlertBox extends StatefulWidget {
  String icon;
  String message;
  Function onPressedNeverMind;
  Function onPressedCancelBooking;
  AlertBox(
      {this.icon,
      this.message,
      this.onPressedCancelBooking,
      this.onPressedNeverMind});
  @override
  _AlertBoxState createState() => _AlertBoxState();
}

class _AlertBoxState extends State<AlertBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 1.9),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Constants.kitGradients[3],
        //color:Colors.red,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: screenHeight(context, dividedBy: 6.5),
            width: screenWidth(context, dividedBy: 1),
            padding: EdgeInsets.symmetric(
              vertical: screenHeight(context, dividedBy: 30),
              horizontal: screenWidth(context, dividedBy: 30),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10)),
              color: Color(0xff3F4A40),
            ),
            child: SvgPicture.asset(widget.icon),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 35),
          ),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 15),
            ),
            child: Text(
              widget.message,
              style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'OpenSansRegular',
                  fontSize: 16),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 20),
          ),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
            ),
            child: Column(
              children: [
                BuildButton(
                  title: "Never Mind",
                  transparent: true,
                  onPressed: () {
                    widget.onPressedNeverMind();
                  },
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 37),
                ),
                BuildButton(
                  title: "Cancel Booking",
                  transparent: true,
                  onPressed: () {
                    widget.onPressedCancelBooking();
                  },
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
