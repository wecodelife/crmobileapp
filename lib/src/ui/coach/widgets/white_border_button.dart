import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class WhiteBorderButton extends StatefulWidget {
  final String title;
  final Function onPressed;
  final bool transparent;
  final String icon;
  final bool iconTrue;
  WhiteBorderButton(
      {this.title, this.icon, this.onPressed, this.transparent, this.iconTrue});
  @override
  _WhiteBorderButtonState createState() => _WhiteBorderButtonState();
}

class _WhiteBorderButtonState extends State<WhiteBorderButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1.1),
      height: screenHeight(context, dividedBy: 22),
      child: RaisedButton(
        onPressed: () {
          widget.onPressed();
        },
        color: widget.transparent == true
            ? Colors.transparent
            : Constants.kitGradients[1],
        shape: RoundedRectangleBorder(side: BorderSide(color: Colors.white)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            widget.iconTrue == true
                ? SvgPicture.asset(
                    widget.icon,
                    color: Colors.white,
                  )
                : Container(),
            widget.iconTrue == true
                ? SizedBox(
                    width: screenWidth(context, dividedBy: 20),
                  )
                : Container(),
            Text(
              widget.title,
              style: TextStyle(
                  color:
                      widget.transparent == true ? Colors.white : Colors.white,
                  fontFamily: 'OswaldBold',
                  fontSize: 16),
            ),
          ],
        ),
      ),
    );
  }
}
