import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProfilePageInformationTile extends StatefulWidget {
  final String icon;
  final String title;
  final Function onPressed;
  ProfilePageInformationTile({this.icon, this.title, this.onPressed});

  @override
  _ProfilePageInformationTileState createState() =>
      _ProfilePageInformationTileState();
}

class _ProfilePageInformationTileState
    extends State<ProfilePageInformationTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Container(
        height: screenHeight(context, dividedBy: 18),
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: screenWidth(context, dividedBy: 16),
                height: screenHeight(context, dividedBy: 28),
                child: SvgPicture.asset(
                  widget.icon,
                  color: Colors.white,
                ),
              ),
              Spacer(
                flex: 1,
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.7),
                child: Text(
                  widget.title,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                      fontFamily: 'OpenSansRegular'),
                ),
              ),
              Spacer(
                flex: 3,
              ),
              Icon(
                Icons.arrow_forward_ios_outlined,
                size: 17,
                color: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }
}
