import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class RequestZoomMeeting extends StatefulWidget {
  final Function onPressed;
  RequestZoomMeeting({this.onPressed});
  @override
  _RequestZoomMeetingState createState() => _RequestZoomMeetingState();
}

class _RequestZoomMeetingState extends State<RequestZoomMeeting> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 10),
      width: screenWidth(context, dividedBy: 1),
      color: Constants.kitGradients[6],
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          BuildButton(
            onPressed: () {
              widget.onPressed();
            },
            title: "Request Zoom Meeting",
            disabled: false,
          ),
        ],
      ),
    );
  }
}
