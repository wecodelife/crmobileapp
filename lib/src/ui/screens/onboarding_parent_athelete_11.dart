import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/add_athlete_request_model.dart';
import 'package:app_template/src/models/get_sports_response_model.dart';
import 'package:app_template/src/ui/screens/gal.dart';
import 'package:app_template/src/ui/screens/onboarding_athlete_17.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/drop_down_cr_app.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/list_tile_select_sports.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class OnBoardingParentAthlete11 extends StatefulWidget {
  final int parentId;
  final int imageId;
  final int athleteCount;
  OnBoardingParentAthlete11({this.parentId, this.athleteCount, this.imageId});
  @override
  _OnBoardingParentAthlete11State createState() =>
      _OnBoardingParentAthlete11State();
}

class _OnBoardingParentAthlete11State extends State<OnBoardingParentAthlete11> {
  TextEditingController nameTextEditingController = new TextEditingController();
  TextEditingController addressTextEditingController =
      new TextEditingController();
  TextEditingController yourEmailTextEditingController =
      new TextEditingController();
  TextEditingController phoneNumberTextEditingController =
      new TextEditingController();
  List<String> genderList = ["Male", "Female"];
  List<String> designationList = ["Coach", "Ref"];
  String genderDropDownValue;
  String designationDropDownValue;
  UserBloc userBloc = UserBloc();
  DateTime selectedDate;
  int count = 0;
  int athleteCount = 1;
  int athleteCountDummy = 0;
  bool disabled;
  bool isValid;
  bool onClicked;
  List<int> selectedSports = [];
  List<String> sports = [
    "Basketball",
    "Volleyball",
    "Football",
    "Swimming",
    "Baseball",
    "Hockey",
    "Soccer",
    "Tennis"
  ];
  List<String> sportsIcon = [
    "assets/icons/basketball.svg",
    "assets/icons/volleyball.svg",
    "assets/icons/basket_ball_icon.svg",
    "assets/icons/swimming.svg",
    "assets/icons/baseball.svg",
    "assets/icons/hockey.svg",
    "assets/icons/soccer.svg",
    "assets/icons/tennis.svg"
  ];
  // DateTime currentDate=DateTime.now();
  Future<String> addItems(int value) async {
    if (onClicked == true) {
      incrementCounter();
      setState(() {
        // selectedSports.add(snapshot
        //     .data.data[index].id);
        selectedSports.add(value);
      });
      return "fhfhgf" + selectedSports.toString();
    } else {
      decrementCounter();
      selectedSports.remove(value);
      return "gfh" + selectedSports.toString();
    }
  }

  String formattedDate;
  _selectDate(BuildContext context) async {
    selectedDate = DateTime.now();
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        formattedDate = DateFormat('dd/MM/yyyy').format(selectedDate);
        print(selectedDate);
      });
  }

  didValuesUpdate() {
    if (nameTextEditingController.text != "" &&
        yourEmailTextEditingController.text != "" &&
        addressTextEditingController.text != "" &&
        phoneNumberTextEditingController.text != "" &&
        genderDropDownValue != "" &&
        designationDropDownValue != "" &&
        selectedDate != null &&
        count > 0 &&
        genderDropDownValue != "") {
      if (isValid =
          EmailValidator.validate(yourEmailTextEditingController.text)) {
        setState(() {
          disabled = false;
        });
      } else {
        showToast("Enter a Valid Email.");
      }
    } else {
      showToast("Enter all the Details");
    }
  }

  void incrementCounter() {
    count = count + 1;
  }

  void decrementCounter() {
    count = count - 1;
  }

  @override
  void initState() {
    athleteCount = widget.athleteCount != null ? widget.athleteCount : 0;
    athleteCountDummy = ObjectFactory().appHive.getAthleteCount();
    userBloc.getSports();
    formattedDate = (DateFormat('dd/mm/yyyy').format(DateTime.now()));
    disabled = true;
    print(athleteCount.toString());
    print(athleteCountDummy.toString());
    print(selectedDate);
    userBloc.addAthleteResponse.listen((event) {
      if (event.status == 200 || event.status == 201) {
        setState(() {
          athleteCount = athleteCount + 1;
        });
        athleteCountDummy <= athleteCount
            ? push(context, OnBoardingAthlete17())
            : push(
                context,
                OnBoardingParentAthlete11(
                  parentId: widget.parentId,
                  athleteCount: athleteCount,
                ));
      }
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: ListView(
        children: [
          Container(
            height: screenHeight(context, dividedBy: 1.28),
            child: ListView(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                TextProgressIndicator(
                  heading: "LETS SET UP YOUR " +
                      athleteCountDummy.toString() +
                      " ATHLETES",
                  descriptionText: "We will setup each athlete one at a time.",
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                Row(
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 20),
                    ),
                    OnBoardingProgressIndicator(
                      width: 1.9,
                      progressIndicator: true,
                    ),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 20),
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 20),
                ),
                Center(
                  child: Text(
                    "Add a Profile Picture",
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'OpenSansRegular',
                        fontSize: 14),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    push(
                        context,
                        GalleryScreen(
                          imageKey: "athlete $athleteCount Image",
                        ));
                  },
                  child: Icon(
                    Icons.add_circle,
                    size: 80,
                    color: Theme.of(context).buttonColor,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 12)),
                  child: Column(
                    children: [
                      FormFeildUserDetails(
                        labelText: "Name",
                        textEditingController: nameTextEditingController,
                        onValueChanged: (name) {},
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Row(
                        children: [
                          Text(
                            "Gender",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'OpenSansRegular',
                                fontSize: 16),
                          ),
                        ],
                      ),
                      Container(
                        width: screenWidth(context, dividedBy: 1.1),
                        child: DropDownCrApp(
                          title: "Select",
                          dropDownValue: genderDropDownValue,
                          onClicked: (value) {
                            setState(() {
                              genderDropDownValue = value;
                            });
                          },
                          dropDownList: genderList,
                        ),
                      ),
                      Row(
                        children: [
                          Text(
                            "Date of Birth",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'OpenSansRegular',
                                fontSize: 16),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                formattedDate,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontFamily: 'OpenSansRegular'),
                              ),
                              GestureDetector(
                                  onTap: () {
                                    _selectDate(context);
                                  },
                                  child: Icon(
                                    Icons.calendar_today,
                                    color: Colors.white,
                                  )),
                            ],
                          ),
                          Divider(
                            thickness: 2,
                            color: Colors.white30,
                          )
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      Row(
                        children: [
                          Text(
                            "Select Your Sports",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'OpenSansRegular',
                                fontSize: 16),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      Container(
                        height: screenHeight(context, dividedBy: 5.5),
                        width: screenWidth(context, dividedBy: 1.07),
                        child: StreamBuilder<GetSportsResponseModel>(
                            stream: userBloc.getSportsResponse,
                            builder: (context, snapshot) {
                              return snapshot.hasData
                                  ? ListView.builder(
                                      shrinkWrap: true,
                                      scrollDirection: Axis.horizontal,
                                      itemCount: snapshot.data.data.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        print(snapshot.data.data[index].name);
                                        return ListTileSelectSports(
                                          label: snapshot.data.data[index].name,
                                          icon: snapshot.data.data[index].icon,
                                          onValueChanged: (onClick) async {
                                            onClicked = onClick;
                                            await addItems(
                                                snapshot.data.data[index].id);

                                            print(selectedSports.toString());
                                          },
                                        );
                                      },
                                    )
                                  : Container();
                            }),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 70),
          ),
          Row(
            children: [
              SizedBox(
                width: screenWidth(context, dividedBy: 30),
              ),
              BuildButton(
                title: "NEXT",
                disabled: false,
                onPressed: () {
                  userBloc.addAthlete(
                      addAthleteRequestModel: AddAthleteRequestModel(
                    parent: widget.parentId,
                    sports: selectedSports,
                    gender: genderDropDownValue == "Male" ? 1 : 2,
                    dob: selectedDate,
                    name: nameTextEditingController.text,
                    profPic: ObjectFactory().appHive.getImagePath(
                                key: "athlete $athleteCount Image") !=
                            null
                        ? int.parse(ObjectFactory()
                            .appHive
                            .getImageId("athlete $athleteCount Imageid"))
                        : null,
                  ));
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
