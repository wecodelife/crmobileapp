import 'package:app_template/src/models/add_coach_request.dart';
import 'package:app_template/src/ui/coach/screens/final_credentials_page.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BackgroundInformationPassed extends StatefulWidget {
  final List<TrainerSport> sports;
  final String name;
  final String address;
  final String ssn;
  final DateTime dob;
  BackgroundInformationPassed(
      {this.sports, this.name, this.address, this.dob, this.ssn});
  @override
  _BackgroundInformationPassedState createState() =>
      _BackgroundInformationPassedState();
}

class _BackgroundInformationPassedState
    extends State<BackgroundInformationPassed> {
  bool disabled = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[11],
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 20)),
            child: Column(children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              ),
              TextProgressIndicator(
                heading: "LETS GET SOME BACKGROUND INFORMATION",
                descriptionText:
                    "We are going to need some background information from you.",
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              ),
              OnBoardingProgressIndicator(
                width: 1.6,
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 5),
              ),
              SvgPicture.asset(
                "assets/icons/circle_tick.svg",
                height: screenHeight(context, dividedBy: 7),
                width: screenWidth(context, dividedBy: 6),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              ),
              Center(
                child: Text(
                  "BACKGROUND CHECK PASSED",
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'OswaldBold',
                    fontSize: 16,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ]),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 3.5),
          ),
          BuildButton(
            onPressed: () {
              push(
                  context,
                  FinalCredentialsPage(
                    dob: widget.dob,
                    ssn: widget.ssn,
                    address: widget.address,
                    name: widget.name,
                    sports: widget.sports,
                  ));
            },
            transparent: true,
            title: "NEXT",
          ),
        ],
      ),
    );
  }
}
