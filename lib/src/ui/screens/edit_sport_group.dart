import 'package:app_template/src/ui/widgets/appbar_cr.dart';
import 'package:app_template/src/ui/widgets/search_bar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class EditSportsGroup extends StatefulWidget {
  final List<String> label;
  EditSportsGroup({this.label});
  @override
  _EditSportsGroupState createState() => _EditSportsGroupState();
}

class _EditSportsGroupState extends State<EditSportsGroup> {
  TextEditingController searchTextEditingController =
      new TextEditingController();
  // List <String> icons =[
  //   "assets/icons/soccer_icon.svg",
  //   "assets/icons/basket_icon.png",
  //   "assets/icons/soccer_icon.svg",
  //   "assets/icons/basket_icon.png",
  //   "assets/icons/soccer_icon.svg",
  //   "assets/icons/basket_icon.png",
  //   "assets/icons/soccer_icon.svg",
  //   "assets/icons/basket_icon.png",
  //
  // ];
  // List<String> title = [
  //   "Basketball",
  //   "Volleyball",
  //   "Football",
  //   "Swimming",
  //   "Baseball",
  //   "Hockey",
  //   "Soccer",
  //   "Tennis"
  // ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Constants.kitGradients[3],
          appBar: AppBar(
            leading: Container(),
            actions: [
              AppBarCr(
                pageTitle: "GROUPS",
                rightTextYes: true,
                rightText: "Done",
                onTapRightIcon: () {
                  pop(context);
                },
                leftIcon: "assets/icons/back_button.svg",
              ),
            ],
          ),
          body: Container(
              child: Column(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(8),
                child: SearchBar(
                  textEditingController: searchTextEditingController,
                  hintText: "Start typing to search..",
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 1.5),
                //color:Colors.yellow,
                child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 10.0,
                      mainAxisSpacing: 8.0),
                  itemCount: widget.label.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      // margin: EdgeInsets.symmetric(horizontal: 6, vertical: 5),
                      width: screenWidth(context, dividedBy: 1),
                      height: screenHeight(context, dividedBy: 5),
                      // height: 300,
                      decoration: BoxDecoration(
                        border: Border.all(color: Constants.kitGradients[1]),
                        color: Constants.kitGradients[6],
                        image: DecorationImage(
                          image: AssetImage("assets/images/dummy_bg.jpg"),
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.circular(7),
                      ),
                      child: Stack(
                        children: [
                          Container(
                            width: screenWidth(context, dividedBy: 1),
                            height: screenHeight(context, dividedBy: 5),
                            child: Image.network(
                              "https://image.shutterstock.com/image-photo/business-woman-drawing-global"
                              "-structure-260nw-1006041130.jpg",
                              fit: BoxFit.cover,
                            ),
                          ),
                          Center(
                            child: Text(
                              widget.label[index],
                              style: TextStyle(
                                  fontSize: 12,
                                  color: Constants.kitGradients[0],
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "OpenSansRegular"),
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
              )
            ],
          ))),
    );
  }
}
