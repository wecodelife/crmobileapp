// To parse this JSON data, do
//
//     final addAthleteRequestModel = addAthleteRequestModelFromJson(jsonString);

import 'dart:convert';

AddAthleteRequestModel addAthleteRequestModelFromJson(String str) =>
    AddAthleteRequestModel.fromJson(json.decode(str));

String addAthleteRequestModelToJson(AddAthleteRequestModel data) =>
    json.encode(data.toJson());

class AddAthleteRequestModel {
  AddAthleteRequestModel({
    this.name,
    this.gender,
    this.parent,
    this.sports,
    this.profPic,
    this.dob,
  });

  final String name;
  final int gender;
  final int parent;
  final List<int> sports;
  final dynamic profPic;
  final DateTime dob;

  factory AddAthleteRequestModel.fromJson(Map<String, dynamic> json) =>
      AddAthleteRequestModel(
        name: json["name"] == null ? null : json["name"],
        gender: json["gender"] == null ? null : json["gender"],
        parent: json["parent"] == null ? null : json["parent"],
        sports: json["sports"] == null
            ? null
            : List<int>.from(json["sports"].map((x) => x)),
        profPic: json["prof_pic"],
        dob: json["dob"] == null ? null : DateTime.parse(json["dob"]),
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "gender": gender == null ? null : gender,
        "parent": parent == null ? null : parent,
        "sports":
            sports == null ? null : List<dynamic>.from(sports.map((x) => x)),
        "prof_pic": profPic,
        "dob": dob == null
            ? null
            : "${dob.year.toString().padLeft(4, '0')}-${dob.month.toString().padLeft(2, '0')}-${dob.day.toString().padLeft(2, '0')}",
      };
}
