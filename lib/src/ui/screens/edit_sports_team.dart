import 'package:app_template/src/ui/widgets/appbar_cr.dart';
import 'package:app_template/src/ui/widgets/search_bar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class EditSportsTeam extends StatefulWidget {
  final List<String> label;
  EditSportsTeam({this.label});
  @override
  _EditSportsTeamState createState() => _EditSportsTeamState();
}

class _EditSportsTeamState extends State<EditSportsTeam> {
  TextEditingController searchTextEditingController =
      new TextEditingController();
  // List <String> icons =[
  //   "assets/icons/soccer_icon.svg",
  //   "assets/icons/basket_icon.png",
  //   "assets/icons/soccer_icon.svg",
  //   "assets/icons/basket_icon.png",
  //   "assets/icons/soccer_icon.svg",
  //   "assets/icons/basket_icon.png",
  //   "assets/icons/soccer_icon.svg",
  //   "assets/icons/basket_icon.png",
  //
  // ];
  // List<String> title = [
  //   "Basketball",
  //   "Volleyball",
  //   "Football",
  //   "Swimming",
  //   "Baseball",
  //   "Hockey",
  //   "Soccer",
  //   "Tennis"
  // ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Constants.kitGradients[3],
          appBar: AppBar(
            leading: Container(),
            actions: [
              AppBarCr(
                pageTitle: "LEAGUES",
                rightTextYes: true,
                rightText: "Done",
                onTapRightIcon: () {
                  pop(context);
                },
                leftIcon: "assets/icons/back_button.svg",
              ),
            ],
          ),
          body: Container(
              child: Column(
            children: [
              Container(
                height: 50,
                padding: EdgeInsets.all(8),
                child: SearchBar(
                  textEditingController: searchTextEditingController,
                  hintText: "Start typing to search..",
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 1.5),
                //color:Colors.yellow,
                child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      childAspectRatio: 2 / 3,
                      crossAxisCount: 3,
                      crossAxisSpacing: 10.0,
                      mainAxisSpacing: 8.0),
                  itemCount: widget.label.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      // margin: EdgeInsets.symmetric(horizontal: 6, vertical: 5),
                      width: screenWidth(context, dividedBy: 1),
                      height: screenHeight(context, dividedBy: 1),
                      // height: 300,
                      decoration: BoxDecoration(
                        border: Border.all(color: Constants.kitGradients[1]),
                        color: Constants.kitGradients[6],
                        // color: Colors.blue,
                        borderRadius: BorderRadius.circular(7),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: screenHeight(context, dividedBy: 100),
                          ),
                          Container(
                            // padding: EdgeInsets.only(bottom: 10),
                            width: screenWidth(context, dividedBy: 1),
                            height: screenHeight(context, dividedBy: 8),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                            ),
                            child: Image.asset(
                              "assets/icons/sports_team.png",
                              fit: BoxFit.contain,
                            ),
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 50),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 4),
                            child: Text(
                              widget.label[index],
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ),
              )
            ],
          ))),
    );
  }
}
