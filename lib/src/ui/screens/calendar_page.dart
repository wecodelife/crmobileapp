import 'package:app_template/material/pickers/date_range_picker_dialog.dart';
import 'package:flutter/material.dart';

class CalendarPage extends StatefulWidget {
  @override
  _CalendarPageState createState() => _CalendarPageState();
}

class _CalendarPageState extends State<CalendarPage> {
  Future<Null> selectDateRange(BuildContext context) async {
    DateTimeRange picked = await showDatesRangePicker(
        context: context,
        initialDateRange: DateTimeRange(
          start: DateTime.now().add(Duration(days: 0)),
          end: DateTime.now().add(Duration(days: 1)),
        ),
        firstDate: DateTime.now(),
        lastDate: DateTime(DateTime.now().year + 5),
        helpText: "Select_Date_Range",
        cancelText: "CANCEL",
        confirmText: "OK",
        saveText: "SAVE",
        errorFormatText: "Invalid_format",
        errorInvalidText: "Out_of_range",
        errorInvalidRangeText: "Invalid_range",
        fieldStartHintText: "Start_Date",
        fieldEndLabelText: "End_Date");
    // if (picked != null) {
    //   state.didChange(picked);
    // }
  }

  @override
  void initState() {
    selectDateRange(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
