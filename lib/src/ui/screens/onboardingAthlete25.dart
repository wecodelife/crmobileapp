import 'package:app_template/src/models/add_coach_request.dart';
import 'package:app_template/src/ui/screens/on_boarding_athlete_finished_page.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OnBoardingAthlete25 extends StatefulWidget {
  final List<TrainerSport> sports;
  OnBoardingAthlete25({this.sports});
  @override
  _OnBoardingAthlete25State createState() => _OnBoardingAthlete25State();
}

class _OnBoardingAthlete25State extends State<OnBoardingAthlete25> {
  List<String> logo = [
    "assets/icons/venmo.svg",
    "assets/icons/pay_pal.svg",
    "assets/icons/stripe.svg"
  ];
  List<String> logoTitle = ["VENMO", "PAYPAL", "STRIPE"];
  bool onClick = false;
  bool disabled = true;
  int count;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 20)),
        child: Column(
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            TextProgressIndicator(
              heading: "LETS GET YOUR MONEY SETUP",
              descriptionText:
                  "Connect an account with any of the services below to be able to pay for items & coaches.",
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            OnBoardingProgressIndicator(
              width: 1.2,
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            Container(
              height: screenHeight(context, dividedBy: 1.6),
              child: ListView.builder(
                  itemCount: 3,
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext cntxt, int index) {
                    return GestureDetector(
                      onTap: () {
                        setState(() {
                          count = index;
                          disabled = false;
                        });
                      },
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: screenHeight(context, dividedBy: 80)),
                        child: Container(
                            height: screenHeight(context, dividedBy: 7),
                            width: screenWidth(context, dividedBy: 1.8),
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: count == index
                                      ? Constants.kitGradients[1]
                                      : Colors.transparent),
                              color: Colors.transparent,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset(logo[index]),
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 50),
                                ),
                                Text(
                                  logoTitle[index],
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.white,
                                    fontFamily: 'OswaldSemiBold',
                                  ),
                                )
                              ],
                            )),
                      ),
                    );
                  }),
            ),
            // SizedBox(
            //   height: screenHeight(context, dividedBy: 4),
            // ),
            BuildButton(
              title: "NEXT",
              disabled: disabled,
              onPressed: () {
                // if()
                push(context, OnBoardingAthleteFinished());
                // push(context, StripeCardPage());
              },
            ),

            SizedBox(
              width: screenWidth(context, dividedBy: 30),
            )
          ],
        ),
      ),
    );
  }
}
