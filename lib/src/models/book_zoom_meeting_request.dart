// To parse this JSON data, do
//
//     final bookZoomMeetingRequest = bookZoomMeetingRequestFromJson(jsonString);

import 'dart:convert';

BookZoomMeetingRequest bookZoomMeetingRequestFromJson(String str) =>
    BookZoomMeetingRequest.fromJson(json.decode(str));

String bookZoomMeetingRequestToJson(BookZoomMeetingRequest data) =>
    json.encode(data.toJson());

class BookZoomMeetingRequest {
  BookZoomMeetingRequest({
    this.parentId,
    this.date,
    this.time,
  });

  final int parentId;
  final String date;
  final String time;

  factory BookZoomMeetingRequest.fromJson(Map<String, dynamic> json) =>
      BookZoomMeetingRequest(
        parentId: json["parent_id"] == null ? null : json["parent_id"],
        date: json["date"] == null ? null : json["date"],
        time: json["time"] == null ? null : json["time"],
      );

  Map<String, dynamic> toJson() => {
        "parent_id": parentId == null ? null : parentId,
        "date": date == null ? null : date,
        "time": time == null ? null : time,
      };
}
