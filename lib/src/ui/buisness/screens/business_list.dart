import 'dart:io';

import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/add_facility_request.dart';
import 'package:app_template/src/models/get_facility_type_response.dart';
import 'package:app_template/src/models/upload_image_request_model.dart';
import 'package:app_template/src/ui/buisness/screens/lease_setup_page.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/drop_down_cr_app.dart';
import 'package:app_template/src/ui/widgets/drop_down_facility.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/select_button.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:time_range_picker/time_range_picker.dart';

class BusinessList extends StatefulWidget {
  final List<int> selectedSports;
  final String businessName;
  final String businessInfo;
  final bool employeeAccess;
  final int facilityType;
  BusinessList(
      {this.selectedSports,
      this.businessInfo,
      this.businessName,
      this.employeeAccess,
      this.facilityType});
  @override
  _BusinessListState createState() => _BusinessListState();
}

class _BusinessListState extends State<BusinessList> {
  TextEditingController addressTextEditingController =
      new TextEditingController();
  TextEditingController businessHoursFromTextEditingController =
      new TextEditingController();
  TextEditingController businessHoursToTextEditingController =
      new TextEditingController();
  TimeRange selectedTime;
  UserBloc userBloc = UserBloc();
  String dropDownValue = "";
  List<int> imageList = [];
  Datums dropDownValueFacilityType, facilityTypeSelected;
  String dropDownFacilityType = "";
  String dropDownDesignation = "";
  // TimeOfDay selectedTime;
  final picker = ImagePicker();
  File imagePicked;
  bool loading = false;
  List<String> sports = [
    "Basketball",
    "Volleyball",
    "Football",
    "Swimming",
    "Baseball",
    "Hockey",
    "Soccer",
    "Tennis"
  ];
  List<String> sportsIcon = [
    "assets/icons/basketball.svg",
    "assets/icons/volleyball.svg",
    "assets/icons/basket_ball_icon.svg",
    "assets/icons/swimming.svg",
    "assets/icons/baseball.svg",
    "assets/icons/hockey.svg",
    "assets/icons/soccer.svg",
    "assets/icons/tennis.svg"
  ];
  List<String> facilityType = [
    "Public",
    "Private",
  ];
  // Future<Null> _selectTime(BuildContext context) async {
  //   final TimeOfDay picked = await showTimePicker(
  //     context: context,
  //     initialTime: selectedTime,
  //   );
  //   if (picked != null)
  //     setState(() {
  //       // selectedTime = picked;
  //       // _hour = selectedTime.hour.toString();
  //       // _minute = selectedTime.minute.toString();
  //       // _time = _hour + ' : ' + _minute;
  //       // _timeController.text = _time;
  //       // _timeController.text = formatDate(
  //       //     DateTime(2019, 08, 1, selectedTime.hour, selectedTime.minute),
  //       //     [hh, ':', nn, " ", am]).toString();
  //     });
  // }
  Future getImageGallery() async {
    final pickedFile =
        await picker.getImage(source: ImageSource.gallery, imageQuality: 25);
    if (pickedFile != null) {
      setState(() {
        imagePicked = File(pickedFile.path);
        loading = true;
      });
      userBloc.uploadImage(
          uploadImageRequest: UploadImageRequest(image: imagePicked));
    }
  }

  Future getImageCamera() async {
    final pickedFile =
        await picker.getImage(source: ImageSource.camera, imageQuality: 25);
    if (pickedFile != null) {
      setState(() {
        imagePicked = File(pickedFile.path);
        loading = true;
      });
      userBloc.uploadImage(
          uploadImageRequest: UploadImageRequest(image: imagePicked));
    }
  }

  @override
  void initState() {
    userBloc.getFacilityType();
    userBloc.uploadImageResponse.listen((event) {
      setState(() {
        loading = false;
      });
      userBloc.getFacilityType();
      dropDownValueFacilityType = null;
      if (event.status == 200 || event.status == 201) {
        imageList.add(event.data.id);
      }
      print("ghhgfgf");
    });
    userBloc.addFacilityResponse.listen((event) {
      setState(() {
        loading = false;
      });
      push(
          context,
          LeaseSetupPage(
            facilityId: event.data.id,
          ));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 12)),
        child: ListView(
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),
            TextProgressIndicator(
              heading: "LETS GET SOME BUSINESS LISTING SET UP",
              descriptionText:
                  "Set your address, add photos, hours and other information for your business.",
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),
            OnBoardingProgressIndicator(
              width: 3.5,
              progressIndicator: true,
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            FormFeildUserDetails(
              textEditingController: addressTextEditingController,
              labelText: "Enter Address",
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            Row(
              children: [
                Text(
                  "Facility Type",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'OpenSansRegular',
                      fontSize: 16),
                ),
              ],
            ),
            // DropDownCrApp(
            //   title: "Select",
            //   dropDownList: [
            //     "Community YMCA",
            //   ],
            //   dropDownValue: dropDownFacilityType,
            //   onClicked: (value) {
            //     setState(() {
            //       dropDownFacilityType = value;
            //     });
            //   },
            // ),
            StreamBuilder<GetFacilityTypeResponse>(
                stream: userBloc.getFacilityTypeResponse,
                builder: (context, snapshot) {
                  return snapshot.hasData
                      ? Container(
                          width: screenWidth(context, dividedBy: 1),
                          height: screenHeight(context, dividedBy: 10),
                          child: DropDownCoachRefAppFacility(
                            dropDownList: snapshot.data.data,
                            title: "Select",
                            onClicked: (value) {
                              setState(() {
                                dropDownValueFacilityType = value;
                                // dropDownClicked = true;
                                facilityTypeSelected = value;
                              });
                            },
                            dropDownValue: dropDownValueFacilityType,
                          ))
                      : Container();
                }),
            FormFeildUserDetails(
              textEditingController: businessHoursFromTextEditingController,
              labelText: "Business Hours",
              // hintText: "(time format : 10:30 - 13:30)",
              readOnly: true,
              onPressed: () async {
                TimeRange result = await showTimeRangePicker(
                    context: context,
                    hideTimes: false,
                    // use24HourFormat: true,
                    timeTextStyle: TextStyle(
                        color: Constants.kitGradients[0], fontSize: 24),
                    activeTimeTextStyle: TextStyle(
                        color: Constants.kitGradients[0], fontSize: 24),
                    labelStyle: TextStyle(color: Constants.kitGradients[0]),
                    strokeColor: Constants.kitGradients[1],
                    selectedColor: Constants.kitGradients[1],
                    handlerColor: Constants.kitGradients[1]);
                setState(() {
                  selectedTime = result;
                  businessHoursFromTextEditingController.text =
                      selectedTime.startTime.toString().substring(10, 15) +
                          " - " +
                          selectedTime.endTime.toString().substring(10, 15);
                });
                print(selectedTime.toString());
              },
            ),
            // SizedBox(
            //   height: screenHeight(context, dividedBy: 50),
            // ),
            // FormFeildUserDetails(
            //   textEditingController: businessHoursToTextEditingController,
            //   labelText: "Business Hours To",
            //   hintText: "(time format : 13:30)",
            // ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            // Row(
            //   children: [
            //     Text(
            //       "Available Sports to Practice",
            //       style: TextStyle(
            //           color: Colors.white,
            //           fontFamily: 'OpenSansRegular',
            //           fontSize: 16),
            //     ),
            //   ],
            // ),
            // Container(
            //   height: screenHeight(context, dividedBy: 10),
            //   child: DropDownCrApp(
            //     title: "Select",
            //     dropDownList: sports,
            //     dropDownValue: dropDownValue,
            //     onClicked: (value) {
            //       setState(() {
            //         dropDownValue = value;
            //       });
            //     },
            //   ),
            // ),
            // SizedBox(
            //   height: screenHeight(context, dividedBy: 50),
            // ),
            Row(
              children: [
                Text(
                  "Multiple Courts/Field Designations/Rates",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'OpenSansRegular',
                      fontSize: 16),
                ),
              ],
            ),
            DropDownCrApp(
              title: "Select",
              dropDownList: ["Yes", "No"],
              dropDownValue: dropDownDesignation,
              onClicked: (value) {
                setState(() {
                  dropDownDesignation = value;
                });
              },
            ),
            Text(
              "You have selected ${imageList.length} images",
              style: TextStyle(
                  color: Colors.white, fontFamily: ' OswaldBold', fontSize: 15),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            SelectButton(
              onPressed: () {
                bottomSheetAddPhotos(context);
              },
              title: "Add Photos",
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 100),
            ),
            SelectButton(
              onPressed: () {},
              title: "Medical Liability/Release",
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            BuildButton(
              title: "Next",
              disabled: false,
              onPressed: () {
                // print("asa" + widget.selectedSports.toString());
                // print("asa" + imageList.toString());
                setState(() {
                  loading = true;
                });
                userBloc.addFacility(
                    addFacilityRequest: AddFacilityRequest(
                        name: widget.businessName,
                        address: addressTextEditingController.text,
                        sports: widget.selectedSports,
                        phone: null,
                        images: imageList,
                        facilityType: facilityTypeSelected.id,
                        email: null,
                        availableSports: widget.selectedSports,
                        businessHoursFrom:
                            selectedTime.startTime.toString().substring(10, 15),
                        businessHoursTo:
                            selectedTime.endTime.toString().substring(10, 15),
                        employeeAccess: true,
                        info: widget.businessInfo,
                        lat: null,
                        lng: null,
                        multipleCourtsRates:
                            dropDownDesignation == "Yes" ? true : false));
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
          ],
        ),
      ),
    );
  }

  void bottomSheetAddPhotos(context) {
    showModalBottomSheet<dynamic>(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (BuildContext context, setState) {
            return Container(
                color: Colors.white,
                height: screenHeight(context, dividedBy: 4),
                width: screenWidth(context, dividedBy: 1),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          right: screenWidth(context, dividedBy: 10)),
                      child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Cancel",
                            style: TextStyle(
                              color: Constants.kitGradients[3],
                              fontFamily: "Poppins",
                              fontSize: 12,
                            ),
                          )),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                            onTap: () {
                              getImageCamera();
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.camera,
                              size: 50,
                              color: Constants.kitGradients[3],
                            )),
                        GestureDetector(
                            onTap: () {
                              getImageGallery();
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.image,
                              size: 50,
                              color: Constants.kitGradients[3],
                            )),
                      ],
                    ),
                  ],
                ));
          });
        });
  }
}
