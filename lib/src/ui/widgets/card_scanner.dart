import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class CardScanner extends StatefulWidget {
  final Function onPressed;
  CardScanner({this.onPressed});
  @override
  _CardScannerState createState() => _CardScannerState();
}

class _CardScannerState extends State<CardScanner> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(vertical: screenHeight(context, dividedBy: 40)),
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              widget.onPressed();
            },
            child: Container(
              height: screenHeight(context, dividedBy: 10),
              width: screenWidth(context, dividedBy: 1.2),
              decoration: BoxDecoration(
                color: Colors.transparent,
                border: Border.all(color: Constants.kitGradients[1]),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      width: screenWidth(context, dividedBy: 17),
                      height: screenHeight(context, dividedBy: 20),
                      child: Icon(
                        Icons.camera_alt_rounded,
                        color: Constants.kitGradients[1],
                        size: 20,
                      )),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 80),
                  ),
                  Text(
                    "ScanCard",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                        fontFamily: "OpenSansSemiBold"),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
