// To parse this JSON data, do
//
//     final addBankingInfoRequest = addBankingInfoRequestFromJson(jsonString);

import 'dart:convert';

AddBankingInfoRequest addBankingInfoRequestFromJson(String str) =>
    AddBankingInfoRequest.fromJson(json.decode(str));

String addBankingInfoRequestToJson(AddBankingInfoRequest data) =>
    json.encode(data.toJson());

class AddBankingInfoRequest {
  AddBankingInfoRequest({
    this.trainer,
    this.bank,
    this.routingNumber,
    this.accountNumber,
    this.debitCardOpted,
  });

  final int trainer;
  final int bank;
  final String routingNumber;
  final String accountNumber;
  final bool debitCardOpted;

  factory AddBankingInfoRequest.fromJson(Map<String, dynamic> json) =>
      AddBankingInfoRequest(
        trainer: json["trainer"] == null ? null : json["trainer"],
        bank: json["bank"] == null ? null : json["bank"],
        routingNumber:
            json["routing_number"] == null ? null : json["routing_number"],
        accountNumber:
            json["account_number"] == null ? null : json["account_number"],
        debitCardOpted:
            json["debit_card_opted"] == null ? null : json["debit_card_opted"],
      );

  Map<String, dynamic> toJson() => {
        "trainer": trainer == null ? null : trainer,
        "bank": bank == null ? null : bank,
        "routing_number": routingNumber == null ? null : routingNumber,
        "account_number": accountNumber == null ? null : accountNumber,
        "debit_card_opted": debitCardOpted == null ? null : debitCardOpted,
      };
}
