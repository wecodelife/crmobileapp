import 'package:app_template/src/ui/screens/on_boarding_parent_Athlete_23.dart';
import 'package:app_template/src/ui/screens/on_boarding_parent_athelete_2.dart';
import 'package:app_template/src/ui/screens/on_boarding_parent_athlete_22.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class MyBottomBarDemo extends StatefulWidget {
  @override
  _MyBottomBarDemoState createState() => new _MyBottomBarDemoState();
}

class _MyBottomBarDemoState extends State<MyBottomBarDemo> {
  int _pageIndex = 0;
  PageController _pageController;

  List<Widget> tabPages = [
    OnBoardingParentAthlete2(),
    OnBoardingParentAthlete22(),
    OnBoardingParentAthlete23(),
    OnBoardingParentAthlete23(),
    OnBoardingParentAthlete23(),
  ];

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: _pageIndex);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        height: screenHeight(context, dividedBy: 10),
        child: BottomNavigationBar(
          fixedColor: Constants.kitGradients[0],
          unselectedItemColor: Constants.kitGradients[0],
          currentIndex: _pageIndex,
          onTap: onTabTapped,
          backgroundColor: Colors.black,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: ImageIcon(
                  AssetImage("assets/icons/basket_icon.png"),
                  size: 40,
                  color: Constants.kitGradients[0],
                ),
                label: ""),
            BottomNavigationBarItem(
                icon: ImageIcon(
                  AssetImage("assets/icons/chat_icon.png"),
                  size: 40,
                  color: Constants.kitGradients[0],
                ),
                label: ""),
            BottomNavigationBarItem(
                icon: ImageIcon(
                  AssetImage("assets/icons/magnify_glass_icon.png"),
                  size: 40,
                  color: Constants.kitGradients[0],
                ),
                label: ""),
            BottomNavigationBarItem(
                icon: ImageIcon(
                  AssetImage("assets/icons/schedule_icon.png"),
                  size: 40,
                  color: Constants.kitGradients[0],
                ),
                label: ""),
            BottomNavigationBarItem(
                icon: ImageIcon(
                  AssetImage("assets/icons/profile_icon.png"),
                  size: 40,
                  color: Constants.kitGradients[0],
                ),
                label: ""),
          ],
        ),
      ),
      body: Container(
        color: Colors.blue,
        child: PageView(
          children: tabPages,
          onPageChanged: onPageChanged,
          controller: _pageController,
        ),
      ),
    );
  }

  void onPageChanged(int page) {
    setState(() {
      this._pageIndex = page;
    });
  }

  void onTabTapped(int index) {
    this._pageController.animateToPage(index,
        duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
  }
}
