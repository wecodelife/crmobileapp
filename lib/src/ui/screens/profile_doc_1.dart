import 'dart:io';

import 'package:app_template/src/ui/screens/bottom_sheet_new_document.dart';
import 'package:app_template/src/ui/widgets/appbar_cr.dart';
import 'package:app_template/src/ui/widgets/bottom_navigation_bar_widget.dart';
import 'package:app_template/src/ui/widgets/new_document_list_tile.dart';
import 'package:app_template/src/ui/widgets/search_bar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfileDoc1 extends StatefulWidget {
  @override
  _ProfileDoc1State createState() => _ProfileDoc1State();
}

class _ProfileDoc1State extends State<ProfileDoc1> {
  TextEditingController searchTextEditingController =
      new TextEditingController();
  int count = 1;
  bool filePicked;
  File fileName;

  String files;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          leading: Container(),
          actions: [
            AppBarCr(
              pageTitle: "NEW DOCUMENTS",
              leftIcon: "assets/icons/back_button.svg",
              onTapLeftIcon: () {},
              rightIcon: "assets/icons/icon_plus.svg",
              onTapRightIcon: () {
                showBottomSheet(context);
              },
            )
          ],
        ),
        bottomNavigationBar: BottomNavigationWidget(),
        body: SingleChildScrollView(
          child: Container(
            height: screenHeight(context, dividedBy: 1),
            width: screenWidth(context, dividedBy: 1),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 20)),
              child: Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  SearchBar(
                    textEditingController: searchTextEditingController,
                    hintText: "Start typing to search....",
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 80),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      count == 1
                          ? GestureDetector(
                              onTap: () {
                                setState(() {
                                  count = 2;
                                });
                              },
                              child: Icon(
                                Icons.keyboard_arrow_up,
                                size: 24,
                                color: Colors.white,
                              ),
                            )
                          : GestureDetector(
                              onTap: () {
                                setState(() {
                                  count = 1;
                                });
                              },
                              child: Icon(
                                Icons.keyboard_arrow_down,
                                size: 24,
                                color: Colors.white,
                              ),
                            ),
                      Container(
                        width: screenWidth(context, dividedBy: 3),
                        child: Text(
                          "Release Forums" + "(" + "3" ")",
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'OpenSansSemiBold',
                              fontSize: screenWidth(context, dividedBy: 28)),
                        ),
                      ),
                      Container(
                        width: screenWidth(
                          context,
                          dividedBy: 2,
                        ),
                        height: 2,
                        color: Colors.white,
                      )
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  count == 1
                      ? Expanded(
                          flex: 1,
                          child: ListView.builder(
                              padding: EdgeInsets.zero,
                              itemCount: 2,
                              itemBuilder: (BuildContext context, int index) {
                                return NewDocumentListTile();
                              }),
                        )
                      : Container(),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      count == 2
                          ? GestureDetector(
                              onTap: () {
                                setState(() {
                                  count = 1;
                                });
                              },
                              child: Icon(
                                Icons.keyboard_arrow_up,
                                size: 24,
                                color: Colors.white,
                              ),
                            )
                          : GestureDetector(
                              onTap: () {
                                setState(() {
                                  count = 2;
                                });
                              },
                              child: Icon(
                                Icons.keyboard_arrow_down,
                                size: 24,
                                color: Colors.white,
                              ),
                            ),
                      Text(
                        "Realease Forums" + "(" + "3" ")",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'OpenSansSemiBold',
                            fontSize: 14),
                      ),
                      Container(
                        width: screenWidth(
                          context,
                          dividedBy: 2,
                        ),
                        height: 2,
                        color: Colors.white,
                      )
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  count == 2
                      ? Expanded(
                          flex: 1,
                          child: ListView.builder(
                              padding: EdgeInsets.zero,
                              itemCount: 2,
                              itemBuilder: (BuildContext context, int index) {
                                return NewDocumentListTile();
                              }),
                        )
                      : Container()
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void showBottomSheet(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        enableDrag: true,
        isDismissible: true,
        backgroundColor: Constants.kitGradients[5],
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(10), topLeft: Radius.circular(10)),
            side: BorderSide()),
        context: context,
        builder: (BuildContext context) {
          return new BottomSheetNewDocument(
            onFilePicking: (value) {
              fileName = value;
            },
          );
        });
  }

  // void filePick() async {
  //   FilePickerResult result = await FilePicker.platform.pickFiles();
  //
  //   if (result != null) {
  //     file = File(result.files.single.path);
  //     fileName = file.path;
  //
  //     filePicked = true;
  //   } else {
  //     // User canceled the picker
  //   }
  // }
}
