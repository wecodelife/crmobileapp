// To parse this JSON data, do
//
//     final addFacilityRequest = addFacilityRequestFromJson(jsonString);

import 'dart:convert';

AddFacilityRequest addFacilityRequestFromJson(String str) =>
    AddFacilityRequest.fromJson(json.decode(str));

String addFacilityRequestToJson(AddFacilityRequest data) =>
    json.encode(data.toJson());

class AddFacilityRequest {
  AddFacilityRequest({
    this.name,
    this.info,
    this.employeeAccess,
    this.address,
    this.facilityType,
    this.lat,
    this.lng,
    this.businessHoursFrom,
    this.businessHoursTo,
    this.images,
    this.sports,
    this.email,
    this.phone,
    this.availableSports,
    this.multipleCourtsRates,
  });

  final String name;
  final String info;
  final bool employeeAccess;
  final String address;
  final int facilityType;
  final String lat;
  final String lng;
  final String businessHoursFrom;
  final String businessHoursTo;
  final List<int> images;
  final List<int> sports;
  final String email;
  final String phone;
  final List<int> availableSports;
  final bool multipleCourtsRates;

  factory AddFacilityRequest.fromJson(Map<String, dynamic> json) =>
      AddFacilityRequest(
        name: json["name"] == null ? null : json["name"],
        info: json["info"] == null ? null : json["info"],
        employeeAccess:
            json["employee_access"] == null ? null : json["employee_access"],
        address: json["address"] == null ? null : json["address"],
        facilityType:
            json["facility_type"] == null ? null : json["facility_type"],
        lat: json["lat"] == null ? null : json["lat"],
        lng: json["lng"] == null ? null : json["lng"],
        businessHoursFrom: json["business_hours_from"] == null
            ? null
            : json["business_hours_from"],
        businessHoursTo: json["business_hours_to"] == null
            ? null
            : json["business_hours_to"],
        images: json["images"] == null
            ? null
            : List<int>.from(json["images"].map((x) => x)),
        sports: json["sports"] == null
            ? null
            : List<int>.from(json["sports"].map((x) => x)),
        email: json["email"] == null ? null : json["email"],
        phone: json["phone"] == null ? null : json["phone"],
        availableSports: json["available_sports"] == null
            ? null
            : List<int>.from(json["available_sports"].map((x) => x)),
        multipleCourtsRates: json["multiple_courts_rates"] == null
            ? null
            : json["multiple_courts_rates"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "info": info == null ? null : info,
        "employee_access": employeeAccess == null ? null : employeeAccess,
        "address": address == null ? null : address,
        "facility_type": facilityType == null ? null : facilityType,
        "lat": lat == null ? null : lat,
        "lng": lng == null ? null : lng,
        "business_hours_from":
            businessHoursFrom == null ? null : businessHoursFrom,
        "business_hours_to": businessHoursTo == null ? null : businessHoursTo,
        "images":
            images == null ? null : List<dynamic>.from(images.map((x) => x)),
        "sports":
            sports == null ? null : List<dynamic>.from(sports.map((x) => x)),
        "email": email == null ? null : email,
        "phone": phone == null ? null : phone,
        "available_sports": availableSports == null
            ? null
            : List<dynamic>.from(availableSports.map((x) => x)),
        "multiple_courts_rates":
            multipleCourtsRates == null ? null : multipleCourtsRates,
      };
}
