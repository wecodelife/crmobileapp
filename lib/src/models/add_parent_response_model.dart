// To parse this JSON data, do
//
//     final addParentResponseModel = addParentResponseModelFromJson(jsonString);

import 'dart:convert';

AddParentResponseModel addParentResponseModelFromJson(String str) =>
    AddParentResponseModel.fromJson(json.decode(str));

String addParentResponseModelToJson(AddParentResponseModel data) =>
    json.encode(data.toJson());

class AddParentResponseModel {
  AddParentResponseModel({
    this.message,
    this.data,
    this.status,
  });

  final String message;
  final Data data;
  final int status;

  factory AddParentResponseModel.fromJson(Map<String, dynamic> json) =>
      AddParentResponseModel(
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "data": data == null ? null : data.toJson(),
        "status": status == null ? null : status,
      };
}

class Data {
  Data({
    this.id,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.name,
    this.address,
    this.email,
    this.phoneNumber,
    this.gender,
    this.dob,
    this.isCoachOrRef,
    this.trainingAtHome,
    this.trainerChooseFacilities,
    this.user,
    this.profPic,
    this.sports,
  });

  final int id;
  final bool isActive;
  final bool isDeleted;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String name;
  final String address;
  final String email;
  final String phoneNumber;
  final int gender;
  final DateTime dob;
  final bool isCoachOrRef;
  final dynamic trainingAtHome;
  final dynamic trainerChooseFacilities;
  final String user;
  final dynamic profPic;
  final List<int> sports;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"] == null ? null : json["id"],
        isActive: json["is_active"] == null ? null : json["is_active"],
        isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        name: json["name"] == null ? null : json["name"],
        address: json["address"] == null ? null : json["address"],
        email: json["email"] == null ? null : json["email"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        gender: json["gender"] == null ? null : json["gender"],
        dob: json["dob"] == null ? null : DateTime.parse(json["dob"]),
        isCoachOrRef:
            json["is_coach_or_ref"] == null ? null : json["is_coach_or_ref"],
        trainingAtHome: json["training_at_home"],
        trainerChooseFacilities: json["trainer_choose_facilities"],
        user: json["user"] == null ? null : json["user"],
        profPic: json["prof_pic"],
        sports: json["sports"] == null
            ? null
            : List<int>.from(json["sports"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "is_active": isActive == null ? null : isActive,
        "is_deleted": isDeleted == null ? null : isDeleted,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "name": name == null ? null : name,
        "address": address == null ? null : address,
        "email": email == null ? null : email,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "gender": gender == null ? null : gender,
        "dob": dob == null
            ? null
            : "${dob.year.toString().padLeft(4, '0')}-${dob.month.toString().padLeft(2, '0')}-${dob.day.toString().padLeft(2, '0')}",
        "is_coach_or_ref": isCoachOrRef == null ? null : isCoachOrRef,
        "training_at_home": trainingAtHome,
        "trainer_choose_facilities": trainerChooseFacilities,
        "user": user == null ? null : user,
        "prof_pic": profPic,
        "sports":
            sports == null ? null : List<dynamic>.from(sports.map((x) => x)),
      };
}
