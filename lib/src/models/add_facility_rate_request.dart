// To parse this JSON data, do
//
//     final addFacilityRateRequest = addFacilityRateRequestFromJson(jsonString);

import 'dart:convert';

AddFacilityRateRequest addFacilityRateRequestFromJson(String str) =>
    AddFacilityRateRequest.fromJson(json.decode(str));

String addFacilityRateRequestToJson(AddFacilityRateRequest data) =>
    json.encode(data.toJson());

class AddFacilityRateRequest {
  AddFacilityRateRequest({
    this.facility,
    this.leaseRateType,
    this.rateAmount,
    this.feesApplicable,
    this.discountsApplicable,
  });

  final int facility;
  final int leaseRateType;
  final String rateAmount;
  final bool feesApplicable;
  final bool discountsApplicable;

  factory AddFacilityRateRequest.fromJson(Map<String, dynamic> json) =>
      AddFacilityRateRequest(
        facility: json["facility"] == null ? null : json["facility"],
        leaseRateType:
            json["lease_rate_type"] == null ? null : json["lease_rate_type"],
        rateAmount: json["rate_amount"] == null ? null : json["rate_amount"],
        feesApplicable:
            json["fees_applicable"] == null ? null : json["fees_applicable"],
        discountsApplicable: json["discounts_applicable"] == null
            ? null
            : json["discounts_applicable"],
      );

  Map<String, dynamic> toJson() => {
        "facility": facility == null ? null : facility,
        "lease_rate_type": leaseRateType == null ? null : leaseRateType,
        "rate_amount": rateAmount == null ? null : rateAmount,
        "fees_applicable": feesApplicable == null ? null : feesApplicable,
        "discounts_applicable":
            discountsApplicable == null ? null : discountsApplicable,
      };
}
