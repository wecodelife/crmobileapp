import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/add_authorized_contact_request_model.dart';
import 'package:app_template/src/ui/screens/onboarding_parent_athelete_11.dart';
import 'package:app_template/src/ui/widgets/add_phone_number_button.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/drop_down_cr_app.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class OnBoardingParentAthlete8 extends StatefulWidget {
  final int parentId;
  OnBoardingParentAthlete8({this.parentId});
  @override
  _OnBoardingParentAthlete8State createState() =>
      _OnBoardingParentAthlete8State();
}

class _OnBoardingParentAthlete8State extends State<OnBoardingParentAthlete8> {
  int count;
  List<String> dropdownList = ["OneDay", "TwoDay"];
  TextEditingController emailTextEditingController =
      new TextEditingController();

  String dropDownValue;
  bool disabled = true;
  UserBloc userBloc = UserBloc();
  bool reload = false;
  bool loading = false;

  @override
  void initState() {
    userBloc.addAuthorizedContactResponse.listen((event) {
      setState(() {
        loading = false;
      });
      if (event.status == 200 || event.status == 201) {
        reload == true
            ? push(
                context,
                OnBoardingParentAthlete8(
                  parentId: widget.parentId,
                ))
            : push(
                context,
                OnBoardingParentAthlete11(
                  parentId: widget.parentId,
                ));
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: loading == true
          ? Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 12)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 40),
                      ),
                      TextProgressIndicator(
                        heading: "ADD A SPOUSE OR AUTHORIZED PERSON?",
                        descriptionText:
                            "An email will be be sent to them for them to setup their account.You can skip this step.",
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      OnBoardingProgressIndicator(
                        width: 2.3,
                      ),
                      Column(
                        children: [
                          SizedBox(
                            height: screenHeight(context, dividedBy: 40),
                          ),
                          FormFeildUserDetails(
                            textEditingController: emailTextEditingController,
                            labelText: "Their Email",
                            onValueChanged: (emailValue) {},
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 40),
                          ),
                          Row(
                            children: [
                              Text("Access level",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: ' OswaldBold',
                                      fontSize: 16)),
                            ],
                          ),
                          Container(
                              width: screenWidth(context, dividedBy: 1),
                              height: screenHeight(context, dividedBy: 10),
                              child: DropDownCrApp(
                                title: "Select",
                                onClicked: (value) {
                                  dropDownValue = value;
                                },
                                dropDownList: dropdownList,
                                dropDownValue: dropDownValue,
                              )),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 40),
                          ),
                          AddPhoneNumberButton(
                            onPressed: () {
                              setState(() {
                                loading = true;
                                reload = true;
                              });
                              userBloc.addAuthorizedContact(
                                  addAuthorizedContactRequestModel:
                                      AddAuthorizedContactRequestModel(
                                          email:
                                              emailTextEditingController.text,
                                          parent: widget.parentId,
                                          accessLevel: dropDownValue == "OneDay"
                                              ? 1
                                              : 2));
                            },
                          )
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 3),
                      ),
                      BuildButton(
                        title: "NEXT",
                        disabled: false,
                        onPressed: () {
                          setState(() {
                            loading = true;
                            reload = false;
                          });
                          userBloc.addAuthorizedContact(
                              addAuthorizedContactRequestModel:
                                  AddAuthorizedContactRequestModel(
                                      email: emailTextEditingController.text,
                                      parent: widget.parentId,
                                      accessLevel:
                                          dropDownValue == "OneDay" ? 1 : 2));
                        },
                      )
                    ],
                  ),
                )
              ],
            ),
    );
  }
}
