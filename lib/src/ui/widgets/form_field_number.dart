import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';

class NumberFieldData extends StatefulWidget {
  final String labelText;
  final bool readOnly;
  final ValueChanged onValueChanged;
  final TextEditingController textEditingController;

  NumberFieldData({
    this.labelText,
    this.textEditingController,
    this.onValueChanged,
    this.readOnly,
  });
  @override
  _NumberFieldDataState createState() => _NumberFieldDataState();
}

class _NumberFieldDataState extends State<NumberFieldData> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 12),
      child: TextField(
        keyboardType: TextInputType.number,
        controller: widget.textEditingController,
        autofocus: false,
        readOnly: widget.readOnly ?? false,
        onChanged: widget.onValueChanged,
        style: TextStyle(
          color: Constants.kitGradients[0],
          fontFamily: 'OpenSansRegular',
          fontSize: 16,
        ),
        cursorColor: Constants.kitGradients[0],
        decoration: InputDecoration(
          hintStyle: TextStyle(
              color: Colors.white,
              fontSize: 14,
              fontFamily: 'OpenSansSemiBold'),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Constants.kitGradients[0]),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Constants.kitGradients[0]),
          ),
          labelText: widget.labelText,
          labelStyle: TextStyle(
            color: Constants.kitGradients[0],
            fontFamily: 'OpenSansRegular',
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}
