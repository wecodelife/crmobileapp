import 'package:app_template/src/ui/buisness/screens/facilities2.dart';
import 'package:app_template/src/ui/widgets/appbar_cr.dart';
import 'package:app_template/src/ui/widgets/facility_soccer_filed_tile.dart';
import 'package:app_template/src/ui/widgets/search_bar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class Facilities1Page extends StatefulWidget {
  @override
  _Facilities1PageState createState() => _Facilities1PageState();
}

class _Facilities1PageState extends State<Facilities1Page> {
  TextEditingController searchTextEditingController =
      new TextEditingController();

  List<String> titles = [
    "Basketball Field",
    "Volleyball Field",
    "Football Field",
    "Swimming Field",
    "Baseball Field",
    "Hockey Field",
    "Soccer Field",
    "Tennis Field"
  ];
  List<String> subtitles = [
    "Field A",
    "Field C",
    "Field B",
    "Field D",
    "Field A",
    "Field C",
    "Field B",
    "Field D"
  ];
  List<String> images = [
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png"
  ];
  List<String> icons = [
    "assets/icons/basketball.svg",
    "assets/icons/volleyball.svg",
    "assets/icons/basket_ball_icon.svg",
    "assets/icons/swimming.svg",
    "assets/icons/baseball.svg",
    "assets/icons/hockey.svg",
    "assets/icons/soccer.svg",
    "assets/icons/tennis.svg"
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Constants.kitGradients[3],
        appBar: AppBar(
          leading: Container(),
          actions: [
            AppBarCr(
              leftTextYes: true,
              leftText: "",
              onTapLeftIcon: () {},
              onTapRightIcon: () {
                //push(context, NewChat());
              },
              rightTextYes: false,
              rightIcon: "assets/icons/icon_plus.svg",
              pageTitle: "FACILITIES",
            ),
          ],
        ),
        // appBar: AppBar(
        //   backgroundColor: Constants.kitGradients[11],
        //   elevation: 0,
        //   leading: Container(),
        //   actions: [
        //     Container(
        //       padding: EdgeInsets.symmetric(
        //           horizontal: screenWidth(context, dividedBy: 30)),
        //       child: Row(
        //         crossAxisAlignment: CrossAxisAlignment.center,
        //         children: [
        //           Text(
        //             "FACILITIES",
        //             style: TextStyle(
        //                 color: Constants.kitGradients[0],
        //                 fontFamily: 'OswaldBold',
        //                 fontSize: screenWidth(context, dividedBy: 22)),
        //           ),
        //           SizedBox(
        //             width: screenWidth(context, dividedBy: 3),
        //           ),
        //           GestureDetector(
        //               child: SvgPicture.asset(
        //             "assets/icons/icon_plus.svg",
        //           ))
        //         ],
        //       ),
        //     )
        //   ],
        // ),
        body: Container(
          width: screenWidth(context, dividedBy: 1),
          //color:Colors.red,
          child: ListView(
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30)),
                child: SearchBar(
                  hintText: "Start typing to search..",
                  textEditingController: searchTextEditingController,
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              Container(
                //flex: 4,
                child: ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: titles.length,
                    itemBuilder: (BuildContext context, int index) {
                      return FacilitySoccerFieldTile(
                        title: titles[index],
                        image: images[index],
                        subtitle: subtitles[index],
                        icon: icons[index],
                        hasIcon: true,
                        hasButton: true,
                        pricePerDay: "200",
                        pricePerHour: "40",
                        hasPrice: true,
                        buttonTitle: "Edit",
                        onPressed: () {
                          push(context, Facilities2());
                        },
                      );
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }
}
