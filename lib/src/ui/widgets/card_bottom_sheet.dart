import 'package:app_template/src/models/get_payment_method_response.dart';
import 'package:app_template/src/models/payment_values.dart';
import 'package:app_template/src/ui/widgets/bottom_sheet_item.dart';
import 'package:app_template/src/ui/widgets/select_button.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class BottomSheetCard extends StatefulWidget {
  final List<DatumPayment> label;
  final String title;
  final bool checked;
  final Function onPressed;
  final double height;
  final ValueChanged<PaymentValues> onClicked;
  BottomSheetCard(
      {this.label,
      this.title,
      this.checked,
      this.onPressed,
      this.height,
      this.onClicked});
  @override
  _BottomSheetCardState createState() => _BottomSheetCardState();
}

class _BottomSheetCardState extends State<BottomSheetCard> {
  // List<bool> check = [
  //   false,
  //   false,
  //   false,
  //   false,
  //   false,
  //   false,
  //   false,
  //   false,
  //   false
  // ];
  List<int> selectedItem = [];
  List<String> selectedItemName = [];
  String vale = "";
  // bool selected = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Constants.kitGradients[11],
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: widget.height),
      child: Column(
        children: [
          Spacer(
            flex: 1,
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            child: Row(
              children: [
                Spacer(
                  flex: 1,
                ),
                Text(
                  widget.title,
                  style: TextStyle(
                      fontFamily: "OpenSansRegular",
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: Constants.kitGradients[0]),
                ),
                Spacer(
                  flex: 20,
                ),
                GestureDetector(
                  onTap: () {
                    pop(context);
                  },
                  child: Icon(
                    Icons.close,
                    size: 20,
                    color: Constants.kitGradients[0],
                  ),
                ),
                Spacer(
                  flex: 1,
                ),
              ],
            ),
          ),
          Spacer(
            flex: 2,
          ),
          Row(
            children: [Container()],
          ),
          Container(
            height: screenHeight(context, dividedBy: 3),
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              itemCount: widget.label.length,
              itemBuilder: (BuildContext context, int index) {
                return HomeBottomSheetItem(
                  title: widget.title,
                  label: widget.label[index].name,
                  onClicked: (value) {
                    // setState(() {
                    //   check[index] = value;
                    // });
                    if (selectedItem.contains(widget.label[index].id)) {
                      setState(() {
                        // selected = true;
                        selectedItem.remove(widget.label[index].id);
                        selectedItemName.remove(widget.label[index].name);
                      });
                    } else {
                      setState(() {
                        selectedItem.add(widget.label[index].id);
                        selectedItemName.add(widget.label[index].name);
                      });
                    }
                    print(vale);
                  },
                );
              },
            ),
          ),
          Spacer(
            flex: 1,
          ),
          SelectButton(
            title: "Confirm",
            onPressed: () {
              widget.onClicked(
                  PaymentValues(name: selectedItemName, id: selectedItem));
              pop(context);
            },
          ),
          Spacer(
            flex: 1,
          ),
        ],
      ),
    );
  }
}
