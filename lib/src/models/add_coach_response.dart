// To parse this JSON data, do
//
//     final addCoachResponse = addCoachResponseFromJson(jsonString);

import 'dart:convert';

AddCoachResponse addCoachResponseFromJson(String str) =>
    AddCoachResponse.fromJson(json.decode(str));

String addCoachResponseToJson(AddCoachResponse data) =>
    json.encode(data.toJson());

class AddCoachResponse {
  AddCoachResponse({
    this.id,
    this.name,
    this.address,
    this.trainerType,
    this.trainingAtHome,
    this.clientsReservePay,
    this.videoStatement,
    this.yearsOfExp,
    this.highestLevelPlayed,
    this.highestEducation,
    this.dob,
    this.ssn,
    this.trainerSports,
  });

  final int id;
  final String name;
  final String address;
  final int trainerType;
  final int trainingAtHome;
  final bool clientsReservePay;
  final int videoStatement;
  final int yearsOfExp;
  final int highestLevelPlayed;
  final int highestEducation;
  final DateTime dob;
  final String ssn;
  final List<TrainerSport> trainerSports;

  factory AddCoachResponse.fromJson(Map<String, dynamic> json) =>
      AddCoachResponse(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        address: json["address"] == null ? null : json["address"],
        trainerType: json["trainer_type"] == null ? null : json["trainer_type"],
        trainingAtHome:
            json["training_at_home"] == null ? null : json["training_at_home"],
        clientsReservePay: json["clients_reserve_pay"] == null
            ? null
            : json["clients_reserve_pay"],
        videoStatement:
            json["video_statement"] == null ? null : json["video_statement"],
        yearsOfExp: json["years_of_exp"] == null ? null : json["years_of_exp"],
        highestLevelPlayed: json["highest_level_played"] == null
            ? null
            : json["highest_level_played"],
        highestEducation: json["highest_education"] == null
            ? null
            : json["highest_education"],
        dob: json["dob"] == null ? null : DateTime.parse(json["dob"]),
        ssn: json["ssn"] == null ? null : json["ssn"],
        trainerSports: json["trainer_sports"] == null
            ? null
            : List<TrainerSport>.from(
                json["trainer_sports"].map((x) => TrainerSport.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "address": address == null ? null : address,
        "trainer_type": trainerType == null ? null : trainerType,
        "training_at_home": trainingAtHome == null ? null : trainingAtHome,
        "clients_reserve_pay":
            clientsReservePay == null ? null : clientsReservePay,
        "video_statement": videoStatement == null ? null : videoStatement,
        "years_of_exp": yearsOfExp == null ? null : yearsOfExp,
        "highest_level_played":
            highestLevelPlayed == null ? null : highestLevelPlayed,
        "highest_education": highestEducation == null ? null : highestEducation,
        "dob": dob == null
            ? null
            : "${dob.year.toString().padLeft(4, '0')}-${dob.month.toString().padLeft(2, '0')}-${dob.day.toString().padLeft(2, '0')}",
        "ssn": ssn == null ? null : ssn,
        "trainer_sports": trainerSports == null
            ? null
            : List<dynamic>.from(trainerSports.map((x) => x.toJson())),
      };
}

class TrainerSport {
  TrainerSport({
    this.sport,
    this.trainerType,
    this.ageRange,
    this.sessionIncrements,
    this.rateBy,
    this.isVolunteerOpted,
    this.isClinicCampsOpted,
  });

  final int sport;
  final int trainerType;
  final int ageRange;
  final int sessionIncrements;
  final int rateBy;
  final bool isVolunteerOpted;
  final bool isClinicCampsOpted;

  factory TrainerSport.fromJson(Map<String, dynamic> json) => TrainerSport(
        sport: json["sport"] == null ? null : json["sport"],
        trainerType: json["trainer_type"] == null ? null : json["trainer_type"],
        ageRange: json["age_range"] == null ? null : json["age_range"],
        sessionIncrements: json["session_increments"] == null
            ? null
            : json["session_increments"],
        rateBy: json["rate_by"] == null ? null : json["rate_by"],
        isVolunteerOpted: json["is_volunteer_opted"] == null
            ? null
            : json["is_volunteer_opted"],
        isClinicCampsOpted: json["is_clinic_camps_opted"] == null
            ? null
            : json["is_clinic_camps_opted"],
      );

  Map<String, dynamic> toJson() => {
        "sport": sport == null ? null : sport,
        "trainer_type": trainerType == null ? null : trainerType,
        "age_range": ageRange == null ? null : ageRange,
        "session_increments":
            sessionIncrements == null ? null : sessionIncrements,
        "rate_by": rateBy == null ? null : rateBy,
        "is_volunteer_opted":
            isVolunteerOpted == null ? null : isVolunteerOpted,
        "is_clinic_camps_opted":
            isClinicCampsOpted == null ? null : isClinicCampsOpted,
      };
}
