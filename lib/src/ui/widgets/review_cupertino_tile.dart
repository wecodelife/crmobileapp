import 'package:app_template/src/ui/widgets/profile_icon_widget.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ReviewCupertinoTile extends StatefulWidget {
  final String heading;
  final String userName;
  final String phoneNumber;
  final String imageName;
  final double textPadding;
  final double radius;
  final double fontSizeHeading;
  final double fontSizePhoneNumber;
  final double imagePadding;
  ReviewCupertinoTile(
      {this.heading,
      this.imageName,
      this.imagePadding,
      this.fontSizeHeading,
      this.textPadding,
      this.radius,
      this.userName,
      this.phoneNumber,
      this.fontSizePhoneNumber});
  @override
  _ReviewCupertinoTileState createState() => _ReviewCupertinoTileState();
}

class _ReviewCupertinoTileState extends State<ReviewCupertinoTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 4),
      decoration: BoxDecoration(
          color: Constants.kitGradients[5],
          borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: [
          Spacer(flex: 3),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30)),
            child: Text(
              "“Lorem ipsum dolor sit amet, consectetuer adipiscing elit, "
              "sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi “",
              style: TextStyle(color: Colors.white, fontSize: 14),
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 30),
          ),
          ProfileIconWidget(
            imageName: widget.imageName,
            phoneNumber: widget.phoneNumber,
            radius: 20,
            textPadding: 70,
            userName: "DanSmith",
            fontSizeHeading: 15,
            fontSizedPhoneNUmber: 15,
            imagePadding: 100,
          ),
          Spacer(
            flex: 3,
          )
        ],
      ),
    );
  }
}
