import 'package:app_template/src/ui/buisness/screens/profile_invites_6.dart';
import 'package:app_template/src/ui/buisness/widgets/bottom_navigation_bar_business.dart';
import 'package:app_template/src/ui/widgets/profile_page_information_tile.dart';
import 'package:app_template/src/ui/widgets/ymca_profile_tile.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class Profile2Page extends StatefulWidget {
  @override
  _Profile2PageState createState() => _Profile2PageState();
}

class _Profile2PageState extends State<Profile2Page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: BottomNavigationBarBusiness(),
      backgroundColor: Constants.kitGradients[3],
      body: SingleChildScrollView(
          child: Column(
        children: [
          Container(
            color: Constants.kitGradients[11],
            width: screenWidth(context, dividedBy: 1),
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 20),
                ),
                Text(
                  "PROFILE",
                  style: TextStyle(
                      color: Constants.kitGradients[0],
                      fontSize: 17,
                      fontFamily: 'OswaldBold'),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 40),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 30)),
                  child: YmcaProfileTile(
                    onPressed: () {
                      // push(
                      //     context, FacilityDetailsPage());
                    },
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30)),
            child: Container(
              height: screenHeight(context, dividedBy: 7),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Constants.kitGradients[11],
              ),
              child: Column(
                children: [
                  Spacer(
                    flex: 1,
                  ),
                  ProfilePageInformationTile(
                    icon: "assets/icons/profile_icon.svg",
                    title: "Information",
                    onPressed: () {
                      //push(context, EditProfile());
                    },
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  ProfilePageInformationTile(
                    icon: "assets/icons/preferences_icon.svg",
                    title: "Preferences",
                    onPressed: () {
                      // push(context, UserList());
                    },
                  ),
                  Spacer(
                    flex: 1,
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30)),
            child: Container(
                height: screenHeight(context, dividedBy: 2.3),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Constants.kitGradients[11],
                ),
                child: Column(
                  children: [
                    Spacer(
                      flex: 1,
                    ),
                    ProfilePageInformationTile(
                      icon: "assets/icons/scholarship_icon.svg",
                      title: "Scholarships",
                      onPressed: () {
                        //push(context, ProfileMoney());
                      },
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    ProfilePageInformationTile(
                      icon: "assets/icons/marketplace_icon.svg",
                      title: "Marketplace",
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    ProfilePageInformationTile(
                      icon: "assets/icons/credentials_icon.svg",
                      title: "Credentials",
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    ProfilePageInformationTile(
                      icon: "assets/icons/money_icon.svg",
                      title: "Money",
                      onPressed: () {
                        // push(context, ProfileDoc1());
                      },
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    ProfilePageInformationTile(
                      icon: "assets/icons/invites_icon.svg",
                      title: "Invites",
                      onPressed: () {
                        push(context, ProfileInvites6());
                      },
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    ProfilePageInformationTile(
                      icon: "assets/icons/employees_icon.svg",
                      title: "Employees",
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    ProfilePageInformationTile(
                      icon: "assets/icons/lost_found_icon.svg",
                      title: "Lost &  Found",
                    ),
                  ],
                )),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
        ],
      )),
    );
  }
}
