// To parse this JSON data, do
//
//     final getTimeSlotsRequest = getTimeSlotsRequestFromJson(jsonString);

import 'dart:convert';

GetTimeSlotsRequest getTimeSlotsRequestFromJson(String str) =>
    GetTimeSlotsRequest.fromJson(json.decode(str));

String getTimeSlotsRequestToJson(GetTimeSlotsRequest data) =>
    json.encode(data.toJson());

class GetTimeSlotsRequest {
  GetTimeSlotsRequest({
    this.date,
  });

  final String date;

  factory GetTimeSlotsRequest.fromJson(Map<String, dynamic> json) =>
      GetTimeSlotsRequest(
        date: json["date"] == null ? null : json["date"],
      );

  Map<String, dynamic> toJson() => {
        "date": date == null ? null : date,
      };
}
