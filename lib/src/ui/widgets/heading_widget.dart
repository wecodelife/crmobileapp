import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class HeadingWidget extends StatefulWidget {
  final String heading;
  HeadingWidget({this.heading});
  @override
  _HeadingWidgetState createState() => _HeadingWidgetState();
}

class _HeadingWidgetState extends State<HeadingWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: screenHeight(context, dividedBy: 30),
        ),
        Row(
          children: [
            Text(
              widget.heading,
              style: TextStyle(
                  fontSize: 20,
                  fontFamily: 'OswaldBold',
                  color: Constants.kitGradients[1]),
            ),
          ],
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 80),
        ),
      ],
    );
  }
}
