// To parse this JSON data, do
//
//     final updateUserTypeRequestModel = updateUserTypeRequestModelFromJson(jsonString);

import 'dart:convert';

UpdateUserTypeRequestModel updateUserTypeRequestModelFromJson(String str) =>
    UpdateUserTypeRequestModel.fromJson(json.decode(str));

String updateUserTypeRequestModelToJson(UpdateUserTypeRequestModel data) =>
    json.encode(data.toJson());

class UpdateUserTypeRequestModel {
  UpdateUserTypeRequestModel({
    this.userType,
    this.invitationType,
  });

  final int userType;
  final String invitationType;

  factory UpdateUserTypeRequestModel.fromJson(Map<String, dynamic> json) =>
      UpdateUserTypeRequestModel(
        userType: json["user_type"] == null ? null : json["user_type"],
        invitationType:
            json["invitation_type"] == null ? null : json["invitation_type"],
      );

  Map<String, dynamic> toJson() => {
        "user_type": userType == null ? null : userType,
        "invitation_type": invitationType == null ? null : invitationType,
      };
}
