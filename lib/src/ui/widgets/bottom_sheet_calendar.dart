import 'package:app_template/src/ui/widgets/calender_carousel.dart';
import 'package:flutter/material.dart';

class BottomSheetCalendar extends StatefulWidget {
  @override
  _BottomSheetCalendarState createState() => _BottomSheetCalendarState();
}

class _BottomSheetCalendarState extends State<BottomSheetCalendar> {
  @override
  Widget build(BuildContext context) {
    return CarouselCalender(
      onValueChanged: (date) {},
    );
  }
}
