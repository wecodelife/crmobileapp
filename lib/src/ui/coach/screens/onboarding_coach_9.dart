import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/add_coach_request.dart';
import 'package:app_template/src/models/get_sports_response_model.dart';
import 'package:app_template/src/ui/coach/screens/train_client.dart';
import 'package:app_template/src/ui/coach/widgets/drop_down_coach_and_ref.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class OnBoardingCoach9 extends StatefulWidget {
  final List<Datum> selectedSports;
  OnBoardingCoach9({this.selectedSports});
  @override
  _OnBoardingCoach9State createState() => _OnBoardingCoach9State();
}

class _OnBoardingCoach9State extends State<OnBoardingCoach9> {
  int i;
  bool loading = false;
  String job1 = "";
  String job2 = "";
  String job3 = "";
  String age1 = "";
  String age2 = "";
  String age3 = "";
  String session1 = "";
  String session2 = "";
  String session3 = "";
  String rate1 = "";
  String rate2 = "";
  String rate3 = "";
  String volunteer1 = "";
  String volunteer2 = "";
  String volunteer3 = "";
  String clinic1 = "";
  String clinic2 = "";
  String clinic3 = "";
  List<String> ageList = [];
  List<String> sessionList = [];
  // ["Any", "Long", "Short"];
  List<String> rateByList = [];

  List<String> jobDropDown = [];
  List<String> dropDownAge = [];
  List<int> dropDownAgeId = [];
  List<int> ageIdList = [];
  List<String> dropDownSession = [];
  List<int> dropDownSessionId = [];
  List<int> sessionIdList = [];
  List<String> dropDownRate = [];
  List<int> dropDownRateId = [];
  List<int> rateIdList = [];
  List<String> dropDownVolunteer = [];
  List<String> dropDownClinic = [];
  List<TrainerSport> sports = [];
  UserBloc userBloc = new UserBloc();
  // void addAge() {
  //   for (i = 10; i < 80; i++) {
  //     ageList.add(i.toString());
  //   }
  // }

  @override
  void initState() {
    // jobDropDown = [job1, job2, job3];
    // dropDownAge = [age1, age2, age3];
    // dropDownSession = [session1, session2, session3];
    // dropDownRate = [rate1, rate2, rate3];
    // dropDownVolunteer = [volunteer1, volunteer2, volunteer3];
    // dropDownClinic = [clinic1, clinic2, clinic3];
    // addAge();
    // userBloc.getLevelOfEducation();
    userBloc.getAgeRange();
    userBloc.getSessionIncrement();
    userBloc.getRateByView();
    userBloc.getSessionIncrementResponse.listen((event) {
      for (int i = 0; i < event.data.length; i++) {
        sessionList.add(event.data[i].name);
        sessionIdList.add(event.data[i].id);
      }
      setState(() {
        loading = false;
      });
    });
    userBloc.getRateByViewResponse.listen((event) {
      for (int i = 0; i < event.data.length; i++) {
        rateByList.add(event.data[i].value);
        rateIdList.add(event.data[i].id);
      }
      setState(() {
        loading = false;
      });
    });
    userBloc.getAgeRangeResponse.listen((event) {
      for (int i = 0; i < event.data.length; i++) {
        ageList.add(event.data[i].value);
        ageIdList.add(event.data[i].id);
      }
      setState(() {
        loading = false;
      });
    });
    // TODO: implement initState
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        appBar: AppBar(
          leading: Container(),
          backgroundColor: Constants.kitGradients[6],
          actions: [OnBoardingAppBar()],
        ),
        body: loading == false
            ? Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 20)),
                    child: Column(
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 60),
                        ),
                        TextProgressIndicator(
                          heading: "LETS GET YOUR REQUIREMENTS FOR EACH SPORTS",
                          descriptionText:
                              "Set up your Age Range, time increments, rates, volunteer, clinics/camps.",
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 60),
                        ),
                        OnBoardingProgressIndicator(
                          width: 5,
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 60),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 50)),
                    child: Column(
                      children: [
                        Container(
                          height: screenHeight(context, dividedBy: 1.6),
                          child: ListView.builder(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: widget.selectedSports.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    height:
                                        screenHeight(context, dividedBy: 1.6),
                                    width: screenWidth(context, dividedBy: 1.1),
                                    color: Constants.kitGradients[4]
                                        .withOpacity(.1),
                                    child: ListView(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: screenWidth(context,
                                                  dividedBy: 20)),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              SizedBox(
                                                height: screenHeight(context,
                                                    dividedBy: 40),
                                              ),
                                              Row(
                                                children: [
                                                  ClipRRect(
                                                    child: Container(
                                                      width: screenWidth(
                                                          context,
                                                          dividedBy: 6),
                                                      height: screenWidth(
                                                          context,
                                                          dividedBy: 6),
                                                      child: Image.asset(
                                                        "assets/images/fishing_image.png",
                                                        fit: BoxFit.cover,
                                                      ),
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                  ),
                                                  SizedBox(
                                                    width: screenWidth(context,
                                                        dividedBy: 40),
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      widget
                                                          .selectedSports[index]
                                                          .name,
                                                      style: TextStyle(
                                                          fontSize: 14,
                                                          color: Constants
                                                              .kitGradients[1]),
                                                    ),
                                                    width: screenWidth(context,
                                                        dividedBy: 1.9),
                                                  ),
                                                  Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: [
                                                      Container(
                                                        child: Text(
                                                          (index + 1)
                                                                  .toString() +
                                                              "/${widget.selectedSports.length.toString()}",
                                                          style: TextStyle(
                                                              fontSize:
                                                                  screenWidth(
                                                                      context,
                                                                      dividedBy:
                                                                          30),
                                                              color:
                                                                  Colors.white),
                                                        ),
                                                        width: screenWidth(
                                                            context,
                                                            dividedBy: 17),
                                                        alignment: Alignment
                                                            .centerRight,
                                                      ),
                                                      SizedBox(
                                                        height: screenHeight(
                                                            context,
                                                            dividedBy: 16),
                                                      )
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: screenHeight(context,
                                                    dividedBy: 50),
                                              ),
                                              DropDownCoachAndRef(
                                                label:
                                                    "What do you want to do?",
                                                hintText: "Select",
                                                dropDownList: ["Coach", "Ref"],
                                                dropDownValue:
                                                    jobDropDown.length > index
                                                        ? jobDropDown[index]
                                                        : "",
                                                onClicked: (value) {
                                                  setState(() {
                                                    if (jobDropDown.length >
                                                        index) {
                                                      jobDropDown
                                                          .removeAt(index);
                                                    }
                                                    jobDropDown.insert(
                                                        index, value);
                                                    // jobDropDown[index] = value;
                                                    if (sports.length > index) {
                                                      sports.removeAt(index);
                                                    }
                                                    sports.insert(
                                                        index,
                                                        TrainerSport(
                                                            sessionIncrements:
                                                                dropDownSessionId.length > index
                                                                    ? dropDownSessionId[
                                                                        index]
                                                                    : null,
                                                            ageRange: dropDownAgeId.length >
                                                                    index
                                                                ? dropDownAgeId[
                                                                    index]
                                                                : null,
                                                            trainerType: jobDropDown
                                                                        .length >
                                                                    index
                                                                ? jobDropDown[index] ==
                                                                        "Coach"
                                                                    ? 1
                                                                    : 2
                                                                : 1,
                                                            isClinicCampsOpted:
                                                                dropDownClinic.length >
                                                                        index
                                                                    ? dropDownClinic[index] ==
                                                                            "Yes"
                                                                        ? true
                                                                        : false
                                                                    : false,
                                                            isVolunteerOpted:
                                                                dropDownVolunteer.length >
                                                                        index
                                                                    ? dropDownVolunteer[index] ==
                                                                            "Yes"
                                                                        ? true
                                                                        : false
                                                                    : false,
                                                            rateBy: dropDownRateId
                                                                        .length >
                                                                    index
                                                                ? dropDownRateId[
                                                                    index]
                                                                : null,
                                                            sport: widget
                                                                .selectedSports[index]
                                                                .id));
                                                  });
                                                  print(sports[index]
                                                          .trainerType
                                                          .toString() +
                                                      sports[index]
                                                          .ageRange
                                                          .toString() +
                                                      sports[index]
                                                          .sessionIncrements
                                                          .toString() +
                                                      sports[index]
                                                          .rateBy
                                                          .toString() +
                                                      sports[index]
                                                          .isVolunteerOpted
                                                          .toString() +
                                                      sports[index]
                                                          .isClinicCampsOpted
                                                          .toString());
                                                },
                                              ),
                                              DropDownCoachAndRef(
                                                label: "Age Range",
                                                hintText: "Select",
                                                // dropDownType: "age",
                                                dropDownList: ageList,
                                                // dropDownAgeList:
                                                //     snapshot.data.data,
                                                dropDownValue:
                                                    dropDownAge.length > index
                                                        ? dropDownAge[index]
                                                        : "",
                                                // dropDownAge[index] ?? '',
                                                onClicked: (value) {
                                                  setState(() {
                                                    if (dropDownAge.length >
                                                        index) {
                                                      dropDownAge
                                                          .removeAt(index);
                                                    }
                                                    dropDownAge.insert(
                                                        index, value);
                                                    // dropDownAge[index] =
                                                    //     value;
                                                    // dropDownAgeId[index] =
                                                    //     ageIdList[ageList
                                                    //         .indexOf(value)];
                                                    if (dropDownAgeId.length >
                                                        index) {
                                                      dropDownAgeId
                                                          .removeAt(index);
                                                    }
                                                    dropDownAgeId.insert(
                                                        index,
                                                        ageIdList[ageList
                                                            .indexOf(value)]);
                                                    if (sports.length > index) {
                                                      sports.removeAt(index);
                                                    }
                                                    sports.insert(
                                                        index,
                                                        TrainerSport(
                                                            sessionIncrements:
                                                                dropDownSessionId.length > index
                                                                    ? dropDownSessionId[
                                                                        index]
                                                                    : null,
                                                            ageRange: dropDownAgeId.length >
                                                                    index
                                                                ? dropDownAgeId[
                                                                    index]
                                                                : null,
                                                            trainerType: jobDropDown
                                                                        .length >
                                                                    index
                                                                ? jobDropDown[index] ==
                                                                        "Coach"
                                                                    ? 1
                                                                    : 2
                                                                : 1,
                                                            isClinicCampsOpted:
                                                                dropDownClinic.length >
                                                                        index
                                                                    ? dropDownClinic[index] ==
                                                                            "Yes"
                                                                        ? true
                                                                        : false
                                                                    : false,
                                                            isVolunteerOpted:
                                                                dropDownVolunteer.length >
                                                                        index
                                                                    ? dropDownVolunteer[index] ==
                                                                            "Yes"
                                                                        ? true
                                                                        : false
                                                                    : false,
                                                            rateBy: dropDownRateId
                                                                        .length >
                                                                    index
                                                                ? dropDownRateId[
                                                                    index]
                                                                : null,
                                                            sport: widget
                                                                .selectedSports[index]
                                                                .id));
                                                  });
                                                  print(ageList.indexOf(value));
                                                  print(
                                                      ageIdList[6].toString());
                                                },
                                              ),
                                              DropDownCoachAndRef(
                                                label: "Session increment",
                                                hintText: "Select",
                                                dropDownList: sessionList,
                                                // dropDownType: "session",
                                                // dropDownSession:
                                                //     snapshot.data.data,
                                                dropDownValue:
                                                    dropDownSession.length >
                                                            index
                                                        ? dropDownSession[index]
                                                        : "",
                                                // dropDownSession[index] ??
                                                //         '',
                                                onClicked: (value) {
                                                  setState(() {
                                                    // dropDownSession[
                                                    //     index] = value;
                                                    if (dropDownSession.length >
                                                        index) {
                                                      dropDownSession
                                                          .removeAt(index);
                                                    }
                                                    dropDownSession.insert(
                                                        index, value);
                                                    // dropDownSessionId[index] =
                                                    //     sessionIdList[
                                                    //         sessionList.indexOf(
                                                    //             value)];
                                                    if (dropDownSessionId
                                                            .length >
                                                        index) {
                                                      dropDownSessionId
                                                          .removeAt(index);
                                                    }
                                                    dropDownSessionId.insert(
                                                        index,
                                                        sessionIdList[
                                                            sessionList.indexOf(
                                                                value)]);
                                                    if (sports.length > index) {
                                                      sports.removeAt(index);
                                                    }
                                                    sports.insert(
                                                        index,
                                                        TrainerSport(
                                                            sessionIncrements:
                                                                dropDownSessionId.length > index
                                                                    ? dropDownSessionId[
                                                                        index]
                                                                    : null,
                                                            ageRange: dropDownAgeId.length >
                                                                    index
                                                                ? dropDownAgeId[
                                                                    index]
                                                                : null,
                                                            trainerType: jobDropDown
                                                                        .length >
                                                                    index
                                                                ? jobDropDown[index] ==
                                                                        "Coach"
                                                                    ? 1
                                                                    : 2
                                                                : 1,
                                                            isClinicCampsOpted:
                                                                dropDownClinic.length >
                                                                        index
                                                                    ? dropDownClinic[index] ==
                                                                            "Yes"
                                                                        ? true
                                                                        : false
                                                                    : false,
                                                            isVolunteerOpted:
                                                                dropDownVolunteer.length >
                                                                        index
                                                                    ? dropDownVolunteer[index] ==
                                                                            "Yes"
                                                                        ? true
                                                                        : false
                                                                    : false,
                                                            rateBy: dropDownRateId
                                                                        .length >
                                                                    index
                                                                ? dropDownRateId[
                                                                    index]
                                                                : null,
                                                            sport: widget
                                                                .selectedSports[index]
                                                                .id));
                                                  });
                                                  print(dropDownSessionId[index]
                                                      .toString());
                                                },
                                              ),
                                              DropDownCoachAndRef(
                                                label: "Rate By",
                                                hintText: "Select",
                                                // dropDownType: "rate",
                                                // dropDownRate:
                                                //     snapshot.data.data,
                                                dropDownList: rateByList,
                                                dropDownValue:
                                                    dropDownRate.length > index
                                                        ? dropDownRate[index]
                                                        : "",
                                                // dropDownRate[index] ?? '',
                                                onClicked: (value) {
                                                  setState(() {
                                                    if (dropDownRate.length >
                                                        index) {
                                                      dropDownRate
                                                          .removeAt(index);
                                                    }
                                                    dropDownRate.insert(
                                                        index, value);
                                                    // dropDownRate[index] =
                                                    //     value;
                                                    if (dropDownRateId.length >
                                                        index) {
                                                      dropDownRateId
                                                          .removeAt(index);
                                                    }
                                                    dropDownRateId.insert(
                                                        index,
                                                        rateIdList[rateByList
                                                            .indexOf(value)]);
                                                    if (sports.length > index) {
                                                      sports.removeAt(index);
                                                    }
                                                    sports.insert(
                                                        index,
                                                        TrainerSport(
                                                            sessionIncrements:
                                                                dropDownSessionId.length > index
                                                                    ? dropDownSessionId[
                                                                        index]
                                                                    : null,
                                                            ageRange: dropDownAgeId.length >
                                                                    index
                                                                ? dropDownAgeId[
                                                                    index]
                                                                : null,
                                                            trainerType: jobDropDown
                                                                        .length >
                                                                    index
                                                                ? jobDropDown[index] ==
                                                                        "Coach"
                                                                    ? 1
                                                                    : 2
                                                                : 1,
                                                            isClinicCampsOpted:
                                                                dropDownClinic.length >
                                                                        index
                                                                    ? dropDownClinic[index] ==
                                                                            "Yes"
                                                                        ? true
                                                                        : false
                                                                    : false,
                                                            isVolunteerOpted:
                                                                dropDownVolunteer.length >
                                                                        index
                                                                    ? dropDownVolunteer[index] ==
                                                                            "Yes"
                                                                        ? true
                                                                        : false
                                                                    : false,
                                                            rateBy: dropDownRateId
                                                                        .length >
                                                                    index
                                                                ? dropDownRateId[
                                                                    index]
                                                                : null,
                                                            sport: widget
                                                                .selectedSports[index]
                                                                .id));
                                                  });
                                                },
                                              ),
                                              DropDownCoachAndRef(
                                                label:
                                                    "Do you want to volunteer?",
                                                hintText: "Select",
                                                dropDownList: ["Yes", "No"],
                                                dropDownValue: dropDownVolunteer
                                                            .length >
                                                        index
                                                    ? dropDownVolunteer[index]
                                                    : "",
                                                // dropDownVolunteer[index] ??
                                                //         '',
                                                onClicked: (value) {
                                                  setState(() {
                                                    if (dropDownVolunteer
                                                            .length >
                                                        index) {
                                                      dropDownVolunteer
                                                          .removeAt(index);
                                                    }
                                                    dropDownVolunteer.insert(
                                                        index, value);
                                                    // dropDownVolunteer[index] =
                                                    //     value;
                                                    if (sports.length > index) {
                                                      sports.removeAt(index);
                                                    }
                                                    sports.insert(
                                                        index,
                                                        TrainerSport(
                                                            sessionIncrements:
                                                                dropDownSessionId.length > index
                                                                    ? dropDownSessionId[
                                                                        index]
                                                                    : null,
                                                            ageRange: dropDownAgeId.length >
                                                                    index
                                                                ? dropDownAgeId[
                                                                    index]
                                                                : null,
                                                            trainerType: jobDropDown
                                                                        .length >
                                                                    index
                                                                ? jobDropDown[index] ==
                                                                        "Coach"
                                                                    ? 1
                                                                    : 2
                                                                : 1,
                                                            isClinicCampsOpted:
                                                                dropDownClinic.length >
                                                                        index
                                                                    ? dropDownClinic[index] ==
                                                                            "Yes"
                                                                        ? true
                                                                        : false
                                                                    : false,
                                                            isVolunteerOpted:
                                                                dropDownVolunteer.length >
                                                                        index
                                                                    ? dropDownVolunteer[index] ==
                                                                            "Yes"
                                                                        ? true
                                                                        : false
                                                                    : false,
                                                            rateBy: dropDownRateId
                                                                        .length >
                                                                    index
                                                                ? dropDownRateId[
                                                                    index]
                                                                : null,
                                                            sport: widget
                                                                .selectedSports[index]
                                                                .id));
                                                  });
                                                },
                                              ),
                                              DropDownCoachAndRef(
                                                label: "Clinic/Camps",
                                                hintText: "Select",
                                                dropDownList: ["Yes", "No"],
                                                dropDownValue:
                                                    dropDownClinic.length >
                                                            index
                                                        ? dropDownClinic[index]
                                                        : "",
                                                // dropDownClinic[index] ?? '',
                                                onClicked: (value) {
                                                  setState(() {
                                                    if (dropDownClinic.length >
                                                        index) {
                                                      dropDownClinic
                                                          .removeAt(index);
                                                    }
                                                    dropDownClinic.insert(
                                                        index, value);
                                                    // dropDownClinic[index] =
                                                    //     value;
                                                    if (sports.length > index) {
                                                      sports.removeAt(index);
                                                    }
                                                    sports.insert(
                                                        index,
                                                        TrainerSport(
                                                            sessionIncrements:
                                                                dropDownSessionId.length > index
                                                                    ? dropDownSessionId[
                                                                        index]
                                                                    : null,
                                                            ageRange: dropDownAgeId.length >
                                                                    index
                                                                ? dropDownAgeId[
                                                                    index]
                                                                : null,
                                                            trainerType: jobDropDown
                                                                        .length >
                                                                    index
                                                                ? jobDropDown[index] ==
                                                                        "Coach"
                                                                    ? 1
                                                                    : 2
                                                                : 1,
                                                            isClinicCampsOpted:
                                                                dropDownClinic.length >
                                                                        index
                                                                    ? dropDownClinic[index] ==
                                                                            "Yes"
                                                                        ? true
                                                                        : false
                                                                    : false,
                                                            isVolunteerOpted:
                                                                dropDownVolunteer.length >
                                                                        index
                                                                    ? dropDownVolunteer[index] ==
                                                                            "Yes"
                                                                        ? true
                                                                        : false
                                                                    : false,
                                                            rateBy: dropDownRateId
                                                                        .length >
                                                                    index
                                                                ? dropDownRateId[
                                                                    index]
                                                                : null,
                                                            sport: widget
                                                                .selectedSports[index]
                                                                .id));
                                                  });
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              }),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 30),
                        ),
                        BuildButton(
                          title: "NEXT",
                          onPressed: () {
                            push(
                                context,
                                TrainClient(
                                  sports: sports,
                                ));
                            print(sports.toString());
                            // print("a" +
                            //     sports[0].ageRange.toString() +
                            //     sports[0].sessionIncrements.toString() +
                            //     sports[0].rateBy.toString() +
                            //     sports[0].trainerType.toString() +
                            //     sports[0].isVolunteerOpted.toString() +
                            //     sports[0].isClinicCampsOpted.toString() +
                            //     sports[0].sport.toString());
                            // print("b" +
                            //     sports[1].ageRange.toString() +
                            //     sports[1].sessionIncrements.toString() +
                            //     sports[1].rateBy.toString() +
                            //     sports[1].trainerType.toString() +
                            //     sports[1].isVolunteerOpted.toString() +
                            //     sports[1].isClinicCampsOpted.toString() +
                            //     sports[1].sport.toString());
                            // print("c" +
                            //     sports[2].ageRange.toString() +
                            //     sports[2].sessionIncrements.toString() +
                            //     sports[2].rateBy.toString() +
                            //     sports[2].trainerType.toString() +
                            //     sports[2].isVolunteerOpted.toString() +
                            //     sports[2].isClinicCampsOpted.toString() +
                            //     sports[2].sport.toString());
                            // userBloc.addCoach(
                            //     addCoachRequest: AddCoachRequest(
                            //         address: "test",
                            //         name: "test",
                            //         dob: DateTime.parse(DateFormat('yyyy-MM-dd')
                            //             .format(DateTime.now())),
                            //         clientsReservePay: true,
                            //         highestEducation: 1,
                            //         highestLevelPlayed: 1,
                            //         trainerType: 1,
                            //         trainingAtHome: 1,
                            //         videoStatement: 1,
                            //         yearsOfExp: 1,
                            //         trainerSports: sports
                            //         //     [
                            //         //   TrainerSport(
                            //         //       sport: 1,
                            //         //       sessionIncrements: 1,
                            //         //       ageRange: 1,
                            //         //       trainerType: 1,
                            //         //       isClinicCampsOpted: true,
                            //         //       isVolunteerOpted: true,
                            //         //       rateBy: 3),
                            //         //   TrainerSport(
                            //         //       sport: 1,
                            //         //       sessionIncrements: 1,
                            //         //       ageRange: 1,
                            //         //       trainerType: 1,
                            //         //       isClinicCampsOpted: true,
                            //         //       isVolunteerOpted: true,
                            //         //       rateBy: 3),
                            //         //   TrainerSport(
                            //         //       sport: 1,
                            //         //       sessionIncrements: 1,
                            //         //       ageRange: 1,
                            //         //       trainerType: 1,
                            //         //       isClinicCampsOpted: true,
                            //         //       isVolunteerOpted: true,
                            //         //       rateBy: 3)
                            //         // ]
                            //         ));
                            // setState(() {
                            //   loading = true;
                            // });
                            // print("ssdj $id ${widget.dropDownValue.id.toString()}");
                            // userBloc.updateUserType(
                            //     updateUserTypeRequestModel: UpdateUserTypeRequestModel(
                            //         userType: id,
                            //         invitationType:
                            //         widget.dropDownValue.id.toString()));
                          },
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 30),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            : Container(
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 1),
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ));
  }
}
