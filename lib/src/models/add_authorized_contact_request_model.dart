// To parse this JSON data, do
//
//     final addAuthorizedContactRequestModel = addAuthorizedContactRequestModelFromJson(jsonString);

import 'dart:convert';

AddAuthorizedContactRequestModel addAuthorizedContactRequestModelFromJson(
        String str) =>
    AddAuthorizedContactRequestModel.fromJson(json.decode(str));

String addAuthorizedContactRequestModelToJson(
        AddAuthorizedContactRequestModel data) =>
    json.encode(data.toJson());

class AddAuthorizedContactRequestModel {
  AddAuthorizedContactRequestModel({
    this.parent,
    this.email,
    this.accessLevel,
  });

  final int parent;
  final String email;
  final int accessLevel;

  factory AddAuthorizedContactRequestModel.fromJson(
          Map<String, dynamic> json) =>
      AddAuthorizedContactRequestModel(
        parent: json["parent"] == null ? null : json["parent"],
        email: json["email"] == null ? null : json["email"],
        accessLevel: json["access_level"] == null ? null : json["access_level"],
      );

  Map<String, dynamic> toJson() => {
        "parent": parent == null ? null : parent,
        "email": email == null ? null : email,
        "access_level": accessLevel == null ? null : accessLevel,
      };
}
