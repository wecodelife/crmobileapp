// To parse this JSON data, do
//
//     final uploadImageRequest = uploadImageRequestFromJson(jsonString);

import 'dart:convert';

UploadImageRequest uploadImageRequestFromJson(String str) =>
    UploadImageRequest.fromJson(json.decode(str));

String uploadImageRequestToJson(UploadImageRequest data) =>
    json.encode(data.toJson());

class UploadImageRequest {
  UploadImageRequest({
    this.image,
  });

  final dynamic image;

  factory UploadImageRequest.fromJson(Map<String, dynamic> json) =>
      UploadImageRequest(
        image: json["image"] == null ? null : json["image"],
      );

  Map<String, dynamic> toJson() => {
        "image": image == null ? null : image,
      };
}
