import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class SubmitButton extends StatefulWidget {
  final Function onPressed;
  final double borderRadius;
  final Color buttonBackgroundColor;
  final String buttonName;
  final bool buttonBorder;
  final double buttonWidth;
  final double buttonHeight;
  final Color textColor;
  final bool disabled;

  SubmitButton(
      {this.onPressed,
      this.borderRadius,
      this.buttonBackgroundColor,
      this.buttonName,
      this.buttonBorder,
      this.buttonWidth,
      this.buttonHeight,
      this.textColor,this.disabled});

  @override
  _SubmitButtonState createState() => _SubmitButtonState();
}

class _SubmitButtonState extends State<SubmitButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.buttonWidth,
      height: widget.buttonHeight,
      child: RaisedButton(
        onPressed: (){},
        color: widget.buttonBackgroundColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(widget.borderRadius),
            side: widget.buttonBorder == true
                ? BorderSide(color: Constants.kitGradients[1])
                : BorderSide.none),
        child: Center(
            child: Text(
          widget.buttonName,
          style: TextStyle(
              fontFamily: 'OswaldBold',
              fontSize: 17.0,
              color:widget.textColor),
        )),
      ),
    );
  }
}
