import 'dart:io';

import 'package:app_template/src/ui/coach/screens/coach_job_list_page.dart';
import 'package:app_template/src/ui/widgets/appbar_cr.dart';
import 'package:app_template/src/ui/widgets/coment_box.dart';
import 'package:app_template/src/ui/widgets/e_signature_widget.dart';
import 'package:app_template/src/ui/widgets/select_button.dart';
import 'package:app_template/src/ui/widgets/select_date_widget.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:draggable_scrollbar/draggable_scrollbar.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:signature/signature.dart';

class ProfileDoc2 extends StatefulWidget {
  @override
  _ProfileDoc2State createState() => _ProfileDoc2State();
}

class _ProfileDoc2State extends State<ProfileDoc2> {
  ScrollController scrollController = new ScrollController();
  List<String> eSignatureType = ["Draw", "Type", "Upload"];
  TextEditingController eSignatureTextEditingController =
      new TextEditingController();

  final SignatureController _controller = SignatureController(
      penStrokeWidth: 5,
      penColor: Colors.white,
      exportBackgroundColor: Constants.kitGradients[6]);
  int count = 0;
  DateTime selectedDate;
  String formattedDate;
  File file;
  String fileName = "";
  _selectDate(BuildContext context) async {
    selectedDate = DateTime.now();
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        formattedDate = DateFormat('dd/MM/yyyy').format(selectedDate);
        print(selectedDate);
      });
  }

  @override
  void initState() {
    _controller.addListener(() {
      print("Value Changed");
      String formattedDate = (DateFormat('dd/mm/yyyy').format(DateTime.now()));
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        actions: [
          AppBarCr(
            leftIcon: "assets/icons/back_button.svg",
            onTapLeftIcon: () {},
            pageTitle: "MEDICAL/LIABILITY RELEASE",
            rightTextYes: true,
            rightText: "",
          ),
        ],
      ),
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 20)),
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 40),
                ),
                Container(
                  height: screenHeight(context, dividedBy: 2.4),
                  width: screenWidth(context, dividedBy: 1),
                  color: Constants.kitGradients[3],
                  child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 40),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: screenWidth(context, dividedBy: 30),
                          ),
                          Text(
                            "Medical/Liability Release",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontFamily: 'OpenSansSemiBold'),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 80),
                      ),
                      Container(
                        height: screenHeight(context, dividedBy: 3),
                        width: screenWidth(context, dividedBy: 1.2),
                        child: DraggableScrollbar(
                          padding: EdgeInsets.all(5),
                          alwaysVisibleScrollThumb: true,
                          controller: scrollController,
                          child: ListView.builder(
                              controller: scrollController,
                              shrinkWrap: true,
                              scrollDirection: Axis.vertical,
                              itemCount: 1,
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  child: Text(
                                    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore "
                                    "et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gub"
                                    "ergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam"
                                    " nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolo"
                                    "res et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem"
                                    " ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt "
                                    "ut labore et",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: 'OpenSansSemiBold',
                                        fontSize: 16),
                                  ),
                                );
                              }),
                          heightScrollThumb: 150,
                          backgroundColor: Colors.blue,
                          scrollThumbBuilder: (
                            Color backgroundColor,
                            Animation<double> thumbAnimation,
                            Animation<double> labelAnimation,
                            double height, {
                            Text labelText,
                            BoxConstraints labelConstraints,
                          }) {
                            return FadeTransition(
                              opacity: thumbAnimation,
                              child: Container(
                                height: height,
                                width: 2,
                                color: Constants.kitGradients[5],
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: Constants.kitGradients[5],
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20)),
            ),
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: screenWidth(context, dividedBy: 4.5),
                    ),
                    Container(
                      width: screenWidth(context, dividedBy: 2),
                      child: Center(
                        child: Text(
                          "E-SIGNATURE",
                          style: TextStyle(
                              color: Constants.kitGradients[1],
                              fontSize: 18,
                              fontFamily: 'OswaldBold'),
                        ),
                      ),
                    ),
                    Container(
                      width: screenWidth(context, dividedBy: 4.5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Icon(
                            Icons.close,
                            size: 20,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: screenWidth(context, dividedBy: 30),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 1.2),
                  height: screenHeight(context, dividedBy: 19),
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: 3,
                      itemBuilder: (BuildContext context, int index) {
                        return ESignatureTile(
                          count: count,
                          index: index,
                          heading: eSignatureType[index],
                          onPressed: () {
                            setState(() {
                              count = index;
                              print(count);
                            });
                          },
                        );
                      }),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 80),
                ),
                count == 0
                    ? Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: screenWidth(context, dividedBy: 20)),
                        child: Column(
                          children: [
                            Container(
                              child: Signature(
                                controller: _controller,
                                height: screenHeight(context, dividedBy: 4),
                                backgroundColor: Constants.kitGradients[6],
                              ),
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 30),
                            ),
                            // Row(
                            //   children: [
                            //     GestureDetector(
                            //       onTap: () async {
                            //         setState(() {
                            //           _controller.clear();
                            //         });
                            //       },
                            //       child: Container(
                            //         width: screenWidth(context, dividedBy: 10),
                            //         height:
                            //             screenHeight(context, dividedBy: 18),
                            //         color: Colors.white,
                            //         child: Icon(
                            //           Icons.clear,
                            //           size: 20,
                            //         ),
                            //       ),
                            //     ),
                            //   ],
                            // )
                          ],
                        ),
                      )
                    : Container(),
                count == 1
                    ? Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: screenWidth(context, dividedBy: 20)),
                        child: CommentBox(
                          eSignatureTextEditingController:
                              eSignatureTextEditingController,
                          boxHeight: 4,
                          boxWidth: 1.12,
                        ),
                      )
                    : Container(),
                count == 2
                    ? Container(
                        height: screenHeight(context, dividedBy: 3.9),
                        width: screenWidth(context, dividedBy: 1.12),
                        color: Constants.kitGradients[3],
                        child: DottedBorder(
                          dashPattern: [16, 6],
                          strokeWidth: 2.0,
                          color: Constants.kitGradients[1],
                          child: GestureDetector(
                            onTap: () {
                              filePick();
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.cloud_upload,
                                  size: 50,
                                  color: Colors.white,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      fileName == "" ? "Browse" : "",
                                      style: TextStyle(
                                          color: Constants.kitGradients[1],
                                          fontSize: 15,
                                          fontFamily: '14',
                                          decoration: TextDecoration.underline),
                                    ),
                                    Text(
                                      fileName == ""
                                          ? " to Upload"
                                          : fileName.split("/").last,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15,
                                        fontFamily: '14',
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                    : Container(),
                SizedBox(
                  height: screenHeight(context, dividedBy: 60),
                ),
                SelectDateWidget(
                  calenderFunction: () {
                    _selectDate(context);
                  },
                  formattedDate: formattedDate,
                  selectedDate: selectedDate,
                  iconName: "assets/icons/calendar_icon2.svg",
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 80),
                ),
                SelectButton(
                  title: "Submit",
                  onPressed: () async {
                    if (_controller.isNotEmpty) {
                      var data = _controller.toImage();
                      print(data);
                      push(context, CoachJobListPage());
                    }
                  },
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 60),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Future filePick() async {
    FilePickerResult result = await FilePicker.platform.pickFiles();

    if (result != null) {
      file = File(result.files.single.path);
      setState(() {
        fileName = file.path;
      });
    } else {
      // User canceled the picker
    }
  }
}
