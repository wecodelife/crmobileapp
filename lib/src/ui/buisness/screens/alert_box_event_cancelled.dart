import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AlertBoxEventCancelled extends StatefulWidget {
  final String icon;
  final String price;
  final Function onPressed;
  AlertBoxEventCancelled({this.icon, this.price, this.onPressed});
  @override
  _AlertBoxEventCancelledState createState() => _AlertBoxEventCancelledState();
}

class _AlertBoxEventCancelledState extends State<AlertBoxEventCancelled> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onPressed,
      child: Container(
        width: screenWidth(context, dividedBy: 1),
        height: screenHeight(context, dividedBy: 2.8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Constants.kitGradients[3],
          //color:Colors.red,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: screenHeight(context, dividedBy: 6.5),
              width: screenWidth(context, dividedBy: 1),
              padding: EdgeInsets.symmetric(
                vertical: screenHeight(context, dividedBy: 30),
                horizontal: screenWidth(context, dividedBy: 30),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
                color: Color(0xff3F4A40),
              ),
              child: SvgPicture.asset(widget.icon),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 35),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 15),
              ),
              height: screenHeight(context, dividedBy: 9.3),
              child: Text(
                "The training session has been canceled and \$" +
                    widget.price +
                    " has been automatically withdrawn from your account.",
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'OpenSansRegular',
                    fontSize: 16),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 15),
            ),
          ],
        ),
      ),
    );
  }
}
