import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class DesignationSelectionBox extends StatefulWidget {
  String descriptionText;
  String heading;
  bool selected;
  Function onPressed;
  DesignationSelectionBox({this.descriptionText,this.heading,this.selected,this.onPressed});
  @override
  _DesignationSelectionBoxState createState() => _DesignationSelectionBoxState();
}

class _DesignationSelectionBoxState extends State<DesignationSelectionBox> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [

        SizedBox(
          height: screenHeight(context, dividedBy: 40),
        ),

        GestureDetector(
          onTap: widget.onPressed,
          child: Container(
            width: screenWidth(context, dividedBy: 1.1),
            height: screenHeight(context,dividedBy: 7.6),
            decoration: BoxDecoration(
              color: widget.selected==true?Theme.of(context).buttonColor:Theme.of(context).scaffoldBackgroundColor,
                border: Border.all(
                    color: widget.selected==true?Theme.of(context).buttonColor:Constants.kitGradients[4],),),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [

                        SizedBox(height: screenHeight(context,dividedBy: 60),),

                        Container(
                          width: screenWidth(context,dividedBy: 1.2),
                          child: Center(
                            child: Text(
                              widget.heading,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'OswaldBold',
                                  fontSize: 15
                              ),
                            ),
                          ),
                        ),

                        SizedBox(
                          height: screenHeight(context,dividedBy: 400),
                        ),


                        Container(
                          width: screenWidth(context,dividedBy: 1.2),
                          child: Text(
                            widget.descriptionText,
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'OpenSansRegular',
                              fontSize:screenWidth(context,dividedBy: 29),
                            ),
                            textAlign:TextAlign.center,

                          ),
                        ),

                      ],

                    ),
          ),
        ),
      ],
    );
  }
}
