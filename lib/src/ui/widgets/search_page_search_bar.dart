import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SearchBarSearchPage extends StatefulWidget {
  final String hintText;
  final TextEditingController textEditingController;
  final bool rightIcon;
  final String leftIcon;
  SearchBarSearchPage(
      {this.textEditingController,
      this.hintText,
      this.rightIcon,
      this.leftIcon});
  @override
  _SearchBarSearchPageState createState() => _SearchBarSearchPageState();
}

class _SearchBarSearchPageState extends State<SearchBarSearchPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 20),
      decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(0),
          border: Border.all(color: Constants.kitGradients[1])),
      child: TextField(
        cursorColor: Colors.white,
        controller: widget.textEditingController,
        style: TextStyle(backgroundColor: Colors.black),
        decoration: InputDecoration(
            focusColor: Colors.black,
            hintText: widget.hintText,
            hintStyle: TextStyle(color: Colors.white),
            prefixIcon: Container(
              width: 10,
              height: 10,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: SvgPicture.asset(
                  widget.leftIcon,
                  color: Colors.white,
                ),
              ),
            ),
            suffixIcon: widget.rightIcon == true
                ? Container(
                    width: screenWidth(context, dividedBy: 20),
                    child: Icon(
                      Icons.clear,
                      size: 25,
                      color: Colors.white,
                    ),
                  )
                : Container(width: 1),
            border: InputBorder.none),
      ),
    );
  }
}
