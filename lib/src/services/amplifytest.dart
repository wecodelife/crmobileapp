// import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
// import 'package:amplify_flutter/amplify.dart';
// import 'package:app_template/src/services/amplifyconfiguration.dart';
// import 'package:flutter/cupertino.dart';
// class Auth extends ChangeNotifier {
//   Amplify amplifyInstance;
//
//   Auth() {
//     this.amplifyInstance = Amplify;
//     configureCognitoPluginWrapper();
//   }
//
//   Future<void> configureCognitoPluginWrapper() async {
//     await configureCognitoPlugin();
//   }
//
//   Future<void> configureCognitoPlugin() async {
//     AmplifyAuthCognito authPlugin = AmplifyAuthCognito();
//
//     amplifyInstance.addPlugin(
//       authPlugins: [authPlugin],
//     );
//
//     await amplifyInstance.configure(amplifyconfig);
//
//     authPlugin.events.listenToAuth((event) {
//       print(event);
//       switch (event["eventName"]) {
//         case "SIGNED_IN":
//           print("HUB: USER IS SIGNED IN");
//           break;
//         case "SIGNED_OUT":
//           print("HUB: USER IS SIGNED OUT");
//           break;
//         case "SESSION_EXPIRED":
//           print("HUB: USER SESSION HAS EXPIRED");
//           break;
//         default:
//           print("CONFIGURATION EVENT");
//       }
//     });
//   }
