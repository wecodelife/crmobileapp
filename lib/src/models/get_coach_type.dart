// To parse this JSON data, do
//
//     final getCoachType = getCoachTypeFromJson(jsonString);

import 'dart:convert';

GetCoachType getCoachTypeFromJson(String str) =>
    GetCoachType.fromJson(json.decode(str));

String getCoachTypeToJson(GetCoachType data) => json.encode(data.toJson());

class GetCoachType {
  GetCoachType({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final List<Datum> data;

  factory GetCoachType.fromJson(Map<String, dynamic> json) => GetCoachType(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.name,
    this.order,
  });

  final String name;
  final String order;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        name: json["name"] == null ? null : json["name"],
        order: json["order"] == null ? null : json["order"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "order": order == null ? null : order,
      };
}
