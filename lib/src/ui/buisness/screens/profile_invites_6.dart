import 'package:app_template/src/ui/buisness/screens/profile_invites_8.dart';
import 'package:app_template/src/ui/widgets/appbar_cr.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/form_field_number.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class ProfileInvites6 extends StatefulWidget {
  @override
  _ProfileInvites6State createState() => _ProfileInvites6State();
}

class _ProfileInvites6State extends State<ProfileInvites6> {
  TextEditingController nameTextEditingController = new TextEditingController();
  TextEditingController emailTextEditingController =
      new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Constants.kitGradients[3],
          appBar: AppBar(
            leading: Container(),
            actions: [
              AppBarCr(
                //transparent: false,
                pageTitle: "INVITES",
                rightTextYes: false,
                noRightIcon: true,
                onTapRightIcon: () {
                  pop(context);
                },
                leftIcon: "assets/icons/back_button.svg",
              ),
            ],
          ),
          body: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30)),
            child: Container(
                width: screenWidth(context, dividedBy: 1),
                //color:Colors.red,
                child: ListView(children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  TextProgressIndicator(
                    heading: "INVITE PEOPLE",
                    descriptionText:
                        "Know someone who would love this app? Enter their name, phone number or email address and send them an invite!",
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  FormFeildUserDetails(
                    textEditingController: nameTextEditingController,
                    labelText: "Name",
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  NumberFieldData(
                    textEditingController: emailTextEditingController,
                    labelText: "Phone Number",
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  FormFeildUserDetails(
                    textEditingController: emailTextEditingController,
                    labelText: "Email",
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 3),
                  ),
                  BuildButton(
                    title: "INVITE",
                    disabled: false,
                    onPressed: () {
                      push(context, ProfileInvites8());
                    },
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                ])),
          )),
    );
  }
}
