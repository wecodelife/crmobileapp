import 'package:app_template/src/ui/screens/profile_doc_2.dart';
import 'package:app_template/src/ui/widgets/summary_widget_email.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NewDocumentListTile extends StatefulWidget {
  final Function onPressed;
  NewDocumentListTile({this.onPressed});
  @override
  _NewDocumentListTileState createState() => _NewDocumentListTileState();
}

class _NewDocumentListTileState extends State<NewDocumentListTile> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            push(context, ProfileDoc2());
          },
          child: Container(
              height: screenHeight(context, dividedBy: 3.8),
              width: screenWidth(context, dividedBy: 1),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  color: Constants.kitGradients[5]),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    height: screenHeight(context, dividedBy: 3),
                    width: screenWidth(context, dividedBy: 50),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          bottomLeft: Radius.circular(10)),
                      color: Constants.kitGradients[12],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 40),
                        vertical: screenHeight(context, dividedBy: 30)),
                    child: Container(
                      width: screenWidth(context, dividedBy: 1.22),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Medical/Liability Release",
                                style: TextStyle(
                                    fontSize: 13,
                                    fontFamily: 'OpenSansSemiBold',
                                    color: Colors.white),
                              ),
                              Spacer(
                                flex: 3,
                              ),
                              Container(
                                  width: screenWidth(context, dividedBy: 19),
                                  height: screenHeight(context, dividedBy: 40),
                                  child: SvgPicture.asset(
                                      "assets/icons/arrow_button.svg"))
                            ],
                          ),
                          Row(
                            children: [
                              SummaryTileEmail(
                                heading: "Sent",
                                subheading: "12/10/2012",
                                width: 3.8,
                              ),
                              SummaryTileEmail(
                                heading: "Recipient",
                                subheading: "Dan Smith",
                                width: 3.8,
                              ),
                              SummaryTileEmail(
                                  heading: "Contact Method",
                                  subheading: "Email",
                                  width: 3.7),
                            ],
                          ),
                          Row(
                            children: [
                              SummaryTileEmail(
                                heading: "Status",
                                subheading: "Not Signed",
                                width: 3.8,
                              ),
                              SummaryTileEmail(
                                heading: "Date",
                                subheading: "-",
                                width: 3.8,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 40),
        )
      ],
    );
  }
}
