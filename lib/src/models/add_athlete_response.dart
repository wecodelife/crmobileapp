// To parse this JSON data, do
//
//     final addAthleteResponseModel = addAthleteResponseModelFromJson(jsonString);

import 'dart:convert';

AddAthleteResponseModel addAthleteResponseModelFromJson(String str) =>
    AddAthleteResponseModel.fromJson(json.decode(str));

String addAthleteResponseModelToJson(AddAthleteResponseModel data) =>
    json.encode(data.toJson());

class AddAthleteResponseModel {
  AddAthleteResponseModel({
    this.message,
    this.data,
    this.status,
  });

  final String message;
  final List<Datum> data;
  final int status;

  factory AddAthleteResponseModel.fromJson(Map<String, dynamic> json) =>
      AddAthleteResponseModel(
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
        "status": status == null ? null : status,
      };
}

class Datum {
  Datum({
    this.id,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.name,
    this.gender,
    this.parent,
    this.dob,
    this.profPic,
    this.sports,
  });

  final int id;
  final bool isActive;
  final bool isDeleted;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String name;
  final int gender;
  final int parent;
  final DateTime dob;
  final dynamic profPic;
  final List<int> sports;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        isActive: json["is_active"] == null ? null : json["is_active"],
        isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        name: json["name"] == null ? null : json["name"],
        gender: json["gender"] == null ? null : json["gender"],
        parent: json["parent"] == null ? null : json["parent"],
        dob: json["dob"] == null ? null : DateTime.parse(json["dob"]),
        profPic: json["prof_pic"],
        sports: json["sports"] == null
            ? null
            : List<int>.from(json["sports"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "is_active": isActive == null ? null : isActive,
        "is_deleted": isDeleted == null ? null : isDeleted,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "name": name == null ? null : name,
        "gender": gender == null ? null : gender,
        "parent": parent == null ? null : parent,
        "dob": dob == null
            ? null
            : "${dob.year.toString().padLeft(4, '0')}-${dob.month.toString().padLeft(2, '0')}-${dob.day.toString().padLeft(2, '0')}",
        "prof_pic": profPic,
        "sports":
            sports == null ? null : List<dynamic>.from(sports.map((x) => x)),
      };
}
