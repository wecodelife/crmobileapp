import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class OnBoardingProgressIndicator extends StatefulWidget {
  final double width;
  bool progressIndicator;
  OnBoardingProgressIndicator({this.width,this.progressIndicator});
  @override
  _OnBoardingProgressIndicatorState createState() => _OnBoardingProgressIndicatorState();
}

class _OnBoardingProgressIndicatorState extends State<OnBoardingProgressIndicator> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context,dividedBy: 70),
      width: screenWidth(context,dividedBy: 1.16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Theme.of(context).primaryColorLight,
      ),

      child: widget.progressIndicator!=false?Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: screenHeight(context,dividedBy: 70),
            width: screenWidth(context,dividedBy: widget.width),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Theme.of(context).primaryColorDark,
            ),

          ),
        ],
      ):Container()


    );
  }
}
