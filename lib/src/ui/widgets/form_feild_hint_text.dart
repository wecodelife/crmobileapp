import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FormFieldHintText extends StatefulWidget {
  final String labelText;
  final bool readOnly;
  final ValueChanged onValueChanged;
  final TextEditingController textEditingController;
  final String hintText;

  FormFieldHintText({
    this.labelText,
    this.textEditingController,
    this.onValueChanged,
    this.readOnly,
    this.hintText,
  });
  @override
  _FormFieldHintTextState createState() => _FormFieldHintTextState();
}

class _FormFieldHintTextState extends State<FormFieldHintText> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextField(
        controller: widget.textEditingController,
        autofocus: false,
        readOnly: widget.readOnly ?? false,
        onChanged: widget.onValueChanged,
        style: TextStyle(
          color: Constants.kitGradients[0],
          fontFamily: 'OpenSansRegular',
          fontSize: 16,
        ),
        cursorColor: Constants.kitGradients[0],
        decoration: InputDecoration(
          hintStyle: TextStyle(
              color: Colors.white,
              fontSize: 14,
              fontFamily: 'OpenSansSemiBold'),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Constants.kitGradients[0]),
          ),
          hintText: widget.hintText,
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Constants.kitGradients[0]),
          ),
          labelStyle: TextStyle(
            color: Constants.kitGradients[0],
            fontFamily: 'OpenSansRegular',
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}
