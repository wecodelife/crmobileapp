import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RatingWidget extends StatefulWidget {
  final Function onPressed;
  final bool divider;
  final bool rated;
  final String heading;
  final IconData icon;
  RatingWidget(
      {this.divider, this.onPressed, this.rated, this.heading, this.icon});
  @override
  _RatingWidgetState createState() => _RatingWidgetState();
}

class _RatingWidgetState extends State<RatingWidget> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        GestureDetector(
          onTap: () {
            widget.onPressed();
          },
          child: Container(
            width: screenWidth(context, dividedBy: 20),
            child: widget.rated == true
                ? Icon(
                    widget.icon,
                    size: 16,
                    color: Constants.kitGradients[1],
                  )
                : Icon(
                    widget.icon,
                    color: Colors.white,
                    size: 18,
                  ),
          ),
        ),
        Container(
          width: screenWidth(context, dividedBy: 18),
          child: Text(widget.heading,
              style: TextStyle(
                  fontSize: 13,
                  fontFamily: 'OpenSansRegular',
                  color: Colors.white),
              overflow: TextOverflow.ellipsis),
        ),
        SizedBox(
          width: screenWidth(context, dividedBy: 100),
        ),
        widget.divider == true
            ? Container(
                color: Colors.white,
                height: screenWidth(context, dividedBy: 40),
                width: 1,
              )
            : Container(),
        SizedBox(
          width: screenWidth(context, dividedBy: 100),
        )
      ],
    );
  }
}
