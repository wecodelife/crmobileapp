import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/add_coach_request.dart';
import 'package:app_template/src/models/upload_video_request.dart';
import 'package:app_template/src/ui/coach/screens/social_media_integreations_page.dart';
import 'package:app_template/src/ui/coach/widgets/bottom_sheet_add_references.dart';
import 'package:app_template/src/ui/screens/gal.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/drop_down_cr_app.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/select_button.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';

class FinalCredentialsPage extends StatefulWidget {
  final List<TrainerSport> sports;
  final String name;
  final String address;
  final String ssn;
  final DateTime dob;
  FinalCredentialsPage(
      {this.sports, this.ssn, this.dob, this.address, this.name});
  @override
  _FinalCredentialsPageState createState() => _FinalCredentialsPageState();
}

class _FinalCredentialsPageState extends State<FinalCredentialsPage> {
  String experience;
  // UserBloc userBloc = UserBloc();
  List<String> experienceList = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10"
  ];
  UserBloc userBloc = new UserBloc();
  String highestLevelPlay = "";
  int highestLevelPlayId;
  // List<String> highestLevelPlayList = ["1", "2"];
  List<String> highestLevelPlayList = [];
  List<int> highestLevelPlayListId = [];
  String education = "";
  int educationId;
  // List<String> educationList = ["1", "2"];
  List<String> educationList = [];
  List<int> educationListId = [];
  List<String> stateList = [];
  List<int> stateListId = [];
  PickedFile file;
  ImagePicker picker = ImagePicker();
  bool loading = true;
  int videoId;

  Future videoPicked() async {
    file = await picker.getVideo(
      source: ImageSource.camera,
      maxDuration: Duration(minutes: 1),
    );
    userBloc.uploadVideo(uploadVideoRequest: UploadVideoRequest(video: file));
    setState(() {
      loading = true;
    });
  }

  @override
  void initState() {
    userBloc.uploadVideoResponse.listen((event) {
      setState(() {
        loading = false;
        videoId = event.data.id;
      });
    });
    userBloc.getLevelOfPlay();
    userBloc.getLevelOfEducation();
    userBloc.getLevelOfPlayResponse.listen((event) {
      for (int i = 0; i < event.data.length; i++) {
        highestLevelPlayList.add(event.data[i].value);
        highestLevelPlayListId.add(event.data[i].id);
      }
      setState(() {
        loading = false;
      });
    });
    userBloc.getLevelOfEducationResponse.listen((event) {
      for (int i = 0; i < event.data.length; i++) {
        educationList.add(event.data[i].value);
        educationListId.add(event.data[i].id);
      }
      setState(() {
        loading = false;
      });
    });
    userBloc.getStates();
    userBloc.getStatesResponse.listen((event) {
      for (int i = 0; i < event.data.length; i++) {
        stateList.add(event.data[i].name);
        stateListId.add(event.data[i].id);
      }

      // setState(() {
      //   loading = false;
      // });
    });
    userBloc.addCoachResponse.listen((event) {
      setState(() {
        loading = false;
      });
      ObjectFactory().appHive.putCoachId(value: event.id);
      push(context, SocialMediaIntegrationPage());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[11],
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: loading == true
          ? Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 20)),
                child: Column(children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                  TextProgressIndicator(
                    heading: "LETS GET SOME FINAL CREDENTIALS",
                    descriptionText:
                        "We are going to collect some last bits of information from you.",
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                  OnBoardingProgressIndicator(
                    width: 1.5,
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 20),
                  ),
                ]),
              ),
              Container(
                height: screenHeight(context, dividedBy: 1.7),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: screenHeight(context, dividedBy: 30),
                  ),
                  child: ListView(
                    children: [
                      Center(
                        child: Text(
                          "Add a Profile Picture",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                              fontFamily: 'OpenSansRegular'),
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 70),
                      ),
                      GestureDetector(
                        onTap: () {
                          push(
                              context,
                              GalleryScreen(
                                imageKey: "Coach Image",
                              ));
                        },
                        child: Container(
                          child: SvgPicture.asset(
                            "assets/icons/add_profile_icon.svg",
                            fit: BoxFit.contain,
                            height: screenHeight(context, dividedBy: 9),
                            width: screenWidth(context, dividedBy: 20),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 50),
                      ),
                      SelectButton(
                        title: file == null
                            ? "Record Video Statement"
                            : "Video Uploaded",
                        onPressed: () {
                          videoPicked();
                        },
                        icon: "assets/icons/video_recording_icon.svg",
                        iconTrue: true,
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 40),
                      ),
                      Text(
                        "How many years of experience do you have?",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: 'OpenSansRegular'),
                      ),
                      Container(
                        width: screenWidth(context, dividedBy: 1),
                        child: DropDownCrApp(
                          dropDownValue: experience,
                          dropDownList: experienceList,
                          title: "Select",
                          onClicked: (value) {
                            setState(() {
                              experience = value;
                            });
                            print("a" + experience);
                          },
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 40),
                      ),
                      Text(
                        "What is the highest level you have played at?",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: 'OpenSansRegular'),
                      ),
                      Container(
                        width: screenWidth(context, dividedBy: 1),
                        child: DropDownCrApp(
                          dropDownValue: highestLevelPlay,
                          dropDownList: highestLevelPlayList,
                          title: "Select",
                          onClicked: (value) {
                            setState(() {
                              highestLevelPlay = value;
                              highestLevelPlayId = highestLevelPlayListId[
                                  highestLevelPlayList.indexOf(value)];
                            });
                            print(highestLevelPlayId.toString());
                          },
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 40),
                      ),
                      Text(
                        "What is the highest level of education?",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: 'OpenSansRegular'),
                      ),
                      Container(
                        width: screenWidth(context, dividedBy: 1),
                        child: DropDownCrApp(
                          dropDownValue: education,
                          dropDownList: educationList,
                          title: "Select",
                          onClicked: (value) {
                            setState(() {
                              education = value;
                              educationId =
                                  educationListId[educationList.indexOf(value)];
                            });
                            print(educationId.toString());
                          },
                        ),
                      ),
                      SelectButton(
                        title: "Add References",
                        icon: "assets/icons/plus_icon.svg",
                        onPressed: () {
                          showBottomSheet();
                        },
                        iconTrue: true,
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      SelectButton(
                        title: "Add Certifications",
                        icon: "assets/icons/plus_icon.svg",
                        onPressed: () {},
                        iconTrue: true,
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 40),
              ),
              BuildButton(
                onPressed: () {
                  setState(() {
                    loading = true;
                  });
                  // print("adada" +);
                  userBloc.addCoach(
                      addCoachRequest: AddCoachRequest(
                          trainerType:
                              int.parse(ObjectFactory().appHive.getCoachType()),
                          trainerSports: widget.sports,
                          yearsOfExp: int.parse(experience),
                          videoStatement: videoId,
                          trainingAtHome: int.parse(
                              ObjectFactory().appHive.getTrainingAtHome()),
                          highestLevelPlayed: highestLevelPlayId,
                          highestEducation: educationId,
                          clientsReservePay:
                              ObjectFactory().appHive.getClientsReservePay(),
                          dob: widget.dob,
                          name: widget.name,
                          address: widget.address,
                          profPic: ObjectFactory()
                                      .appHive
                                      .getImagePath(key: "Coach Image") !=
                                  null
                              ? int.parse(ObjectFactory()
                                  .appHive
                                  .getImageId("Coach Imageid"))
                              : null,
                          ssn: widget.ssn));
                },
                transparent: true,
                title: "NEXT",
              )
            ]),
    );
  }

  void showBottomSheet() {
    showModalBottomSheet(
        isScrollControlled: true,
        enableDrag: true,
        isDismissible: true,
        backgroundColor: Constants.kitGradients[5],
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(10), topLeft: Radius.circular(10)),
            side: BorderSide()),
        context: context,
        builder: (BuildContext context) {
          return BottomSheetAddReferences(
            stateList: stateList,
            stateListId: stateListId,
          );
        });
  }
}
