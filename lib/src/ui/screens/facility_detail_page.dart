import 'package:app_template/material/pickers/date_range_picker_dialog.dart';
import 'package:app_template/src/ui/widgets/amenities_selection_grid_tile.dart';
import 'package:app_template/src/ui/widgets/calender_carousel.dart';
import 'package:app_template/src/ui/widgets/facility_detail_summary.dart';
import 'package:app_template/src/ui/widgets/list_tile_select_sports.dart';
import 'package:app_template/src/ui/widgets/profile_icon_widget.dart';
import 'package:app_template/src/ui/widgets/soccer_feild_selection_tile.dart';
import 'package:app_template/src/ui/widgets/time_slot_app_bar.dart';
import 'package:app_template/src/ui/widgets/time_slot_tile.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';

class FacilityDetailsPage extends StatefulWidget {
  @override
  _FacilityDetailsPageState createState() => _FacilityDetailsPageState();
}

class _FacilityDetailsPageState extends State<FacilityDetailsPage> {
  List<String> imgList = [
    "assets/icons/facility_bg_image.png",
    "assets/icons/facility_bg_image.png",
    "assets/icons/facility_bg_image.png"
  ];
  int _current = 0;
  int reviewCurrentIndex = 0;
  double reviewPosition;
  double position;
  int count = 1;
  bool calendar = false;
  bool timeSlots;
  bool onClicked;
  DateTime currentDate;
  bool slotSelection = false;
  bool showSummary = false;
  bool selected = false;
  bool deselected = false;
  int itemCount;
  List<String> sports = [
    "Basketball",
    "Volleyball",
    "Football",
    "Swimming",
    "Baseball",
    "Hockey",
    "Soccer",
    "Tennis"
  ];
  List<String> sportsIcon = [
    "assets/icons/basketball.svg",
    "assets/icons/volleyball.svg",
    "assets/icons/basket_ball_icon.svg",
    "assets/icons/swimming.svg",
    "assets/icons/baseball.svg",
    "assets/icons/hockey.svg",
    "assets/icons/soccer.svg",
    "assets/icons/tennis.svg"
  ];
  void incrementCounter() {
    count = count + 1;
  }

  void decrementCounter() {
    count = count - 1;
  }

  FToast fToast;
  @override
  void initState() {
    // TODO: implement initState
    fToast = FToast();
    fToast.init(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DotsDecorator(
        activeColor: Colors.red.withOpacity(.50),
        color: Colors.white.withOpacity(.50),
        shape: CircleBorder(),
        size: Size(10, 10));
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
      child: Scaffold(
        body: ListView(
          padding: EdgeInsets.all(0),
          children: [
            Stack(
              children: [
                CarouselSlider(
                  options: CarouselOptions(
                      aspectRatio: screenWidth(context, dividedBy: 1) /
                          screenHeight(context, dividedBy: 2.4),
                      viewportFraction: 1,
                      onPageChanged: (index, reason) {
                        setState(() {
                          _current = index;
                          print(_current);
                        });
                      }),
                  items: imgList.map((i) {
                    return Builder(
                      builder: (BuildContext context) {
                        return Stack(
                          children: [
                            Container(
                              width: screenWidth(context, dividedBy: 1),
                              height: screenHeight(context, dividedBy: 1),
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                image: AssetImage(
                                  i,
                                ),
                                fit: BoxFit.fill,
                              )),
                              child: Column(
                                children: [
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 15),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: screenWidth(context,
                                            dividedBy: 50)),
                                    child: Row(
                                      children: [
                                        GestureDetector(
                                          onTap: () {
                                            pop(context);
                                          },
                                          child: Icon(
                                            Icons.arrow_back,
                                            size: 25,
                                            color: Colors.white,
                                          ),
                                        ),
                                        SizedBox(
                                          width: screenWidth(context,
                                              dividedBy: 1.2),
                                        ),
                                        Column(
                                          children: [
                                            SizedBox(
                                              height: screenHeight(context,
                                                  dividedBy: 80),
                                            ),
                                            Container(
                                                width: screenWidth(context,
                                                    dividedBy: 22),
                                                child: SvgPicture.asset(
                                                    "assets/icons/notification_icon.svg")),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Positioned(
                              top: screenHeight(context, dividedBy: 3),
                              left: screenWidth(context, dividedBy: 2.5),
                              child: Row(
                                children: [
                                  DotsIndicator(
                                    dotsCount: 3,
                                    position: _current.toDouble(),
                                    axis: Axis.horizontal,
                                    decorator: DotsDecorator(
                                        activeColor:
                                            Colors.red.withOpacity(.40),
                                        color: Colors.white.withOpacity(.40),
                                        shape: CircleBorder(),
                                        size: Size(10, 10)),
                                  ),
                                ],
                              ),
                            )
                          ],
                        );
                      },
                    );
                  }).toList(),
                ),
                Column(
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 2.7),
                    ),
                    Container(
                      width: screenWidth(context, dividedBy: 1),
                      decoration: BoxDecoration(
                        color: Constants.kitGradients[6],
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: screenHeight(context, dividedBy: 20),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal:
                                    screenWidth(context, dividedBy: 30)),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      "YMCA",
                                      style: TextStyle(
                                          fontSize: 30,
                                          fontFamily: 'AvenirBlack',
                                          color: Colors.white),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 100),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.location_on_outlined,
                                      size: 20,
                                      color: Colors.white,
                                    ),
                                    SizedBox(
                                      width: 2,
                                    ),
                                    Container(
                                      width: screenWidth(context, dividedBy: 2),
                                      child: Text(
                                          "76A Eighth Avenue, Pittsburgh",
                                          style: TextStyle(
                                              fontSize: 13,
                                              fontFamily: 'OpenSansRegular',
                                              color: Colors.white),
                                          overflow: TextOverflow.ellipsis),
                                    ),
                                    Spacer(
                                      flex: 6,
                                    ),
                                    Container(
                                      width:
                                          screenWidth(context, dividedBy: 20),
                                      child: Icon(
                                        Icons.phone_outlined,
                                        size: 20,
                                        color: Constants.kitGradients[1],
                                      ),
                                    ),
                                    Spacer(
                                      flex: 3,
                                    ),
                                    Container(
                                      width:
                                          screenWidth(context, dividedBy: 20),
                                      child: Icon(
                                        Icons.mail_outline,
                                        size: 20,
                                        color: Constants.kitGradients[1],
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 100),
                                ),
                                Row(
                                  children: [
                                    Text(
                                      "ABOUT",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontFamily: 'AvenirBlack',
                                          color: Constants.kitGradients[1]),
                                    ),
                                  ],
                                ),
                                Text(
                                  "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod "
                                  "tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontFamily: 'AvenirBook',
                                      color: Colors.white),
                                ),
                                Row(
                                  children: [
                                    Text(
                                      "AMENITIES",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontFamily: 'AvenirBlack',
                                          color: Constants.kitGradients[1]),
                                    ),
                                  ],
                                ),
                                GridView.builder(
                                    padding: EdgeInsets.all(0),
                                    itemCount: 4,
                                    shrinkWrap: true,
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                            crossAxisCount: 2,
                                            crossAxisSpacing: screenWidth(
                                                context,
                                                dividedBy: 30),
                                            childAspectRatio: screenWidth(
                                                    context,
                                                    dividedBy: 1.2) /
                                                screenHeight(context,
                                                    dividedBy: 4),
                                            mainAxisSpacing: screenHeight(
                                                context,
                                                dividedBy: 40)),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return AmenitiesSelectionGridTile(
                                        title: "Parking onSite",
                                        bgImage:
                                            "assets/icons/basket_ball_icon.svg",
                                      );
                                    }),
                                Row(
                                  children: [
                                    Text(
                                      "SPORTS",
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontFamily: 'OswaldBold',
                                          color: Constants.kitGradients[1]),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 30),
                                ),
                                Container(
                                  width: screenWidth(context, dividedBy: 1),
                                  height: screenHeight(context, dividedBy: 7),
                                  child: ListView.builder(
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    itemCount: sportsIcon.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return ListTileSelectSports(
                                        icon: sportsIcon[index],
                                        label: sports[index],
                                        onValueChanged: (onClick) {
                                          setState(() {
                                            onClicked = onClick;
                                          });
                                        },
                                      );
                                    },
                                  ),
                                ),
                                Row(
                                  children: [
                                    Text(
                                      "SPACES",
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontFamily: 'OswaldBold',
                                          color: Constants.kitGradients[1]),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 30),
                                ),
                              ],
                            ),
                          ),
                          calendar == true
                              ? CarouselCalender(
                                  onValueChanged: (date) {},
                                )
                              : slotSelection == true
                                  ? Container(
                                      height:
                                          screenHeight(context, dividedBy: 3.1),
                                      color: Constants.kitGradients[5],
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: screenWidth(context,
                                                dividedBy: 30)),
                                        child: Column(
                                          children: [
                                            AppBarTimeSlot(
                                              icon: true,
                                              pageTitle: "SELECT A DATE",
                                              pageNavigation: () {
                                                setState(() {
                                                  calendar = true;
                                                  timeSlots = false;
                                                });
                                              },
                                            ),
                                            Expanded(
                                              child: GridView.builder(
                                                itemCount: 16,
                                                shrinkWrap: true,
                                                scrollDirection: Axis.vertical,
                                                gridDelegate:
                                                    SliverGridDelegateWithFixedCrossAxisCount(
                                                        childAspectRatio: 3 / 1,
                                                        crossAxisCount: 4,
                                                        crossAxisSpacing:
                                                            screenWidth(context,
                                                                dividedBy: 40),
                                                        mainAxisSpacing:
                                                            screenHeight(
                                                                context,
                                                                dividedBy: 60)),
                                                itemBuilder:
                                                    (BuildContext context,
                                                        int index) {
                                                  itemCount == index
                                                      ? selected = true
                                                      : selected = false;

                                                  return GestureDetector(
                                                      onTap: () {
                                                        showSummary = true;
                                                        slotSelection = false;
                                                        itemCount = index;
                                                        print("itemCount" +
                                                            itemCount
                                                                .toString());
                                                        setState(() {});
                                                      },
                                                      child: TimeSlotTile(
                                                        selected: selected,
                                                        deselected: false,
                                                        time: "10:00 AM",
                                                      ));
                                                },
                                              ),
                                            ),
                                            SizedBox(
                                              height: screenHeight(context,
                                                  dividedBy: 40),
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  : showSummary == true
                                      ? FacilityDetailSummary(
                                          onPressed: () {
                                            setState(() {
                                              showSummary = false;
                                              slotSelection = false;
                                            });
                                          },
                                          name: "DanSmith",
                                          date: "20/12/2020",
                                          amount: "100",
                                          duration: "1 hour",
                                          email: "disonshibu2015@gmail.com",
                                          endTime: "10:00AM",
                                          phone: "345345435",
                                          sportImage:
                                              "assets/icons/basket_ball_icon.svg",
                                          sports: "Soccer",
                                          startTime: "10:00",
                                          sportsDetails: true)
                                      : Container(
                                          height: screenHeight(context,
                                              dividedBy: 4),
                                          width: screenWidth(context,
                                              dividedBy: 1),
                                          child: ListView.builder(
                                              itemCount: 4,
                                              shrinkWrap: true,
                                              padding: EdgeInsets.all(0),
                                              scrollDirection: Axis.vertical,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return Container(
                                                  height: screenHeight(context,
                                                      dividedBy: 9),
                                                  color:
                                                      Constants.kitGradients[6],
                                                  child: Column(
                                                    children: [
                                                      SoccerFieldSelectionTile(
                                                        title: "Soccer Feild",
                                                        image:
                                                            "assets/icons/soccer_feild.svg",
                                                        subtitle: "Field B",
                                                        icon:
                                                            "assets/icons/basket_ball_icon.svg",
                                                        pricePerDay: "24",
                                                        pricePerHour: "24",
                                                        onPressed: () {
                                                          setState(() {
                                                            selectDateRange(
                                                                context);
                                                          });
                                                        },
                                                      ),
                                                      Spacer(
                                                        flex: 1,
                                                      ),
                                                      Padding(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                horizontal:
                                                                    screenWidth(
                                                                        context,
                                                                        dividedBy:
                                                                            20)),
                                                        child: Container(
                                                          height: 1,
                                                          color: Colors.grey,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                );
                                              }),
                                        ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 22),
                          ),
                          Container(
                            width: screenWidth(context, dividedBy: 1),
                            height: screenHeight(context, dividedBy: 4),
                            color: Colors.white12,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal:
                                    screenWidth(context, dividedBy: 30)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 40),
                                ),
                                Text(
                                  "REVIEWS",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontFamily: 'AvenirBlack',
                                      color: Constants.kitGradients[1]),
                                ),
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 30),
                                ),
                                CarouselSlider(
                                  options: CarouselOptions(
                                      aspectRatio: screenWidth(context,
                                              dividedBy: 1) /
                                          screenHeight(context, dividedBy: 2.4),
                                      viewportFraction: 1,
                                      onPageChanged: (index, reason) {
                                        setState(() {
                                          reviewCurrentIndex = index;
                                          print(reviewCurrentIndex);
                                        });
                                      }),
                                  items: imgList.map((i) {
                                    return Builder(
                                      builder: (BuildContext context) {
                                        return Stack(
                                          children: [
                                            Container(
                                              width: screenWidth(context,
                                                  dividedBy: 1),
                                              height: screenHeight(context,
                                                  dividedBy: 4),
                                              decoration: BoxDecoration(
                                                  color:
                                                      Constants.kitGradients[5],
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: Column(
                                                children: [
                                                  Spacer(flex: 3),
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal:
                                                                screenWidth(
                                                                    context,
                                                                    dividedBy:
                                                                        30)),
                                                    child: Text(
                                                      "“Lorem ipsum dolor sit amet, consectetuer adipiscing elit, "
                                                      "sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi “",
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 14),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: screenHeight(
                                                        context,
                                                        dividedBy: 30),
                                                  ),
                                                  ProfileIconWidget(
                                                    imageName:
                                                        "https://i.pinimg.com/474x/bc/d4/ac/bcd4ac32cc7d3f98b5e54bde37d6b09e.jpg",
                                                    phoneNumber: "78686887",
                                                    radius: 20,
                                                    textPadding: 70,
                                                    userName: "DanSmith",
                                                    fontSizeHeading: 15,
                                                    fontSizedPhoneNUmber: 15,
                                                    imagePadding: 100,
                                                  ),
                                                  Spacer(
                                                    flex: 3,
                                                  )
                                                ],
                                              ),
                                            ),
                                            Positioned(
                                              top: screenHeight(context,
                                                  dividedBy: 3),
                                              left: screenWidth(context,
                                                  dividedBy: 2.5),
                                              child: Row(
                                                children: [
                                                  DotsIndicator(
                                                    dotsCount: 3,
                                                    position: reviewCurrentIndex
                                                        .toDouble(),
                                                    axis: Axis.horizontal,
                                                    decorator: DotsDecorator(
                                                        activeColor: Constants
                                                            .kitGradients[1]
                                                            .withOpacity(.40),
                                                        color: Colors.white
                                                            .withOpacity(.40),
                                                        shape: CircleBorder(),
                                                        size: Size(10, 10)),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        );
                                      },
                                    );
                                  }).toList(),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<Null> selectDateRange(BuildContext context) async {
    DateTimeRange picked = await showDatesRangePicker(
        context: context,
        // initialDateRange: DateTimeRange(
        //   start: DateTime.now().add(Duration(days: 0)),
        //   end: DateTime.now().add(Duration(days: 1)),
        // ),
        firstDate: DateTime.now(),
        lastDate: DateTime(DateTime.now().year + 5),
        helpText: "Select_Date_Range",
        cancelText: "CANCEL",
        confirmText: "OK",
        saveText: "SAVE",
        errorFormatText: "Invalid_format",
        errorInvalidText: "Out_of_range",
        errorInvalidRangeText: "Invalid_range",
        fieldStartHintText: "Start_Date",
        fieldEndLabelText: "End_Date");

    // if (picked != null) {
    //   state.didChange(picked);
    // }
  }
}
