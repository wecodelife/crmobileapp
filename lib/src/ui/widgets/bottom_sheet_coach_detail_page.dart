import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/book_zoom_meeting_request.dart';
import 'package:app_template/src/ui/screens/coach_details_page.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/calender_carousel.dart';
import 'package:app_template/src/ui/widgets/facility_detail_summary.dart';
import 'package:app_template/src/ui/widgets/time_slot_app_bar.dart';
import 'package:app_template/src/ui/widgets/time_slot_tile.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:app_template/src/models/get_trainer_list_response.dart';
import 'package:fluttertoast/fluttertoast.dart';

class BottomSheetCoachDetailPage extends StatefulWidget {
  final String name;
  final String imageName;
  final List<String> timeSlots;
  final String date;
  final List<TrainerSport> sports;
  BottomSheetCoachDetailPage(
      {this.imageName, this.name, this.timeSlots, this.date, this.sports});
  @override
  _BottomSheetCoachDetailPageState createState() =>
      _BottomSheetCoachDetailPageState();
}

class _BottomSheetCoachDetailPageState
    extends State<BottomSheetCoachDetailPage> {
  bool slotSelection = true;
  bool timeSlots = false;
  bool showSummary = false;
  UserBloc userBloc = UserBloc();
  bool selected = false;
  int itemCount = 100;
  bool calendar = false;
  String selectedTime = '';
  bool loading = false;

  FToast fToast;

  @override
  void initState() {
    fToast = FToast();
    fToast.init(context);
    print("name" + ObjectFactory().appHive.getParentName());
    userBloc.bookZoomMeetingResponse.listen((event) {
      setState(() {
        loading = false;
        showToastCompleted(context, fToast, "Zoom meeting has been appointed");
      });
      push(
          context,
          CoachDetailsPage(
            name: widget.name,
            imageName: widget.imageName,
            sports: widget.sports,
          ));
    });
    super.initState();
  }

  // Future<Null> selectDateRange(BuildContext context) async {
  //   DateTimeRange picked = await showDatesRangePicker(
  //       context: context,
  //       // initialDateRange: DateTimeRange(
  //       //   start: DateTime.now().add(Duration(days: 0)),
  //       //   end: DateTime.now().add(Duration(days: 1)),
  //       // ),
  //
  //       firstDate: DateTime.now(),
  //       lastDate: DateTime(DateTime.now().year + 5),
  //       helpText: "Select_Date_Range",
  //       cancelText: "CANCEL",
  //       confirmText: "OK",
  //       saveText: "SAVE",
  //       errorFormatText: "Invalid_format",
  //       errorInvalidText: "Out_of_range",
  //       errorInvalidRangeText: "Invalid_range",
  //       fieldStartHintText: "Start_Date",
  //       fieldEndLabelText: "End_Date");
  //
  //   // if (picked != null) {
  //   //   state.didChange(picked);
  //   // }
  // }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 2),
      color: Constants.kitGradients[5],
      child: calendar == true
          ? CarouselCalender(
              onValueChanged: (date) {},
            )
          : slotSelection == true
              ? Container(
                  height: screenHeight(context, dividedBy: 3.1),
                  color: Constants.kitGradients[5],
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 30)),
                    child: Column(
                      children: [
                        AppBarTimeSlot(
                          pageTitle: "SELECT A DATE",
                          rightIcon: true,
                          onTapRightIcon: () {
                            pop(context);
                            // push(context, CoachDetailsPage());
                          },
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 1),
                          child: Row(
                            children: [
                              Spacer(
                                flex: 1,
                              ),
                              Text(
                                "Date:",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontFamily: 'OpenSansSemiBold'),
                              ),
                              Spacer(
                                flex: 1,
                              ),
                              Text(
                                widget.date,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontFamily: 'OpenSansSemiBold'),
                              ),
                              Spacer(
                                flex: 11,
                              ),
                              SvgPicture.asset(
                                  "assets/icons/calendar_icon.svg"),
                              Spacer(
                                flex: 1,
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 40),
                        ),
                        Expanded(
                          child: GridView.builder(
                            itemCount: widget.timeSlots.length,
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    childAspectRatio: 3 / 1,
                                    crossAxisCount: 4,
                                    crossAxisSpacing:
                                        screenWidth(context, dividedBy: 40),
                                    mainAxisSpacing:
                                        screenHeight(context, dividedBy: 60)),
                            itemBuilder: (BuildContext context, int index) {
                              itemCount == index
                                  ? selected = true
                                  : selected = false;
                              return GestureDetector(
                                  onTap: () {
                                    itemCount = index;

                                    print("itemCount" + itemCount.toString());
                                    setState(() {
                                      selectedTime = widget.timeSlots[index];
                                    });
                                  },
                                  child: TimeSlotTile(
                                    selected: selected,
                                    deselected: false,
                                    time: widget.timeSlots[index],
                                  ));
                            },
                          ),
                        ),
                        BuildButton(
                          title: "NEXT",
                          transparent: false,
                          disabled: false,
                          onPressed: () {
                            setState(() {
                              showSummary = true;
                              slotSelection = false;
                            });
                          },
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                      ],
                    ),
                  ),
                )
              : showSummary == true
                  ? FacilityDetailSummary(
                      onPressed: () {
                        setState(() {
                          showSummary = false;
                          slotSelection = false;
                        });
                        userBloc.bookZoomMeeting(
                            bookZoomMeetingRequest: BookZoomMeetingRequest(
                                date: widget.date,
                                parentId: ObjectFactory().appHive.getParentId(),
                                time: selectedTime));
                        // pop(context);
                      },
                      name: ObjectFactory().appHive.getParentName(),
                      date: widget.date,
                      amount: "100",
                      duration: "1 hour",
                      email: ObjectFactory().appHive.getParentEmail(),
                      endTime: (int.parse(selectedTime.split(":").first) + 1)
                              .toString() +
                          selectedTime.substring(2, 8),
                      phone: ObjectFactory().appHive.getParentNumber(),
                      sportImage: "assets/icons/basket_ball_icon.svg",
                      sports: "Soccer",
                      startTime: selectedTime,
                    )
                  : Container(),
    );
  }

  // _showToast() {
  //   Widget toast = Container(
  //       height: screenHeight(context, dividedBy: 2),
  //       width: screenWidth(context, dividedBy: 1.2),
  //       decoration: BoxDecoration(borderRadius: BorderRadius.circular(30)),
  //       child: Column(
  //         children: [
  //           Container(
  //             height: screenHeight(context, dividedBy: 10),
  //             decoration: BoxDecoration(
  //                 color: Constants.kitGradients[1],
  //                 borderRadius: BorderRadius.only(
  //                     topLeft: Radius.circular(20),
  //                     topRight: Radius.circular(20))),
  //             child: Center(
  //                 child: Icon(
  //               Icons.check_circle_outline,
  //               size: 40,
  //               color: Colors.white,
  //             )),
  //           ),
  //           Container(
  //             decoration: BoxDecoration(
  //                 color: Constants.kitGradients[5],
  //                 borderRadius: BorderRadius.only(
  //                     bottomLeft: Radius.circular(20),
  //                     bottomRight: Radius.circular(20))),
  //             height: screenHeight(context, dividedBy: 10),
  //             child: Center(
  //               child: Text(
  //                 "Zoom meeting has been appointed",
  //                 style: TextStyle(
  //                     color: Colors.white,
  //                     fontSize: 16,
  //                     fontFamily: 'OpenSansSemiBold'),
  //               ),
  //             ),
  //           )
  //         ],
  //       ));
  //   fToast.showToast(
  //     child: toast,
  //     gravity: ToastGravity.CENTER,
  //     toastDuration: Duration(seconds: 2),
  //   );
  // }
}
