import 'package:app_template/src/ui/screens/new_payment.dart';
import 'package:app_template/src/ui/screens/profile_new_address.dart';
import 'package:app_template/src/ui/widgets/appbar_cr.dart';
import 'package:app_template/src/ui/widgets/bottom_navigation_bar_widget.dart';
import 'package:app_template/src/ui/widgets/heading_widget.dart';
import 'package:app_template/src/ui/widgets/select_button.dart';
import 'package:app_template/src/ui/widgets/summary_widget_email.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProfileMoney extends StatefulWidget {
  @override
  _ProfileMoneyState createState() => _ProfileMoneyState();
}

class _ProfileMoneyState extends State<ProfileMoney> {
  int count = 4;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[6],
          actions: [
            AppBarCr(
              pageTitle: "MONEY",
              rightText: "Done",
              rightTextYes: true,
              onTapLeftIcon: () {
                pop(context);
              },
              leftIcon: "assets/icons/back_button.svg",
              onTapRightIcon: () {
                pop(context);
              },
            )
          ],
        ),
        bottomNavigationBar: BottomNavigationWidget(),
        body: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 10)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              Text(
                "BILLING",
                style: TextStyle(
                    color: Constants.kitGradients[1],
                    fontSize: 18,
                    fontFamily: 'OswaldBold'),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 50),
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.2),
                height: screenHeight(context, dividedBy: 10),
                decoration: BoxDecoration(
                  color: Constants.kitGradients[5],
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: Constants.kitGradients[1]),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Spacer(
                      flex: 1,
                    ),
                    SummaryTileEmail(
                      heading: "Billing Address",
                      subheading: "928 Leher Junction Apt.047",
                      width: 1.8,
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    Icon(
                      Icons.check_circle,
                      size: 20,
                      color: Constants.kitGradients[1],
                    ),
                    Spacer(
                      flex: 1,
                    )
                  ],
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 80),
              ),
              SelectButton(
                title: "NEW ADDRESS",
                onPressed: () {
                  push(context, ProfileNewAddress());
                },
                transparent: true,
              ),
              HeadingWidget(
                heading: "PAYMENT METHOD",
              ),
              Container(
                height: screenHeight(context, dividedBy: 8),
                child:
                    Center(child: SvgPicture.asset("assets/icons/stripe.svg")),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    width: screenWidth(context, dividedBy: 5),
                    child: Text(
                      "Account",
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: "OpenSansRegular",
                          fontSize: screenWidth(context, dividedBy: 28)),
                    ),
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 1.7),
                    alignment: Alignment.bottomRight,
                    child: Text(
                      "danSmith@gmail.com",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: "OpenSansRegular",
                        fontSize: screenWidth(context, dividedBy: 26),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 50),
              ),
              SelectButton(
                title: "RE LINK STRIPE",
                onPressed: () {
                  push(context, NewPayment());
                },
                transparent: true,
              )
            ],
          ),
        ));
  }
}
