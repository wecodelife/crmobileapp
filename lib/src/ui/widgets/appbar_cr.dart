import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AppBarCr extends StatefulWidget {
  final String leftText;
  final bool leftTextYes;
  final String pageTitle;
  final String rightIcon;
  final String leftIcon;
  final Function onTapLeftIcon, onTapRightIcon;
  final bool fontBold;
  final bool noRightIcon;
  final bool transparent;
  final bool rightTextYes;
  final String rightText;
  AppBarCr(
      {this.fontBold,
      this.pageTitle,
      this.rightIcon,
      this.leftIcon,
      this.onTapLeftIcon,
      this.onTapRightIcon,
      this.noRightIcon,
      this.transparent,
      this.rightText,
      this.rightTextYes,
      this.leftText,
      this.leftTextYes});
  @override
  _AppBarCrState createState() => _AppBarCrState();
}

class _AppBarCrState extends State<AppBarCr> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      color: widget.transparent != true
          ? Colors.black
          : Colors.black.withOpacity(0.1),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: screenWidth(context, dividedBy: 30),
          ),
          GestureDetector(
            onTap: () {
              pop(context);
              widget.onTapLeftIcon();
            },
            child: Container(
              width: screenWidth(context, dividedBy: 7.5),
              child: widget.leftTextYes == true
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          widget.leftText,
                          style: TextStyle(
                              fontSize: screenWidth(context, dividedBy: 28),
                              fontFamily: 'OpenSansSemiBold',
                              color: Colors.white),
                        ),
                      ],
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SvgPicture.asset(
                          widget.leftIcon,
                          width: screenWidth(context, dividedBy: 15),
                        ),
                      ],
                    ),
            ),
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 19),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1.8),
            child: Center(
              child: Text(
                widget.pageTitle,
                style: TextStyle(
                    color: Constants.kitGradients[1],
                    fontFamily: 'OswaldBold',
                    fontSize: screenWidth(context, dividedBy: 22)),
              ),
            ),
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 30),
          ),
          widget.noRightIcon == true
              ? Container(
                  width: screenWidth(context, dividedBy: 7.5),
                )
              : widget.rightTextYes == true
                  ? GestureDetector(
                      onTap: widget.onTapRightIcon,
                      child: Container(
                        width: screenWidth(context, dividedBy: 7.5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              widget.rightText,
                              style: TextStyle(
                                  fontSize: screenWidth(context, dividedBy: 22),
                                  fontFamily: 'OpenSansExtraBold',
                                  color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    )
                  : GestureDetector(
                      onTap: widget.onTapRightIcon,
                      child: Container(
                          width: screenWidth(context, dividedBy: 7.5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              SvgPicture.asset(widget.rightIcon),
                            ],
                          )),
                    ),
          SizedBox(
            width: screenWidth(context, dividedBy: 40),
          )
        ],
      ),
    );
  }
}
