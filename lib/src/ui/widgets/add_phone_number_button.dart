import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class AddPhoneNumberButton extends StatefulWidget {
  final Function onPressed;
  AddPhoneNumberButton({this.onPressed});
  @override
  _AddPhoneNumberButtonState createState() => _AddPhoneNumberButtonState();
}

class _AddPhoneNumberButtonState extends State<AddPhoneNumberButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1.2),
      height: screenHeight(context, dividedBy: 25),
      child: RaisedButton(
        color: Constants.kitGradients[1],
        onPressed: () {
          widget.onPressed();
        },
        child: Text(
          "+ Add Another Person",
          style: TextStyle(
              color: Colors.white, fontSize: 12, fontFamily: 'OswaldSemiBold'),
        ),
      ),
    );
  }
}
