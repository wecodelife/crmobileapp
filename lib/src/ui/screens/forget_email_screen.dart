import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

class ForgetEmailScreen extends StatefulWidget {
  @override
  _ForgetEmailScreenState createState() => _ForgetEmailScreenState();
}

class _ForgetEmailScreenState extends State<ForgetEmailScreen> {
  bool loading = false;
  int id;
  bool disabled = true;
  TextEditingController emailTextEditingController =
      new TextEditingController();

  void emailValidator(String email) {
    final bool isValid = EmailValidator.validate(email);
    if (isValid == true) {
      print(1);
    } else {
      showToast("Please enter a valid email");
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: loading == false
          ? Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      TextProgressIndicator(
                        heading: "LET'S GET YOU BACK INTO YOUR ACCOUNT",
                        descriptionText:
                            "Enter the email address you used when creating your account and we will send it to you.",
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 4),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: FormFeildUserDetails(
                    labelText: "Email *",
                    textEditingController: emailTextEditingController,
                    onValueChanged: (emailValue) {
                      setState(() {
                        if (emailTextEditingController.text != "") {
                          setState(() {
                            disabled = false;
                          });
                        } else {
                          setState(() {
                            disabled = true;
                          });
                        }
                      });
                      // emailCheck();
                    },
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 4),
                ),
                BuildButton(
                  title: "CANCEL",
                  onPressed: () {},
                  transparent: true,
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 40),
                ),
                BuildButton(
                  title: "SEND",
                  onPressed: () {
                    emailValidator(emailTextEditingController.text);
                  },
                  disabled: disabled,
                )
              ],
            )
          : Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }
}
