import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EditSports extends StatefulWidget {
  @override
  _EditSportsState createState() => _EditSportsState();
}

class _EditSportsState extends State<EditSports> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'SPORTS', style: TextStyle(color: Colors.yellow),
          ),
          backgroundColor: Colors.black,
          leading: BackButton(
          color: Colors.white),
          actions: <Widget>[
            FlatButton(
              textColor: Colors.white,
              onPressed: () {},
              child: Text("Done", style: TextStyle(
                  fontSize: 16)),
              shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
          ],
        ),
        body:

        Column(
          children: [
            Container(
                padding: EdgeInsets.all(20),
                child:Container(
                  color: Colors.black,
                  child: TextField(
                    style: TextStyle(
                      backgroundColor: Colors.black
                    ),
                    decoration: InputDecoration(
                      focusColor: Colors.black,
                      hintText: "Start typing to search...",
                        hintStyle: TextStyle(color: Colors.white),

                        prefixIcon: Icon(Icons.search, color: Colors.white)
                    ),
                  ),
                )
            ),
            ClipRRect(
              //color: Colors.orange,
              borderRadius: BorderRadius.circular(20),
            ),
          ],
        )
      );
  }
}
