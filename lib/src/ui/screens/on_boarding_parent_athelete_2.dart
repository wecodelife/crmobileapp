import 'package:app_template/src/ui/screens/on_boarding_parent_athlete_5.dart';
import 'package:app_template/src/ui/widgets/add_athelete_number_widget.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class OnBoardingParentAthlete2 extends StatefulWidget {
  @override
  _OnBoardingParentAthlete2State createState() =>
      _OnBoardingParentAthlete2State();
}

class _OnBoardingParentAthlete2State extends State<OnBoardingParentAthlete2> {
  int count = 1;

  increment() {
    count++;
  }

  decrement() {
    count--;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(leading: Container(), actions: [OnBoardingAppBar()]),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 12)),
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                TextProgressIndicator(
                  heading: "HOW MANY ATHLETES NEED TRAINING?",
                  descriptionText:
                      "This can be you, a spouse, kids, or anyone looking to get some extra help.",
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                OnBoardingProgressIndicator(
                  width: 9,
                  progressIndicator: true,
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 6),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 1.2),
                  child: Center(
                    child: Text(
                      "ATHLETES",
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'OswaldBold',
                          fontSize: 16),
                    ),
                  ),
                ),
                AddAthleteNumber(
                  count: count,
                  incrementOnTap: () {
                    setState(() {
                      increment();
                    });
                  },
                  decrementOnTap: () {
                    setState(() {
                      decrement();
                    });
                  },
                )
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 3.2),
          ),
          BuildButton(
            title: "NEXT",
            onPressed: () {
              ObjectFactory().appHive.putAthleteCount(value: count);
              push(context, OnBoardingParentAthlete5());
            },
          ),
        ],
      ),
    );
  }
}
