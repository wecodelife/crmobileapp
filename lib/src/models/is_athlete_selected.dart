// To parse this JSON data, do
//
//     final isAthleteSelected = isAthleteSelectedFromJson(jsonString);

import 'dart:convert';

import 'package:app_template/src/models/get_athlete_list_response.dart';

IsAthleteSelected isAthleteSelectedFromJson(String str) =>
    IsAthleteSelected.fromJson(json.decode(str));

String isAthleteSelectedToJson(IsAthleteSelected data) =>
    json.encode(data.toJson());

class IsAthleteSelected {
  IsAthleteSelected({
    this.isSelected,
    this.sports,
  });

  final bool isSelected;
  final List<Sport> sports;

  factory IsAthleteSelected.fromJson(Map<String, dynamic> json) =>
      IsAthleteSelected(
        isSelected: json["isSelected"] == null ? null : json["isSelected"],
        sports: json["sports"] == null ? null : json["sports"],
      );

  Map<String, dynamic> toJson() => {
        "isSelected": isSelected == null ? null : isSelected,
        "sports": sports == null ? null : sports,
      };
}
