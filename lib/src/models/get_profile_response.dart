// To parse this JSON data, do
//
//     final getProfileResponse = getProfileResponseFromJson(jsonString);

import 'dart:convert';

GetProfileResponse getProfileResponseFromJson(String str) =>
    GetProfileResponse.fromJson(json.decode(str));

String getProfileResponseToJson(GetProfileResponse data) =>
    json.encode(data.toJson());

class GetProfileResponse {
  GetProfileResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final Data data;

  factory GetProfileResponse.fromJson(Map<String, dynamic> json) =>
      GetProfileResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
        "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    this.username,
    this.id,
    this.name,
    this.email,
    this.phone,
    this.profPic,
    this.gender,
    this.dateOfBirth,
    this.sports,
  });

  final String username;
  final int id;
  final String name;
  final String email;
  final String phone;
  final ProfPic profPic;
  final int gender;
  final DateTime dateOfBirth;
  final List<Sport> sports;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        username: json["Username"] == null ? null : json["Username"],
        id: json["id"] == null ? null : json["id"],
        name: json["Name"] == null ? null : json["Name"],
        email: json["Email"] == null ? null : json["Email"],
        phone: json["Phone"] == null ? null : json["Phone"],
        profPic: json["prof_pic"] == null
            ? null
            : ProfPic.fromJson(json["prof_pic"]),
        gender: json["Gender"] == null ? null : json["Gender"],
        dateOfBirth: json["Date of Birth"] == null
            ? null
            : DateTime.parse(json["Date of Birth"]),
        sports: json["sports"] == null
            ? null
            : List<Sport>.from(json["sports"].map((x) => Sport.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Username": username == null ? null : username,
        "id": id == null ? null : id,
        "Name": name == null ? null : name,
        "Email": email == null ? null : email,
        "Phone": phone == null ? null : phone,
        "prof_pic": profPic == null ? null : profPic.toJson(),
        "Gender": gender == null ? null : gender,
        "Date of Birth": dateOfBirth == null
            ? null
            : "${dateOfBirth.year.toString().padLeft(4, '0')}-${dateOfBirth.month.toString().padLeft(2, '0')}-${dateOfBirth.day.toString().padLeft(2, '0')}",
        "sports": sports == null
            ? null
            : List<dynamic>.from(sports.map((x) => x.toJson())),
      };
}

class ProfPic {
  ProfPic({
    this.imageFile,
  });

  final dynamic imageFile;

  factory ProfPic.fromJson(Map<String, dynamic> json) => ProfPic(
        imageFile: json["image_file"],
      );

  Map<String, dynamic> toJson() => {
        "image_file": imageFile,
      };
}

class Sport {
  Sport({
    this.id,
    this.name,
    this.icon,
    this.iconFile,
    this.description,
  });

  final int id;
  final String name;
  final String icon;
  final dynamic iconFile;
  final String description;

  factory Sport.fromJson(Map<String, dynamic> json) => Sport(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        icon: json["icon"] == null ? null : json["icon"],
        iconFile: json["iconFile"],
        description: json["description"] == null ? null : json["description"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "icon": icon == null ? null : icon,
        "iconFile": iconFile,
        "description": description == null ? null : description,
      };
}
