import 'dart:async';

import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify.dart';
import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/invitation_type_response_model.dart';
import 'package:app_template/src/services/amplifyconfiguration.dart';
import 'package:app_template/src/ui/screens/on_boarding_5.dart';
import 'package:app_template/src/ui/widgets/auth_button.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/check_box.dart';
import 'package:app_template/src/ui/widgets/drop_down_coach_ref.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/form_feild_password.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class OnBoarding8 extends StatefulWidget {
  @override
  _OnBoarding8State createState() => _OnBoarding8State();
}

class _OnBoarding8State extends State<OnBoarding8> {
  // AuthUser user = AuthUser(userId: null, username: null);
  bool checked = false;
  bool loading = false;
  bool dropDownClicked = false;
  // List<String> dropDownList = [""];
  Datum dropDownValue;
  CognitoAuthSession authSession;
  UserBloc userBloc = new UserBloc();
  bool disabled = true;
  String isSignUpComplete = "";
  bool _amplifyConfigured = false;
  bool passwordCorrect = false;
  TextEditingController emailTextEditingController =
      new TextEditingController();
  TextEditingController passwordTextEditingController =
      new TextEditingController();
  TextEditingController verifyPasswordTextEditingController =
      new TextEditingController();
  TextEditingController usernameTextEditingController =
      new TextEditingController();
  void emailCheck() {
    if (emailTextEditingController.text.trim() != "") {
      setState(() {
        disabled = false;
      });
    }
  }

  bool _passwordCheck() {
    if (passwordTextEditingController.text.length > 5 &&
        verifyPasswordTextEditingController.text.length > 5 &&
        passwordTextEditingController.text ==
            verifyPasswordTextEditingController.text) {
      return true;
    } else {
      showToast("Passwords doesn't match");
      return false;
    }
  }

  bool _allFieldsFilled() {
    if (passwordTextEditingController.text.isNotEmpty &&
        verifyPasswordTextEditingController.text.isNotEmpty &&
        emailTextEditingController.text.isNotEmpty &&
        checked == true &&
        dropDownClicked == true) {
      return false;
    } else {
      return true;
    }
  }

  @override
  void initState() {
    print(authSession.toString());
    disabled = true;
    isSignUpComplete = ObjectFactory().appHive.getIsSignIn() != null
        ? ObjectFactory().appHive.getIsSignIn()
        : "false";
    print(isSignUpComplete.toString());
    userBloc.getUserInvitationType();
    userBloc.basicUserDetailsResponse.listen((event) {
      setState(() {
        loading = false;
      });
      push(
          context,
          OnBoarding5(
            dropDownValue: dropDownValue.id,
          ));
      ObjectFactory().appHive.putUserId(userId: event.cognitoId);
    });
    // Amplify.addPlugin(AmplifyAuthCognito());
    _configureAmplify();
    // Amplify.configure(amplifyconfig);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: SingleChildScrollView(
        child: Container(
          height: screenHeight(context, dividedBy: 1),
          width: screenWidth(context, dividedBy: 1),
          color: Theme.of(context).scaffoldBackgroundColor,
          child: loading == true
              ? Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 1),
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                )
              : Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: screenWidth(context, dividedBy: 12)),
                      child: Column(
                        children: [
                          SizedBox(
                            height: screenHeight(context, dividedBy: 30),
                          ),
                          TextProgressIndicator(
                            heading: "LET'S CREATE YOUR ACCOUNT",
                            descriptionText:
                                "Create an account through Facebook or Google "
                                "or enter an email and password.",
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 30),
                          ),
                          OnBoardingProgressIndicator(
                            width: 2,
                            progressIndicator: false,
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 30),
                          ),
                          Row(
                            children: [
                              Text("CREATE ACOOUNT",
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColorDark,
                                      fontFamily: ' OswaldBold',
                                      fontSize: 15)),
                            ],
                          ),
                          FormFeildUserDetails(
                            labelText: "Email *",
                            onValueChanged: (emailValue) {
                              // emailCheck();
                            },
                            textEditingController: emailTextEditingController,
                          ),
                          FormFeildUserDetails(
                            labelText: "Username *",
                            onValueChanged: (usernameValue) {
                              // emailCheck();
                            },
                            textEditingController:
                                usernameTextEditingController,
                          ),
                          FormFeildPassword(
                            labelText: "Password *",
                            onPressed: (value) {
                              // passwordCheck();
                            },
                            textEditingController:
                                passwordTextEditingController,
                          ),
                          FormFeildPassword(
                            labelText: "Verify Password *",
                            onPressed: (value) {
                              // passwordCheck();
                            },
                            textEditingController:
                                verifyPasswordTextEditingController,
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 50),
                          ),
                          Row(
                            children: [
                              CheckBox(
                                checked: checked,
                                onTap: () {
                                  setState(() {
                                    checked = !checked;
                                    if (checked == true) {
                                      disabled = false;
                                    } else {
                                      disabled = true;
                                    }
                                  });
                                },
                              ),
                              SizedBox(
                                width: screenWidth(context, dividedBy: 50),
                              ),
                              RichText(
                                text: TextSpan(
                                  text: 'Agree to ',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 13,
                                      fontFamily: "OpenSansRegular"),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: "Terms/Conditions/Privacy ",
                                        style: TextStyle(
                                            fontSize: 13,
                                            fontFamily: "OpenSansRegular",
                                            color: Colors.white,
                                            decoration:
                                                TextDecoration.underline)),
                                    TextSpan(
                                        text: " *",
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontFamily: "OpenSansRegular",
                                            color: Colors.white)),
                                  ],
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 50),
                          ),
                          Row(
                            children: [
                              Text("How did you hear about us?",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: ' OswaldBold',
                                      fontSize: 15)),
                            ],
                          ),
                          StreamBuilder<InvitationTypeResponseModel>(
                              stream: userBloc.userInvitationTypeResponse,
                              builder: (context, snapshot) {
                                return snapshot.hasData
                                    ? Container(
                                        width:
                                            screenWidth(context, dividedBy: 1),
                                        height: screenHeight(context,
                                            dividedBy: 10),
                                        child: DropDownCoachRefApp(
                                          dropDownList: snapshot.data.data,
                                          title: "Select",
                                          onClicked: (value) {
                                            setState(() {
                                              dropDownValue = value;
                                              dropDownClicked = true;
                                            });
                                          },
                                          dropDownValue: dropDownValue,
                                        ))
                                    : Container();
                              }),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 30),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              AuthButton(
                                boxWidth: 2.5,
                                buttonColor:
                                    Theme.of(context).scaffoldBackgroundColor,
                                mediaSymbol: "assets/icons/facebook.svg",
                                mediaName: "Facebook",
                              ),
                              AuthButton(
                                boxWidth: 2.5,
                                buttonColor:
                                    Theme.of(context).scaffoldBackgroundColor,
                                mediaName: "Google",
                                mediaSymbol: "assets/icons/google_icon.svg",
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 40),
                    ),
                    BuildButton(
                      isLoading: loading,
                      disabled: _allFieldsFilled(),
                      title: "CREATE ACCOUNT",
                      onPressed: () {
                        print(1);
                        // passwordCheck();
                        if (_passwordCheck() == true) {
                          setState(() {
                            loading = true;
                          });
                          _signUp();
                        }
                      },
                    )
                  ],
                ),
        ),
      ),
    );
  }

  Future<void> _signUp() async {
    print(emailTextEditingController.text);
    print(passwordTextEditingController.text);
    try {
      Map<String, String> userAttributes = {
        'email': emailTextEditingController.text,
      };
      // _signOut();
      SignUpResult res = await Amplify.Auth.signUp(
          username: usernameTextEditingController.text,
          password: passwordTextEditingController.text,
          options: CognitoSignUpOptions(userAttributes: userAttributes));
      // setState(() {
      //   loading = false;
      // });
      otpAlertBox(
          title: "Confirm user by clicking on the link sent to your mail",
          context: context,
          stayOnPage: false,
          onPressed: () async {
            // _signOut();
            // if (res.nextStep.) {
            try {
              SignInResult rests = await Amplify.Auth.signIn(
                  username: usernameTextEditingController.text,
                  password: passwordTextEditingController.text);
            } on AuthException catch (e) {
              if (e.message.contains("User not confirmed")) {
                showToast("Please confirm user and try again");
              }
              print(e.message);
            }
            // }
            // } else {
            //   // print(res.nextStep.signUpStep.toString());
            // }
            print("working");
            print(res.isSignUpComplete.toString());
            print(res.nextStep.signUpStep.toString());
            print(res.nextStep.additionalInfo.toString());
            print(res.nextStep.codeDeliveryDetails.deliveryMedium);
            print(res.nextStep.codeDeliveryDetails.attributeName);
            print(res.nextStep.codeDeliveryDetails.destination);
            // if (res.isSignUpComplete) {
            CognitoAuthSession rest = await Amplify.Auth.fetchAuthSession(
                options: CognitoSessionOptions(getAWSCredentials: true));
            setState(() {
              ObjectFactory().appHive.putIsSignIn(rest.isSignedIn.toString());
              ObjectFactory()
                  .appHive
                  .putToken(token: rest.userPoolTokens.idToken);
              pop(context);
              userBloc.getBasicUserDetails();
              print(rest.isSignedIn.toString());
              print(rest.userPoolTokens.idToken);
            });
          }
          // }
          );
    } on AuthException catch (e) {
      print("e" + e.message);
      showToast(e.message);
      if (e.message.contains("User not confirmed")) {
        showToast("Please confirm user and try again");
      }
      print("signedin");
    }
  }

  void _signOut() async {
    try {
      await Amplify.Auth.signOut();
      print("signed out");
    } on AuthException catch (e) {
      print("e" + e.message);
    }
  }

  void _configureAmplify() async {
    if (!mounted) return;
    Amplify.addPlugin(AmplifyAuthCognito());
    try {
      await Amplify.configure(amplifyconfig);
    } on AmplifyAlreadyConfiguredException {
      print("Amplify was already configured. Was the app restarted?");
    }
    try {
      setState(() {
        _amplifyConfigured = true;
      });
    } catch (e) {
      print(e);
    }
  }
}
