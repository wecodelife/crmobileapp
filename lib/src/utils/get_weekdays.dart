class GetDay {
  List<String> weekList = [
    "Sun",
    "Mon",
    "Tue",
    "Wed",
    "Thu",
    "Fri",
    "Sat",
  ];
  List<String> weeks = [
    "S",
    "M",
    "T",
    "W",
    "T",
    "F",
    "S",
  ];

  List<String> getWeekDay(String weekday) {
    String day;
    String week;
    switch (weekday) {
      case "1":
        {
          day = weeks[0];
          week = weekList[1];
          break;
        }
      case "2":
        {
          day = weeks[1];
          week = weekList[2];

          break;
        }
      case "3":
        {
          day = weeks[2];
          week = weekList[3];

          break;
        }
      case "4":
        {
          day = weeks[3];
          week = weekList[4];

          break;
        }
      case "5":
        {
          day = weeks[4];
          week = weekList[5];

          break;
        }
      case "6":
        {
          day = weeks[5];
          week = weekList[6];

          break;
        }
      case "7":
        {
          day = weeks[6];
          week = weekList[0];

          break;
        }
    }
    return [day, week];
  }
}
