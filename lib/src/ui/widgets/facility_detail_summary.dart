import 'package:app_template/src/ui/screens/coach_details_page.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/check_box.dart';
import 'package:app_template/src/ui/widgets/profile_picture_chat.dart';
import 'package:app_template/src/ui/widgets/summary_widget_email.dart';
import 'package:app_template/src/ui/widgets/time_slot_app_bar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class FacilityDetailSummary extends StatefulWidget {
  final Function onPressed;
  final String phone;
  final String email;
  final String date;
  final String startTime;
  final String endTime;
  final String sports;
  final String duration;
  final String amount;
  final String name;
  final String sportImage;
  final bool sportsDetails;

  FacilityDetailSummary({
    this.sportsDetails,
    this.onPressed,
    this.date,
    this.email,
    this.duration,
    this.amount,
    this.endTime,
    this.phone,
    this.sports,
    this.startTime,
    this.name,
    this.sportImage,
  });

  @override
  _FacilityDetailSummaryState createState() => _FacilityDetailSummaryState();
}

class _FacilityDetailSummaryState extends State<FacilityDetailSummary> {
  bool checked = false;
  bool disabled = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 1.9),
      width: screenWidth(context, dividedBy: 1),
      color: Constants.kitGradients[5],
      child: Column(
        children: [
          AppBarTimeSlot(
            pageTitle: "SUMMARY",
            icon: widget.sportsDetails != true ? false : true,
            rightIcon: widget.sportsDetails != true ? true : false,
            pageNavigation: () {
              widget.onPressed();
            },
            onTapRightIcon: () {
              push(context, CoachDetailsPage());
            },
          ),
          Spacer(
            flex: 1,
          ),
          ProfilePictureChat(
            image: "assets/images/user_image.png",
            name: widget.name,
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 20)),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SummaryTileEmail(
                      heading: "Phone:",
                      subheading: widget.phone,
                      width: 3.4,
                    ),
                    SummaryTileEmail(
                      heading: "email:",
                      subheading: widget.email,
                      width: 1.9,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SummaryTileEmail(
                      heading: "Date",
                      subheading: widget.date,
                      width: 3.4,
                    ),
                    SummaryTileEmail(
                      heading: "StarTime",
                      subheading: widget.startTime,
                      width: 3.4,
                    ),
                    SummaryTileEmail(
                      heading: "EndTime",
                      subheading: widget.endTime,
                      width: 3.4,
                    ),
                  ],
                ),
                widget.sportsDetails == true
                    ? Row(
                        children: [
                          SummaryTileEmail(
                            heading: "Sports",
                            subheading: widget.sports,
                            width: 3.4,
                            icon: true,
                            image: widget.sportImage,
                          ),
                          SummaryTileEmail(
                            heading: "Duration",
                            subheading: widget.duration,
                            width: 3.4,
                          ),
                          SummaryTileEmail(
                            heading: "Amount",
                            subheading: "\u0024" + widget.amount,
                            width: 3.4,
                          ),
                        ],
                      )
                    : Container(),
                SizedBox(
                  height: screenHeight(context, dividedBy: 40),
                ),
                Row(
                  children: [
                    CheckBox(
                      checked: checked,
                      onTap: () {
                        setState(() {
                          checked = !checked;
                          if (checked == true) {
                            disabled = false;
                          } else {
                            disabled = true;
                          }
                        });
                      },
                    ),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 50),
                    ),
                    RichText(
                      text: TextSpan(
                        text: ' I agree with Simplybookme.',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: screenWidth(context, dividedBy: 35),
                            fontFamily: "OpenSansRegular"),
                        children: <TextSpan>[
                          TextSpan(
                              text: "Terms/Conditions/Privacy",
                              style: TextStyle(
                                  fontSize: screenWidth(context, dividedBy: 35),
                                  fontFamily: "OpenSansSemiBold",
                                  color: Colors.blueAccent,
                                  decoration: TextDecoration.underline)),
                          TextSpan(
                              text: "",
                              style: TextStyle(
                                  fontSize: screenWidth(context, dividedBy: 35),
                                  fontFamily: "OpenSansSemiBold",
                                  color: Colors.white)),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
          Spacer(
            flex: 3,
          ),
          BuildButton(
            title: "Submit",
            onPressed: () {
              widget.onPressed();
            },
            disabled: disabled,
          ),
          Spacer(
            flex: 2,
          )
        ],
      ),
    );
  }
}
