import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SearchBar extends StatefulWidget {
  final String hintText;
  final Function onTap;
  final TextEditingController textEditingController;
  final bool rightIcon;
  SearchBar(
      {this.textEditingController, this.hintText, this.rightIcon, this.onTap});
  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 20),
      decoration: BoxDecoration(
        color: Colors.black,
      ),
      child: TextField(
        cursorColor: Colors.white,
        controller: widget.textEditingController,
        onTap: () {
          widget.onTap();
          print("tttt");
        },
        style: TextStyle(backgroundColor: Colors.black),
        decoration: InputDecoration(
            focusColor: Colors.black,
            hintText: widget.hintText,
            hintStyle: TextStyle(color: Colors.white),
            prefixIcon: Container(
              width: 8,
              height: 8,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: SvgPicture.asset("assets/icons/search_icon.svg"),
              ),
            ),
            suffixIcon: widget.rightIcon == true
                ? Container(
                    width: screenWidth(context, dividedBy: 20),
                    child: Icon(
                      Icons.clear,
                      size: 25,
                      color: Colors.white,
                    ),
                  )
                : Container(width: 1),
            border: InputBorder.none),
      ),
    );
  }
}
