// To parse this JSON data, do
//
//     final getBankResponse = getBankResponseFromJson(jsonString);

import 'dart:convert';

GetBankResponse getBankResponseFromJson(String str) =>
    GetBankResponse.fromJson(json.decode(str));

String getBankResponseToJson(GetBankResponse data) =>
    json.encode(data.toJson());

class GetBankResponse {
  GetBankResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final List<Datum> data;

  factory GetBankResponse.fromJson(Map<String, dynamic> json) =>
      GetBankResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.name,
  });

  final int id;
  final String name;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
      };
}
