// To parse this JSON data, do
//
//     final getRateByViewResponse = getRateByViewResponseFromJson(jsonString);

import 'dart:convert';

GetRateByViewResponse getRateByViewResponseFromJson(String str) =>
    GetRateByViewResponse.fromJson(json.decode(str));

String getRateByViewResponseToJson(GetRateByViewResponse data) =>
    json.encode(data.toJson());

class GetRateByViewResponse {
  GetRateByViewResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final List<DatumRate> data;

  factory GetRateByViewResponse.fromJson(Map<String, dynamic> json) =>
      GetRateByViewResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null
            ? null
            : List<DatumRate>.from(
                json["data"].map((x) => DatumRate.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DatumRate {
  DatumRate({
    this.id,
    this.value,
    this.order,
  });

  final int id;
  final String value;
  final int order;

  factory DatumRate.fromJson(Map<String, dynamic> json) => DatumRate(
        id: json["id"] == null ? null : json["id"],
        value: json["value"] == null ? null : json["value"],
        order: json["order"] == null ? null : json["order"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "value": value == null ? null : value,
        "order": order == null ? null : order,
      };
}
