// To parse this JSON data, do
//
//     final updateUserTypeResponseModel = updateUserTypeResponseModelFromJson(jsonString);

import 'dart:convert';

UpdateUserTypeResponseModel updateUserTypeResponseModelFromJson(String str) =>
    UpdateUserTypeResponseModel.fromJson(json.decode(str));

String updateUserTypeResponseModelToJson(UpdateUserTypeResponseModel data) =>
    json.encode(data.toJson());

class UpdateUserTypeResponseModel {
  UpdateUserTypeResponseModel({
    this.message,
    this.status,
  });

  final String message;
  final int status;

  factory UpdateUserTypeResponseModel.fromJson(Map<String, dynamic> json) =>
      UpdateUserTypeResponseModel(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
      };
}
