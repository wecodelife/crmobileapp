import 'dart:io';

import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_profile_response.dart';
import 'package:app_template/src/ui/screens/edit_sport_page.dart';
import 'package:app_template/src/ui/screens/gal.dart';
import 'package:app_template/src/ui/widgets/appbar_cr.dart';
import 'package:app_template/src/ui/widgets/bottom_navigation_bar_widget.dart';
import 'package:app_template/src/ui/widgets/information_widget.dart';
import 'package:app_template/src/ui/widgets/list_tile_add_sports.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final picker = ImagePicker();
  File imagePicked;
  File galleryPhoto;
  bool selectPhoto = false;
  UserBloc userBloc = UserBloc();
  List<File> playerPhotosList = [];
  List<int> iconList = [];
  List<String> sports = [
    "Basketball",
    "Volleyball",
    "Football",
    "Swimming",
    "Baseball",
    "Hockey",
    "Soccer",
    "Tennis"
  ];
  List<String> sportsIcon = [
    "assets/icons/basketball.svg",
    "assets/icons/volleyball.svg",
    "assets/icons/basket_ball_icon.svg",
    "assets/icons/swimming.svg",
    "assets/icons/baseball.svg",
    "assets/icons/hockey.svg",
    "assets/icons/soccer.svg",
    "assets/icons/tennis.svg"
  ];

  Future getImageGallery() async {
    final pickedFile =
        await picker.getImage(source: ImageSource.gallery, imageQuality: 25);
    if (pickedFile != null) {
      setState(() {
        imagePicked = File(pickedFile.path);
        playerPhotosList.add(imagePicked);
      });
    }
  }

  Future getImageCamera() async {
    final pickedFile =
        await picker.getImage(source: ImageSource.camera, imageQuality: 25);
    if (pickedFile != null) {
      setState(() {
        imagePicked = File(pickedFile.path);
      });
    }
  }

  @override
  void initState() {
    userBloc.getProfile();
    userBloc.getProfileResponse.listen((event) {
      if (event.status == 200) {
        for (int i = 0; i < event.data.sports.length; i++) {
          iconList.add(event.data.sports[i].id);
        }
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: Container(),
          actions: [
            AppBarCr(
              pageTitle: "EDIT PROFILE",
              rightTextYes: true,
              rightText: "Done",
              leftIcon: "assets/icons/back_button.svg",
              onTapLeftIcon: () {},
              onTapRightIcon: () {
                pop(context);
              },
            ),
          ],
        ),
        bottomNavigationBar: BottomNavigationWidget(),
        body: StreamBuilder<GetProfileResponse>(
            stream: userBloc.getProfileResponse,
            builder: (context, snapshot) {
              return ListView(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 20)),
                    child: Column(
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 40),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {});
                            bottomSheetAddPhotos(context);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                width: screenWidth(context, dividedBy: 70),
                              ),
                              Stack(
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(100),
                                    child: Container(
                                        width:
                                            screenWidth(context, dividedBy: 2),
                                        height:
                                            screenWidth(context, dividedBy: 2),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            shape: BoxShape.circle),
                                        child: Image(
                                          image: AssetImage(
                                              "assets/images/user_image.png"),
                                        )),
                                  ),
                                  Column(
                                    children: [
                                      SizedBox(
                                        height: screenHeight(context,
                                            dividedBy: 5.4),
                                      ),
                                      Row(
                                        children: [
                                          SizedBox(
                                            width: screenWidth(context,
                                                dividedBy: 2.9),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              push(context, GalleryScreen());
                                            },
                                            child: CircleAvatar(
                                              backgroundColor:
                                                  Constants.kitGradients[1],
                                              radius: screenWidth(context,
                                                  dividedBy: 20),
                                              child: Icon(
                                                Icons.camera_alt_rounded,
                                                color: Colors.white,
                                                size: screenWidth(context,
                                                    dividedBy: 15),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                        InformationWidget(
                            label: "UserName", text: snapshot.data.data.name
                            // "Child 1",
                            ),
                        Divider(
                          thickness: 2.0,
                          color: Colors.white12,
                        ),
                        InformationWidget(
                            label: "Email", text: snapshot.data.data.email
                            // "dansmith2051@gmail.com",
                            ),
                        Divider(
                          thickness: 2.0,
                          color: Colors.white24,
                        ),
                        InformationWidget(
                            label: "Phone", text: snapshot.data.data.phone
                            // "46576778798",
                            ),
                        Divider(
                          thickness: 2.0,
                          color: Colors.white24,
                        ),
                        InformationWidget(
                          label: "Gender",
                          text: "Male",
                        ),
                        Divider(
                          thickness: 2.0,
                          color: Colors.white24,
                        ),
                        InformationWidget(
                            label: "Date of Birth",
                            text: snapshot.data.data.dateOfBirth
                                .toString()
                                .split(" ")
                                .first
                            // "26/11/2020",
                            ),
                        Divider(
                          thickness: 2.0,
                          color: Colors.white24,
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 100),
                        ),
                        Row(
                          children: [
                            Text(
                              "Sports",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontFamily: "OpenSansRegular",
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 60),
                        ),
                        Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                push(
                                    context,
                                    EditSportsPage(
                                      // athleteId: ,
                                      sportId: iconList,
                                      icon: sportsIcon,
                                      label: sports,
                                    ));
                              },
                              child: Container(
                                  height: screenHeight(context, dividedBy: 7),
                                  width: screenWidth(context, dividedBy: 5),
                                  child: Column(
                                    children: [
                                      Icon(
                                        Icons.add,
                                        color: Constants.kitGradients[1],
                                        size: 60,
                                      ),
                                      Text(
                                        "Add",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: 'OpenSansRegular',
                                        ),
                                      )
                                    ],
                                  )),
                            ),
                            Container(
                              width: screenWidth(context, dividedBy: 1.45),
                              height: screenHeight(context, dividedBy: 8),
                              child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: snapshot.data.data.sports.length,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return ListTileAddSports(
                                        image: snapshot
                                            .data.data.sports[index].icon,
                                        // "assets/icons/basket_ball_icon.svg",
                                        label: snapshot
                                            .data.data.sports[index].name
                                        // "Football",
                                        );
                                  }),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 30),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 30),
                        )
                      ],
                    ),
                  ),
                ],
              );
            }),
      ),
    );
  }

  void bottomSheetAddPhotos(context) {
    showModalBottomSheet<dynamic>(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (BuildContext context, setState) {
            return Container(
                color: Colors.white,
                height: screenHeight(context, dividedBy: 4),
                width: screenWidth(context, dividedBy: 1),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          right: screenWidth(context, dividedBy: 10)),
                      child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Cancel",
                            style: TextStyle(
                              color: Constants.kitGradients[1],
                              fontFamily: "Poppins",
                              fontSize: 14,
                            ),
                          )),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                            onTap: () {
                              getImageCamera();
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.camera,
                              size: 50,
                              color: Constants.kitGradients[1],
                            )),
                        GestureDetector(
                            onTap: () {
                              getImageGallery();
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.image,
                              size: 50,
                              color: Constants.kitGradients[1],
                            )),
                      ],
                    ),
                  ],
                ));
          });
        });
  }
}
