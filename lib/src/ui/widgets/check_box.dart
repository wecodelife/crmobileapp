import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CheckBox extends StatefulWidget {
  bool checked;
  Function onTap;
  CheckBox({this.checked, this.onTap});

  @override
  _CheckBoxState createState() => _CheckBoxState();
}

class _CheckBoxState extends State<CheckBox> {
  bool checked = false;
  @override
  Widget build(BuildContext context) {
    return Container(
        child: GestureDetector(
            onTap: widget.onTap,
            child: Container(
              height: screenHeight(context, dividedBy: 20),
              width: screenWidth(context, dividedBy: 15),
              child: widget.checked == true
                  ? SvgPicture.asset("assets/icons/check_box_checked.svg")
                  : SvgPicture.asset("assets/icons/check_box_unchecked1.svg"),
            )));
  }
}
