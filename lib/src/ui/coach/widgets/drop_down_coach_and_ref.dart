import 'package:app_template/src/models/get_age_range_response.dart';
import 'package:app_template/src/models/get_rate_by_view.dart';
import 'package:app_template/src/models/get_session_increment_response.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DropDownCoachAndRef extends StatefulWidget {
  final List<String> dropDownList;
  final List<DatumAge> dropDownAgeList;
  final List<DatumSession> dropDownSession;
  final List<DatumRate> dropDownRate;
  final String dropDownType;
  final String hintText;
  final String label;
  final String dropDownValue;
  // final String dropDownValue;
  final ValueChanged onClicked;
  DropDownCoachAndRef(
      {this.hintText,
      this.label,
      this.dropDownList,
      this.dropDownValue,
      this.dropDownAgeList,
      this.onClicked,
      this.dropDownType,
      this.dropDownSession,
      this.dropDownRate});
  @override
  _DropDownCoachAndRefState createState() => _DropDownCoachAndRefState();
}

String dropdownValue;

class _DropDownCoachAndRefState extends State<DropDownCoachAndRef> {
  @override
  void initState() {
    // type = String;
    // TODO: implement initState
    super.initState();
  }

  // Type dropDownType = String;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: TextStyle(
              color: Colors.white, fontSize: 12, fontFamily: 'OpenSansRegular'),
        ),
        Container(
          height: screenHeight(context, dividedBy: 14),
          width: screenWidth(context, dividedBy: 1.24),
          // color: Colors.red,
          child: DropdownButton(
            underline: Divider(
              thickness: 1.0,
              color: Constants.kitGradients[0],
            ),
            icon: Row(
              children: [
                Icon(
                  Icons.arrow_drop_down,
                  size: 30,
                  color: Constants.kitGradients[0],
                ),
              ],
            ),
            iconSize: 20,
            dropdownColor: Colors.black,
            value: widget.dropDownValue == ""
                ? dropdownValue
                : widget.dropDownValue,
            style: TextStyle(fontSize: 16, color: Constants.kitGradients[0]),
            items: widget.dropDownList
                .map<DropdownMenuItem<String>>(
                    (String value) => DropdownMenuItem<String>(
                        value: value,
                        child: Container(
                            width: screenWidth(context, dividedBy: 1.4),
                            height: screenHeight(context, dividedBy: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  value,
                                  style:
                                      TextStyle(fontFamily: 'OpenSansRegular'),
                                ),
                              ],
                            ))))
                .toList(),
            onChanged: (selectedValue) {
              setState(() {
                print(selectedValue);
              });
              widget.onClicked(selectedValue);
            },
            hint: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  widget.hintText,
                  style:
                      TextStyle(fontSize: 18, color: Constants.kitGradients[0]),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
