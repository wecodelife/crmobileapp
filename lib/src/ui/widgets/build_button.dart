import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class BuildButton extends StatefulWidget {
  final String title;
  final Function onPressed;
  final bool transparent;
  final bool faultReport;
  final bool disabled;
  final bool arrowIcon;
  final bool isLoading;
  BuildButton(
      {this.title,
      this.onPressed,
      this.transparent,
      this.disabled,
      this.arrowIcon,
      this.faultReport,
      this.isLoading});
  @override
  _BuildButtonState createState() => _BuildButtonState();
}

class _BuildButtonState extends State<BuildButton> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: screenHeight(context, dividedBy: 20),
        width: screenWidth(context, dividedBy: 1.08),
        child: widget.transparent == true
            ? RaisedButton(
                color: Colors.transparent,
                shape: RoundedRectangleBorder(
                    side: BorderSide(color: Constants.kitGradients[1])),
                onPressed: () {
                  if (widget.disabled != true) widget.onPressed();
                },
                child: Center(
                    child: Text(
                  widget.title,
                  style: TextStyle(
                      color: Constants.kitGradients[0],
                      fontFamily: 'OswaldBold',
                      fontSize: 16),
                )),
              )
            : RaisedButton(
                color: widget.disabled == true
                    ? Colors.transparent
                    : widget.faultReport == true
                        ? Constants.kitGradients[13]
                        : Theme.of(context).buttonColor,
                shape: RoundedRectangleBorder(
                    side: widget.disabled == true
                        ? BorderSide(color: Constants.kitGradients[1])
                        : BorderSide.none,
                    borderRadius: BorderRadius.circular(0)),
                onPressed: () {
                  if (widget.disabled != true) widget.onPressed();
                },
                child: Center(
                  child: widget.isLoading != true
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              widget.title,
                              style: TextStyle(
                                  color: widget.disabled == true
                                      ? Constants.kitGradients[0]
                                          .withOpacity(0.38)
                                      : Constants.kitGradients[0],
                                  fontFamily: 'OswaldBold',
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                            widget.arrowIcon == true
                                ? Icon(
                                    Icons.arrow_right_alt_sharp,
                                    color: Colors.white,
                                    size: 20,
                                  )
                                : Container()
                          ],
                        )
                      : Container(
                          height: screenHeight(context, dividedBy: 20),
                          width: screenWidth(context, dividedBy: 10),
                          child: CircularProgressIndicator(
                            valueColor: new AlwaysStoppedAnimation<Color>(
                                Constants.kitGradients[0]),
                          ),
                        ),
                ),
              ));
  }
}
