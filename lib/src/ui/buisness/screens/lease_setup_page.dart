import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/add_facility_rate_request.dart';
import 'package:app_template/src/ui/buisness/screens/business_banking_setup.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/drop_down_cr_app.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class LeaseSetupPage extends StatefulWidget {
  final int facilityId;
  LeaseSetupPage({this.facilityId});
  @override
  _LeaseSetupPageState createState() => _LeaseSetupPageState();
}

class _LeaseSetupPageState extends State<LeaseSetupPage> {
  TextEditingController addressTextEditingController =
      new TextEditingController();
  TextEditingController rateAmountTextEditingController =
      new TextEditingController();
  String dropDownFees;
  UserBloc userBloc = UserBloc();
  String dropDownDiscount;
  String dropDownLeaseRate = "";
  int dropDownLeaseRateId;
  String dropDownSelectedValue;
  List<String> sports = [
    "Basketball",
    "Volleyball",
    "Football",
    "Swimming",
    "Baseball",
    "Hockey",
    "Soccer",
    "Tennis"
  ];
  List<String> sportsIcon = [
    "assets/icons/basketball.svg",
    "assets/icons/volleyball.svg",
    "assets/icons/basket_ball_icon.svg",
    "assets/icons/swimming.svg",
    "assets/icons/baseball.svg",
    "assets/icons/hockey.svg",
    "assets/icons/soccer.svg",
    "assets/icons/tennis.svg"
  ];
  List<String> leaseRateList = [];
  List<int> leaseRateListId = [];
  List<String> feesList = ["Yes", "No"];
  List<String> discountRate = ["Yes", "No"];
  bool loading = true;
  @override
  void initState() {
    userBloc.getLeaseRates();
    userBloc.getLeaseRateResponse.listen((event) {
      for (int i = 0; i < event.data.length; i++) {
        leaseRateList.add(event.data[i].name);
        leaseRateListId.add(event.data[i].id);
      }
      setState(() {
        loading = false;
      });
    });
    userBloc.addFacilityRateResponse.listen((event) {
      setState(() {
        loading = false;
      });
      push(
          context,
          BankingSetUp(
            facilityId: widget.facilityId,
          ));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[6],
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: loading == true
          ? Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 12)),
              child: ListView(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  TextProgressIndicator(
                    heading: "LETS GET YOUR LEASE AND RATE SET UP",
                    descriptionText:
                        "Set up how much it costs for people to come and use your facility and equipment.",
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  OnBoardingProgressIndicator(
                    width: 1.6,
                    progressIndicator: true,
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 20),
                  ),
                  Row(
                    children: [
                      Text(
                        "Lease Rate",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'OpenSansRegular',
                            fontSize: 16),
                      ),
                    ],
                  ),
                  DropDownCrApp(
                    title: "Select",
                    dropDownList: leaseRateList,
                    dropDownValue: dropDownLeaseRate,
                    onClicked: (value) {
                      setState(() {
                        dropDownLeaseRate = value;
                        dropDownLeaseRateId =
                            leaseRateListId[leaseRateList.indexOf(value)];
                      });
                      print(dropDownLeaseRateId.toString());
                    },
                  ),
                  FormFeildUserDetails(
                    textEditingController: rateAmountTextEditingController,
                    labelText: "Rate Amount",
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  Row(
                    children: [
                      Text(
                        "Do you Have Fees?",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'OpenSansRegular',
                            fontSize: 16),
                      ),
                    ],
                  ),
                  Container(
                    height: screenHeight(context, dividedBy: 10),
                    child: DropDownCrApp(
                      title: "Select",
                      dropDownList: feesList,
                      dropDownValue: dropDownFees,
                      onClicked: (value) {
                        setState(() {
                          dropDownFees = value;
                        });
                      },
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  Row(
                    children: [
                      Text(
                        "Do you Have Discount?",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'OpenSansRegular',
                            fontSize: 16),
                      ),
                    ],
                  ),
                  DropDownCrApp(
                    title: "Select",
                    dropDownList: discountRate,
                    dropDownValue: dropDownDiscount,
                    onClicked: (value) {
                      setState(() {
                        dropDownDiscount = value;
                      });
                    },
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 14),
                  ),
                  BuildButton(
                    title: "Next",
                    disabled: false,
                    onPressed: () {
                      setState(() {
                        loading = true;
                      });
                      userBloc.addFacilityRate(
                          addFacilityRateRequest: AddFacilityRateRequest(
                              facility: widget.facilityId,
                              leaseRateType: dropDownLeaseRateId,
                              rateAmount: rateAmountTextEditingController.text,
                              discountsApplicable:
                                  dropDownDiscount == "Yes" ? true : false,
                              feesApplicable:
                                  dropDownFees == "Yes" ? true : false));
                    },
                  )
                ],
              ),
            ),
    );
  }
}
