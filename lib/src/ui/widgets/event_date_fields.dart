import 'package:flutter/material.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';

class EventDateField extends StatefulWidget {
  final label;
  final data;
  EventDateField({this.data,this.label});
  @override
  _EventDateFieldState createState() => _EventDateFieldState();
}

class _EventDateFieldState extends State<EventDateField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  widget.label+":",
                  style: TextStyle(
                    color: Constants.kitGradients[0],
                    fontFamily: 'OpenSansRegular',
                    fontSize: 16,
                  ),
                ),
                Text(
                  widget.data,
                  style: TextStyle(
                    color: Constants.kitGradients[0],
                    fontFamily: 'OpenSansRegular',
                    fontSize: 16,
                  ),
                )
              ],
            ),
          ),
          Divider(
            color: Constants.kitGradients[0],
          ),
          SizedBox(height: screenHeight(context, dividedBy: 70),)
        ],
      ),
    );
  }
}
