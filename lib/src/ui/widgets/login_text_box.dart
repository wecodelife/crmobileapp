import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginTextBox extends StatefulWidget {
  final String label;
  final TextEditingController textEditingController;
  LoginTextBox({this.label, this.textEditingController});
  @override
  _LoginTextBoxState createState() => _LoginTextBoxState();
}

class _LoginTextBoxState extends State<LoginTextBox> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.label,
            style: TextStyle(fontSize: 12, color: Constants.kitGradients[0]),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            height: screenHeight(context, dividedBy: 20),
            width: screenWidth(context, dividedBy: 1.08),
            color: Constants.kitGradients[3],
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: widget.textEditingController,
                cursorColor: Colors.white,
                style: TextStyle(color: Colors.white),
              ),
              // child: TextField(
              //   controller: widget.textEditingController,
              //   cursorColor: Colors.white,
              //   style: TextStyle(color: Colors.white),
              //   decoration: InputDecoration(border: InputBorder.none),
              // ),
            ),
          )
        ],
      ),
    );
  }
}
