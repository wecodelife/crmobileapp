import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ChatScreenTextField extends StatefulWidget {
  final String hintText;
  final TextEditingController textEditingController;
  ChatScreenTextField({this.textEditingController, this.hintText});
  @override
  _ChatScreenTextFieldState createState() => _ChatScreenTextFieldState();
}

class _ChatScreenTextFieldState extends State<ChatScreenTextField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: screenWidth(context, dividedBy: 15)),
      child: Row(
        children: [
          Container(
            width: screenWidth(context, dividedBy: 9),
            height: screenHeight(context, dividedBy: 21),
            color: Constants.kitGradients[6],
            child: Icon(
              Icons.camera_alt_rounded,
              color: Colors.white,
            ),
          ),
          Container(
            height: screenHeight(context, dividedBy: 20),
            width: screenWidth(context, dividedBy: 1.22),
            decoration: BoxDecoration(
              color: Colors.black,
            ),
            child: TextField(
              style: TextStyle(color: Colors.white),
              controller: widget.textEditingController,
              cursorColor: Constants.kitGradients[1],
              expands: true,
              maxLines: null,
              minLines: null,
              decoration: InputDecoration(
                  hintText: widget.hintText,
                  contentPadding: EdgeInsets.all(8.0),
                  hintStyle: TextStyle(
                    color: Colors.white,
                    backgroundColor: Colors.black,
                  ),
                  // prefixIconConstraints: BoxConstraints(
                  //     maxWidth: screenWidth(context, dividedBy: 6),
                  //     maxHeight: screenHeight(
                  //       context,
                  //       dividedBy: 2.9,
                  //     ),
                  //     minHeight: screenHeight(
                  //       context,
                  //       dividedBy: 2.9,
                  //     ),
                  //     minWidth: screenWidth(context, dividedBy: 10)),
                  suffixIcon: Container(
                      width: screenWidth(context, dividedBy: 40),
                      height: screenHeight(context, dividedBy: 38),
                      color: Colors.black,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SvgPicture.asset(
                            "assets/icons/message_forward.svg"),
                      )),
                  border: InputBorder.none),
            ),
          ),
        ],
      ),
    );
  }
}
