import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_athlete_list_response.dart';
import 'package:app_template/src/ui/screens/onboarding_parent_athelete_11.dart';
import 'package:app_template/src/ui/screens/profile_page.dart';
import 'package:app_template/src/ui/screens/user_details.dart';
import 'package:app_template/src/ui/widgets/appbar_cr.dart';
import 'package:app_template/src/ui/widgets/bottom_navigation_bar_widget.dart';
import 'package:app_template/src/ui/widgets/select_button.dart';
import 'package:app_template/src/ui/widgets/user_profile_tile.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class UserList extends StatefulWidget {
  @override
  _UserListState createState() => _UserListState();
}

class _UserListState extends State<UserList> {
  UserBloc userBloc = UserBloc();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userBloc.getAthleteList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // bottomNavigationBar: BottomNavigationWidget(),
      appBar: AppBar(leading: Container(), actions: [
        AppBarCr(
          pageTitle: "USERS",
          onTapLeftIcon: () {
            push(context, ProfilePage());
          },
          rightText: "Done",
          leftIcon: "assets/icons/back_button.svg",
          onTapRightIcon: () {
            pop(context);
          },
          rightTextYes: true,
        ),
      ]),
      bottomNavigationBar: BottomNavigationWidget(),
      body: Column(
        children: [
          SizedBox(
            height: screenHeight(context, dividedBy: 50),
          ),
          Expanded(
            child: StreamBuilder<GetAthleteListResponse>(
                stream: userBloc.getAthleteListResponse,
                builder: (context, snapshot) {
                  return ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemCount: snapshot.data.data.length + 1,
                      itemBuilder: (BuildContext context, int index) {
                        return index == 0
                            ? Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                        screenWidth(context, dividedBy: 20)),
                                child: SelectButton(
                                  title: "+ Add User",
                                  onPressed: () {
                                    push(
                                        context,
                                        OnBoardingParentAthlete11(
                                          // athleteCount: 1,
                                          parentId: 2,
                                        ));
                                  },
                                  transparent: true,
                                ),
                              )
                            : Column(
                                children: [
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 40),
                                  ),
                                  UserListTile(
                                    onPressed: () {
                                      push(
                                          context,
                                          UserDetails(
                                            athleteData:
                                                snapshot.data.data[index - 1],
                                          ));
                                    },
                                    athleteData: snapshot.data.data[index - 1],
                                  ),
                                ],
                              );
                      });
                }),
          ),
        ],
      ),
    );
  }
}
