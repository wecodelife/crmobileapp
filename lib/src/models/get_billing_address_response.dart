// To parse this JSON data, do
//
//     final getBillingAddressResponse = getBillingAddressResponseFromJson(jsonString);

import 'dart:convert';

GetBillingAddressResponse getBillingAddressResponseFromJson(String str) =>
    GetBillingAddressResponse.fromJson(json.decode(str));

String getBillingAddressResponseToJson(GetBillingAddressResponse data) =>
    json.encode(data.toJson());

class GetBillingAddressResponse {
  GetBillingAddressResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final List<Datum> data;

  factory GetBillingAddressResponse.fromJson(Map<String, dynamic> json) =>
      GetBillingAddressResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.parent,
    this.name,
    this.phoneNumber,
    this.address,
    this.building,
    this.city,
    this.state,
    this.zip,
    this.isDefault,
  });

  final int id;
  final int parent;
  final String name;
  final String phoneNumber;
  final String address;
  final String building;
  final String city;
  final int state;
  final int zip;
  final bool isDefault;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        parent: json["parent"] == null ? null : json["parent"],
        name: json["name"] == null ? null : json["name"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        address: json["address"] == null ? null : json["address"],
        building: json["building"] == null ? null : json["building"],
        city: json["city"] == null ? null : json["city"],
        state: json["state"] == null ? null : json["state"],
        zip: json["zip"] == null ? null : json["zip"],
        isDefault: json["is_default"] == null ? null : json["is_default"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "parent": parent == null ? null : parent,
        "name": name == null ? null : name,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "address": address == null ? null : address,
        "building": building == null ? null : building,
        "city": city == null ? null : city,
        "state": state == null ? null : state,
        "zip": zip == null ? null : zip,
        "is_default": isDefault == null ? null : isDefault,
      };
}
