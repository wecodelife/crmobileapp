// To parse this JSON data, do
//
//     final addBusinessBankingInfo = addBusinessBankingInfoFromJson(jsonString);

import 'dart:convert';

AddBusinessBankingInfo addBusinessBankingInfoFromJson(String str) => AddBusinessBankingInfo.fromJson(json.decode(str));

String addBusinessBankingInfoToJson(AddBusinessBankingInfo data) => json.encode(data.toJson());

class AddBusinessBankingInfo {
  AddBusinessBankingInfo({
    this.facility,
    this.paymentMethods,
    this.bank,
    this.routingNumber,
    this.accountNumber,
  });

  final int facility;
  final List<int> paymentMethods;
  final int bank;
  final String routingNumber;
  final String accountNumber;

  factory AddBusinessBankingInfo.fromJson(Map<String, dynamic> json) => AddBusinessBankingInfo(
    facility: json["facility"] == null ? null : json["facility"],
    paymentMethods: json["payment_methods"] == null ? null : List<int>.from(json["payment_methods"].map((x) => x)),
    bank: json["bank"] == null ? null : json["bank"],
    routingNumber: json["routing_number"] == null ? null : json["routing_number"],
    accountNumber: json["account_number"] == null ? null : json["account_number"],
  );

  Map<String, dynamic> toJson() => {
    "facility": facility == null ? null : facility,
    "payment_methods": paymentMethods == null ? null : List<dynamic>.from(paymentMethods.map((x) => x)),
    "bank": bank == null ? null : bank,
    "routing_number": routingNumber == null ? null : routingNumber,
    "account_number": accountNumber == null ? null : accountNumber,
  };
}
