import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/summary_widget_email.dart';
import 'package:app_template/src/ui/widgets/time_slot_app_bar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class TrainingSessionBottomSheet extends StatefulWidget {
  Function onPressedReschedule;
  Function onPressedNoShow;
  Function onPressedCancelButton;
  TrainingSessionBottomSheet(
      {this.onPressedNoShow,
      this.onPressedReschedule,
      this.onPressedCancelButton});
  @override
  _TrainingSessionBottomSheetState createState() =>
      _TrainingSessionBottomSheetState();
}

class _TrainingSessionBottomSheetState
    extends State<TrainingSessionBottomSheet> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      width: screenWidth(context, dividedBy: 1),
      //height: screenHeight(context, dividedBy: 1),
      //color: Colors.red,
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: 30),
        ),
        child: Column(
          children: [
            AppBarTimeSlot(
              pageTitle: "TRAINING SESSION",
              pageNavigation: () {},
              rightIcon: true,
              icon: false,
              onTapRightIcon: () {
                pop(context);
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),
            Container(
              width: screenWidth(context, dividedBy: 1),
              padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30),
                vertical: screenHeight(context, dividedBy: 30),
              ),
              decoration: BoxDecoration(
                color: Constants.kitGradients[7],
                borderRadius: BorderRadius.circular(3),
              ),
              child: Column(
                children: [
                  CircleAvatar(
                    radius: screenWidth(context, dividedBy: 10),
                    backgroundImage:
                        NetworkImage("https://via.placeholder.com/150/92c952"),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 80),
                  ),
                  SizedBox(
                    child: Text(
                      "George Jones",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                          fontFamily: 'OpenSSansRegular'),
                    ),
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: screenWidth(context, dividedBy: 30),
                      ),
                      SummaryTileEmail(
                        heading: "Phone",
                        subheading: "(5555)-5555-5555",
                        width: 3.1,
                      ),
                      SummaryTileEmail(
                        heading: "Email",
                        subheading: "george_jones@gmail.com",
                        width: 2.2,
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: screenWidth(context, dividedBy: 30),
                      ),
                      SummaryTileEmail(
                        heading: "Date",
                        subheading: "12/10/2020",
                        width: 3.1,
                      ),
                      SummaryTileEmail(
                        heading: "Start Time",
                        subheading: "09:00AM",
                        width: 3.95,
                      ),
                      SummaryTileEmail(
                          heading: "End Time",
                          subheading: "11:00AM",
                          width: 3.95),
                    ],
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: screenWidth(context, dividedBy: 30),
                      ),
                      SummaryTileEmail(
                        icon: true,
                        image: "assets/icons/soccer_icon.svg",
                        heading: "Sport",
                        subheading: "Football",
                        width: 3.1,
                      ),
                      SummaryTileEmail(
                        heading: "Duration",
                        subheading: "1 Hours",
                        width: 3.95,
                      ),
                      SummaryTileEmail(
                        heading: "Rate",
                        subheading: "\$40",
                        width: 3.95,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 20),
              //color: Colors.green,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: screenWidth(context, dividedBy: 2.3),
                    child: BuildButton(
                      title: "RESCHEDULE",
                      transparent: true,
                      onPressed: () {
                        widget.onPressedReschedule();
                      },
                    ),
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 30),
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 2.3),
                    child: BuildButton(
                      title: "NO SHOW",
                      transparent: true,
                      disabled: false,
                      onPressed: () {
                        widget.onPressedNoShow();
                      },
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            BuildButton(
              title: "CANCEL BOOKING",
              disabled: false,
              onPressed: () {
                widget.onPressedCancelButton();
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
          ],
        ),
      ),
    ));
  }
}
