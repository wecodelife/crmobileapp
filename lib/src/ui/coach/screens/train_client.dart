import 'package:app_template/src/models/add_coach_request.dart';
import 'package:app_template/src/ui/coach/screens/reserve_and_pay_for_client.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class TrainClient extends StatefulWidget {
  final List<TrainerSport> sports;
  TrainClient({this.sports});
  @override
  _TrainClientState createState() => _TrainClientState();
}

class _TrainClientState extends State<TrainClient> {
  bool yes = false;
  bool no = false;
  bool mayBe = false;
  bool transparentYes = true;
  bool transparentNo = true;
  bool transparentMayBe = true;
  bool disabled = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[3],
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 20)),
        child: Column(
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            TextProgressIndicator(
              heading: "WILL YOU TRAIN CLIENTS AT THEIR HOMES",
              descriptionText:
                  "Do you coaches to travel to clients home to train them? If not that's okay.",
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            OnBoardingProgressIndicator(
              width: 3.2,
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 5),
            ),
            BuildButton(
              title: "YES",
              transparent: transparentYes,
              onPressed: () {
                setState(() {
                  yes = true;
                  transparentYes = false;
                  no = false;
                  transparentNo = true;
                  mayBe = false;
                  transparentMayBe = true;
                  disabled = false;
                });
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            BuildButton(
              title: "NO",
              transparent: transparentNo,
              onPressed: () {
                setState(() {
                  no = true;
                  transparentNo = false;
                  yes = false;
                  transparentYes = true;
                  mayBe = false;
                  transparentMayBe = true;
                  disabled = false;
                });
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            BuildButton(
              title: "MAYBE",
              transparent: transparentMayBe,
              onPressed: () {
                setState(() {
                  mayBe = true;
                  transparentMayBe = false;
                  yes = false;
                  transparentYes = true;
                  no = false;
                  transparentNo = true;
                  disabled = false;
                });
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 3.9),
            ),
            BuildButton(
                title: "NEXT",
                disabled: disabled,
                onPressed: () {
                  ObjectFactory().appHive.putTrainingAtHome(
                      value: yes == true
                          ? "1"
                          : no == true
                              ? "2"
                              : "3");
                  push(
                      context,
                      ReserveAndPayForClient(
                        sports: widget.sports,
                      ));
                })
          ],
        ),
      ),
    );
  }
}
