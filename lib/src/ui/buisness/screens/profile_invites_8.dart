import 'package:app_template/src/ui/buisness/screens/profile2_page.dart';
import 'package:app_template/src/ui/widgets/appbar_cr.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProfileInvites8 extends StatefulWidget {
  @override
  _ProfileInvites8State createState() => _ProfileInvites8State();
}

class _ProfileInvites8State extends State<ProfileInvites8> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Constants.kitGradients[3],
          appBar: AppBar(
            leading: Container(),
            actions: [
              AppBarCr(
                //transparent: false,
                pageTitle: "INVITES",
                rightTextYes: false,
                noRightIcon: true,
                onTapRightIcon: () {
                  pop(context);
                },
                leftIcon: "assets/icons/back_button.svg",
                onTapLeftIcon: () {
                  push(context, Profile2Page());
                },
              ),
            ],
          ),
          body: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30)),
            child: Container(
                width: screenWidth(context, dividedBy: 1),
                //color:Colors.red,
                child: ListView(children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  TextProgressIndicator(
                    heading: "INVITE PEOPLE",
                    descriptionText:
                        "Know someone who would love this app? Enter their name, phone number or email address and send them an invite!",
                  ),
                  Container(
                    //color:Colors.greenAccent,
                    padding: EdgeInsets.symmetric(
                        vertical: screenHeight(context, dividedBy: 4)),
                    child:
                        SvgPicture.asset("assets/icons/invites_right_icon.svg"),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                ])),
          )),
    );
  }
}
