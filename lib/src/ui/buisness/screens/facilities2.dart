import 'package:flutter/material.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:app_template/src/ui/widgets/home_listview_header.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/list_tile_select_sports.dart';

class Facilities2 extends StatefulWidget {
  @override
  _Facilities2State createState() => _Facilities2State();
}

class _Facilities2State extends State<Facilities2> {
  TextEditingController facilityNameTextEditingController =
      new TextEditingController();
  TextEditingController facilityDesignationTextEditingController =
      new TextEditingController();
  TextEditingController ratePerHourTextEditingController =
      new TextEditingController();
  TextEditingController ratePerDayTextEditingController =
      new TextEditingController();
  bool shrink = false;
  bool onClicked;
  List<String> sports = [
    "Basketball",
    "Volleyball",
    "Football",
    "Swimming",
    "Baseball",
    "Hockey",
    "Soccer",
    "Tennis"
  ];
  List<String> sportsIcon = [
    "assets/icons/basketball.svg",
    "assets/icons/volleyball.svg",
    "assets/icons/basket_ball_icon.svg",
    "assets/icons/swimming.svg",
    "assets/icons/baseball.svg",
    "assets/icons/hockey.svg",
    "assets/icons/soccer.svg",
    "assets/icons/tennis.svg"
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: .77),
                child: Stack(
                  children: [
                    Image(
                      width: screenWidth(context, dividedBy: 1),
                      image:
                          AssetImage("assets/images/login_page_background.jpg"),
                    ),
                    Container(
                      color: Colors.transparent,
                      padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 35),
                      ),
                      width: screenWidth(context, dividedBy: 1),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          IconButton(
                            icon: Icon(Icons.arrow_back),
                            color: Colors.white,
                            onPressed: () {},
                          ),
                          GestureDetector(
                            child:Container(
                              padding: EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 35),),
                              child:SvgPicture.asset("assets/icons/delete.svg"),
                            )
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                top:screenHeight(context, dividedBy: 2),
                child:Container(
                width: screenWidth(context, dividedBy: 1),
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30),
                    vertical: screenHeight(context, dividedBy: 30)),
                decoration: BoxDecoration(
                  color: Color(0xFF000000),
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(8),
                    topLeft: Radius.circular(8),
                  ),
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Facility Name",
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'OpenSansRegular',
                              fontSize: 16),
                        ),
                        GestureDetector(
                            child: Container(
                                child: SvgPicture.asset(
                                    "assets/icons/calendar_icon.svg")))
                      ],
                    ),
                    FormFeildUserDetails(
                      textEditingController: facilityNameTextEditingController,
                      //labelText: "Rate Amount",
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 50),
                    ),
                    Row(
                      children: [
                        Text(
                          "Facility Designation",
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'OpenSansRegular',
                              fontSize: 16),
                        ),
                      ],
                    ),
                    FormFeildUserDetails(
                      textEditingController:
                          facilityDesignationTextEditingController,
                      //labelText: "Rate Amount",
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 50),
                    ),
                    Row(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          width: screenWidth(context, dividedBy: 2.25),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Text(
                                    "Rate Per Hour",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: 'OpenSansRegular',
                                        fontSize: 16),
                                  ),
                                ],
                              ),
                              FormFeildUserDetails(
                                textEditingController:
                                    ratePerHourTextEditingController,
                                //labelText: "Rate Amount",
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: screenWidth(context, dividedBy: 24),
                        ),
                        Container(
                          alignment: Alignment.center,
                          width: screenWidth(context, dividedBy: 2.25),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Text(
                                    "Rate Per Day",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: 'OpenSansRegular',
                                        fontSize: 16),
                                  ),
                                ],
                              ),
                              FormFeildUserDetails(
                                textEditingController:
                                    ratePerHourTextEditingController,
                                //labelText: "Rate Amount",
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 50),
                    ),
                    HomeListViewHeader(
                      onPressedEditIcon: () {},
                      title: "SPORTS",
                      hasBackIcon: false,
                      hasEditIcon: false,
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 100),
                    ),
                    if (shrink == false)
                      Container(
                        width: screenWidth(context, dividedBy: 1),
                        height: screenHeight(context, dividedBy: 6),
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: sportsIcon.length,
                          itemBuilder: (BuildContext context, int index) {
                            return ListTileSelectSports(
                              label: sports[index],
                              icon: sportsIcon[index],
                              onValueChanged: (onClick) {
                                setState(() {
                                  onClicked = onClick;
                                });
                              },
                            );
                          },
                        ),
                      ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 20),
                    ),
                    BuildButton(
                      title: "SAVE",
                      disabled: false,
                      onPressed: () {},
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 50),
                    ),
                  ],
                ),
              ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
