import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/material.dart';

class BackButtonCr extends StatefulWidget {
  final String title;
  final Function onPressed;
  final bool transparent;
  BackButtonCr({this.title, this.onPressed, this.transparent});
  @override
  _BackButtonCrState createState() => _BackButtonCrState();
}

class _BackButtonCrState extends State<BackButtonCr> {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Constants.kitGradients[1],
      onPressed: () {
        widget.onPressed();
      },
      padding: EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          side: BorderSide(color: Constants.kitGradients[1])),
      child: Center(
          child: Text(
        widget.title,
        style: TextStyle(
            color: Constants.kitGradients[0],
            fontFamily: 'MuliBold',
            fontSize: 14),
      )),
    );
    ;
  }
}
