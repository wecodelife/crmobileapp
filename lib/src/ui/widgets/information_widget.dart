import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class InformationWidget extends StatefulWidget {
  final String label;
  final String text;

  InformationWidget({this.label, this.text});
  @override
  _InformationWidgetState createState() => _InformationWidgetState();
}

class _InformationWidgetState extends State<InformationWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          Row(
            children: [
              SizedBox(
                width: screenWidth(context, dividedBy: 400),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 50),
              ),
              Flexible(
                flex: 1,
                child: Container(
                  width: screenWidth(context, dividedBy: 3.25),
                  child: Text(
                    widget.label,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: screenWidth(context, dividedBy: 22),
                      fontFamily: "OpenSansRegular",
                    ),
                  ),
                ),
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.7),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Flexible(
                      flex: 0,
                      child: Container(
                        width: screenWidth(context, dividedBy: 1.7),
                        child: Text(
                          widget.text,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: screenWidth(context, dividedBy: 22),
                              fontFamily: "OpenSansSemiBold",
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 120),
          )
        ],
      ),
    );
  }
}
