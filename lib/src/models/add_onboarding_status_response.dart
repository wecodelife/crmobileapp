// To parse this JSON data, do
//
//     final addOnBoardStatusResponse = addOnBoardStatusResponseFromJson(jsonString);

import 'dart:convert';

AddOnBoardStatusResponse addOnBoardStatusResponseFromJson(String str) =>
    AddOnBoardStatusResponse.fromJson(json.decode(str));

String addOnBoardStatusResponseToJson(AddOnBoardStatusResponse data) =>
    json.encode(data.toJson());

class AddOnBoardStatusResponse {
  AddOnBoardStatusResponse({
    this.message,
    this.status,
  });

  final String message;
  final int status;

  factory AddOnBoardStatusResponse.fromJson(Map<String, dynamic> json) =>
      AddOnBoardStatusResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
      };
}
