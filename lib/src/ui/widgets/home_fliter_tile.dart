import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeFilterTile extends StatefulWidget {
  final String label;
  final Function onPressed;
  final bool colorChange;
  final Color color;
  final int index;

  HomeFilterTile(
      {this.label, this.onPressed, this.colorChange, this.color, this.index});

  @override
  _HomeFilterTileState createState() => _HomeFilterTileState();
}

class _HomeFilterTileState extends State<HomeFilterTile> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: () {
          widget.onPressed();
        },
        child: Container(
          height: screenHeight(context, dividedBy: 20),
          decoration: BoxDecoration(
              color: widget.colorChange == true
                  ? Constants.kitGradients[1]
                  : Constants.kitGradients[11],
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Constants.kitGradients[1])),
          child: Center(
            child: Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 30),
                ),
                Text(
                  widget.label,
                  style: TextStyle(
                      fontFamily: "SFProText",
                      fontSize: 14,
                      color: Constants.kitGradients[0]),
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 50),
                ),
                Container(
                  width: 2,
                  height: 17,
                  color: Colors.white,
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 50),
                ),
                SvgPicture.asset("assets/icons/down_icon.svg",
                    height: screenWidth(context, dividedBy: 30),
                    width: screenWidth(context, dividedBy: 30)),
                SizedBox(
                  width: screenWidth(context, dividedBy: 30),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
