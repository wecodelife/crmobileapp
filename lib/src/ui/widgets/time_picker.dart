import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';

class TimePicker extends StatefulWidget {
  ValueChanged<String> time;
  TimePicker({this.time});
  @override
  _TimePickerState createState() => _TimePickerState();
}

class _TimePickerState extends State<TimePicker> {
  String _setTime, _setDate;

  String _hour, _minute, _time;

  String dateTime;
  bool selected;

  TimeOfDay selectedTime = TimeOfDay(hour: 00, minute: 00);

  TextEditingController _timeController = TextEditingController();
  Future<Null> _selectTime(BuildContext context) async {
    TimeOfDay picked = await showTimePicker(
        context: context,
        initialTime: selectedTime,
        builder: (BuildContext context, Widget child) {
          return Theme(
              data: ThemeData.light().copyWith(
                backgroundColor: Constants.kitGradients[3],
                canvasColor: Constants.kitGradients[3],
                cardColor: Constants.kitGradients[3],
                primaryColor: Constants.kitGradients[3],
                accentColor: Constants.kitGradients[3],
                colorScheme:
                    ColorScheme.light(primary: Constants.kitGradients[1]),
                buttonTheme:
                    ButtonThemeData(textTheme: ButtonTextTheme.primary),
              ),
              child: child);
        });
    if (picked != null)
      setState(() {
        selected = true;
        selectedTime = picked;
        _hour = selectedTime.hour.toString();
        _minute = selectedTime.minute.toString();
        _time = _hour + ' : ' + _minute;
        _timeController.text = _time;
        dateTime = formatDate(
            DateTime(2019, 08, 1, selectedTime.hour, selectedTime.minute),
            [hh, ':', nn, " ", am]);
        widget.time(selectedTime.toString().substring(10, 15));
      });
    print(selectedTime.toString().substring(10, 15));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        onTap: () {
          _selectTime(context);
        },
        child: Card(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10))),
          elevation: 3,
          child: Container(
            width: screenWidth(context, dividedBy: 5),
            height: screenHeight(context, dividedBy: 15),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(color: Constants.kitGradients[1])),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  selected == true
                      ? Text(
                          "$dateTime",
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: Constants.kitGradients[2]),
                        )
                      : Text("Pick Time",
                          style: TextStyle(
                              color: Constants.kitGradients[2],
                              fontFamily: 'Poppins',
                              fontSize: 14)),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
