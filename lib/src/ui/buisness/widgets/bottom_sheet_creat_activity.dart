import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/event_date_fields.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/time_slot_app_bar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class BottomSheetCreateActivity extends StatefulWidget {
  Function onPressed;
  BottomSheetCreateActivity({this.onPressed});
  @override
  _BottomSheetCreateActivityState createState() =>
      _BottomSheetCreateActivityState();
}

class _BottomSheetCreateActivityState extends State<BottomSheetCreateActivity> {
  TextEditingController titleTextEditingController =
      new TextEditingController();
  TextEditingController contactTextEditingController =
      new TextEditingController();
  TextEditingController phoneTextEditingController =
      new TextEditingController();
  TextEditingController emailTextEditingController =
      new TextEditingController();
  TextEditingController locationTextEditingController =
      new TextEditingController();
  TextEditingController activityTypeTextEditingController =
      new TextEditingController();
  TextEditingController workerTypeTextEditingController =
      new TextEditingController();
  bool disabled = true;

  bool isValid = false;
  void emailValidator() {
    isValid = EmailValidator.validate(emailTextEditingController.text);
  }

  FToast fToast;
  void didUpdate() {
    if (activityTypeTextEditingController.text != null &&
        workerTypeTextEditingController.text != null &&
        titleTextEditingController.text != null &&
        contactTextEditingController.text != null &&
        phoneTextEditingController.text != null &&
        emailTextEditingController.text != null &&
        locationTextEditingController.text != null) {
      setState(() {
        disabled = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      //height: screenHeight(context, dividedBy: 1),
      color: Constants.kitGradients[3],
      child: Column(
        children: [
          AppBarTimeSlot(
            pageTitle: "ACTIVITY",
            pageNavigation: () {},
            rightIcon: true,
            icon: false,
            onTapRightIcon: () {
              pop(context);
            },
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 100),
          ),
          Container(
            height: screenHeight(context, dividedBy: 2),
            color: Constants.kitGradients[11],
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
            ),
            child: ListView(
              children: [
                FormFeildUserDetails(
                  textEditingController: activityTypeTextEditingController,
                  labelText: "Activity Type",
                  onValueChanged: (value) {
                    print(activityTypeTextEditingController);
                    didUpdate();
                  },
                ),
                FormFeildUserDetails(
                  textEditingController: workerTypeTextEditingController,
                  labelText: "Worker Type",
                  onValueChanged: (value) {
                    didUpdate();
                  },
                ),
                FormFeildUserDetails(
                  textEditingController: titleTextEditingController,
                  labelText: "Title",
                  onValueChanged: (value) {
                    didUpdate();
                  },
                ),
                FormFeildUserDetails(
                  textEditingController: contactTextEditingController,
                  labelText: "Contact",
                  onValueChanged: (value) {
                    didUpdate();
                  },
                ),
                FormFeildUserDetails(
                  textEditingController: phoneTextEditingController,
                  labelText: "Phone",
                  onValueChanged: (value) {
                    didUpdate();
                  },
                ),
                FormFeildUserDetails(
                  textEditingController: emailTextEditingController,
                  labelText: "Email",
                  onValueChanged: (value) {
                    didUpdate();
                  },
                ),
                FormFeildUserDetails(
                  textEditingController: locationTextEditingController,
                  labelText: "Location",
                  onValueChanged: (value) {
                    didUpdate();
                  },
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 40),
                ),
                EventDateField(
                  label: "Date",
                  data: "12/11/2020",
                ),
                EventDateField(
                  label: "Start Time",
                  data: "5:30PM",
                ),
                EventDateField(
                  label: "End Time",
                  data: "6:30PM",
                ),
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 50),
          ),
          BuildButton(
            title: "SUBMIT",
            disabled: disabled,
            onPressed: () {
              emailValidator();
              if (isValid == true) {
                widget.onPressed();
              } else {
                showToast("Please enter a valid email");
              }
            },
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 50),
          ),
        ],
      ),
    );
  }
}
