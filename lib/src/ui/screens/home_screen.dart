import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_athlete_list_response.dart';
import 'package:app_template/src/models/get_facility_response.dart';
import 'package:app_template/src/models/get_sports_response_model.dart';
import 'package:app_template/src/models/get_trainer_list_response.dart';
import 'package:app_template/src/ui/screens/coach_details_page.dart';
import 'package:app_template/src/ui/screens/edit_sport_group.dart';
import 'package:app_template/src/ui/screens/edit_sport_page.dart';
import 'package:app_template/src/ui/screens/edit_sports_league.dart';
import 'package:app_template/src/ui/screens/edit_sportss.dart';
import 'package:app_template/src/ui/screens/facility_detail_page.dart';
import 'package:app_template/src/ui/screens/team_mate_detail_page.dart';
import 'package:app_template/src/ui/widgets/bottom_navigation_bar_widget.dart';
import 'package:app_template/src/ui/widgets/home_athlete_tile.dart';
import 'package:app_template/src/ui/widgets/home_facility_main_tile.dart';
import 'package:app_template/src/ui/widgets/home_fliter_tile.dart';
import 'package:app_template/src/ui/widgets/home_group_tile.dart';
import 'package:app_template/src/ui/widgets/home_listview_header.dart';
import 'package:app_template/src/ui/widgets/home_page_league_widget.dart';
import 'package:app_template/src/ui/widgets/home_teammate_tile.dart';
import 'package:app_template/src/ui/widgets/home_trainer_tile.dart';
import 'package:app_template/src/ui/widgets/list_tile_select_sports.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/rating_bottom_sheet.dart';
import 'package:app_template/src/ui/widgets/soccer_feild_selection_tile.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  final int userType;
  HomePage({this.userType});
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool onClicked;
  int parentId;
  int selectedIndex;
  bool shrink = false;
  bool athleteSelected = false;
  UserBloc userBloc = UserBloc();
  List<String> ratings = ["4.5+", "4+", "3+"];
  List<String> groups = ["Camp", "Clinic", "Groups<5", "Teams 5+"];
  List<String> price = ["\u0024", "\u0024\u0024"];
  List<String> gender = ["Male", "Female"];
  List<String> sports = [
    "Basketball",
    "Volleyball",
    "Football",
    "Swimming",
    "Baseball",
    "Hockey",
    "Soccer",
    "Tennis"
  ];
  List<Sport> sportList = [];
  List<String> sportsIcon = [
    "assets/icons/basketball.svg",
    "assets/icons/volleyball.svg",
    "assets/icons/basket_ball_icon.svg",
    "assets/icons/swimming.svg",
    "assets/icons/baseball.svg",
    "assets/icons/hockey.svg",
    "assets/icons/soccer.svg",
    "assets/icons/tennis.svg"
  ];
  List<String> time = [
    "Any",
    "Early AM",
    "Morning",
    "Afternoon",
    "Evening",
    "Late"
  ];
  List<String> week = [
    "Any",
    "Today",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
  ];
  List<String> distance = [
    "Any",
    "<10 Miles",
    "<25 Miles",
    "<50 Miles",
    "<100 Miles"
  ];
  List<String> facilities = ["Any", "Coach Reserves", "My Home"];
  List<bool> color = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];
  List<bool> isClicked = [];
  List<String> filters = [
    "Over 4.5",
    "Groups",
    "\u0024, \u0024\u0024",
    "Gender",
    "Any Time of Day",
    "Day of Week",
    "Under 10 Miles",
    "Remote",
    "Facilities"
  ];
  int filterIndex = 0;
  @override
  void initState() {
    userBloc.getFacility();
    userBloc.getSports();
    userBloc.getTrainerList();
    userBloc.getAthleteList();
    if (widget.userType == 2) {
      userBloc.getProfile();
      userBloc.getProfileResponse.listen((event) {
        ObjectFactory().appHive.putParentId(value: event.data.id);
        ObjectFactory().appHive.putParentName(value: event.data.name);
        ObjectFactory().appHive.putParentEmail(value: event.data.email);
        ObjectFactory().appHive.putParentNumber(value: event.data.phone);
      });
    }
    userBloc.getAthleteListResponse.listen((event) {
      for (int i = 0; i < event.data.length; i++) {
        setState(() {
          isClicked.insert(i, false);
        });
      }
    });
    userBloc.getTrainerListResponse.listen((event) {
      print(event.data[0].name);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: true,
      child: Scaffold(
        bottomNavigationBar: BottomNavigationWidget(),
        appBar: AppBar(leading: Container(), actions: [OnBoardingAppBar()]),
        body: SingleChildScrollView(
          child: Container(
            // height: screenHeight(context, dividedBy: 1),
            width: screenWidth(context, dividedBy: 1),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 100),
                ),
                if (shrink == false)
                  HomeListViewHeader(
                    title: "ATHLETES",
                    hasEditIcon: true,
                  ),
                if (shrink == false)
                  StreamBuilder<GetAthleteListResponse>(
                      stream: userBloc.getAthleteListResponse,
                      builder: (context, snapshot) {
                        return snapshot.hasData
                            ? Container(
                                height: screenHeight(context, dividedBy: 3.8),
                                width: screenWidth(context, dividedBy: 1),
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  scrollDirection: Axis.horizontal,
                                  itemCount:
                                      // 5,
                                      snapshot.data.data.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return HomeAthleteTile(
                                      // imgUrl:
                                      // "https://image.shutterstock.com/image-photo/business-woman-drawing-global"
                                      // "-structure-260nw-1006041130.jpg",
                                      imgUrl:
                                          snapshot.data.data[index].profPic !=
                                                  null
                                              ? Urls.baseUrl +
                                                  snapshot.data.data[index]
                                                      .profPic.imageFile
                                              : null,
                                      onClick: isClicked[index],
                                      name: snapshot.data.data[index].name,
                                      sports: snapshot.data.data[index].sports,
                                      index: index,
                                      selectedIndex: selectedIndex,
                                      onPressed: () {
                                        setState(() {
                                          selectedIndex = index;
                                          isClicked[index] = !isClicked[index];
                                          sportList =
                                              snapshot.data.data[index].sports;
                                          athleteSelected = isClicked[index];
                                        });
                                      },
                                      // sportsList: (value) {
                                      //   setState(() {
                                      //     selectedIndex = index;
                                      //     athleteSelected = value.isSelected;
                                      //     sportList = value.sports;
                                      //   });
                                      // },
                                    );
                                  },
                                ),
                              )
                            : Container();
                      }),
                if (shrink == false)
                  HomeListViewHeader(
                    onPressedEditIcon: () {
                      push(
                          context,
                          EditSportsPage(
                            label: sports,
                            icon: sportsIcon,
                          ));
                    },
                    title: "SPORTS",
                    hasBackIcon: true,
                    hasEditIcon: true,
                  ),
                if (shrink == false)
                  SizedBox(
                    height: screenHeight(context, dividedBy: 100),
                  ),
                if (shrink == false)
                  Container(
                    width: screenWidth(context, dividedBy: 1),
                    height: screenHeight(context, dividedBy: 7),
                    child: StreamBuilder<GetSportsResponseModel>(
                        stream: userBloc.getSportsResponse,
                        builder: (context, snapshot) {
                          return snapshot.hasData
                              ? ListView.builder(
                                  shrinkWrap: true,
                                  scrollDirection: Axis.horizontal,
                                  itemCount:
                                      // sportsIcon.length,
                                      athleteSelected == true
                                          ? sportList.length
                                          : snapshot.data.data.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return ListTileSelectSports(
                                      label:
                                          // sports[index],
                                          athleteSelected == true
                                              ? sportList[index].name
                                              : snapshot.data.data[index].name,
                                      icon:
                                          // sportsIcon[index],
                                          athleteSelected == true
                                              ? sportList[index].icon
                                              : snapshot.data.data[index].icon,
                                      onValueChanged: (onClick) {
                                        setState(() {
                                          onClicked = onClick;
                                        });
                                      },
                                    );
                                  },
                                )
                              : Container();
                        }),
                  ),
                Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 15),
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: 9,
                    itemBuilder: (BuildContext context, int index) {
                      return HomeFilterTile(
                        label: filters[index],
                        colorChange: color[index],
                        onPressed: () {
                          displayBottomSheet(context);
                          setState(() {
                            filterIndex = index;
                          });
                        },
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 80),
                ),
                GestureDetector(
                  onTap: () {
                    print(DateTime.now().toUtc());
                    setState(() {
                      shrink = !shrink;
                    });
                  },
                  child: Container(
                    width: screenWidth(context, dividedBy: 1),
                    height: screenHeight(context, dividedBy: 5),
                    color: Constants.kitGradients[8],
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Spacer(
                          flex: 1,
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 1.4),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                // color: Colors.blue,
                                child: Image.asset(
                                  "assets/icons/coach_ref_text_icon.png",
                                  fit: BoxFit.fill,
                                  width: screenWidth(context, dividedBy: 2.5),
                                  height: screenHeight(context, dividedBy: 30),
                                ),
                              ),
                              Spacer(
                                flex: 1,
                              ),
                              Text(
                                "SUMMER SOCCER CAMP",
                                style: TextStyle(
                                    color: Constants.kitGradients[0],
                                    fontFamily: "MontserratRegular",
                                    fontWeight: FontWeight.w900,
                                    fontSize: 14),
                              ),
                              Spacer(
                                flex: 1,
                              ),
                              Text(
                                "The best place to become a better soccer player is in the company of coaches and players who share your passion. ",
                                style: TextStyle(
                                    color: Constants.kitGradients[0],
                                    fontFamily: "MontserratRegular",
                                    fontWeight: FontWeight.w500,
                                    fontSize: 10),
                              ),
                              Spacer(
                                flex: 1,
                              ),
                              Text(
                                "Register Now",
                                style: TextStyle(
                                    color: Constants.kitGradients[0],
                                    fontFamily: "MontserratRegular",
                                    fontWeight: FontWeight.w400,
                                    fontSize: 11),
                              ),
                              Spacer(
                                flex: 1,
                              ),
                            ],
                          ),
                        ),
                        Image.asset("assets/images/soccer_player_image.png",
                            height: screenHeight(context, dividedBy: 3),
                            width: screenWidth(context, dividedBy: 4)),
                      ],
                    ),
                  ),
                ),
                HomeListViewHeader(
                  onPressedEditIcon: () {
                    push(
                        context,
                        EditSportsGroup(
                          label: ["Training Group", "Training Group"],
                        ));
                  },
                  title: "GROUPS",
                  hasEditIcon: true,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    child: Container(
                      color: Constants.kitGradients[5],
                      height: screenHeight(context, dividedBy: 3),
                      width: screenHeight(context, dividedBy: 1),
                      child: Scrollbar(
                        child: ListView.builder(
                            itemCount: 3,
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            itemBuilder: (BuildContext context, int index) {
                              return HomeGroupTile();
                            }),
                      ),
                    ),
                  ),
                ),
                HomeListViewHeader(
                  onPressedEditIcon: () {
                    push(
                        context,
                        EditSportsCircleAvatarList(
                          label: ["PeerName1", "PeerName1"],
                          title: "TEAMMATES",
                        ));
                  },
                  title: "TEAMMATES",
                  hasBackIcon: true,
                  hasEditIcon: true,
                ),
                Container(
                  height: screenHeight(context, dividedBy: 5),
                  width: screenHeight(context, dividedBy: 1),
                  child: ListView.builder(
                      itemCount: 10,
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (BuildContext context, int index) {
                        return HomeTeammateTile(
                          onPressed: () {
                            push(context, TeamMateDetailsPage());
                          },
                        );
                      }),
                ),
                HomeListViewHeader(
                  title: "FACILITIES",
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: StreamBuilder<GetFacilityResponse>(
                      stream: userBloc.getFacilityResponse,
                      builder: (context, snapshot) {
                        return snapshot.hasData
                            ? Container(
                                height: screenHeight(context, dividedBy: 2.3),
                                width: screenHeight(context, dividedBy: 1),
                                child: Theme(
                                  data: ThemeData(
                                    highlightColor: Constants
                                        .kitGradients[9], //Does not work
                                  ),
                                  child: Scrollbar(
                                    child: ListView.builder(
                                        itemCount: snapshot.data.data.length,
                                        shrinkWrap: true,
                                        scrollDirection: Axis.horizontal,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Card(
                                              color: Constants.kitGradients[5],
                                              child: Container(
                                                height: screenHeight(context,
                                                    dividedBy: 2.5),
                                                width: screenWidth(context,
                                                    dividedBy: 1.1),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    HomeFacilityMainTile(
                                                      onPressed: () {
                                                        push(context,
                                                            FacilityDetailsPage());
                                                      },
                                                      address: snapshot.data
                                                          .data[index].address,
                                                      imgUrl: snapshot
                                                                  .data
                                                                  .data[index]
                                                                  .images
                                                                  .length >
                                                              0
                                                          ? snapshot
                                                              .data
                                                              .data[index]
                                                              .images[0]
                                                              .imageUrl
                                                          : null,
                                                      name:
                                                          // "YMCA",
                                                          snapshot.data
                                                              .data[index].name,
                                                      fromTime:
                                                          // "M-F: 9AM-6PM",
                                                          snapshot
                                                              .data
                                                              .data[index]
                                                              .businessHoursFrom,
                                                      toTime:
                                                          // "S-Su: 11AM-6PM",
                                                          snapshot
                                                              .data
                                                              .data[index]
                                                              .businessHoursTo,
                                                    ),
                                                    SizedBox(
                                                      height: screenHeight(
                                                          context,
                                                          dividedBy: 100),
                                                    ),
                                                    Text(
                                                      "   Rentals",
                                                      style: TextStyle(
                                                          fontSize: 13,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          color: Constants
                                                              .kitGradients[0]),
                                                    ),
                                                    SizedBox(
                                                      height: screenHeight(
                                                          context,
                                                          dividedBy: 100),
                                                    ),
                                                    Expanded(
                                                      child: ListView.builder(
                                                          itemCount: 3,
                                                          shrinkWrap: true,
                                                          scrollDirection:
                                                              Axis.vertical,
                                                          itemBuilder:
                                                              (BuildContext
                                                                      context,
                                                                  int index) {
                                                            return Container(
                                                              height:
                                                                  screenHeight(
                                                                      context,
                                                                      dividedBy:
                                                                          8),
                                                              color: Constants
                                                                  .kitGradients[5],
                                                              child: Column(
                                                                children: [
                                                                  SoccerFieldSelectionTile(
                                                                    hasIcon:
                                                                        false,
                                                                    title:
                                                                        "Soccer Feild",
                                                                    image:
                                                                        "assets/images/soccer_field.png",
                                                                    subtitle:
                                                                        "Field B",
                                                                    icon:
                                                                        "assets/icons/basket_ball_icon.svg",
                                                                    pricePerDay:
                                                                        "24",
                                                                    pricePerHour:
                                                                        "24",
                                                                    onPressed:
                                                                        () {},
                                                                  ),
                                                                  Spacer(
                                                                    flex: 1,
                                                                  ),
                                                                  Padding(
                                                                    padding: EdgeInsets.symmetric(
                                                                        horizontal: screenWidth(
                                                                            context,
                                                                            dividedBy:
                                                                                20)),
                                                                    child:
                                                                        Container(
                                                                      height: 1,
                                                                      color: Constants
                                                                          .kitGradients[0],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            );
                                                          }),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        }),
                                  ),
                                ),
                              )
                            : Container();
                      }),
                ),
                HomeListViewHeader(
                  title: "YOUR TRAINERS",
                  hasBackIcon: true,
                  hasEditIcon: true,
                  onPressedEditIcon: () {
                    push(
                        context,
                        EditSportsCircleAvatarList(
                          title: "TRAINERS",
                          label: [
                            "Trainer Name",
                            "Trainer Name",
                            "Trainer Name"
                          ],
                        ));
                  },
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 20),
                ),
                Row(
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 30),
                    ),
                    StreamBuilder<GetTrainerListResponse>(
                        stream: userBloc.getTrainerListResponse,
                        builder: (context, snapshot) {
                          return snapshot.hasData
                              ? Container(
                                  height: screenHeight(context, dividedBy: 3),
                                  width: screenWidth(context, dividedBy: 1.1),
                                  child: ListView.builder(
                                      itemCount: snapshot.data.data.length,
                                      shrinkWrap: true,
                                      scrollDirection: Axis.horizontal,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return HomeTrainerTile(
                                          sports: snapshot
                                              .data.data[index].trainerSports,
                                          name: snapshot.data.data[index].name,
                                          imageName: snapshot.data.data[index]
                                                      .profPic !=
                                                  null
                                              ? snapshot.data.data[index]
                                                  .profPic.imageFile
                                              : null,
                                          hasIcon: true,
                                          onPressed: () {
                                            push(
                                                context,
                                                CoachDetailsPage(
                                                  sports: snapshot
                                                      .data
                                                      .data[index]
                                                      .trainerSports,
                                                  name:
                                                      // "Coach",
                                                      snapshot.data.data[index]
                                                          .name,
                                                  imageName: snapshot
                                                              .data
                                                              .data[index]
                                                              .profPic !=
                                                          null
                                                      ? snapshot
                                                          .data
                                                          .data[index]
                                                          .profPic
                                                          .imageFile
                                                      : null,
                                                ));
                                          },
                                        );
                                      }),
                                )
                              : Container();
                        }),
                  ],
                ),
                HomeListViewHeader(
                  title: "LEAGUES & TEAMS NEAR ME",
                  hasEditIcon: true,
                  itemNumber: "2 items",
                  onPressedEditIcon: () {
                    push(
                        context,
                        EditSportsLeague(
                          label: ["Edge Sports Adult Basketball"],
                        ));
                  },
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 30)),
                  child: Container(
                    height: screenHeight(context, dividedBy: 3.5),
                    width: screenWidth(context, dividedBy: 1),
                    child: ListView.builder(
                        itemCount: 2,
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemBuilder: (BuildContext context, int index) {
                          return HomePageLeagueWidget(
                            location: "Robinson",
                            heading: "Plex Paul Adult League",
                            subHeading:
                                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod",
                            imageName: "assets/images/league_image.svg",
                            onPressed: () {
                              push(context, TeamMateDetailsPage());
                            },
                          );
                        }),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void displayBottomSheet(context) {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return filterIndex == 0
            ? BottomSheetHome(
                height: 3,
                title: "Ratings",
                label: ratings,
                onClicked: (value) {
                  setState(() {
                    color[0] = value;
                  });
                },
              )
            : filterIndex == 1
                ? BottomSheetHome(
                    height: 2.5,
                    title: "Groups",
                    label: groups,
                    onClicked: (value) {
                      setState(() {
                        color[1] = value;
                      });
                    },
                  )
                : filterIndex == 2
                    ? BottomSheetHome(
                        height: 3,
                        title: "Cost",
                        label: price,
                        onClicked: (value) {
                          setState(() {
                            color[2] = value;
                          });
                        },
                      )
                    : filterIndex == 3
                        ? BottomSheetHome(
                            height: 3,
                            title: "Gender",
                            label: gender,
                            onClicked: (value) {
                              setState(() {
                                color[3] = value;
                              });
                            },
                          )
                        : filterIndex == 4
                            ? BottomSheetHome(
                                height: 1.5,
                                title: "Time of Day",
                                label: time,
                                onClicked: (value) {
                                  setState(() {
                                    color[4] = value;
                                  });
                                },
                              )
                            : filterIndex == 5
                                ? BottomSheetHome(
                                    height: 1.2,
                                    title: "Day Of Week",
                                    label: week,
                                    onClicked: (value) {
                                      setState(() {
                                        color[5] = value;
                                      });
                                    },
                                  )
                                : filterIndex == 6
                                    ? BottomSheetHome(
                                        height: 2,
                                        title: "Distance",
                                        label: distance,
                                        onClicked: (value) {
                                          setState(() {
                                            color[6] = value;
                                          });
                                        },
                                      )
                                    : filterIndex == 7
                                        ? BottomSheetHome(
                                            height: 4,
                                            title: "Remote",
                                            onClicked: (value) {
                                              setState(() {
                                                color[7] = value;
                                              });
                                            },
                                          )
                                        : BottomSheetHome(
                                            height: 2.8,
                                            title: "Facilities",
                                            label: facilities,
                                            onClicked: (value) {
                                              setState(() {
                                                color[8] = value;
                                              });
                                            },
                                          );
      },
    );
  }
}
