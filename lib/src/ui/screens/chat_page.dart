import 'package:app_template/src/models/chat_message_model.dart';
import 'package:app_template/src/ui/widgets/bottom_navigation_bar_widget.dart';
import 'package:app_template/src/ui/widgets/chat_screen_textfeild.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_chat_bubble/bubble_type.dart';
import 'package:flutter_chat_bubble/chat_bubble.dart';
import 'package:flutter_chat_bubble/clippers/chat_bubble_clipper_3.dart';

class ChatPage extends StatefulWidget {
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  TextEditingController chatTextEditingController = new TextEditingController();
  List<ChatMessage> messages = [
    ChatMessage(messageContent: "Hello, Will", messageType: "receiver"),
    ChatMessage(messageContent: "How have you been?", messageType: "receiver"),
    ChatMessage(
        messageContent: "Hey Kriss, I am doing fine dude. wbu?",
        messageType: "sender"),
    ChatMessage(messageContent: "ehhhh, doing OK.", messageType: "receiver"),
    ChatMessage(
        messageContent: "Is there any thing wrong?", messageType: "sender"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndDocked,
      floatingActionButton: Padding(
        padding:
            EdgeInsets.only(bottom: screenHeight(context, dividedBy: 19.9)),
        child: ChatScreenTextField(
          textEditingController: chatTextEditingController,
          hintText: "  Type Message...",
        ),
      ),
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[3],
        toolbarHeight: screenHeight(context, dividedBy: 8),
        leading: GestureDetector(
          onTap: () {
            pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.white,
            size: 30,
          ),
        ),
        leadingWidth: screenWidth(context, dividedBy: 8),
        centerTitle: true,
        title: Container(
          child: Column(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(40),
                child: Container(
                    width: screenWidth(context, dividedBy: 6),
                    height: screenWidth(context, dividedBy: 7),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                    ),
                    child: Image(
                      image: AssetImage("assets/images/user_image.png"),
                    )),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 200),
              ),
              Text(
                "Dan Smith",
                style: TextStyle(
                    fontSize: 12,
                    color: Colors.white,
                    fontFamily: 'OpenSansRegular'),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationWidget(),
      body: ListView(
        children: [
          Container(
              height: screenHeight(context, dividedBy: 1),
              child: ListView.builder(
                itemCount: messages.length,
                itemBuilder: (BuildContext cntxt, int index) {
                  print(messages[index].messageType);
                  return messages[index].messageType == "receiver"
                      ? getReceiverView(
                          ChatBubbleClipper3(type: BubbleType.receiverBubble),
                          context)
                      : getSenderView(
                          ChatBubbleClipper3(type: BubbleType.sendBubble),
                          context);
                },
              )),
        ],
      ),
    );
  }

  getSenderView(CustomClipper clipper, BuildContext context) => ChatBubble(
        clipper: clipper,
        elevation: 0,
        alignment: Alignment.topRight,
        margin: EdgeInsets.only(top: 20),
        backGroundColor: Colors.white,
        child: Container(
          constraints: BoxConstraints(
            maxWidth: screenWidth(context, dividedBy: 1.4),
          ),
          child: Text(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            style: TextStyle(color: Colors.black),
          ),
        ),
      );

  getReceiverView(CustomClipper clipper, BuildContext context) => ChatBubble(
        clipper: clipper,
        elevation: 0,
        margin: EdgeInsets.only(top: 20),
        backGroundColor: Constants.kitGradients[5],
        child: Container(
          constraints: BoxConstraints(
            maxWidth: screenWidth(context, dividedBy: 1.4),
          ),
          child: Text(
            "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
}
