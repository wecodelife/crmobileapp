import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SelectButton extends StatefulWidget {
  final String title;
  final Function onPressed;
  final bool transparent;
  final String icon;
  final bool iconTrue;
  SelectButton(
      {this.title, this.icon, this.onPressed, this.transparent, this.iconTrue});
  @override
  _SelectButtonState createState() => _SelectButtonState();
}

class _SelectButtonState extends State<SelectButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1.1),
      height: screenHeight(context, dividedBy: 22),
      child: RaisedButton(
        onPressed: () {
          widget.onPressed();
        },
        color: widget.transparent == true
            ? Colors.transparent
            : Constants.kitGradients[1],
        shape: RoundedRectangleBorder(
            side: BorderSide(color: Constants.kitGradients[1])),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            widget.iconTrue == true
                ? SvgPicture.asset(
                    widget.icon,
                    color: Colors.white,
                  )
                : Container(),
            widget.iconTrue == true
                ? SizedBox(
                    width: screenWidth(context, dividedBy: 20),
                  )
                : Container(),
            Text(
              widget.title,
              style: TextStyle(
                  color: widget.transparent == true
                      ? Constants.kitGradients[0]
                      : Colors.white,
                  fontFamily: 'MuliBold',
                  fontSize: 16),
            ),
          ],
        ),
      ),
    );
  }
}
