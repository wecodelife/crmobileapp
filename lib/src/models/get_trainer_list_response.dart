// To parse this JSON data, do
//
//     final getTrainerListResponse = getTrainerListResponseFromJson(jsonString);

import 'dart:convert';

GetTrainerListResponse getTrainerListResponseFromJson(String str) =>
    GetTrainerListResponse.fromJson(json.decode(str));

String getTrainerListResponseToJson(GetTrainerListResponse data) =>
    json.encode(data.toJson());

class GetTrainerListResponse {
  GetTrainerListResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final List<Datum> data;

  factory GetTrainerListResponse.fromJson(Map<String, dynamic> json) =>
      GetTrainerListResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.name,
    this.address,
    this.trainerType,
    this.trainingAtHome,
    this.clientsReservePay,
    this.videoStatement,
    this.yearsOfExp,
    this.highestLevelPlayed,
    this.highestEducation,
    this.dob,
    this.ssn,
    this.trainerSports,
    this.profPic,
  });

  final int id;
  final String name;
  final String address;
  final int trainerType;
  final int trainingAtHome;
  final bool clientsReservePay;
  final ProfPic videoStatement;
  final int yearsOfExp;
  final Highest highestLevelPlayed;
  final Highest highestEducation;
  final DateTime dob;
  final String ssn;
  final List<TrainerSport> trainerSports;
  final ProfPic profPic;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        address: json["address"] == null ? null : json["address"],
        trainerType: json["trainer_type"] == null ? null : json["trainer_type"],
        trainingAtHome:
            json["training_at_home"] == null ? null : json["training_at_home"],
        clientsReservePay: json["clients_reserve_pay"] == null
            ? null
            : json["clients_reserve_pay"],
        videoStatement: json["video_statement"] == null
            ? null
            : ProfPic.fromJson(json["video_statement"]),
        yearsOfExp: json["years_of_exp"] == null ? null : json["years_of_exp"],
        highestLevelPlayed: json["highest_level_played"] == null
            ? null
            : Highest.fromJson(json["highest_level_played"]),
        highestEducation: json["highest_education"] == null
            ? null
            : Highest.fromJson(json["highest_education"]),
        dob: json["dob"] == null ? null : DateTime.parse(json["dob"]),
        ssn: json["ssn"] == null ? null : json["ssn"],
        trainerSports: json["trainer_sports"] == null
            ? null
            : List<TrainerSport>.from(
                json["trainer_sports"].map((x) => TrainerSport.fromJson(x))),
        profPic: json["prof_pic"] == null
            ? null
            : ProfPic.fromJson(json["prof_pic"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "address": address == null ? null : address,
        "trainer_type": trainerType == null ? null : trainerType,
        "training_at_home": trainingAtHome == null ? null : trainingAtHome,
        "clients_reserve_pay":
            clientsReservePay == null ? null : clientsReservePay,
        "video_statement":
            videoStatement == null ? null : videoStatement.toJson(),
        "years_of_exp": yearsOfExp == null ? null : yearsOfExp,
        "highest_level_played":
            highestLevelPlayed == null ? null : highestLevelPlayed.toJson(),
        "highest_education":
            highestEducation == null ? null : highestEducation.toJson(),
        "dob": dob == null
            ? null
            : "${dob.year.toString().padLeft(4, '0')}-${dob.month.toString().padLeft(2, '0')}-${dob.day.toString().padLeft(2, '0')}",
        "ssn": ssn == null ? null : ssn,
        "trainer_sports": trainerSports == null
            ? null
            : List<dynamic>.from(trainerSports.map((x) => x.toJson())),
        "prof_pic": profPic == null ? null : profPic.toJson(),
      };
}

class Highest {
  Highest({
    this.id,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.value,
    this.order,
  });

  final int id;
  final bool isActive;
  final bool isDeleted;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String value;
  final int order;

  factory Highest.fromJson(Map<String, dynamic> json) => Highest(
        id: json["id"] == null ? null : json["id"],
        isActive: json["is_active"] == null ? null : json["is_active"],
        isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        value: json["value"] == null ? null : json["value"],
        order: json["order"] == null ? null : json["order"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "is_active": isActive == null ? null : isActive,
        "is_deleted": isDeleted == null ? null : isDeleted,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "value": value == null ? null : value,
        "order": order == null ? null : order,
      };
}

class ProfPic {
  ProfPic({
    this.id,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.imageFile,
    this.videoFile,
  });

  final int id;
  final bool isActive;
  final bool isDeleted;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String imageFile;
  final String videoFile;

  factory ProfPic.fromJson(Map<String, dynamic> json) => ProfPic(
        id: json["id"] == null ? null : json["id"],
        isActive: json["is_active"] == null ? null : json["is_active"],
        isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        imageFile: json["image_file"] == null ? null : json["image_file"],
        videoFile: json["video_file"] == null ? null : json["video_file"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "is_active": isActive == null ? null : isActive,
        "is_deleted": isDeleted == null ? null : isDeleted,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "image_file": imageFile == null ? null : imageFile,
        "video_file": videoFile == null ? null : videoFile,
      };
}

class TrainerSport {
  TrainerSport({
    this.sport,
    this.trainerType,
    this.ageRange,
    this.sessionIncrements,
    this.rateBy,
    this.isVolunteerOpted,
    this.isClinicCampsOpted,
  });

  final CoachSports sport;
  final int trainerType;
  final int ageRange;
  final int sessionIncrements;
  final int rateBy;
  final bool isVolunteerOpted;
  final bool isClinicCampsOpted;

  factory TrainerSport.fromJson(Map<String, dynamic> json) => TrainerSport(
        sport:
            json["sport"] == null ? null : CoachSports.fromJson(json["sport"]),
        trainerType: json["trainer_type"] == null ? null : json["trainer_type"],
        ageRange: json["age_range"] == null ? null : json["age_range"],
        sessionIncrements: json["session_increments"] == null
            ? null
            : json["session_increments"],
        rateBy: json["rate_by"] == null ? null : json["rate_by"],
        isVolunteerOpted: json["is_volunteer_opted"] == null
            ? null
            : json["is_volunteer_opted"],
        isClinicCampsOpted: json["is_clinic_camps_opted"] == null
            ? null
            : json["is_clinic_camps_opted"],
      );

  Map<String, dynamic> toJson() => {
        "sport": sport == null ? null : sport.toJson(),
        "trainer_type": trainerType == null ? null : trainerType,
        "age_range": ageRange == null ? null : ageRange,
        "session_increments":
            sessionIncrements == null ? null : sessionIncrements,
        "rate_by": rateBy == null ? null : rateBy,
        "is_volunteer_opted":
            isVolunteerOpted == null ? null : isVolunteerOpted,
        "is_clinic_camps_opted":
            isClinicCampsOpted == null ? null : isClinicCampsOpted,
      };
}

class CoachSports {
  CoachSports({
    this.id,
    this.name,
    this.icon,
    this.iconFile,
    this.description,
  });

  final int id;
  final String name;
  final String icon;
  final dynamic iconFile;
  final String description;

  factory CoachSports.fromJson(Map<String, dynamic> json) => CoachSports(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        icon: json["icon"] == null ? null : json["icon"],
        iconFile: json["iconFile"],
        description: json["description"] == null ? null : json["description"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "icon": icon == null ? null : icon,
        "iconFile": iconFile,
        "description": description == null ? null : description,
      };
}
