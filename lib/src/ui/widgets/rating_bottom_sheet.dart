import 'package:app_template/src/ui/widgets/bottom_sheet_button.dart';
import 'package:app_template/src/ui/widgets/bottom_sheet_item.dart';
import 'package:app_template/src/ui/widgets/select_button.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class BottomSheetHome extends StatefulWidget {
  final List<String> label;
  final String title;
  final bool checked;
  final Function onPressed;
  final double height;
  final ValueChanged<bool> onClicked;
  BottomSheetHome(
      {this.label,
      this.title,
      this.checked,
      this.onPressed,
      this.height,
      this.onClicked});
  @override
  _BottomSheetHomeState createState() => _BottomSheetHomeState();
}

class _BottomSheetHomeState extends State<BottomSheetHome> {
  List<bool> check = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];
  bool selected = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Constants.kitGradients[11],
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: widget.height),
      child: Column(
        children: [
          Spacer(
            flex: 1,
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            child: Row(
              children: [
                Spacer(
                  flex: 1,
                ),
                Text(
                  widget.title,
                  style: TextStyle(
                      fontFamily: "OpenSansRegular",
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: Constants.kitGradients[0]),
                ),
                Spacer(
                  flex: 20,
                ),
                GestureDetector(
                  onTap: () {
                    pop(context);
                  },
                  child: Icon(
                    Icons.close,
                    size: 20,
                    color: Constants.kitGradients[0],
                  ),
                ),
                Spacer(
                  flex: 1,
                ),
              ],
            ),
          ),
          Spacer(
            flex: 1,
          ),
          Row(
            children: [Container()],
          ),
          widget.title == "Remote"
              ? Container(
                  width: screenWidth(context, dividedBy: 1),
                  child: Row(
                    children: [
                      Spacer(
                        flex: 2,
                      ),
                      BottomSheetButton(
                        label: "Yes",
                        onPressed: () {
                          setState(() {
                            selected = !selected;
                          });
                          widget.onClicked(selected);
                        },
                      ),
                      Spacer(
                        flex: 1,
                      ),
                      BottomSheetButton(
                        label: "No",
                        onPressed: () {
                          setState(() {
                            selected = !selected;
                          });
                          widget.onClicked(selected);
                        },
                      ),
                      Spacer(
                        flex: 15,
                      )
                    ],
                  ),
                )
              : ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemCount: widget.label.length,
                  itemBuilder: (BuildContext context, int index) {
                    return HomeBottomSheetItem(
                      title: widget.title,
                      label: widget.label[index],
                      onClicked: (value) {
                        setState(() {
                          check[index] = value;
                        });
                        if (check.contains(true)) {
                          setState(() {
                            selected = true;
                          });
                        } else {
                          setState(() {
                            selected = false;
                          });
                        }
                        widget.onClicked(selected);
                      },
                    );
                  },
                ),
          Spacer(
            flex: 1,
          ),
          SelectButton(
            title: "View Details",
          ),
          Spacer(
            flex: 1,
          ),
        ],
      ),
    );
  }
}
