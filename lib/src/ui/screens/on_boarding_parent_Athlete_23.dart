import 'dart:io';

import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/add_coach_request.dart';
import 'package:app_template/src/models/add_facility_request.dart';
import 'package:app_template/src/models/get_facility_type_response.dart';
import 'package:app_template/src/models/get_sports_response_model.dart';
import 'package:app_template/src/models/upload_image_request_model.dart';
import 'package:app_template/src/ui/coach/screens/background_information.dart';
import 'package:app_template/src/ui/screens/on_boarding_parent_athlete_22.dart';
import 'package:app_template/src/ui/screens/onboardingAthlete25.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/drop_down_facility.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/list_tile_select_sports.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/select_button.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:time_range_picker/time_range_picker.dart';

class OnBoardingParentAthlete23 extends StatefulWidget {
  final String SessionName;
  final List<TrainerSport> sports;

  OnBoardingParentAthlete23({this.SessionName, this.sports});
  @override
  _OnBoardingParentAthlete23State createState() =>
      _OnBoardingParentAthlete23State();
}

class _OnBoardingParentAthlete23State extends State<OnBoardingParentAthlete23> {
  TextEditingController nameTextEditingController = new TextEditingController();
  TextEditingController addressTextEditingController =
      new TextEditingController();
  TextEditingController businessHoursFromTextEditingController =
      new TextEditingController();
  TextEditingController businessHoursToTextEditingController =
      new TextEditingController();
  TextEditingController emailTextEditingController =
      new TextEditingController();
  TextEditingController numberTextEditingController =
      new TextEditingController();
  UserBloc userBloc = UserBloc();
  TimeRange selectedTime;
  Datums dropDownValueFacilityType, facilityTypeSelected;
  int count = 0;
  // Datum sportsAvailable;
  List<String> sportsIcon = [
    "assets/icons/basketball.svg",
    "assets/icons/volleyball.svg",
    "assets/icons/basket_ball_icon.svg",
    "assets/icons/swimming.svg",
    "assets/icons/baseball.svg",
    "assets/icons/hockey.svg",
    "assets/icons/soccer.svg",
    "assets/icons/tennis.svg"
  ];
  bool dropDownClicked = false;
  List<String> facility = ["one", "two"];
  List<String> sportsAvailableDropDownList = ["one", "two"];
  List<int> imageList = [];
  bool disabled = true;
  bool loading = false;
  bool onClicked;
  List<int> selectedSports = [];
  final picker = ImagePicker();
  File imagePicked;

  Future getImageGallery() async {
    final pickedFile =
        await picker.getImage(source: ImageSource.gallery, imageQuality: 25);
    if (pickedFile != null) {
      setState(() {
        imagePicked = File(pickedFile.path);
        loading = true;
      });
      userBloc.uploadImage(
          uploadImageRequest: UploadImageRequest(image: imagePicked));
    }
  }

  Future getImageCamera() async {
    final pickedFile =
        await picker.getImage(source: ImageSource.camera, imageQuality: 25);
    if (pickedFile != null) {
      setState(() {
        imagePicked = File(pickedFile.path);
        loading = true;
      });
      userBloc.uploadImage(
          uploadImageRequest: UploadImageRequest(image: imagePicked));
    }
  }

  Future<String> addItems(int value) async {
    if (onClicked == true) {
      incrementCounter();
      setState(() {
        // selectedSports.add(snapshot
        //     .data.data[index].id);
        selectedSports.add(value);
      });
      return "fhfhgf" + selectedSports.toString();
    } else {
      decrementCounter();
      selectedSports.remove(value);
      return "gfh" + selectedSports.toString();
    }
  }

  void incrementCounter() {
    count = count + 1;
  }

  void decrementCounter() {
    count = count - 1;
  }

  @override
  void initState() {
    userBloc.getFacilityType();
    userBloc.getSports();
    userBloc.uploadImageResponse.listen((event) {
      setState(() {
        loading = false;
      });
      userBloc.getFacilityType();
      userBloc.getSports();
      dropDownValueFacilityType = null;
      if (event.status == 200 || event.status == 201) {
        imageList.add(event.data.id);
      }
      print("ghhgfgf");
    });
    userBloc.addFacilityResponse.listen((event) {
      setState(() {
        loading = false;
      });
      if (widget.SessionName == "coach") {
        push(
            context,
            BackgroundInformation(
              sports: widget.sports,
            ));
      } else {
        push(
            context,
            OnBoardingAthlete25(
              sports: widget.sports,
            ));
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[6],
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: loading == true
          ? Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Container(
              height: screenHeight(context, dividedBy: 1),
              width: screenWidth(context, dividedBy: 1),
              color: Theme.of(context).scaffoldBackgroundColor,
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 12)),
                    child: Column(
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 30),
                        ),
                        TextProgressIndicator(
                            heading:
                                "LETS SELECT SOME PREFERRED TRAINING FACILITY",
                            descriptionText:
                                "Find some facilities, search some facilities, invite facilities, or add your own."),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 30),
                        ),
                        OnBoardingProgressIndicator(
                          width: 1.4,
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 30),
                        ),
                        Container(
                          height: screenHeight(context, dividedBy: 1.95),
                          child: ListView(
                            children: [
                              FormFeildUserDetails(
                                labelText: "Name of Facility",
                                onValueChanged: (nameValue) {
                                  // emailCheck();
                                },
                                textEditingController:
                                    nameTextEditingController,
                              ),
                              FormFeildUserDetails(
                                labelText: "Email",
                                onValueChanged: (emailValue) {
                                  // emailCheck();
                                },
                                textEditingController:
                                    emailTextEditingController,
                              ),
                              FormFeildUserDetails(
                                labelText: "Phone Number",
                                onValueChanged: (numberValue) {
                                  // emailCheck();
                                },
                                textEditingController:
                                    numberTextEditingController,
                              ),
                              FormFeildUserDetails(
                                labelText: "Enter Address",
                                onValueChanged: (addressValue) {
                                  // emailCheck();
                                },
                                textEditingController:
                                    addressTextEditingController,
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 30),
                              ),
                              Row(
                                children: [
                                  Text("Facility Type",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: ' OswaldBold',
                                          fontSize: 15)),
                                ],
                              ),
                              StreamBuilder<GetFacilityTypeResponse>(
                                  stream: userBloc.getFacilityTypeResponse,
                                  builder: (context, snapshot) {
                                    return snapshot.hasData
                                        ? Container(
                                            width: screenWidth(context,
                                                dividedBy: 1),
                                            height: screenHeight(context,
                                                dividedBy: 10),
                                            child: DropDownCoachRefAppFacility(
                                              dropDownList: snapshot.data.data,
                                              title: "Select",
                                              onClicked: (value) {
                                                setState(() {
                                                  dropDownValueFacilityType =
                                                      value;
                                                  dropDownClicked = true;
                                                  facilityTypeSelected = value;
                                                });
                                              },
                                              dropDownValue:
                                                  dropDownValueFacilityType,
                                            ))
                                        : Container();
                                  }),
                              // Container(
                              //     width: screenWidth(context, dividedBy: 1),
                              //     height: screenHeight(context, dividedBy: 10),
                              //     child: DropDownCrApp(
                              //       dropDownList: facility,
                              //       title: "Select",
                              //       onClicked: (value) {
                              //         dropDownValueFacilityType = value;
                              //       },
                              //       dropDownValue: dropDownValueFacilityType,
                              //     )),
                              FormFeildUserDetails(
                                textEditingController:
                                    businessHoursFromTextEditingController,
                                labelText: "Business Hours From",
                                readOnly: true,
                                onPressed: () async {
                                  TimeRange result = await showTimeRangePicker(
                                      context: context,
                                      hideTimes: false,
                                      // use24HourFormat: true,
                                      timeTextStyle: TextStyle(
                                          color: Constants.kitGradients[0],
                                          fontSize: 24),
                                      activeTimeTextStyle: TextStyle(
                                          color: Constants.kitGradients[0],
                                          fontSize: 24),
                                      labelStyle: TextStyle(
                                          color: Constants.kitGradients[0]),
                                      strokeColor: Constants.kitGradients[1],
                                      selectedColor: Constants.kitGradients[1],
                                      handlerColor: Constants.kitGradients[1]);
                                  setState(() {
                                    selectedTime = result;
                                    businessHoursFromTextEditingController
                                        .text = selectedTime.startTime
                                            .toString()
                                            .substring(10, 15) +
                                        " - " +
                                        selectedTime.endTime
                                            .toString()
                                            .substring(10, 15);
                                  });
                                  print(selectedTime.toString());
                                },

                                // hintText: "(time format : 13:30)",
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 50),
                              ),
                              FormFeildUserDetails(
                                textEditingController:
                                    businessHoursToTextEditingController,
                                labelText: "Business Hours To",
                                hintText: "(time format : 13:30)",
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 50),
                              ),
                              Row(
                                children: [
                                  Text("Available sports to Practice",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: ' OswaldBold',
                                          fontSize: 15)),
                                ],
                              ),
                              // StreamBuilder<GetSportsResponseModel>(
                              //     stream: userBloc.getSportsResponse,
                              //     builder: (context, snapshot) {
                              //       return snapshot.hasData
                              //           ? Container(
                              //               width: screenWidth(context,
                              //                   dividedBy: 1),
                              //               height: screenHeight(context,
                              //                   dividedBy: 10),
                              //               child: DropDownCoachRefApp(
                              //                 dropDownList: snapshot.data.data,
                              //                 title: "Select",
                              //                 onClicked: (value) {
                              //                   setState(() {
                              //                     dropDownValue = value;
                              //                     dropDownClicked = true;
                              //                   });
                              //                 },
                              //                 dropDownValue: dropDownValue,
                              //               ))
                              //           : Container();
                              //     }),
                              Container(
                                height: screenHeight(context, dividedBy: 5.5),
                                width: screenWidth(context, dividedBy: 1.07),
                                child: StreamBuilder<GetSportsResponseModel>(
                                    stream: userBloc.getSportsResponse,
                                    builder: (context, snapshot) {
                                      return snapshot.hasData
                                          ? ListView.builder(
                                              shrinkWrap: true,
                                              scrollDirection: Axis.horizontal,
                                              itemCount:
                                                  snapshot.data.data.length,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                print(snapshot
                                                    .data.data[index].name);
                                                return ListTileSelectSports(
                                                  label: snapshot
                                                      .data.data[index].name,
                                                  icon: snapshot
                                                      .data.data[index].icon,
                                                  onValueChanged:
                                                      (onClick) async {
                                                    onClicked = onClick;
                                                    await addItems(snapshot
                                                        .data.data[index].id);

                                                    print(selectedSports
                                                        .toString());
                                                  },
                                                );
                                              },
                                            )
                                          : Container();
                                    }),
                              ),
                              // Container(
                              //     width: screenWidth(context, dividedBy: 1),
                              //     height: screenHeight(context, dividedBy: 10),
                              //     child: DropDownCrApp(
                              //       dropDownList: sportsAvailableDropDownList,
                              //       title: "Select",
                              //       onClicked: (value) {
                              //         sportsAvailable = value;
                              //       },
                              //       dropDownValue: sportsAvailable,
                              //     )),

                              Text(
                                "You have selected ${imageList.length} images",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: ' OswaldBold',
                                    fontSize: 15),
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 30),
                              ),
                              SelectButton(
                                title: " + ADD PHOTOS",
                                onPressed: () {
                                  bottomSheetAddPhotos(context);
                                },
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 80),
                              ),
                              // SelectButton(
                              //   title: "MEDICAL/LIABILITY REALEASE",
                              //   onPressed: () {
                              //     bottomSheetAddPhotos(context);
                              //   },
                              // ),
                              // SizedBox(
                              //   height: screenHeight(context, dividedBy: 50),
                              // )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  BuildButton(
                    title: "SEARCH FACILITIES",
                    onPressed: () {
                      push(
                          context,
                          OnBoardingParentAthlete22(
                            sessionName: widget.SessionName,
                          ));
                    },
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 70),
                  ),
                  BuildButton(
                    title: "NEXT",
                    disabled: false,
                    onPressed: () {
                      setState(() {
                        loading = true;
                      });
                      userBloc.addFacility(
                          addFacilityRequest: AddFacilityRequest(
                              name: nameTextEditingController.text,
                              sports: selectedSports,
                              email: emailTextEditingController.text,
                              address: addressTextEditingController.text,
                              availableSports: selectedSports,
                              businessHoursFrom: selectedTime.startTime
                                  .toString()
                                  .substring(10, 15),
                              businessHoursTo: selectedTime.endTime
                                  .toString()
                                  .substring(10, 15),
                              employeeAccess: false,
                              info: null,
                              multipleCourtsRates: false,
                              // businessHours:
                              //     businessHoursTextEditingController.text,
                              facilityType: facilityTypeSelected.id,
                              images: imageList,
                              lat: null,
                              lng: null,
                              phone: numberTextEditingController.text));
                    },
                  )
                ],
              ),
            ),
    );
  }

  void bottomSheetAddPhotos(context) {
    showModalBottomSheet<dynamic>(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (BuildContext context, setState) {
            return Container(
                color: Colors.white,
                height: screenHeight(context, dividedBy: 4),
                width: screenWidth(context, dividedBy: 1),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          right: screenWidth(context, dividedBy: 10)),
                      child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Cancel",
                            style: TextStyle(
                              color: Constants.kitGradients[3],
                              fontFamily: "Poppins",
                              fontSize: 12,
                            ),
                          )),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                            onTap: () {
                              getImageCamera();
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.camera,
                              size: 50,
                              color: Constants.kitGradients[3],
                            )),
                        GestureDetector(
                            onTap: () {
                              getImageGallery();
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.image,
                              size: 50,
                              color: Constants.kitGradients[3],
                            )),
                      ],
                    ),
                  ],
                ));
          });
        });
  }
}
