import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class YmcaProfileTile extends StatefulWidget {
  final Function onPressed;
  YmcaProfileTile({this.onPressed});
  @override
  _YmcaProfileTileState createState() => _YmcaProfileTileState();
}

class _YmcaProfileTileState extends State<YmcaProfileTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Container(
        width: screenWidth(context, dividedBy: 1),
        color: Constants.kitGradients[11],
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: 8.0,
          ),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 70),
                  ),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Container(
                      width: screenWidth(context, dividedBy: 4.5),
                      height: screenHeight(context, dividedBy: 10),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/images/dummy_bg.jpg"),
                              fit: BoxFit.fill)),
                      child: Image.network(
                        "https://image.shutterstock.com/image-photo/business-woman-drawing-global"
                            "-structure-260nw-1006041130.jpg",
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 60),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 100)),
                    child: Container(
                      width: screenWidth(context, dividedBy: 1.6),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                  width: screenWidth(context, dividedBy: 3),
                                  child: Text(
                                    " YMCA",
                                    style: TextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'OpenSansRegular',
                                        color: Colors.white),
                                  )),
                              Spacer(
                                flex: 1,
                              ),

                            ],
                          ),
                          SizedBox(
                            height: 2.0,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Icon(
                                Icons.location_on,
                                size: 22,
                                color: Colors.white,
                              ),
                              SizedBox(
                                width: 2,
                              ),
                              Container(
                                width: screenWidth(context, dividedBy: 1.8),
                                child: Text("76A Eighth Avenue, Pittsburgh PA, 15211",
                                    style: TextStyle(
                                        fontSize: 13,
                                        fontFamily: 'OpenSansRegular',
                                        color: Colors.white),
                                    ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 120),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 100),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
