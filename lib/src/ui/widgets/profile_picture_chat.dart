import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class ProfilePictureChat extends StatefulWidget {
  final String image;
  final String name;
  ProfilePictureChat({this.image, this.name});
  @override
  _ProfilePictureChatState createState() => _ProfilePictureChatState();
}

class _ProfilePictureChatState extends State<ProfilePictureChat> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(40),
            child: Container(
                width: screenWidth(context, dividedBy: 6),
                height: screenWidth(context, dividedBy: 9),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                ),
                child: Image(
                  image: AssetImage(widget.image),
                )),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 200),
          ),
          Text(
            widget.name,
            style: TextStyle(
                fontSize: 12,
                color: Colors.white,
                fontFamily: 'OpenSansRegular'),
          ),
        ],
      ),
    );
  }
}
