import 'package:app_template/src/models/get_age_range_response.dart';
import 'package:app_template/src/models/get_rate_by_view.dart';
import 'package:app_template/src/models/get_session_increment_response.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DropDownCoachAndRef extends StatefulWidget {
  final List<String> dropDownList;
  final List<DatumAge> dropDownAgeList;
  final List<DatumSession> dropDownSession;
  final List<DatumRate> dropDownRate;
  final String dropDownType;
  final String hintText;
  final String label;
  final String dropDownValue;
  // final String dropDownValue;
  final ValueChanged onClicked;
  DropDownCoachAndRef(
      {this.hintText,
      this.label,
      this.dropDownList,
      this.dropDownValue,
      this.dropDownAgeList,
      this.onClicked,
      this.dropDownType,
      this.dropDownSession,
      this.dropDownRate});
  @override
  _DropDownCoachAndRefState createState() => _DropDownCoachAndRefState();
}

String dropdownValue;

class _DropDownCoachAndRefState extends State<DropDownCoachAndRef> {
  @override
  void initState() {
    // type = String;
    // TODO: implement initState
    super.initState();
  }

  // Type dropDownType = String;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: TextStyle(
              color: Colors.white, fontSize: 12, fontFamily: 'OpenSansRegular'),
        ),
        Container(
            height: screenHeight(context, dividedBy: 14),
            width: screenWidth(context, dividedBy: 1.24),
            // color: Colors.red,
            child: TextField(
              readOnly: true,
            )),
      ],
    );
  }
}
