import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class CommentBox extends StatefulWidget {
  TextEditingController eSignatureTextEditingController =
      new TextEditingController();
  double boxWidth;
  double boxHeight;
  CommentBox({
    this.eSignatureTextEditingController,
    this.boxWidth,
    this.boxHeight,
  });
  @override
  _CommentBoxState createState() => _CommentBoxState();
}

class _CommentBoxState extends State<CommentBox> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: [
          Container(
            width: screenWidth(context, dividedBy: widget.boxWidth),
            height: screenHeight(context, dividedBy: widget.boxHeight),
            color: Constants.kitGradients[6],
            child: TextField(
              controller: widget.eSignatureTextEditingController,
              autofocus: false,
              style: TextStyle(
                color: Colors.grey,
                fontFamily: 'MuliSemiBold',
                fontSize: 18,
              ),
              maxLines: 8,
              cursorColor: Colors.grey,
              decoration: InputDecoration(
                hintStyle: TextStyle(
                  color: Colors.grey,
                  fontFamily: 'MuliSemiBold',
                  fontSize: 18,
                ),
              ),
            ),
          ),
        ]),
      ],
    );
  }
}
