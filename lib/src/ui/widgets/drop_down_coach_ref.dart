import 'package:app_template/src/models/invitation_type_response_model.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DropDownCoachRefApp extends StatefulWidget {
  List<Datum> dropDownList;
  String title;
  Datum dropDownValue;
  ValueChanged<Datum> onClicked;
  DropDownCoachRefApp(
      {this.title, this.dropDownList, this.dropDownValue, this.onClicked});
  @override
  _DropDownCoachRefAppState createState() => _DropDownCoachRefAppState();
}

Datum dropdownValue;

class _DropDownCoachRefAppState extends State<DropDownCoachRefApp> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 10),
      width: screenWidth(context, dividedBy: 1.23),
      child: DropdownButton<Datum>(
        underline: Divider(
          thickness: 1.0,
          color: Constants.kitGradients[0],
        ),
        icon: Row(
          children: [
            Icon(
              Icons.arrow_drop_down,
              size: 30,
              color: Constants.kitGradients[0],
            ),
          ],
        ),
        iconSize: 20,
        dropdownColor: Colors.black,
        value:
            widget.dropDownValue == null ? dropdownValue : widget.dropDownValue,
        style: TextStyle(fontSize: 16, color: Constants.kitGradients[0]),
        items: widget.dropDownList
            .map<DropdownMenuItem<Datum>>(
                (Datum value) => DropdownMenuItem<Datum>(
                    value: value,
                    child: Container(
                        width: screenWidth(context, dividedBy: 1.34),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              value.name,
                              style: TextStyle(fontFamily: 'OpenSansRegular'),
                            ),
                          ],
                        ))))
            .toList(),
        onChanged: (selectedValue) {
          setState(() {
            widget.dropDownValue = selectedValue;
            print(widget.dropDownValue);
          });
          widget.onClicked(selectedValue);
        },
        hint: Column(
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),
            Text(
              widget.title,
              style: TextStyle(fontSize: 18, color: Constants.kitGradients[0]),
            ),
          ],
        ),
      ),
    );
  }
}
