// import 'dart:io';
//
// import 'package:app_template/src/models/anyline_result.dart';
// import 'package:app_template/src/utils/constants.dart';
// import 'package:flutter/material.dart';
// import 'package:photo_view/photo_view.dart';
//
// class ResultDisplay extends StatefulWidget {
//   final Result result;
//   ResultDisplay({this.result});
//   static const routeName = '/resultDisplay';
//
//   @override
//   _ResultDisplayState createState() => _ResultDisplayState();
// }
//
// class _ResultDisplayState extends State<ResultDisplay> {
//   @override
//   Widget build(BuildContext context) {
//     final Result result = ModalRoute.of(context).settings.arguments;
//
//     return Scaffold(
//       backgroundColor: Constants.kitGradients[3],
//       appBar: AppBar(
//         backgroundColor: Constants.kitGradients[3],
//         centerTitle: true,
//         title: Text(
//           " Result",
//           // style: GoogleFonts.montserrat(fontWeight: FontWeight.w400),
//         ),
//         elevation: 0,
//       ),
//       body: ClipRRect(
//         borderRadius: BorderRadius.all(Radius.circular(20)),
//         child: Container(
//           color: Colors.white,
//           child: ResultDetails(result.jsonMap),
//         ),
//       ),
//     );
//   }
// }
//
// class CompositeResultDisplay extends StatefulWidget {
//   static const routeName = '/compositeResultDisplay';
//   final Result result;
//   CompositeResultDisplay({this.result});
//   @override
//   _CompositeResultDisplayState createState() => _CompositeResultDisplayState();
// }
//
// class _CompositeResultDisplayState extends State<CompositeResultDisplay> {
//   @override
//   Widget build(BuildContext context) {
//     final Result result = widget.result;
//
//     var subResults = result.jsonMap.values.take(3);
//
//     List<Map<String, dynamic>> results = [
//       for (Map<String, dynamic> j in subResults) j,
//     ];
//
//     return DefaultTabController(
//       length: results.length,
//       child: Scaffold(
//         appBar: AppBar(
//           elevation: 0,
//           centerTitle: true,
//           // backgroundColor: Styles.backgroundBlack,
//           bottom: TabBar(
//             tabs: createResultTabs(results),
//             indicatorSize: TabBarIndicatorSize.label,
//             indicatorColor: Constants.kitGradients[1],
//             // labelStyle: GoogleFonts.montserrat(),
//           ),
//           title: FittedBox(
//               fit: BoxFit.fitWidth,
//               child: Text(
//                 "Result",
//                 // style: GoogleFonts.montserrat(fontWeight: FontWeight.w400),
//               )),
//         ),
//         body: ClipRRect(
//           borderRadius: BorderRadius.all(Radius.circular(20)),
//           child: TabBarView(
//             children: createResultTabViews(results),
//           ),
//         ),
//       ),
//     );
//   }
//
//   List<Tab> createResultTabs(List<Map<String, dynamic>> results) {
//     List<Tab> resultTabs = [];
//     for (var i = 1; (i <= results.length) && (i <= 3); i++) {
//       resultTabs.add(Tab(
//         text: 'Result $i',
//       ));
//     }
//     return resultTabs;
//   }
//
//   List<ResultDetails> createResultTabViews(List<Map<String, dynamic>> results) {
//     List<ResultDetails> resultTabViews = [];
//     for (var i = 0; (i < results.length) && (i < 3); i++) {
//       resultTabViews.add(ResultDetails(results[i]));
//     }
//     return resultTabViews;
//   }
// }
//
// class ResultDetails extends StatelessWidget {
//   final Map<String, dynamic> json;
//
//   ResultDetails(this.json);
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       color: Colors.white,
//       child: ListView(
//         children: [
//           Image.file(File(json['imagePath'])),
//           ListView.builder(
//               shrinkWrap: true,
//               physics: ScrollPhysics(),
//               itemCount: json.length,
//               itemBuilder: (BuildContext ctx, int index) {
//                 return new ListTile(
//                   title: Text(json.values.toList()[index].toString()),
//                   subtitle: Text(json.keys.toList()[index].toString()),
//                 );
//               }),
//           Container(
//             padding: EdgeInsets.fromLTRB(25, 0, 25, 0),
//             child: TextButton(
//               child: Text('Show Full Image'),
//               onPressed: () {
//                 Navigator.pushNamed(context, FullScreenImage.routeName,
//                     arguments: json['fullImagePath']);
//               },
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
//
// class FullScreenImage extends StatelessWidget {
//   static const routeName = '/resultDisplay/fullImage';
//
//   @override
//   Widget build(BuildContext context) {
//     final String fullImagePath = ModalRoute.of(context).settings.arguments;
//
//     return GestureDetector(
//       child: Container(
//         child: PhotoView(
//           imageProvider: FileImage(File(fullImagePath)),
//         ),
//       ),
//       onTap: () {
//         Navigator.pop(context);
//       },
//     );
//   }
// }
