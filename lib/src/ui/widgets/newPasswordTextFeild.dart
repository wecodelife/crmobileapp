import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class NewPasswordTextFeild extends StatefulWidget {
  final String labelText;
  final bool readOnly;
  final ValueChanged onValueChanged;
  final TextEditingController textEditingController;

  NewPasswordTextFeild({
    this.labelText,
    this.textEditingController,
    this.onValueChanged,
    this.readOnly,
  });
  @override
  _NewPasswordTextFeildState createState() => _NewPasswordTextFeildState();
}

class _NewPasswordTextFeildState extends State<NewPasswordTextFeild> {
  bool visibility = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 12),
      child: TextField(
        controller: widget.textEditingController,
        obscureText: visibility,
        autofocus: false,
        readOnly: widget.readOnly ?? false,
        onChanged: widget.onValueChanged,
        style: TextStyle(
          color: Constants.kitGradients[0],
          fontFamily: 'OpenSansRegular',
          fontSize: 16,
        ),
        cursorColor: Constants.kitGradients[0],
        decoration: InputDecoration(
            hintStyle: TextStyle(
                color: Colors.white,
                fontSize: 14,
                fontFamily: 'OpenSansSemiBold'),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Constants.kitGradients[0]),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Constants.kitGradients[0]),
            ),
            labelText: widget.labelText,
            labelStyle: TextStyle(
              color: Constants.kitGradients[0],
              fontFamily: 'OpenSansRegular',
              fontSize: 16,
            ),
            suffixIcon: GestureDetector(
              onTap: () {
                setState(() {
                  visibility = !visibility;
                });
              },
              child: Icon(
                visibility == true
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
                color: Colors.grey,
              ),
            )),
      ),
    );
  }
}
