import 'dart:async';

import 'package:app_template/src/models/add_athlete_request_model.dart';
import 'package:app_template/src/models/add_athlete_response.dart';
import 'package:app_template/src/models/add_authorized_contact_request_model.dart';
import 'package:app_template/src/models/add_authorized_contact_response_model.dart';
import 'package:app_template/src/models/add_banking_info_request.dart';
import 'package:app_template/src/models/add_banking_info_response.dart';
import 'package:app_template/src/models/add_business_banking_info_request.dart';
import 'package:app_template/src/models/add_business_banking_info_response.dart';
import 'package:app_template/src/models/add_coach_request.dart';
import 'package:app_template/src/models/add_coach_response.dart';
import 'package:app_template/src/models/add_emergency_contact_request_model.dart';
import 'package:app_template/src/models/add_emergency_contact_response_model.dart';
import 'package:app_template/src/models/add_facility_rate_request.dart';
import 'package:app_template/src/models/add_facility_rate_response.dart';
import 'package:app_template/src/models/add_facility_request.dart';
import 'package:app_template/src/models/add_facility_response.dart';
import 'package:app_template/src/models/add_onboarding_status_response.dart';
import 'package:app_template/src/models/add_parent_request_model.dart';
import 'package:app_template/src/models/add_parent_response_model.dart';
import 'package:app_template/src/models/add_reference_request.dart';
import 'package:app_template/src/models/add_reference_response.dart';
import 'package:app_template/src/models/basic_user_details_response.dart';
import 'package:app_template/src/models/book_zoom_meeting_request.dart';
import 'package:app_template/src/models/book_zoom_meeting_response.dart';
import 'package:app_template/src/models/delete_athlete_response.dart';
import 'package:app_template/src/models/get_age_range_response.dart';
import 'package:app_template/src/models/get_athlete_list_response.dart';
import 'package:app_template/src/models/get_bank_response.dart';
import 'package:app_template/src/models/get_billing_address_response.dart';
import 'package:app_template/src/models/get_facility_response.dart';
import 'package:app_template/src/models/get_facility_type_response.dart';
import 'package:app_template/src/models/get_lease_response.dart';
import 'package:app_template/src/models/get_level_of_education_response.dart';
import 'package:app_template/src/models/get_level_of_play_response.dart';
import 'package:app_template/src/models/get_payment_method_response.dart';
import 'package:app_template/src/models/get_profile_response.dart';
import 'package:app_template/src/models/get_rate_by_view.dart';
import 'package:app_template/src/models/get_session_increment_response.dart';
import 'package:app_template/src/models/get_sports_response_model.dart';
import 'package:app_template/src/models/get_states_response.dart';
import 'package:app_template/src/models/get_timeslots_request.dart';
import 'package:app_template/src/models/get_timeslots_response.dart';
import 'package:app_template/src/models/get_trainer_list_response.dart';
import 'package:app_template/src/models/invitation_type_response_model.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/login_response_model.dart';
import 'package:app_template/src/models/state.dart';
import 'package:app_template/src/models/update_sports_request.dart';
import 'package:app_template/src/models/update_sports_response.dart';
import 'package:app_template/src/models/update_user_type_request_model.dart';
import 'package:app_template/src/models/update_user_type_response_model.dart';
import 'package:app_template/src/models/upload_image_request_model.dart';
import 'package:app_template/src/models/upload_image_response_model.dart';
import 'package:app_template/src/models/upload_video_request.dart';
import 'package:app_template/src/models/upload_video_response.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/validators.dart';

import 'base_bloc.dart';

/// api response of login is managed by AuthBloc
/// stream data is handled by StreamControllers

class UserBloc extends Object with Validators implements BaseBloc {
  StreamController<bool> _loading = new StreamController<bool>.broadcast();

  StreamController<LoginResponse> _login =
      new StreamController<LoginResponse>.broadcast();

  StreamController<BasicUserDetailsResponseModel> _basicUserDetails =
      new StreamController<BasicUserDetailsResponseModel>.broadcast();

  StreamController<InvitationTypeResponseModel> _userInvitationType =
      new StreamController<InvitationTypeResponseModel>.broadcast();

  StreamController<UpdateUserTypeResponseModel> _updateUserType =
      new StreamController<UpdateUserTypeResponseModel>.broadcast();

  StreamController<AddParentResponseModel> _addParent =
      new StreamController<AddParentResponseModel>.broadcast();

  StreamController<UploadImageResponse> _uploadImage =
      new StreamController<UploadImageResponse>.broadcast();

  StreamController<GetSportsResponseModel> _getSports =
      new StreamController<GetSportsResponseModel>.broadcast();

  StreamController<AddEmergencyContactResponseModel> _addEmergencyContact =
      new StreamController<AddEmergencyContactResponseModel>.broadcast();

  StreamController<AddAuthorizedContactResponseModel> _addAuthorizedContact =
      new StreamController<AddAuthorizedContactResponseModel>.broadcast();

  StreamController<AddAthleteResponseModel> _addAthlete =
      new StreamController<AddAthleteResponseModel>.broadcast();

  StreamController<AddFacilityResponse> _addFacility =
      new StreamController<AddFacilityResponse>.broadcast();

  StreamController<GetFacilityResponse> _getFacility =
      new StreamController<GetFacilityResponse>.broadcast();

  StreamController<GetFacilityTypeResponse> _getFacilityType =
      new StreamController<GetFacilityTypeResponse>.broadcast();

  StreamController<GetProfileResponse> _getProfile =
      new StreamController<GetProfileResponse>.broadcast();

  StreamController<GetAthleteListResponse> _getAthleteList =
      new StreamController<GetAthleteListResponse>.broadcast();

  StreamController<DeleteAthleteResponse> _deleteAthlete =
      new StreamController<DeleteAthleteResponse>.broadcast();

  StreamController<UpdateSportsResponse> _updateSports =
      new StreamController<UpdateSportsResponse>.broadcast();

  StreamController<GetAgeRangeResponse> _getAgeRange =
      new StreamController<GetAgeRangeResponse>.broadcast();

  StreamController<GetSessionIncrementResponse> _getSessionIncrement =
      new StreamController<GetSessionIncrementResponse>.broadcast();

  StreamController<UploadVideoResponse> _uploadVideo =
      new StreamController<UploadVideoResponse>.broadcast();

  StreamController<GetLevelOfEducationResponse> _getLevelOfEducation =
      new StreamController<GetLevelOfEducationResponse>.broadcast();

  StreamController<GetRateByViewResponse> _getRateByView =
      new StreamController<GetRateByViewResponse>.broadcast();

  StreamController<AddCoachResponse> _addCoach =
      new StreamController<AddCoachResponse>.broadcast();

  StreamController<GetLevelOfPlayResponse> _getLevelOfPlay =
      new StreamController<GetLevelOfPlayResponse>.broadcast();

  StreamController<AddReferenceResponse> _addReference =
      new StreamController<AddReferenceResponse>.broadcast();

  StreamController<GetStatesResponse> _getStates =
      new StreamController<GetStatesResponse>.broadcast();

  StreamController<AddBankingInfoResponse> _addBankingInfo =
      new StreamController<AddBankingInfoResponse>.broadcast();

  StreamController<GetPaymentMethodResponse> _getPaymentMethod =
      new StreamController<GetPaymentMethodResponse>.broadcast();

  StreamController<GetLeaseRateResponse> _getLeaseRates =
      new StreamController<GetLeaseRateResponse>.broadcast();

  StreamController<GetBankResponse> _getBank =
      new StreamController<GetBankResponse>.broadcast();

  StreamController<AddFacilityRateResponse> _addFacilityRate =
      new StreamController<AddFacilityRateResponse>.broadcast();

  StreamController<AddBusinessBankingInfoResponse> _addBusinessBankingInfo =
      new StreamController<AddBusinessBankingInfoResponse>.broadcast();

  StreamController<GetTrainerListResponse> _getTrainerList =
      new StreamController<GetTrainerListResponse>.broadcast();

  StreamController<GetTimeSlotsResponse> _getTimeSlots =
      new StreamController<GetTimeSlotsResponse>.broadcast();

  StreamController<BookZoomMeetingResponse> _bookZoomMeetings =
      new StreamController<BookZoomMeetingResponse>.broadcast();

  StreamController<AddOnBoardStatusResponse> _addOnBoardingStatus =
      new StreamController<AddOnBoardStatusResponse>.broadcast();

  StreamController<GetBillingAddressResponse> _getBillingAddress =
      new StreamController<GetBillingAddressResponse>.broadcast();
  // ignore: close_sinks

  // stream controller is broadcasting the  details

  /// stream for progress bar
  Stream<bool> get loadingListener => _loading.stream;

  Stream<LoginResponse> get loginResponse => _login.stream;

  Stream<BasicUserDetailsResponseModel> get basicUserDetailsResponse =>
      _basicUserDetails.stream;

  Stream<InvitationTypeResponseModel> get userInvitationTypeResponse =>
      _userInvitationType.stream;

  Stream<UpdateUserTypeResponseModel> get updateUserTypeResponse =>
      _updateUserType.stream;

  Stream<UploadImageResponse> get uploadImageResponse => _uploadImage.stream;

  Stream<AddParentResponseModel> get addParentResponse => _addParent.stream;

  Stream<GetSportsResponseModel> get getSportsResponse => _getSports.stream;

  Stream<AddEmergencyContactResponseModel> get addEmergencyContactResponse =>
      _addEmergencyContact.stream;

  Stream<AddAuthorizedContactResponseModel> get addAuthorizedContactResponse =>
      _addAuthorizedContact.stream;

  Stream<AddAthleteResponseModel> get addAthleteResponse => _addAthlete.stream;

  Stream<AddFacilityResponse> get addFacilityResponse => _addFacility.stream;

  Stream<GetFacilityResponse> get getFacilityResponse => _getFacility.stream;

  Stream<GetFacilityTypeResponse> get getFacilityTypeResponse =>
      _getFacilityType.stream;

  Stream<GetProfileResponse> get getProfileResponse => _getProfile.stream;

  Stream<GetAthleteListResponse> get getAthleteListResponse =>
      _getAthleteList.stream;

  Stream<DeleteAthleteResponse> get deleteAthleteResponse =>
      _deleteAthlete.stream;

  Stream<UpdateSportsResponse> get updateSportsResponse => _updateSports.stream;

  Stream<GetAgeRangeResponse> get getAgeRangeResponse => _getAgeRange.stream;

  Stream<GetSessionIncrementResponse> get getSessionIncrementResponse =>
      _getSessionIncrement.stream;

  Stream<UploadVideoResponse> get uploadVideoResponse => _uploadVideo.stream;

  Stream<GetLevelOfEducationResponse> get getLevelOfEducationResponse =>
      _getLevelOfEducation.stream;

  Stream<GetRateByViewResponse> get getRateByViewResponse =>
      _getRateByView.stream;

  Stream<AddCoachResponse> get addCoachResponse => _addCoach.stream;

  Stream<GetLevelOfPlayResponse> get getLevelOfPlayResponse =>
      _getLevelOfPlay.stream;

  Stream<AddReferenceResponse> get addReferenceResponse => _addReference.stream;

  Stream<GetStatesResponse> get getStatesResponse => _getStates.stream;

  Stream<AddBankingInfoResponse> get addBankingInfoResponse =>
      _addBankingInfo.stream;

  Stream<GetPaymentMethodResponse> get getPaymentMethodResponse =>
      _getPaymentMethod.stream;

  Stream<GetLeaseRateResponse> get getLeaseRateResponse =>
      _getLeaseRates.stream;

  Stream<GetBankResponse> get getBankResponse => _getBank.stream;

  Stream<AddFacilityRateResponse> get addFacilityRateResponse =>
      _addFacilityRate.stream;

  Stream<AddBusinessBankingInfoResponse> get addBusinessBankingInfoResponse =>
      _addBusinessBankingInfo.stream;

  Stream<GetTrainerListResponse> get getTrainerListResponse =>
      _getTrainerList.stream;

  Stream<GetTimeSlotsResponse> get getTimeSlotsResponse => _getTimeSlots.stream;

  Stream<BookZoomMeetingResponse> get bookZoomMeetingResponse =>
      _bookZoomMeetings.stream;

  Stream<AddOnBoardStatusResponse> get addOnBoardStatusResponse =>
      _addOnBoardingStatus.stream;

  Stream<GetBillingAddressResponse> get getBillingAddressResponse =>
      _getBillingAddress.stream;

  StreamSink<bool> get loadingSink => _loading.sink;

  login({LoginRequest loginRequest}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.login(
        loginRequest: LoginRequest(
            username: loginRequest.username, password: loginRequest.password));

    if (state is SuccessState) {
      loadingSink.add(false);
      _login.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _login.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getBasicUserDetails() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getBasicUserDetails();

    if (state is SuccessState) {
      loadingSink.add(false);
      _basicUserDetails.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _basicUserDetails.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getUserInvitationType() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getUserInvitationType();

    if (state is SuccessState) {
      loadingSink.add(false);
      _userInvitationType.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _userInvitationType.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  updateUserType(
      {UpdateUserTypeRequestModel updateUserTypeRequestModel}) async {
    loadingSink.add(true);

    State state = await ObjectFactory()
        .repository
        .updateUserType(updateUserTypeRequestModel);

    if (state is SuccessState) {
      loadingSink.add(false);
      _updateUserType.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _updateUserType.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  addParent({AddParentRequestModel addParentRequestModel}) async {
    loadingSink.add(true);

    State state =
        await ObjectFactory().repository.addParent(addParentRequestModel);

    if (state is SuccessState) {
      loadingSink.add(false);
      _addParent.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _addParent.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  uploadImage({UploadImageRequest uploadImageRequest}) async {
    loadingSink.add(true);

    State state =
        await ObjectFactory().repository.uploadImage(uploadImageRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _uploadImage.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _uploadImage.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getSports() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getSports();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getSports.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getSports.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  addEmergencyContact(
      {AddEmergencyContactRequestModel addEmergencyContactRequestModel}) async {
    loadingSink.add(true);

    State state = await ObjectFactory()
        .repository
        .addEmergencyContact(addEmergencyContactRequestModel);

    if (state is SuccessState) {
      loadingSink.add(false);
      _addEmergencyContact.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _addEmergencyContact.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  addAuthorizedContact(
      {AddAuthorizedContactRequestModel
          addAuthorizedContactRequestModel}) async {
    loadingSink.add(true);

    State state = await ObjectFactory()
        .repository
        .addAuthorizedContact(addAuthorizedContactRequestModel);

    if (state is SuccessState) {
      loadingSink.add(false);
      _addAuthorizedContact.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _addAuthorizedContact.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  addAthlete({AddAthleteRequestModel addAthleteRequestModel}) async {
    loadingSink.add(true);

    State state =
        await ObjectFactory().repository.addAthlete(addAthleteRequestModel);

    if (state is SuccessState) {
      loadingSink.add(false);
      _addAthlete.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _addAthlete.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getFacility() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getFacility();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getFacility.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getFacility.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  addFacility({AddFacilityRequest addFacilityRequest}) async {
    loadingSink.add(true);

    State state =
        await ObjectFactory().repository.addFacility(addFacilityRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _addFacility.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _addFacility.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getFacilityType() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getFacilityType();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getFacilityType.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getFacilityType.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getProfile() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getProfile();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getProfile.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getProfile.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getAthleteList() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getAthleteList();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getAthleteList.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getAthleteList.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  deleteAthlete({int id}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.deleteAthlete(id);

    if (state is SuccessState) {
      loadingSink.add(false);
      _deleteAthlete.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _deleteAthlete.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  updateSports({UpdateSportsRequest updateSportsRequest}) async {
    loadingSink.add(true);

    State state =
        await ObjectFactory().repository.updateSports(updateSportsRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _updateSports.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _updateSports.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getAgeRange() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getAgeRange();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getAgeRange.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getAgeRange.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getSessionIncrement() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getSessionIncrement();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getSessionIncrement.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getSessionIncrement.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  uploadVideo({UploadVideoRequest uploadVideoRequest}) async {
    loadingSink.add(true);

    State state =
        await ObjectFactory().repository.uploadVideo(uploadVideoRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _uploadVideo.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _uploadVideo.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getLevelOfEducation() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getLevelOfEducation();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getLevelOfEducation.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getLevelOfEducation.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getRateByView() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getRateByView();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getRateByView.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getRateByView.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  addCoach({AddCoachRequest addCoachRequest}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.addCoach(addCoachRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _addCoach.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _addCoach.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getLevelOfPlay() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getLevelOfPlay();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getLevelOfPlay.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getLevelOfPlay.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  addReference({AddReferenceRequest addReferenceRequest}) async {
    loadingSink.add(true);

    State state =
        await ObjectFactory().repository.addReference(addReferenceRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _addReference.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _addReference.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getStates() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getStates();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getStates.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getStates.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  addBankingInfo({AddBankingInfoRequest addBankingInfoRequest}) async {
    loadingSink.add(true);

    State state =
        await ObjectFactory().repository.addBankingInfo(addBankingInfoRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _addBankingInfo.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _addBankingInfo.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getPaymentMethods() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getPaymentMethods();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getPaymentMethod.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getPaymentMethod.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getLeaseRates() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getLeaseRates();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getLeaseRates.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getLeaseRates.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getBanks() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getBanks();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getBank.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getBank.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  addFacilityRate({AddFacilityRateRequest addFacilityRateRequest}) async {
    loadingSink.add(true);

    State state = await ObjectFactory()
        .repository
        .addFacilityRate(addFacilityRateRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _addFacilityRate.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _addFacilityRate.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  addBusinessBankingInfo(
      {AddBusinessBankingInfo addBusinessBankingInfo}) async {
    loadingSink.add(true);

    State state = await ObjectFactory()
        .repository
        .addBusinessBankInfo(addBusinessBankingInfo);

    if (state is SuccessState) {
      loadingSink.add(false);
      _addBusinessBankingInfo.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _addBusinessBankingInfo.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getTrainerList() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getTrainerList();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getTrainerList.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getTrainerList.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getTimeSlots({GetTimeSlotsRequest getTimeSlotsRequest}) async {
    loadingSink.add(true);

    State state =
        await ObjectFactory().repository.getTimeSlots(getTimeSlotsRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _getTimeSlots.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getTimeSlots.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  bookZoomMeeting({BookZoomMeetingRequest bookZoomMeetingRequest}) async {
    loadingSink.add(true);

    State state = await ObjectFactory()
        .repository
        .bookZoomMeeting(bookZoomMeetingRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _bookZoomMeetings.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _bookZoomMeetings.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  addOnBoardingStatus() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.addOnBoardingStatus();

    if (state is SuccessState) {
      loadingSink.add(false);
      _addOnBoardingStatus.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _addOnBoardingStatus.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getBillingAddress() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getBillingAddress();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getBillingAddress.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getBillingAddress.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  ///disposing the stream if it is not using
  @override
  void dispose() {
    _loading?.close();
    _login?.close();
    _basicUserDetails?.close();
    _userInvitationType?.close();
    _updateUserType?.close();
    _addParent?.close();
    _uploadImage?.close();
    _getSports?.close();
    _addEmergencyContact?.close();
    _addAuthorizedContact?.close();
    _addAthlete?.close();
    _addFacility?.close();
    _getFacility?.close();
    _getFacilityType?.close();
    _getProfile?.close();
    _getAthleteList?.close();
    _deleteAthlete?.close();
    _updateSports?.close();
    _getAgeRange?.close();
    _getSessionIncrement?.close();
    _uploadVideo?.close();
    _getLevelOfEducation?.close();
    _getRateByView?.close();
    _addCoach?.close();
    _getLevelOfPlay?.close();
    _addReference?.close();
    _getStates?.close();
    _addBankingInfo?.close();
    _getPaymentMethod?.close();
    _getLeaseRates?.close();
    _getBank?.close();
    _addFacilityRate?.close();
    _addBusinessBankingInfo?.close();
    _getTrainerList?.close();
    _getTimeSlots?.close();
    _bookZoomMeetings?.close();
    _addOnBoardingStatus?.close();
    _getBillingAddress?.close();
  }
}
