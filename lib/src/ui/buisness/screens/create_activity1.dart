import 'package:app_template/material/pickers/date_range_picker_dialog.dart';
import 'package:app_template/src/ui/buisness/widgets/bottom_navigation_bar_business.dart';
import 'package:app_template/src/ui/widgets/appbar_cr.dart';
import 'package:app_template/src/ui/widgets/facility_soccer_filed_tile.dart';
import 'package:app_template/src/ui/widgets/search_page_search_bar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class CreateActivity1 extends StatefulWidget {
  @override
  _CreateActivity1State createState() => _CreateActivity1State();
}

class _CreateActivity1State extends State<CreateActivity1> {
  TextEditingController searchTextEditingController =
      new TextEditingController();

  List<String> titles = [
    "Basketball Field",
    "Volleyball Field",
    "Football Field",
    "Swimming Field",
    "Baseball Field",
    "Hockey Field",
    "Soccer Field",
    "Tennis Field"
  ];
  List<String> subtitles = [
    "Field A",
    "Field C",
    "Field B",
    "Field D",
    "Field A",
    "Field C",
    "Field B",
    "Field D"
  ];
  List<String> images = [
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png"
  ];
  List<String> icons = [
    "assets/icons/basketball.svg",
    "assets/icons/volleyball.svg",
    "assets/icons/basket_ball_icon.svg",
    "assets/icons/swimming.svg",
    "assets/icons/baseball.svg",
    "assets/icons/hockey.svg",
    "assets/icons/soccer.svg",
    "assets/icons/tennis.svg"
  ];
  Future<Null> selectDateRange(BuildContext context) async {
    DateTimeRange picked = await showDatesRangePicker(
        context: context,
        // initialDateRange: DateTimeRange(
        //   start: DateTime.now().add(Duration(days: 0)),
        //   end: DateTime.now().add(Duration(days: 1)),
        // ),

        firstDate: DateTime.now(),
        lastDate: DateTime(DateTime.now().year + 5),
        helpText: "Select_Date_Range",
        cancelText: "CANCEL",
        confirmText: "OK",
        saveText: "SAVE",
        bottomNavigationWidget: BottomNavigationBarBusiness(),
        errorFormatText: "Invalid_format",
        errorInvalidText: "Out_of_range",
        errorInvalidRangeText: "Invalid_range",
        fieldStartHintText: "Start_Date",
        sessionName: "activity",
        fieldEndLabelText: "End_Date");

    // if (picked != null) {
    //   state.didChange(picked);
    // }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Constants.kitGradients[3],
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          leading: Container(),
          actions: [
            AppBarCr(
              leftTextYes: true,
              leftText: "",
              onTapLeftIcon: () {},
              onTapRightIcon: () {
                //push(context, NewChat());
              },
              rightTextYes: false,
              rightIcon: "assets/icons/icon_plus.svg",
              pageTitle: "FACILITIES",
            ),
          ],
        ),
        body: Container(
          width: screenWidth(context, dividedBy: 1),
          //color:Colors.red,
          child: Column(
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              ),
              Row(
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 20),
                  ),
                  Text(
                    "STEP 1 : CHOOSE A FACILITY",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                        fontFamily: 'OswaldBold'),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 30)),
                child: SearchBarSearchPage(
                  hintText: "Start typing to search..",
                  textEditingController: searchTextEditingController,
                  rightIcon: false,
                  leftIcon: "assets/icons/search_icon.svg",
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              Container(
                height: screenHeight(context, dividedBy: 1.37),
                child: ListView(
                  children: [
                    Container(
                      //flex: 4,
                      child: ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: titles.length,
                          itemBuilder: (BuildContext context, int index) {
                            return FacilitySoccerFieldTile(
                              title: titles[index],
                              image: images[index],
                              subtitle: subtitles[index],
                              icon: icons[index],
                              hasIcon: true,
                              hasButton: true,
                              pricePerDay: "200",
                              pricePerHour: "40",
                              hasPrice: true,
                              buttonTitle: "Book Now",
                              onPressed: () {
                                selectDateRange(context);
                              },
                            );
                          }),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
