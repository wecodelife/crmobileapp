import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class GameSelectionTile extends StatefulWidget {
  ValueChanged selectedValue;
  String title;
  GameSelectionTile({this.selectedValue, this.bgImage, this.title});
  int index;
  String bgImage;
  @override
  _GameSelectionTileState createState() => _GameSelectionTileState();
}

class _GameSelectionTileState extends State<GameSelectionTile> {
  bool click = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          click = !click;
        });
        widget.selectedValue(click);
      },
      child: GridTile(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(widget.bgImage), fit: BoxFit.cover),
              border: Border.all(
                  color: click == true
                      ? Theme.of(context).buttonColor
                      : Theme.of(context).scaffoldBackgroundColor)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 20)),
                child: Container(
                  child: Center(
                    child: Text(
                      widget.title,
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'OswaldBold',
                          fontSize: 17),
                      overflow: TextOverflow.visible,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
