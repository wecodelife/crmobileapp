import 'package:app_template/src/ui/widgets/button.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FacilitySoccerFieldTile extends StatefulWidget {
  final String image;
  final String title;
  final String icon;
  final String subtitle;
  final Function onPressed;
  final bool hasIcon;
  final bool hasButton;
  final String buttonTitle;
  final bool hasPrice;
  final String pricePerDay;
  final String pricePerHour;
  FacilitySoccerFieldTile(
      {this.image,
      this.title,
      this.icon,
      this.subtitle,
      this.hasIcon,
      this.hasButton,
      this.buttonTitle,
      this.hasPrice,
      this.pricePerDay,
      this.pricePerHour,
      this.onPressed});
  @override
  _FacilitySoccerFieldTileState createState() =>
      _FacilitySoccerFieldTileState();
}

class _FacilitySoccerFieldTileState extends State<FacilitySoccerFieldTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      child: ListTile(
        leading: Image.asset(widget.image),
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.title,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 13,
                  fontFamily: 'OpenSansSemiBold'),
            ),
            SizedBox(
              width: screenWidth(context, dividedBy: 70),
            ),
            widget.hasIcon == false
                ? Container()
                : SvgPicture.asset(
                    widget.icon,
                    width: screenWidth(
                      context,
                      dividedBy: 20,
                    ),
                    height: screenHeight(context, dividedBy: 40),
                  ),
            Spacer(
              flex: 20,
            ),
            widget.hasPrice == false
                ? Container()
                : Container(
                    width: screenWidth(context, dividedBy: 3),
                    child: Row(
                      children: [
                        Text(
                          "\$" + widget.pricePerHour + "/hr",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 13,
                              fontFamily: 'OpenSansSemiBold'),
                        ),
                        SizedBox(
                          width: screenWidth(context, dividedBy: 70),
                        ),
                        Text(
                          "\$" + widget.pricePerDay + "/day",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 13,
                              fontFamily: 'OpenSansSemiBold'),
                        )
                      ],
                    ))
          ],
        ),
        subtitle: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  widget.subtitle,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 13,
                      fontFamily: 'OpenSansSemiBold'),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 2.6),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          widget.hasButton
                              ? Container(
                                  width: screenWidth(context, dividedBy: 2.7),
                                  height: screenHeight(context, dividedBy: 25),
                                  child: CRButton(
                                    buttonName: widget.buttonTitle,
                                    buttonWidth: 100,
                                    buttonBackgroundColor: Color(0xff3F4A40),
                                    buttonBorder: false,
                                    borderRadius: 0,
                                    textColor: Colors.white,
                                    disabled: true,
                                    onPressed: () {
                                      widget.onPressed();
                                    },
                                  ),
                                )
                              : Container(),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
