import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


class AuthButton extends StatefulWidget {
  Color buttonColor;
  String mediaSymbol;
  String mediaName;
  double boxWidth;
  AuthButton({this.boxWidth,this.buttonColor,this.mediaName,this.mediaSymbol});
  @override
  _AuthButtonState createState() => _AuthButtonState();
}

class _AuthButtonState extends State<AuthButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
         width: screenWidth(context,dividedBy:widget.boxWidth),
        height: screenHeight(context,dividedBy: 20),
        child: RaisedButton(
          elevation: 0,
          padding: EdgeInsets.only(),
          color: widget.buttonColor,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3)),
          onPressed: () {},
          child: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SvgPicture.asset(widget.mediaSymbol,width: screenWidth(context,dividedBy:17 ),),
                Text(
                  widget.mediaName,
                  style: TextStyle(
                      color: Constants.kitGradients[0],
                      fontFamily: 'OpenSansRegular',
                      fontSize: screenWidth(context,dividedBy: 24),),
                  overflow: TextOverflow.clip,
                ),
              ],
            ),
          ),
        ));;
  }
}
