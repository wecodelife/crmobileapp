// To parse this JSON data, do
//
//     final updateSportsRequest = updateSportsRequestFromJson(jsonString);

import 'dart:convert';

UpdateSportsRequest updateSportsRequestFromJson(String str) => UpdateSportsRequest.fromJson(json.decode(str));

String updateSportsRequestToJson(UpdateSportsRequest data) => json.encode(data.toJson());

class UpdateSportsRequest {
  UpdateSportsRequest({
    this.athlete,
    this.sports,
  });

  final int athlete;
  final List<int> sports;

  factory UpdateSportsRequest.fromJson(Map<String, dynamic> json) => UpdateSportsRequest(
    athlete: json["athlete"] == null ? null : json["athlete"],
    sports: json["sports"] == null ? null : List<int>.from(json["sports"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "athlete": athlete == null ? null : athlete,
    "sports": sports == null ? null : List<dynamic>.from(sports.map((x) => x)),
  };
}
