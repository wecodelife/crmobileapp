import 'package:app_template/src/ui/widgets/summary_widget_email.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CoachJobListTile extends StatefulWidget {
  final String locationHeading;
  final String address;
  final String contact;
  final String date;
  final String startTime;
  final String sport;
  final String duration;
  final String amount;
  final Function onPressed;
  CoachJobListTile(
      {this.address,
      this.locationHeading,
      this.contact,
      this.startTime,
      this.date,
      this.duration,
      this.amount,
      this.onPressed,
      this.sport});
  @override
  _CoachJobListTileState createState() => _CoachJobListTileState();
}

class _CoachJobListTileState extends State<CoachJobListTile> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            widget.onPressed();
          },
          child: Container(
            height: screenHeight(context, dividedBy: 2.45),
            width: screenWidth(context, dividedBy: 1),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    bottomLeft: Radius.circular(10)),
                color: Constants.kitGradients[5]),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: screenHeight(context, dividedBy: 2.5),
                  width: screenWidth(context, dividedBy: 50),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        bottomLeft: Radius.circular(10)),
                    color: Constants.kitGradients[12],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      //horizontal: screenWidth(context, dividedBy: 40),
                      vertical: screenHeight(context, dividedBy: 30)),
                  child: Container(
                    width: screenWidth(context, dividedBy: 1.1),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: screenWidth(context, dividedBy: 30),
                            ),
                            Container(
                              width: screenWidth(context, dividedBy: 1.4),
                              child: Text(
                                "Training Sessions",
                                style: TextStyle(
                                    fontSize: 13,
                                    fontFamily: 'OpenSansSemiBold',
                                    color: Colors.white),
                              ),
                            ),
                            // Spacer(
                            //   flex: 4,
                            // ),
                            SizedBox(
                              width: screenWidth(context, dividedBy: 50),
                            ),
                            Container(
                                child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Container(
                                    width: screenWidth(context, dividedBy: 19),
                                    height:
                                        screenHeight(context, dividedBy: 40),
                                    child: SvgPicture.asset(
                                        "assets/icons/call.svg")),
                                SizedBox(
                                  width: screenWidth(context, dividedBy: 50),
                                ),
                                Container(
                                    width: screenWidth(context, dividedBy: 19),
                                    height:
                                        screenHeight(context, dividedBy: 40),
                                    child: SvgPicture.asset(
                                        "assets/icons/email.svg"))
                              ],
                            )),
                          ],
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: screenWidth(context, dividedBy: 30),
                            ),
                            SummaryTileEmail(
                              heading: "Contact",
                              subheading: widget.contact,
                              width: 3.8,
                            ),
                            SummaryTileEmail(
                              heading: "Date",
                              subheading: widget.duration,
                              width: 3.8,
                            ),
                            SummaryTileEmail(
                                heading: "Start Time",
                                subheading: widget.startTime,
                                width: 3.7),
                          ],
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: screenWidth(context, dividedBy: 30),
                            ),
                            SummaryTileEmail(
                              icon: true,
                              image: "assets/icons/soccer_icon.svg",
                              heading: "Sport",
                              subheading: widget.sport,
                              width: 3.8,
                            ),
                            SummaryTileEmail(
                              heading: "Duration",
                              subheading: widget.duration,
                              width: 3.8,
                            ),
                            SummaryTileEmail(
                              heading: "Amount",
                              subheading: "\$" + widget.amount,
                              width: 3.8,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 40),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                            horizontal: screenWidth(context, dividedBy: 30),
                          ),
                          width: screenWidth(context, dividedBy: 3),
                          child: Text(
                            "Location",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'OpenSansSemiBold',
                                fontSize: screenWidth(context, dividedBy: 28)),
                          ),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 60),
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: screenWidth(context, dividedBy: 30),
                            ),
                            Icon(
                              Icons.location_on,
                              size: 20,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: screenWidth(context, dividedBy: 30),
                            ),
                            Container(
                              width: screenWidth(context, dividedBy: 1.4),
                              child: Text(
                                widget.locationHeading,
                                style: TextStyle(
                                    fontSize: 13,
                                    fontFamily: 'OpenSansSemiBold',
                                    color: Colors.white),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 80),
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: screenWidth(context, dividedBy: 30),
                            ),
                            Container(
                              width: screenWidth(context, dividedBy: 1.4),
                              child: Text(
                                widget.address,
                                style: TextStyle(
                                    fontSize: 13,
                                    fontFamily: 'OpenSansSemiBold',
                                    color: Colors.white),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 40),
        )
      ],
    );
  }
}
