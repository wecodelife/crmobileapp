// import 'package:app_template/src/models/anyline_result.dart';
// import 'package:app_template/src/models/anyline_scanmodes.dart';
// import 'package:app_template/src/ui/screens/resut_display.dart';
// import 'package:app_template/src/ui/widgets/anyline_service.dart';
// import 'package:app_template/src/utils/utils.dart';
// import 'package:flutter/material.dart';
//
// class ScanPage extends StatefulWidget {
//   @override
//   _ScanPageState createState() => _ScanPageState();
// }
//
// class _ScanPageState extends State<ScanPage> {
//   AnylineService _anylineService;
//
//   @override
//   void initState() {
//     _anylineService = AnylineServiceImpl();
//     super.initState();
//   }
//
//   Future<void> scan(ScanMode mode) async {
//     Result result = await _anylineService.scan(mode);
//     _openResultDisplay(result);
//   }
//
//   _openResultDisplay(Result result) {
//     result.scanMode.isCompositeScan()
//         ? push(
//             context,
//             CompositeResultDisplay(
//               result: result,
//             ))
//         : push(
//             context,
//             ResultDisplay(
//               result: result,
//             ));
//
//     // Navigator.pushNamed(
//     //     context,
//     //     result.scanMode.isCompositeScan()
//     //         ? CompositeResultDisplay.routeName
//     //         : ResultDisplay.routeName,
//     //     arguments: result);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Center(
//         child: RaisedButton(
//           child: Text("Scan"),
//           onPressed: () {
//             scan(ScanMode.UniversalId);
//           },
//         ),
//       ),
//     );
//   }
// }
