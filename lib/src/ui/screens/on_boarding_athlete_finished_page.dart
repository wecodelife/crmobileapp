import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/ui/screens/home_screen.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class OnBoardingAthleteFinished extends StatefulWidget {
  @override
  _OnBoardingAthleteFinishedState createState() =>
      _OnBoardingAthleteFinishedState();
}

class _OnBoardingAthleteFinishedState extends State<OnBoardingAthleteFinished> {
  UserBloc userBloc = UserBloc();
  bool loading = false;
  @override
  void initState() {
    userBloc.addOnBoardStatusResponse.listen((event) {
      setState(() {
        loading = false;
      });
      push(context, HomePage());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: true,
      child: Scaffold(
        appBar: AppBar(leading: Container(), actions: [OnBoardingAppBar()]),
        body: loading == true
            ? Container(
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 1),
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              )
            : Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 20)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Spacer(
                      flex: 6,
                    ),
                    TextProgressIndicator(
                      heading: "YOU'RE ALL SET UP!",
                      descriptionText:
                          "You are now ready to start using the app and getting get to training!",
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 60),
                    ),
                    // OnBoardingProgressIndicator(
                    //   width: 1,
                    // ),
                    Container(
                      height: screenHeight(context, dividedBy: 70),
                      width: screenWidth(context, dividedBy: 1.05),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Theme.of(context).primaryColorDark,
                      ),
                    ),
                    Spacer(
                      flex: 5,
                    ),
                    BuildButton(
                      title: "FINISHED",
                      onPressed: () {
                        setState(() {
                          loading = true;
                        });
                        userBloc.addOnBoardingStatus();
                      },
                    ),
                    Spacer(
                      flex: 1,
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
