import 'package:app_template/src/app/app.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocDir = await path_provider.getApplicationDocumentsDirectory();
  Hive.init(appDocDir.path);
  await Hive.openBox(Constants.BOX_NAME);
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]).then((_) {
    runApp(MyApp());
  });
}
//
// void scanWithAnyline() async {
//   /// Instantiate the plugin.
//   var anylinePlugin = AnylinePlugin();
//
//   /// Load the config file which also includes the license key (for more info
//   /// visit documentation.anyline.com).
//   var config = await rootBundle.loadString("config/AnalogMeterConfig.json");
//
//   /// Start the scanning process.
//   var stringResult = await anylinePlugin.startScanning(config);
//
//   /// Convert the stringResult to a Map to access the result fields. It is
//   /// recommended to create result classes that fit your use case. For more
//   /// information on that, visit the Flutter Guide on documentation.anyline.com.
//   Map<String, dynamic> result = jsonDecode(stringResult);
// }
