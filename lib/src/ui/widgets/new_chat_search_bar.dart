import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class NewChatSearchBar extends StatefulWidget {
  final bool readOnly;
  final ValueChanged onValueChanged;
  final TextEditingController textEditingController;
  final String hintText;
  NewChatSearchBar({
    this.textEditingController,
    this.onValueChanged,
    this.readOnly,
    this.hintText,
  });
  @override
  _NewChatSearchBarState createState() => _NewChatSearchBarState();
}

class _NewChatSearchBarState extends State<NewChatSearchBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 12),
      child: TextField(
        controller: widget.textEditingController,
        autofocus: false,
        readOnly: widget.readOnly ?? false,
        onChanged: widget.onValueChanged,
        style: TextStyle(
          color: Constants.kitGradients[0],
          fontFamily: 'OpenSansRegular',
          fontSize: 16,
        ),
        cursorColor: Constants.kitGradients[0],
        decoration: InputDecoration(
          hintStyle: TextStyle(
              color: Colors.white,
              fontSize: 14,
              fontFamily: 'OpenSansSemiBold'),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent),
          ),
          hintText: widget.hintText,
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Constants.kitGradients[5]),
          ),
          labelStyle: TextStyle(
            color: Constants.kitGradients[0],
            fontFamily: 'OpenSansRegular',
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}
