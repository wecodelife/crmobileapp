// To parse this JSON data, do
//
//     final getPaymentMethodResponse = getPaymentMethodResponseFromJson(jsonString);

import 'dart:convert';

GetPaymentMethodResponse getPaymentMethodResponseFromJson(String str) =>
    GetPaymentMethodResponse.fromJson(json.decode(str));

String getPaymentMethodResponseToJson(GetPaymentMethodResponse data) =>
    json.encode(data.toJson());

class GetPaymentMethodResponse {
  GetPaymentMethodResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final List<DatumPayment> data;

  factory GetPaymentMethodResponse.fromJson(Map<String, dynamic> json) =>
      GetPaymentMethodResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null
            ? null
            : List<DatumPayment>.from(
                json["data"].map((x) => DatumPayment.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DatumPayment {
  DatumPayment({
    this.id,
    this.name,
    this.order,
  });

  final int id;
  final String name;
  final int order;

  factory DatumPayment.fromJson(Map<String, dynamic> json) => DatumPayment(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        order: json["order"] == null ? null : json["order"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "order": order == null ? null : order,
      };
}
