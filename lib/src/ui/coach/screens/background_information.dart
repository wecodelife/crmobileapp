import 'package:app_template/src/models/add_coach_request.dart';
import 'package:app_template/src/ui/coach/screens/back_ground_information_passed.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/newPasswordTextFeild.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/select_button.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BackgroundInformation extends StatefulWidget {
  final List<TrainerSport> sports;
  BackgroundInformation({this.sports});
  @override
  _BackgroundInformationState createState() => _BackgroundInformationState();
}

class _BackgroundInformationState extends State<BackgroundInformation> {
  TextEditingController nameTextEditingController = new TextEditingController();
  TextEditingController yourAddressTextEditingController =
      new TextEditingController();
  TextEditingController passwordTextEditingController =
      new TextEditingController();
  DateTime selectedDate;
  String formattedDate;
  DateTime formattedDateTime;
  _selectDate(BuildContext context) async {
    selectedDate = DateTime.now();
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        formattedDate = DateFormat('dd/MM/yyyy').format(selectedDate);
        formattedDateTime =
            DateTime.parse(DateFormat('yyyy-MM-dd').format(selectedDate));
        print(selectedDate);
      });
  }

  @override
  void initState() {
    formattedDate = "00/00/0000";
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[6],
        actions: [
          OnBoardingAppBar(),
        ],
      ),
      resizeToAvoidBottomPadding: false,
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 20)),
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 60),
                ),
                TextProgressIndicator(
                  heading: "LETS GET SOME BACKGROUND INFORMATION",
                  descriptionText:
                      "We are going to need some background information from you.",
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 60),
                ),
                OnBoardingProgressIndicator(
                  width: 1.8,
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 60),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 60)),
                  child: Column(
                    children: [
                      FormFeildUserDetails(
                        labelText: "Name",
                        textEditingController: nameTextEditingController,
                      ),
                      FormFeildUserDetails(
                        labelText: "Your Address",
                        textEditingController: yourAddressTextEditingController,
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Row(
                        children: [
                          Text(
                            "Date of Birth",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'OpenSansRegular',
                                fontSize: 16),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                formattedDate,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontFamily: 'OpenSansRegular'),
                              ),
                              GestureDetector(
                                  onTap: () {
                                    _selectDate(context);
                                  },
                                  child: Icon(
                                    Icons.calendar_today,
                                    color: Colors.white,
                                  )),
                            ],
                          ),
                          Divider(
                            thickness: 2,
                            color: Colors.white30,
                          )
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      NewPasswordTextFeild(
                        textEditingController: passwordTextEditingController,
                        labelText: "Social Security Number",
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      SelectButton(
                        title: "Scan For Drivers License",
                        iconTrue: true,
                        icon: "assets/icons/scanner_image.svg",
                        transparent: true,
                        onPressed: () {},
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 6),
          ),
          BuildButton(
            title: "NEXT",
            disabled: false,
            onPressed: () {
              push(
                  context,
                  BackgroundInformationPassed(
                    sports: widget.sports,
                    name: nameTextEditingController.text,
                    address: yourAddressTextEditingController.text,
                    ssn: passwordTextEditingController.text,
                    dob: formattedDateTime,
                  ));
            },
          )
        ],
      ),
    );
  }
}
