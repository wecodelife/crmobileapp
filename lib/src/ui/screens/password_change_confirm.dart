import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class PasswordChangeConfirm extends StatefulWidget {
  @override
  _PasswordChangeConfirmState createState() => _PasswordChangeConfirmState();
}

class _PasswordChangeConfirmState extends State<PasswordChangeConfirm> {
  bool loading = false;
  int id;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: loading == false
          ? SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 20)),
                child: Column(
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 20),
                    ),
                    TextProgressIndicator(
                      heading: "SUCCESS!",
                      descriptionText:
                          "Your password has been reset, you can now return to the login screen.",
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 4),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Theme.of(context).buttonColor),
                      height: 100,
                      width: 100,
                      child: Icon(
                        Icons.check_rounded,
                        size: 100,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 4),
                    ),
                    // BuildButton(
                    //   title: "CANCEL",
                    //   onPressed: () {},
                    //   transparent: true,
                    // ),
                    // SizedBox(
                    //   height: screenHeight(context, dividedBy: 40),
                    // ),
                    BuildButton(
                      title: "SAVE PASSWORD",
                      onPressed: () {},
                      transparent: true,
                    )
                  ],
                ),
              ),
            )
          : Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }
}
