import 'package:hive/hive.dart';

import 'constants.dart';

class AppHive {
  // void main (){}

  // void openBox()async{
  //   await Hive.openBox(Constants.BOX_NAME);
  //
  // }
  static const String _USER_ID = "user_id";
  static const String _NAME = "name";
  static const String _TOKEN = "token";
  static const String _EMAIL = "email";
  static const String _PREFERENCE = "preferenceid";
  static const String _LATITUDE = "longitude";
  static const String _XUSER = "newuserid";
  static const String _PASSWORD = "userpassword";
  static const String _IS_SIGNED_IN = "is signed in";
  static const String _ATHLETE_COUNT = "Athlete count";
  static const String _PARENT_ID = "Parent Id";
  static const String _PARENT_NAME = "Parent Name";
  static const String _PARENT_NUMBER = "Parent Number";
  static const String _PARENT_EMAIL = "Parent Email";
  static const String _IMAGE_ID = "Image Id";
  static const String _COACH_TYPE = "Coach Type";
  static const String _COACH_ID = "Coach Id";
  static const String _TRAINING_AT_HOME = "Training At Home";
  static const String _CLIENTS_RESEREVE_PAY = "Clients Reserve Pay";

  void hivePut({String key, String value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }

  String hiveGet({String key}) {
    // openBox();
    return Hive.box(Constants.BOX_NAME).get(key);
  }

  void hivePutBool({String key, bool value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }

  bool hiveGetBool({String key}) {
    // openBox();
    return Hive.box(Constants.BOX_NAME).get(key);
  }

  void hivePutInt({String key, int value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }

  int hiveGetInt({String key}) {
    // openBox();
    return Hive.box(Constants.BOX_NAME).get(key);
  }

  void hivePutFile({String key, dynamic value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }

  String hiveGetFile({String key}) {
    // openBox();
    return Hive.box(Constants.BOX_NAME).get(key);
  }

  putUserId({String userId}) {
    hivePut(key: _USER_ID, value: userId);
  }

  String getUserId() {
    return hiveGet(key: _USER_ID);
  }

  putUserPassword({String email}) {
    hivePut(key: _PASSWORD, value: email);
  }

  String getUserPassword() {
    return hiveGet(key: _PASSWORD);
  }

  putName({String name}) {
    hivePut(key: _NAME, value: name);
  }

  String getName() {
    return hiveGet(key: _NAME);
  }

  putToken({String token}) {
    hivePut(key: _TOKEN, value: token);
  }

  String getToken() {
    return hiveGet(key: _TOKEN);
  }

  String getEmail() {
    return hiveGet(key: _EMAIL);
  }

  putEmail(String value) {
    return hivePut(key: _EMAIL, value: value);
  }

  putPreference(String value) {
    return hivePut(key: _PREFERENCE, value: value);
  }

  String getPreferenceId() {
    return hiveGet(key: _PREFERENCE);
  }

  putXUser(String value) {
    return hivePut(key: _XUSER, value: value);
  }

  getXUser() {
    return hiveGet(
      key: _XUSER,
    );
  }

  putIsSignIn(String value) {
    return hivePut(key: _IS_SIGNED_IN, value: value);
  }

  getIsSignIn() {
    return hiveGet(
      key: _IS_SIGNED_IN,
    );
  }

  putImagePath({String key, String value}) {
    return hivePut(key: key, value: value);
  }

  getImagePath({String key}) {
    return hiveGet(
      key: key,
    );
  }

  putAthleteCount({int value}) {
    return hivePutInt(key: _ATHLETE_COUNT, value: value);
  }

  getAthleteCount() {
    return hiveGetInt(
      key: _ATHLETE_COUNT,
    );
  }

  putParentId({int value}) {
    return hivePutInt(key: _PARENT_ID, value: value);
  }

  getParentId() {
    return hiveGetInt(
      key: _PARENT_ID,
    );
  }

  putImageId({String key, String value}) {
    return hivePut(key: key, value: value);
  }

  getImageId(String key) {
    return hiveGet(
      key: key,
    );
  }

  putCoachType({String value}) {
    return hivePut(key: _COACH_TYPE, value: value);
  }

  getCoachType() {
    return hiveGet(
      key: _COACH_TYPE,
    );
  }

  putTrainingAtHome({String value}) {
    return hivePut(key: _TRAINING_AT_HOME, value: value);
  }

  getTrainingAtHome() {
    return hiveGet(
      key: _TRAINING_AT_HOME,
    );
  }

  putClientsReservePay({bool value}) {
    return hivePutBool(key: _CLIENTS_RESEREVE_PAY, value: value);
  }

  getClientsReservePay() {
    return hiveGetBool(
      key: _CLIENTS_RESEREVE_PAY,
    );
  }

  putCoachId({int value}) {
    return hivePutInt(key: _COACH_ID, value: value);
  }

  getCoachId() {
    return hiveGetInt(
      key: _COACH_ID,
    );
  }

  putParentName({String value}) {
    return hivePut(key: _PARENT_NAME, value: value);
  }

  getParentName() {
    return hiveGet(
      key: _PARENT_NAME,
    );
  }

  putParentNumber({String value}) {
    return hivePut(key: _PARENT_NUMBER, value: value);
  }

  getParentNumber() {
    return hiveGet(
      key: _PARENT_NUMBER,
    );
  }

  putParentEmail({String value}) {
    return hivePut(key: _PARENT_EMAIL, value: value);
  }

  getParentEmail() {
    return hiveGet(
      key: _PARENT_EMAIL,
    );
  }

  AppHive();
}
