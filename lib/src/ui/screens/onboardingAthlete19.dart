import 'package:app_template/src/ui/screens/on_boarding_parent_athlete_22.dart';
import 'package:app_template/src/ui/screens/onboardingAthlete25.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class OnBoardingAthlete19 extends StatefulWidget {
  @override
  _OnBoardingAthlete19State createState() => _OnBoardingAthlete19State();
}

class _OnBoardingAthlete19State extends State<OnBoardingAthlete19> {
  bool yes;
  bool transparentYes = true;
  bool transparentNo = true;
  bool disabled = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 20)),
        child: Column(
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            TextProgressIndicator(
              heading:
                  "DO YOU WANT THE TRAINER TO RESERVE AND PAY FOR THE FACILITIES",
              descriptionText:
                  "If not you will have to choose the facilities for yourself.",
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            OnBoardingProgressIndicator(
              width: 1.3,
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 5),
            ),
            BuildButton(
              title: "YES THEY WILL CHOOSE AND PAY",
              transparent: transparentYes,
              onPressed: () {
                setState(
                  () {
                    yes = true;
                    transparentYes = false;
                    transparentNo = true;
                    disabled = false;
                  },
                );
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            BuildButton(
              title: "NO  I WILL CHOOSE AND PAY",
              transparent: transparentNo,
              onPressed: () {
                setState(
                  () {
                    yes = false;
                    transparentYes = true;
                    transparentNo = false;
                    disabled = false;
                  },
                );
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 3.4),
            ),
            BuildButton(
              title: "NEXT",
              disabled: disabled,
              onPressed: () {
                if (yes == true) {
                  push(context, OnBoardingAthlete25());
                } else {
                  push(context, OnBoardingParentAthlete22());
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
