import 'package:app_template/src/ui/coach/widgets/coachBottomNavigationBar.dart';
import 'package:app_template/src/ui/screens/team_mate_detail_page.dart';
import 'package:app_template/src/ui/widgets/home_group_tile.dart';
import 'package:app_template/src/ui/widgets/home_listview_header.dart';
import 'package:app_template/src/ui/widgets/home_page_league_widget.dart';
import 'package:app_template/src/ui/widgets/home_teammate_tile.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CoachRefView extends StatefulWidget {
  @override
  _CoachRefViewState createState() => _CoachRefViewState();
}

class _CoachRefViewState extends State<CoachRefView> {
  bool shrink;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: CoachBottomNavigationWidget(),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[3],
        actions: [OnBoardingAppBar()],
      ),
      body: ListView(
        children: [
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          HomeListViewHeader(
            onPressedEditIcon: () {
              // push(
              //     context,
              //     EditSportsCircleAvatarList(
              //       label: ["PeerName1", "PeerName1"],
              //       title: "Students",
              //     ));
            },
            title: "STUDENTS",
            hasBackIcon: false,
            hasEditIcon: true,
          ),
          Container(
            height: screenHeight(context, dividedBy: 5),
            width: screenHeight(context, dividedBy: 1),
            child: ListView.builder(
                itemCount: 10,
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, int index) {
                  return HomeTeammateTile(
                    onPressed: () {
                      push(context, TeamMateDetailsPage());
                    },
                  );
                }),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 60),
          ),
          HomeListViewHeader(
            onPressedEditIcon: () {},
            title: "GROUPS",
            hasEditIcon: true,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: Container(
                color: Constants.kitGradients[5],
                height: screenHeight(context, dividedBy: 3),
                width: screenHeight(context, dividedBy: 1),
                child: Scrollbar(
                  child: ListView.builder(
                      itemCount: 3,
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemBuilder: (BuildContext context, int index) {
                        return HomeGroupTile();
                      }),
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              setState(() {
                shrink = !shrink;
              });
            },
            child: Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 5),
              color: Constants.kitGradients[8],
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Spacer(
                    flex: 1,
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 1.4),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          // color: Colors.blue,
                          child: Image.asset(
                            "assets/icons/coach_ref_text_icon.png",
                            fit: BoxFit.fill,
                            width: screenWidth(context, dividedBy: 2.5),
                            height: screenHeight(context, dividedBy: 30),
                          ),
                        ),
                        Spacer(
                          flex: 1,
                        ),
                        Text(
                          "SUMMER SOCCER CAMP",
                          style: TextStyle(
                              color: Constants.kitGradients[0],
                              fontFamily: "MontserratRegular",
                              fontWeight: FontWeight.w900,
                              fontSize: 14),
                        ),
                        Spacer(
                          flex: 1,
                        ),
                        Text(
                          "The best place to become a better soccer player is in the company of coaches and players who share your passion. ",
                          style: TextStyle(
                              color: Constants.kitGradients[0],
                              fontFamily: "MontserratRegular",
                              fontWeight: FontWeight.w500,
                              fontSize: 10),
                        ),
                        Spacer(
                          flex: 1,
                        ),
                        Text(
                          "Register Now",
                          style: TextStyle(
                              color: Constants.kitGradients[0],
                              fontFamily: "MontserratRegular",
                              fontWeight: FontWeight.w400,
                              fontSize: 11),
                        ),
                        Spacer(
                          flex: 1,
                        ),
                      ],
                    ),
                  ),
                  Image.asset("assets/images/soccer_player_image.png",
                      height: screenHeight(context, dividedBy: 3),
                      width: screenWidth(context, dividedBy: 4)),
                ],
              ),
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          HomeListViewHeader(
            title: "LEAGUES & TEAMS NEAR ME",
            hasEditIcon: true,
            itemNumber: "2 items",
            onPressedEditIcon: () {},
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30)),
            child: Container(
              height: screenHeight(context, dividedBy: 3.5),
              width: screenWidth(context, dividedBy: 1),
              child: ListView.builder(
                  itemCount: 2,
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (BuildContext context, int index) {
                    return HomePageLeagueWidget(
                      location: "Robinson",
                      heading: "Plex Paul Adult League",
                      subHeading:
                          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod",
                      imageName: "assets/images/league_image.svg",
                      onPressed: () {
                        push(context, TeamMateDetailsPage());
                      },
                    );
                  }),
            ),
          ),
          HomeListViewHeader(
            onPressedEditIcon: () {
              // push(
              //     context,
              //     EditSportsCircleAvatarList(
              //       label: ["PeerName1", "PeerName1"],
              //       title: "Students",
              //     ));
            },
            title: "TEAMMATES",
            hasBackIcon: false,
            hasEditIcon: true,
          ),
          Container(
            height: screenHeight(context, dividedBy: 5),
            width: screenHeight(context, dividedBy: 1),
            child: ListView.builder(
                itemCount: 10,
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, int index) {
                  return HomeTeammateTile(
                    onPressed: () {
                      push(context, TeamMateDetailsPage());
                    },
                  );
                }),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 20),
          )
        ],
      ),
    );
  }
}
