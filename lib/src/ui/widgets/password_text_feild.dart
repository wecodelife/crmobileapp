import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PasswordTextField extends StatefulWidget {
  final String label;
  final TextEditingController textEditingController;
  PasswordTextField({this.label, this.textEditingController});
  @override
  _PasswordTextFieldState createState() => _PasswordTextFieldState();
}

class _PasswordTextFieldState extends State<PasswordTextField> {
  bool visibility = true;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.label,
            style: TextStyle(fontSize: 12, color: Constants.kitGradients[0]),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            height: screenHeight(context, dividedBy: 20),
            width: screenWidth(context, dividedBy: 1.08),
            color: Constants.kitGradients[3],
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                obscureText: visibility,
                controller: widget.textEditingController,
                cursorColor: Colors.white,
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                    suffixIcon: GestureDetector(
                        onTap: () {
                          setState(() {
                            visibility = !visibility;
                          });
                        },
                        child: Icon(
                          visibility == true
                              ? Icons.visibility_off_outlined
                              : Icons.visibility_outlined,
                          color: Colors.grey,
                        )),
                    border: InputBorder.none),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
