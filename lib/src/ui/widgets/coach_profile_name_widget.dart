import 'package:app_template/src/models/get_trainer_list_response.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CoachProfileNameWidget extends StatefulWidget {
  final String image;
  final String coachName;
  final double width;
  final String subHeading;
  final List<TrainerSport> sports;
  CoachProfileNameWidget(
      {this.image, this.width, this.subHeading, this.coachName, this.sports});

  @override
  _CoachProfileNameWidgetState createState() => _CoachProfileNameWidgetState();
}

class _CoachProfileNameWidgetState extends State<CoachProfileNameWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: screenHeight(context, dividedBy: 40),
        ),
        Container(
          width: screenWidth(context, dividedBy: widget.width),
          height: screenHeight(context, dividedBy: 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                widget.coachName,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                    fontFamily: 'OpenSSansRegular'),
              ),
              // Row(
              //   children: [
              //     Row(
              //       children: [
              //         SizedBox(
              //           width: 5,
              //         ),
              //         Container(
              //             height: screenHeight(context, dividedBy: 40),
              //             width: screenWidth(context, dividedBy: 20),
              //             child: SvgPicture.asset(widget.image)),
              //         SizedBox(
              //           width: 4,
              //         )
              //       ],
              //     ),
              //     Text(
              //       widget.subHeading,
              //       style: TextStyle(
              //           color: Colors.white,
              //           fontSize: 17,
              //           fontFamily: 'OpenSSansRegular'),
              //     ),
              //   ],
              // )
              Container(
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 30),
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: widget.sports.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Row(
                      children: [
                        SizedBox(
                          width: 5,
                        ),
                        Container(
                            height: screenHeight(context, dividedBy: 40),
                            width: screenWidth(context, dividedBy: 20),
                            child: SvgPicture.string(
                                widget.sports[index].sport.icon
                                //   widget.sports[index].
                                )),
                        SizedBox(
                          width: 4,
                        ),
                        Text(
                          // "Football",
                          widget.sports[index].sport.name,
                          style: TextStyle(
                              color: Constants.kitGradients[0],
                              fontSize: 12,
                              fontFamily: 'OpenSSansRegular'),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
