import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class FormFeildUserDetails extends StatefulWidget {
  final String labelText;
  final String hintText;
  final bool readOnly;
  final ValueChanged onValueChanged;
  final Function onPressed;
  final TextEditingController textEditingController;

  FormFeildUserDetails(
      {this.labelText,
      this.textEditingController,
      this.onValueChanged,
      this.readOnly,
      this.hintText,
      this.onPressed});
  @override
  _FormFeildUserDetailsState createState() => _FormFeildUserDetailsState();
}

class _FormFeildUserDetailsState extends State<FormFeildUserDetails> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 12),
      child: TextField(
        controller: widget.textEditingController,
        autofocus: false,
        onTap: () {
          widget.onPressed();
        },
        readOnly: widget.readOnly ?? false,
        onChanged: (val) {
          widget.onValueChanged(val);
        },
        style: TextStyle(
          color: Constants.kitGradients[0],
          fontFamily: 'OpenSansRegular',
          fontSize: 16,
        ),
        cursorColor: Constants.kitGradients[0],
        decoration: InputDecoration(
          hintStyle: TextStyle(
              color: Colors.white,
              fontSize: 14,
              fontFamily: 'OpenSansSemiBold'),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Constants.kitGradients[0]),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Constants.kitGradients[0]),
          ),
          hintText: widget.hintText,
          labelText: widget.labelText,
          labelStyle: TextStyle(
            color: Constants.kitGradients[0],
            fontFamily: 'OpenSansRegular',
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}
