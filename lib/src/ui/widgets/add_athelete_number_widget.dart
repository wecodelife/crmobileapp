import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class AddAthleteNumber extends StatefulWidget {
  int count;
  Function decrementOnTap;
  Function incrementOnTap;
  AddAthleteNumber({this.count, this.incrementOnTap, this.decrementOnTap});
  @override
  _AddAthleteNumberState createState() => _AddAthleteNumberState();
}

class _AddAthleteNumberState extends State<AddAthleteNumber> {
  int count;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: screenHeight(context, dividedBy: 30),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: screenWidth(
                context,
                dividedBy: 5,
              ),
            ),
            widget.count > 1
                ? GestureDetector(
                    onTap: widget.decrementOnTap,
                    child: Icon(Icons.remove_circle,
                        size: 30, color: Theme.of(context).buttonColor))
                : Icon(Icons.remove_circle, size: 30, color: Colors.white24),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  widget.count.toString(),
                  style: TextStyle(color: Colors.white, fontSize: 18),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 150),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 5),
                  height: 2,
                  color: Colors.white10,
                ),
                SizedBox(
                  width: screenWidth(
                    context,
                    dividedBy: 3,
                  ),
                ),
              ],
            ),
            widget.count < 10
                ? GestureDetector(
                    onTap: widget.incrementOnTap,
                    child: Icon(Icons.add_circle,
                        size: 30, color: Theme.of(context).buttonColor))
                : Icon(Icons.add_circle, size: 30, color: Colors.white24),
          ],
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 30),
        ),
      ],
    );
  }
}
