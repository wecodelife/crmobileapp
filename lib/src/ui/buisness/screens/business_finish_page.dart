import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class BusinessFinishPage extends StatefulWidget {
  @override
  _BusinessFinishPageState createState() => _BusinessFinishPageState();
}

class _BusinessFinishPageState extends State<BusinessFinishPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[6],
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 12),
              vertical: screenHeight(context, dividedBy: 3.2),
            ),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextProgressIndicator(
                  heading: "YOU'RE ALL  SET UP",
                  descriptionText:
                      "You are now ready to start using the app and getting paid!",
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                OnBoardingProgressIndicator(
                  width: 1.2,
                  progressIndicator: true,
                ),
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 20),
          ),
          BuildButton(
            title: "FINISH",
            disabled: false,
            onPressed: () {},
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 80),
          ),
        ],
      ),
    );
  }
}
