import 'package:app_template/src/ui/widgets/back_button.dart';
import 'package:app_template/src/ui/widgets/rating_widget.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SearchTrainerTile extends StatefulWidget {
  final bool hasIcon;
  final bool length;
  final bool index;
  final String rating;
  final Function onPressed;
  SearchTrainerTile(
      {this.hasIcon, this.length, this.index, this.rating, this.onPressed});
  @override
  _SearchTrainerTileState createState() => _SearchTrainerTileState();
}

class _SearchTrainerTileState extends State<SearchTrainerTile> {
  bool bookmarked = false;
  bool rated = false;
  int count = 827;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        child: Container(
          width: screenWidth(context, dividedBy: 1.8),
          height: screenHeight(context, dividedBy: 3.8),
          decoration: BoxDecoration(
              color: Constants.kitGradients[5],
              borderRadius: BorderRadius.all(Radius.circular(5))),
          child: Column(
            children: [
              Stack(
                children: [
                  Container(
                    width: screenWidth(context, dividedBy: 1.8),
                    height: screenHeight(context, dividedBy: 5.2),
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/images/dummy_bg.jpg"),
                            fit: BoxFit.fill)),
                    child: Image.network(
                      "https://image.shutterstock.com/image-photo/business-woman-drawing-global"
                      "-structure-260nw-1006041130.jpg",
                      fit: BoxFit.cover,
                    ),
                  ),
                  Column(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 100),
                      ),
                      Row(
                        children: [
                          Spacer(flex: 1),
                          SvgPicture.asset("assets/icons/online_icon.svg",
                              height: screenWidth(context, dividedBy: 25),
                              width: screenWidth(context, dividedBy: 25)),
                          Spacer(flex: 10),
                          GestureDetector(
                              onTap: () {
                                setState(() {
                                  bookmarked = !bookmarked;
                                });
                              },
                              child: Icon(
                                  bookmarked == true
                                      ? Icons.bookmark
                                      : Icons.bookmark_border,
                                  color: Constants.kitGradients[1],
                                  size: 20)),
                          Spacer(flex: 1),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.8),
                height: screenHeight(context, dividedBy: 9),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Spacer(
                      flex: 8,
                    ),
                    Text(
                      "   Coach Name",
                      style: TextStyle(
                          color: Constants.kitGradients[0],
                          fontSize: 14,
                          fontFamily: 'OpenSSansRegular'),
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    Row(
                      children: [
                        Row(
                          children: [
                            SizedBox(
                              width: 5,
                            ),
                            Container(
                                height: screenHeight(context, dividedBy: 40),
                                width: screenWidth(context, dividedBy: 20),
                                child: SvgPicture.asset(
                                    "assets/icons/soccer_icon.svg")),
                            SizedBox(
                              width: 4,
                            )
                          ],
                        ),
                        Text(
                          "Soccer",
                          style: TextStyle(
                              color: Constants.kitGradients[0],
                              fontSize: 12,
                              fontFamily: 'OpenSansRegular'),
                        ),
                        Spacer(
                          flex: 1,
                        ),
                        RatingWidget(
                          heading: "4.7",
                          icon: Icons.star_rounded,
                          onPressed: () {
                            setState(() {
                              rated = !rated;
                              if (rated == true) {
                                count++;
                              } else {
                                count--;
                              }
                            });
                          },
                          rated: rated,
                          divider: false,
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 14),
                          child: Text(
                            "(" + count.toString() + ")",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: screenWidth(context, dividedBy: 36),
                                fontFamily: 'OpenSansRegular'),
                          ),
                        )
                      ],
                    ),
                    Spacer(
                      flex: 10,
                    ),
                    Row(
                      children: [
                        Spacer(
                          flex: 2,
                        ),
                        Icon(
                          Icons.phone_outlined,
                          size: 20,
                          color: Constants.kitGradients[1],
                        ),
                        SizedBox(
                          width: screenWidth(context, dividedBy: 20),
                        ),
                        Icon(
                          Icons.mail_outline,
                          size: 20,
                          color: Constants.kitGradients[1],
                        ),
                        Spacer(
                          flex: 5,
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 5),
                          height: screenHeight(context, dividedBy: 25),
                          child: BackButtonCr(
                            title: "BookNow",
                            onPressed: () {
                              widget.onPressed();
                            },
                          ),
                        ),
                        Spacer(
                          flex: 1,
                        ),
                      ],
                    ),
                    Spacer(
                      flex: 2,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
