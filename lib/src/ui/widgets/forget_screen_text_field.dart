import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextFieldForgetScreen extends StatefulWidget {
  final String label;
  final TextEditingController textEditingController;
  final ValueChanged onChanged;
  TextFieldForgetScreen(
      {this.label, this.textEditingController, this.onChanged});
  @override
  _TextFieldForgetScreenState createState() => _TextFieldForgetScreenState();
}

class _TextFieldForgetScreenState extends State<TextFieldForgetScreen> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.label,
            style: TextStyle(color: Constants.kitGradients[0]),
          ),
          Container(
              height: screenHeight(context, dividedBy: 25),
              child: TextField(
                controller: widget.textEditingController,
                onChanged: (value) {},
                decoration: InputDecoration(
                  border: new UnderlineInputBorder(
                      borderSide: new BorderSide(color: Colors.grey)),
                  focusedBorder: new UnderlineInputBorder(
                      borderSide: new BorderSide(color: Colors.grey)),
                  enabledBorder: new UnderlineInputBorder(
                      borderSide: new BorderSide(color: Colors.grey)),
                ),
              ))
        ],
      ),
    );
  }
}
