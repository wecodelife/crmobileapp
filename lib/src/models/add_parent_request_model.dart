// To parse this JSON data, do
//
//     final addParentRequestModel = addParentRequestModelFromJson(jsonString);

import 'dart:convert';

AddParentRequestModel addParentRequestModelFromJson(String str) =>
    AddParentRequestModel.fromJson(json.decode(str));

String addParentRequestModelToJson(AddParentRequestModel data) =>
    json.encode(data.toJson());

class AddParentRequestModel {
  AddParentRequestModel({
    this.name,
    this.address,
    this.email,
    this.phoneNumber,
    this.gender,
    this.dob,
    this.user,
    this.sports,
    this.isCoachOrRef,
    this.profPic,
    this.trainingAtHome,
    this.trainerChooseFacilities,
  });

  final String name;
  final String address;
  final String email;
  final String phoneNumber;
  final int gender;
  final DateTime dob;
  final String user;
  final List<int> sports;
  final bool isCoachOrRef;
  final dynamic profPic;
  final dynamic trainingAtHome;
  final dynamic trainerChooseFacilities;

  factory AddParentRequestModel.fromJson(Map<String, dynamic> json) =>
      AddParentRequestModel(
        name: json["name"] == null ? null : json["name"],
        address: json["address"] == null ? null : json["address"],
        email: json["email"] == null ? null : json["email"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        gender: json["gender"] == null ? null : json["gender"],
        dob: json["dob"] == null ? null : DateTime.parse(json["dob"]),
        user: json["user"] == null ? null : json["user"],
        sports: json["sports"] == null
            ? null
            : List<int>.from(json["sports"].map((x) => x)),
        isCoachOrRef:
            json["is_coach_or_ref"] == null ? null : json["is_coach_or_ref"],
        profPic: json["prof_pic"],
        trainingAtHome: json["training_at_home"],
        trainerChooseFacilities: json["trainer_choose_facilities"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "address": address == null ? null : address,
        "email": email == null ? null : email,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "gender": gender == null ? null : gender,
        "dob": dob == null
            ? null
            : "${dob.year.toString().padLeft(4, '0')}-${dob.month.toString().padLeft(2, '0')}-${dob.day.toString().padLeft(2, '0')}",
        "user": user == null ? null : user,
        "sports":
            sports == null ? null : List<dynamic>.from(sports.map((x) => x)),
        "is_coach_or_ref": isCoachOrRef == null ? null : isCoachOrRef,
        "prof_pic": profPic,
        "training_at_home": trainingAtHome,
        "trainer_choose_facilities": trainerChooseFacilities,
      };
}
