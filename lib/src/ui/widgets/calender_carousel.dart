import 'package:app_template/src/ui/widgets/flutter_calender.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart' show DateFormat;

class CarouselCalender extends StatefulWidget {
  final ValueChanged onValueChanged;
  CarouselCalender({Key key, this.onValueChanged}) : super(key: key);
  @override
  _CarouselCalenderState createState() => new _CarouselCalenderState();
}

class _CarouselCalenderState extends State<CarouselCalender> {
  DateTime _currentDate = DateTime(2021, 1, 14);
  String _currentMonth = DateFormat.yMMM().format(DateTime.now());
  String _currentYear = DateTime.now().year.toString();
  DateTime _targetDateTime = new DateTime.now();
  DateTime _minSelectedDate = DateTime.now().subtract(Duration(days: 1));
  DateTime _maxSelectedDate = DateTime(2025, 2, 3);
  String currentYear;
  String currentMonth;
  CalendarCarousel _calendarCarouselHeader;
  String selectedValue;
  String calenderMonth;
  bool currentDateSelected = false;
  List<String> year = [];
  String selectedYear;
  String selectedMonth;

  void yearGenerator() {
    for (int i = 2021; i <= 2025; i++) {
      year.add(i.toString());
    }
  }

  @override
  void initState() {
    currentYear = _targetDateTime.year.toString();
    calenderMonth = _currentMonth;

    print(calenderMonth);
    print(currentYear);
    yearGenerator();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _calendarCarouselHeader = CalendarCarousel<Event>(
      onDayPressed: (DateTime date, List<Event> events) {
        this.setState(() => _currentDate = date);
        currentDateSelected = true;
        widget.onValueChanged(date);
        events.forEach((event) => print(event.title));
      },

      selectedDayBorderColor: Colors.transparent,
      showOnlyCurrentMonthDate: false,
      selectedDayButtonColor: Colors.transparent,

      childAspectRatio: 1 / 1,
      weekendTextStyle: TextStyle(
        color: Colors.grey,
      ),
      thisMonthDayBorderColor: Colors.transparent,
      weekFormat: false,

//      firstDayOfWeek: 4,
      height: screenHeight(context, dividedBy: 1.9),
      selectedDateTime: _currentDate,
      targetDateTime: _targetDateTime,
      customGridViewPhysics: NeverScrollableScrollPhysics(),
      showHeader: false,
      todayTextStyle: TextStyle(
        color: currentDateSelected == false ? Colors.white : Colors.white,
      ),

      weekdayTextStyle: TextStyle(color: Colors.grey),
      dayButtonColor: Colors.transparent,
      daysTextStyle: TextStyle(color: Colors.white, fontSize: 14),
      todayBorderColor: currentDateSelected == false
          ? Constants.kitGradients[1]
          : Colors.transparent,
      todayButtonColor: Colors.transparent,
      selectedDayTextStyle: TextStyle(
        color: Constants.kitGradients[1],
      ),
      minSelectedDate: _minSelectedDate,
      maxSelectedDate: _maxSelectedDate,
      prevDaysTextStyle: TextStyle(
        fontSize: 14,
        color: Colors.grey,
      ),

      inactiveDaysTextStyle: TextStyle(
        color: Colors.grey,
        fontSize: 14,
      ),
      onCalendarChanged: (DateTime date) {
        this.setState(() {
          _targetDateTime = date;
          _currentMonth = DateFormat.MMMM().format(_targetDateTime);
          _currentYear = _targetDateTime.year.toString();
        });
      },
    );

    return Container(
        color: Constants.kitGradients[6],
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: screenHeight(context, dividedBy: 130),
            ),

            Text(
              "SELECT A DATE",
              style: TextStyle(
                color: Constants.kitGradients[1],
                fontSize: 18,
                fontFamily: 'OswaldBold',
              ),
            ),

            Text(
              _currentMonth + " " + _currentYear,
              style: TextStyle(
                color: Constants.kitGradients[1],
                fontSize: 18,
                fontFamily: 'OpenSansSemiBold',
              ),
            ),

            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            Container(
              child: _calendarCarouselHeader,
            ), //
          ],
        ));
  }
}
