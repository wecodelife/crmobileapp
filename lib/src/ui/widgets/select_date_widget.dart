import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SelectDateWidget extends StatefulWidget {
  final DateTime selectedDate;
  final String formattedDate;
  final Function calenderFunction;
  final String iconName;
  SelectDateWidget(
      {this.formattedDate,
      this.selectedDate,
      this.calenderFunction,
      this.iconName});
  @override
  _SelectDateWidgetState createState() => _SelectDateWidgetState();
}

class _SelectDateWidgetState extends State<SelectDateWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 20)),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                widget.selectedDate == null
                    ? "00/00/0000"
                    : widget.formattedDate,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontFamily: 'OpenSansRegular'),
              ),
              GestureDetector(
                  onTap: () {
                    widget.calenderFunction();
                  },
                  child: SvgPicture.asset(widget.iconName)),
            ],
          ),
          Divider(
            thickness: 2,
            color: Colors.white30,
          )
        ],
      ),
    );
  }
}
