// To parse this JSON data, do
//
//     final getLeaseRateResponse = getLeaseRateResponseFromJson(jsonString);

import 'dart:convert';

GetLeaseRateResponse getLeaseRateResponseFromJson(String str) =>
    GetLeaseRateResponse.fromJson(json.decode(str));

String getLeaseRateResponseToJson(GetLeaseRateResponse data) =>
    json.encode(data.toJson());

class GetLeaseRateResponse {
  GetLeaseRateResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final List<Datum> data;

  factory GetLeaseRateResponse.fromJson(Map<String, dynamic> json) =>
      GetLeaseRateResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.name,
  });

  final int id;
  final String name;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
      };
}
