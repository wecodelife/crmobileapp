import 'package:app_template/src/ui/widgets/appbar_cr.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/card_scanner.dart';
import 'package:app_template/src/ui/widgets/check_box.dart';
import 'package:app_template/src/ui/widgets/drop_down%20form%20feild.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NewPayment extends StatefulWidget {
  @override
  _NewPaymentState createState() => _NewPaymentState();
}

class _NewPaymentState extends State<NewPayment> {
  TextEditingController nameTextEditingController = new TextEditingController();
  TextEditingController cardNumberTextEditingController =
      new TextEditingController();

  void yearGenerator() {
    for (int i = 1920; i <= 2021; i++) {
      year.add(i.toString());
    }
  }

  String valueMonth;
  String valueYear;
  List<String> year = [];
  List<String> month = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  bool setDefault = false;

  @override
  void initState() {
    yearGenerator();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(leading: Container(), actions: [
        AppBarCr(
          noRightIcon: true,
          pageTitle: "NEW PAYMENT",
          leftIcon: "assets/icons/back_button.svg",
          onTapLeftIcon: () {
            pop(context);
          },
        ),
      ]),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 12)),
        child: Column(
          children: [
            CardScanner(
              onPressed: () {},
            ),
            FormFeildUserDetails(
              labelText: "Name on Card",
              textEditingController: nameTextEditingController,
            ),
            FormFeildUserDetails(
              labelText: "Card Number",
              textEditingController: cardNumberTextEditingController,
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),
            Row(
              children: [
                Text(
                  "Expiration Date",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                      fontFamily: 'OpenSansRegular'),
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 80),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                DropDownFormField(
                  boxWidth: 2.5,
                  boxHeight: 10,
                  hintPadding: 100,
                  title: "MM",
                  dropDownValue: valueMonth,
                  dropDownList: month,
                  onClicked: (value) {
                    setState(() {
                      valueMonth = value;
                    });
                  },
                ),
                DropDownFormField(
                  boxWidth: 2.5,
                  boxHeight: 10,
                  hintPadding: 100,
                  title: "YYYY",
                  dropDownValue: valueYear,
                  dropDownList: year,
                  onClicked: (value) {
                    setState(() {
                      valueYear = value;
                    });
                  },
                ),
              ],
            ),
            Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 80),
                ),
                Row(
                  children: [
                    CheckBox(
                      onTap: () {
                        setState(() {
                          setDefault = !setDefault;
                        });
                      },
                      checked: setDefault,
                    ),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 30),
                    ),
                    Text(
                      "Make Default",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                          fontFamily: 'OpenSansSemiBold'),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 4),
            ),
            BuildButton(
              title: "Add Card",
              onPressed: () {},
              disabled: false,
            )
          ],
        ),
      ),
    );
  }
}
