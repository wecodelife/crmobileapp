import 'package:app_template/material/pickers/date_range_picker_dialog.dart';
import 'package:app_template/src/ui/buisness/screens/facilities_1.dart';
import 'package:app_template/src/ui/screens/chat_list_page.dart';
import 'package:app_template/src/ui/screens/home_screen.dart';
import 'package:app_template/src/ui/screens/profile_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BottomNavigationBarBusiness extends StatefulWidget {
  @override
  _BottomNavigationBarBusinessState createState() =>
      _BottomNavigationBarBusinessState();
}

class _BottomNavigationBarBusinessState
    extends State<BottomNavigationBarBusiness> {
  Future<Null> selectDateRange(BuildContext context) async {
    DateTimeRange picked = await showDatesRangePicker(
        context: context,
        // initialDateRange: DateTimeRange(
        //   start: DateTime.now().add(Duration(days: 0)),
        //   end: DateTime.now().add(Duration(days: 1)),
        // ),

        firstDate: DateTime.now(),
        lastDate: DateTime(DateTime.now().year + 5),
        helpText: "Select_Date_Range",
        cancelText: "CANCEL",
        confirmText: "OK",
        saveText: "SAVE",
        errorFormatText: "Invalid_format",
        errorInvalidText: "Out_of_range",
        errorInvalidRangeText: "Invalid_range",
        fieldStartHintText: "Start_Date",
        sessionName: "business",
        fieldEndLabelText: "End_Date");

    // if (picked != null) {
    //   state.didChange(picked);
    // }
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      backgroundColor: Colors.white,
      unselectedFontSize: 1,
      showUnselectedLabels: false,
      showSelectedLabels: false,
      selectedLabelStyle: TextStyle(fontSize: 1),
      unselectedLabelStyle: TextStyle(fontSize: 1),
      items: [
        BottomNavigationBarItem(
            backgroundColor: Colors.black,
            icon: GestureDetector(
              onTap: () {
                push(context, HomePage());
              },
              child: SvgPicture.asset(
                "assets/icons/facilities_icon.svg",
                height: 20,
                color: Constants.kitGradients[0],
              ),
            ),
            label: ""),
        BottomNavigationBarItem(
            icon: GestureDetector(
              onTap: () {
                push(
                  context,
                  ChatListPage(),
                );
              },
              child: ImageIcon(
                AssetImage("assets/icons/chat_icon.png"),
                size: 40,
                color: Constants.kitGradients[0],
              ),
            ),
            label: ""),
        BottomNavigationBarItem(
            icon: GestureDetector(
              onTap: () {
                push(context, Facilities1Page());
              },
              child: ImageIcon(
                AssetImage("assets/icons/magnify_glass_icon.png"),
                size: 40,
                color: Constants.kitGradients[0],
              ),
            ),
            label: ""),
        BottomNavigationBarItem(
            icon: GestureDetector(
              onTap: () {
                push(context, HomePage());
              },
              child: ImageIcon(
                AssetImage("assets/icons/add_task_icon.png"),
                size: 40,
                color: Constants.kitGradients[0],
              ),
            ),
            label: ""),
        BottomNavigationBarItem(
          icon: GestureDetector(
            onTap: () {
              push(context, ProfilePage());
            },
            child: ImageIcon(
              AssetImage("assets/icons/profile_icon.png"),
              size: 40,
              color: Constants.kitGradients[0],
            ),
          ),
          label: "",
        ),
      ],
    );
  }
}
