import 'dart:io';

import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/form_feild_hint_text.dart';
import 'package:app_template/src/ui/widgets/time_slot_app_bar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BottomSheetNewDocument extends StatefulWidget {
  final ValueChanged onFilePicking;

  BottomSheetNewDocument({
    this.onFilePicking,
  });
  @override
  _BottomSheetNewDocumentState createState() => _BottomSheetNewDocumentState();
}

class _BottomSheetNewDocumentState extends State<BottomSheetNewDocument> {
  TextEditingController nameTextEditingController = new TextEditingController();
  TextEditingController phoneTextEditingController =
      new TextEditingController();
  TextEditingController emailTextEditingController =
      new TextEditingController();
  File file;
  String fileName = "";
  String files = "";
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            decoration: BoxDecoration(
              color: Constants.kitGradients[5],
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10)),
            ),
            child: AppBarTimeSlot(
              pageTitle: "NEW DOCUMENT",
              rightIcon: true,
              onTapRightIcon: () {
                pop(context);
              },
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 80),
          ),
          Container(
            color: Constants.kitGradients[6],
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 20)),
              child: Column(
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 30),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  GestureDetector(
                    onTap: () async {
                      await filePick();

                      widget.onFilePicking(file);
                      // setState(() {
                      //   fileName = files;
                      // });
                    },
                    child: Row(
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 400),
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 2.6),
                          child: Text(
                            "UploadDocument",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                              fontFamily: "OpenSansRegular",
                            ),
                          ),
                        ),
                        SizedBox(
                          width: screenHeight(context, dividedBy: 17),
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 2.6),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              fileName != ""
                                  ? Text(
                                      "FileUploaded",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 10,
                                          fontFamily: 'OpenSansSemiBold'),
                                    )
                                  : SvgPicture.asset(
                                      "assets/icons/cloudupload.svg")
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Divider(
                    thickness: 1.0,
                    color: Constants.kitGradients[13],
                  ),
                  Container(
                    height: screenHeight(context, dividedBy: 15),
                    child: FormFieldHintText(
                      textEditingController: nameTextEditingController,
                      hintText: "Recipents Name",
                    ),
                  ),
                  Container(
                    height: screenHeight(context, dividedBy: 15),
                    child: FormFieldHintText(
                      textEditingController: phoneTextEditingController,
                      hintText: "PhoneNumber",
                    ),
                  ),
                  Container(
                    height: screenHeight(context, dividedBy: 15),
                    child: FormFieldHintText(
                      textEditingController: emailTextEditingController,
                      hintText: "Email",
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: screenWidth(context, dividedBy: 400),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 50),
                      ),
                      Container(
                        width: screenWidth(context, dividedBy: 2.6),
                        child: Text(
                          "Date",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: "OpenSansRegular",
                          ),
                        ),
                      ),
                      SizedBox(
                        width: screenHeight(context, dividedBy: 17),
                      ),
                      Container(
                        width: screenWidth(context, dividedBy: 2.6),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                "12/10/2020",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'OpenSansBold',
                                    fontSize: 14),
                              )
                            ]),
                      ),
                    ],
                  ),
                  Divider(
                    thickness: 1.0,
                    color: Constants.kitGradients[13],
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          BuildButton(
            transparent: true,
            title: "Submit",
            onPressed: () {},
            disabled: true,
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
        ],
      ),
    );
  }

  Future filePick() async {
    FilePickerResult result = await FilePicker.platform.pickFiles();

    if (result != null) {
      file = File(result.files.single.path);
      setState(() {
        fileName = file.path;
      });
    } else {
      // User canceled the picker
    }
  }
}
