import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class ChatListTile extends StatefulWidget {
  final String name;
  final String phoneNumber;
  final String time;
  final String imageName;
  ChatListTile({this.imageName, this.time, this.name, this.phoneNumber});
  @override
  _ChatListTileState createState() => _ChatListTileState();
}

class _ChatListTileState extends State<ChatListTile> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Row(
          children: [
            SizedBox(
              width: screenWidth(context, dividedBy: 5),
            ),
            Container(
              width: screenWidth(context, dividedBy: 1.35),
              child: Divider(
                thickness: 1.0,
                color: Colors.grey,
              ),
            ),
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: screenWidth(context, dividedBy: 30),
            ),
            Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 20),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 40),
                  height: screenHeight(context, dividedBy: 80),
                  decoration: BoxDecoration(
                    color: Constants.kitGradients[1],
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ],
            ),
            SizedBox(
              width: screenWidth(context, dividedBy: 200),
            ),
            Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(40),
                  child: Container(
                      width: screenWidth(context, dividedBy: 6),
                      height: screenWidth(context, dividedBy: 7),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                      ),
                      child: Image(
                        image: AssetImage(widget.imageName),
                      )),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 120),
                ),
                Row(
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 50),
                    ),
                    Container(
                      width: screenWidth(context, dividedBy: 2.7),
                      child: Text(
                        widget.name,
                        style: TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                            fontFamily: 'OpenSansSemiBold'),
                      ),
                    ),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 20),
                    ),
                    Container(
                      width: screenWidth(context, dividedBy: 4.6),
                      child: Center(
                        child: Text(
                          widget.time,
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                              fontFamily: 'OpenSansSemiBold'),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 150),
                    ),
                    Icon(
                      Icons.arrow_forward_ios_outlined,
                      size: 18,
                      color: Colors.white,
                    )
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 150),
                ),
                Row(
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 40),
                    ),
                    Text(
                      widget.phoneNumber,
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                          fontFamily: 'OpenSansRegular'),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              width: screenWidth(context, dividedBy: 50),
            )
          ],
        ),
      ],
    );
  }
}
