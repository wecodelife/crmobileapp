import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/invitation_type_response_model.dart';
import 'package:app_template/src/ui/screens/on_boarding_parent_athlete_5.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/designation_selection_box.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
// import 'package:simple_time_range_picker/simple_time_range_picker.dart';

class OnBoardingCoach2 extends StatefulWidget {
  final Datum dropDownValue;
  OnBoardingCoach2({this.dropDownValue});
  @override
  _OnBoardingCoach2State createState() => _OnBoardingCoach2State();
}

class _OnBoardingCoach2State extends State<OnBoardingCoach2> {
  bool coach = false;
  bool ref = false;
  String time;
  bool businessman = false;
  bool allSelected = false;
  bool disabled = true;
  bool loading = false;
  int id;
  TimeOfDay _startTime;
  TimeOfDay _endTime;
  UserBloc userBloc = UserBloc();
  didUpdate() {
    if (coach == true ||
        ref == true ||
        businessman == true ||
        allSelected == true) {
      disabled = false;
      ObjectFactory().appHive.putCoachType(value: id.toString());
    } else {
      disabled = true;
    }
  }

  // Future<Null> _selectTime(BuildContext context) async {
  //   TimeOfDay picked = await showTimePicker(
  //     context: context,
  //     initialTime: selectedTime,
  //   );
  //   // if (picked != null)
  //   //   setState(() {
  //   //     selected = true;
  //   //     selectedTime = picked;
  //   //     _hour = selectedTime.hour.toString();
  //   //     _minute = selectedTime.minute.toString();
  //   //     _time = _hour + ' : ' + _minute;
  //   //     _timeController.text = _time;
  //   //     dateTime = formatDate(
  //   //         DateTime(2019, 08, 1, selectedTime.hour, selectedTime.minute),
  //   //         [hh, ':', nn, " ", am]);
  //   //     widget.time(selectedTime.toString().substring(10, 15));
  //   //   });
  //   // print(selectedTime.toString().substring(10, 15));
  // }

  @override
  void initState() {
    // userBloc.updateUserTypeResponse.listen((event) {
    //   setState(() {
    //     loading = false;
    //   });
    //   if (event.status == 200) {
    //     push(context, OnBoardingParentAthlete2());
    //   }
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: loading == false
          ? Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      TextProgressIndicator(
                        heading: "WHAT KIND OF PROFESSIONAL ARE YOU?",
                        descriptionText:
                            "Are you wanting to coach/train or do you want to referee/umpire or maybe both?",
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      OnBoardingProgressIndicator(
                        width: 10,
                      ),
                      DesignationSelectionBox(
                        descriptionText:
                            "Looking to coach/train Athletes or Referee/Umpire games, or maybe do both",
                        heading: "I AM A COACH/ TRAINER",
                        onPressed: () {
                          setState(() {
                            id = 1;
                            coach = !coach;
                            print(coach);
                            ref = false;
                            businessman = false;
                            allSelected = false;
                            didUpdate();
                          });
                        },
                        selected: coach,
                      ),
                      DesignationSelectionBox(
                        descriptionText:
                            "Looking to trade equipment, hire trainers, find leagues or help my kid progress in a sport.",
                        heading: "I AM A REF/UMPIRE",
                        onPressed: () {
                          setState(() {
                            id = 2;
                            ref = !ref;
                            coach = false;
                            businessman = false;
                            allSelected = false;
                            didUpdate();
                          });
                        },
                        selected: ref,
                      ),
                      // DesignationSelectionBox(
                      //   descriptionText:
                      //       "A Club, Organization, or Facility looking to find members or rent out facilities.",
                      //   heading: "I AM A BUSINESSMAN",
                      //   onPressed: () {
                      //     setState(() {
                      //       id = 3;
                      //       businessman = !businessman;
                      //       ref = false;
                      //       coach = false;
                      //       allSelected = false;
                      //       didUpdate();
                      //     });
                      //   },
                      //   selected: businessman,
                      // ),
                      DesignationSelectionBox(
                        descriptionText: "A little bit of both",
                        heading: "I AM A COACH/TRAINER & REF/UMPIRE",
                        onPressed: () async {
                          // TimeRangePicker.show(
                          //   timeRangeViewType: TimeRangeViewType.start,
                          //   context: context,
                          //   unSelectedEmpty: false,
                          //   startTime: TimeOfDay(hour: 19, minute: 45),
                          //   endTime: TimeOfDay(hour: 21, minute: 22),
                          //   onSubmitted: (TimeRangeValue value) {
                          //     setState(() {
                          //       _startTime = value.startTime;
                          //       _endTime = value.endTime;
                          //     });
                          //   },
                          // );
                          setState(() {
                            id = 4;
                            allSelected = !allSelected;
                            ref = false;
                            businessman = false;
                            coach = false;
                            didUpdate();
                          });
                        },
                        selected: allSelected,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 6.5),
                ),
                BuildButton(
                  title: "NEXT",
                  onPressed: () {
                    push(
                        context,
                        OnBoardingParentAthlete5(
                          profession: "coach",
                        ));
                    // setState(() {
                    //   loading = true;
                    // });
                    // print("ssdj $id ${widget.dropDownValue.id.toString()}");
                    // userBloc.updateUserType(
                    //     updateUserTypeRequestModel: UpdateUserTypeRequestModel(
                    //         userType: id,
                    //         invitationType:
                    //         widget.dropDownValue.id.toString()));
                  },
                )
              ],
            )
          : Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }
}
