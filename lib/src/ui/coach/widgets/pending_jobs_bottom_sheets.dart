import 'package:app_template/src/ui/coach/widgets/coach_training_session_location.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/summary_widget_email.dart';
import 'package:app_template/src/ui/widgets/time_slot_app_bar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BottomSheetPendingJobs extends StatefulWidget {
  final String buttonName1;
  final String buttonName2;
  final bool cancelButtonTrue;
  final Function onPressedReschedule;
  final Function onPressedDecline;
  final Function onPressedAccept;
  final String locationAddress;
  final String locationName;
  final bool rated;
  final bool liked;
  final String startTime1;
  final String startTime2;
  final String endTime1;
  final String endTime2;
  final String likes;
  final String bookmarks;
  final String distance;
  final String time;
  final String name;
  final String email;
  final String phoneNumber;
  final String startTime;
  final String endTime;
  final String date;
  final String sportImage;
  final String sportName;
  final String duration;
  final String amount;
  final String profileImageUrl;
  final String buttonName3;
  BottomSheetPendingJobs(
      {this.buttonName1,
      this.buttonName2,
      this.buttonName3,
      this.profileImageUrl,
      this.cancelButtonTrue,
      this.onPressedReschedule,
      this.onPressedAccept,
      this.onPressedDecline,
      this.locationName,
      this.locationAddress,
      this.rated,
      this.liked,
      this.likes,
      this.time,
      this.bookmarks,
      this.distance,
      this.endTime1,
      this.endTime2,
      this.startTime1,
      this.startTime2,
      this.name,
      this.amount,
      this.duration,
      this.startTime,
      this.date,
      this.endTime,
      this.email,
      this.phoneNumber,
      this.sportName,
      this.sportImage});
  @override
  _BottomSheetPendingJobsState createState() => _BottomSheetPendingJobsState();
}

class _BottomSheetPendingJobsState extends State<BottomSheetPendingJobs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[6],
        leading: Container(),
        elevation: 0,
        actions: [
          Container(
            width: screenWidth(context, dividedBy: 1),
            child: AppBarTimeSlot(
              pageTitle: "TRAINING SESSION",
              pageNavigation: () {},
              rightIcon: true,
              icon: false,
              onTapRightIcon: () {
                pop(context);
              },
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 2.7),
            child: ListView(
              shrinkWrap: true,
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 17),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Constants.kitGradients[5],
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: screenHeight(context, dividedBy: 40),
                          ),
                          CircleAvatar(
                            radius: screenWidth(context, dividedBy: 18),
                            backgroundImage:
                                NetworkImage(widget.profileImageUrl),
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 80),
                          ),
                          SizedBox(
                            child: Text(
                              widget.name,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontFamily: 'OpenSSansRegular'),
                            ),
                          ),
                          Row(
                            children: [
                              SummaryTileEmail(
                                heading: "Phone",
                                subheading: widget.phoneNumber,
                                width: 3.1,
                              ),
                              SummaryTileEmail(
                                heading: "Email",
                                subheading: widget.email,
                                width: 2.2,
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              SizedBox(
                                width: screenWidth(context, dividedBy: 30),
                              ),
                              SummaryTileEmail(
                                heading: "Date",
                                subheading: widget.date,
                                width: 3.1,
                              ),
                              SummaryTileEmail(
                                heading: "Start Time",
                                subheading: widget.startTime,
                                width: 3.95,
                              ),
                              SummaryTileEmail(
                                  heading: "End Time",
                                  subheading: widget.endTime,
                                  width: 3.95),
                            ],
                          ),
                          Row(
                            children: [
                              SizedBox(
                                width: screenWidth(context, dividedBy: 30),
                              ),
                              SummaryTileEmail(
                                icon: true,
                                image: widget.sportImage,
                                heading: "Sport",
                                subheading: widget.sportName,
                                width: 3.1,
                              ),
                              SummaryTileEmail(
                                heading: "Duration",
                                subheading: widget.duration,
                                width: 3.95,
                              ),
                              SummaryTileEmail(
                                heading: "Rate",
                                subheading: "\$" + widget.amount,
                                width: 3.95,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 20),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 60),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: CoachTrainingSessionLocation(
                    locationAddress: "76A Eighth Avenue Pittsburgh PA, 15211",
                    locationName: "YMCA",
                    rated: true,
                    bookmarks: "137",
                    distance: "0.4",
                    endTime1: "9AM",
                    endTime2: "9AM",
                    liked: true,
                    likes: "4.7",
                    startTime1: "9AM",
                    startTime2: "9AM",
                    time: "30Min",
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                Container(
                  color: Colors.white,
                  height: screenHeight(context, dividedBy: 4),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 50),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 15)),
            child: BuildButton(
                title: widget.buttonName3,
                disabled: false,
                onPressed: widget.onPressedReschedule),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 50),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 20),
            //color: Colors.green,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  width: screenWidth(context, dividedBy: 2.3),
                  child: BuildButton(
                    title: widget.buttonName1,
                    disabled: false,
                    onPressed: widget.onPressedDecline,
                  ),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 2.3),
                  child: BuildButton(
                      title: widget.buttonName2,
                      disabled: false,
                      onPressed: widget.onPressedAccept),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
