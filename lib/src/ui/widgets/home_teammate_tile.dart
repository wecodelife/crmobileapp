import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class HomeTeammateTile extends StatefulWidget {
  final Function onPressed;
  HomeTeammateTile({this.onPressed});
  @override
  _HomeTeammateTileState createState() => _HomeTeammateTileState();
}

class _HomeTeammateTileState extends State<HomeTeammateTile> {
  bool check = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: screenHeight(context, dividedBy: 100),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: GestureDetector(
            onTap: () {
              if (check == true)
                widget.onPressed();
              else
                setState(() {
                  check = !check;
                });
            },
            child: ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: Container(
                width: screenWidth(context, dividedBy: 4),
                height: screenWidth(context, dividedBy: 4),
                decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: AssetImage("assets/images/dummy_bg.jpg"),
                        fit: BoxFit.fill)),
                child: Image.network(
                  "https://image.shutterstock.com/image-photo/business-woman-drawing-global"
                  "-structure-260nw-1006041130.jpg",
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ),
        check == true
            ? Row(
                children: [
                  Icon(
                    Icons.phone_outlined,
                    size: 20,
                    color: Constants.kitGradients[1],
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 20),
                  ),
                  Icon(
                    Icons.mail_outline,
                    size: 20,
                    color: Constants.kitGradients[1],
                  ),
                ],
              )
            : Container()
      ],
    );
  }
}
