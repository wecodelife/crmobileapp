import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DropDownFormField extends StatefulWidget {
  List<String> dropDownList;
  String title;
  String dropDownValue;
  bool underline;
  bool hintText;
  double boxWidth;
  double boxHeight;
  String fontFamily;
  double hintPadding;
  String hintFontFamily;
  ValueChanged<String> onClicked;
  DropDownFormField(
      {this.fontFamily,
      this.hintText,
      this.title,
      this.dropDownList,
      this.hintPadding,
      this.dropDownValue,
      this.onClicked,
      this.underline,
      this.boxWidth,
      this.boxHeight,
      this.hintFontFamily});
  @override
  _DropDownFormFieldState createState() => _DropDownFormFieldState();
}

String dropdownValue;

class _DropDownFormFieldState extends State<DropDownFormField> {
  @override
  void initState() {
    // TODO: implement initState
    print(widget.dropDownList);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          height: screenHeight(context, dividedBy: widget.boxHeight),
          width: screenWidth(context, dividedBy: widget.boxWidth),
          color: Colors.transparent,
          child: DropdownButton<String>(
              underline: Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 5),
                  ),
                  Divider(
                    thickness: screenHeight(context, dividedBy: 760),
                    color: Colors.grey,
                  ),
                ],
              ),
              icon: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 30),
                    ),
                    Icon(
                      Icons.arrow_drop_down,
                      size: 30,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
              iconSize: 10,
              dropdownColor: Colors.transparent,
              value: widget.dropDownValue == ""
                  ? dropdownValue
                  : widget.dropDownValue,
              style: TextStyle(
                  fontSize: 18,
                  color: Colors.grey,
                  fontFamily: 'SFProText-Regular',
                  fontWeight: FontWeight.w600),
              items: widget.dropDownList
                  .map<DropdownMenuItem<String>>(
                      (String value) => DropdownMenuItem<String>(
                          value: value,
                          child: Container(
                            width: screenWidth(context, dividedBy: 3.7),
                            child: Text(
                              value,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                                fontFamily: widget.fontFamily,
                              ),
                            ),
                          )))
                  .toList(),
              onChanged: (selectedValue) {
                setState(() {
                  widget.dropDownValue = selectedValue;
                });
                widget.onClicked(selectedValue);
              },
              hint: Row(
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: widget.hintPadding),
                  ),
                  Text(
                    widget.title,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontFamily: widget.hintFontFamily),
                  ),
                ],
              )),
        ),
      ],
    );
  }
}
