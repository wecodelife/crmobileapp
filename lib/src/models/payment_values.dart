// To parse this JSON data, do
//
//     final paymentValues = paymentValuesFromJson(jsonString);

import 'dart:convert';

PaymentValues paymentValuesFromJson(String str) =>
    PaymentValues.fromJson(json.decode(str));

String paymentValuesToJson(PaymentValues data) => json.encode(data.toJson());

class PaymentValues {
  PaymentValues({
    this.name,
    this.id,
  });

  final List<String> name;
  final List<int> id;

  factory PaymentValues.fromJson(Map<String, dynamic> json) => PaymentValues(
        name: json["name"] == null
            ? null
            : List<String>.from(json["name"].map((x) => x)),
        id: json["id"] == null
            ? null
            : List<int>.from(json["id"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : List<dynamic>.from(name.map((x) => x)),
        "id": id == null ? null : List<dynamic>.from(id.map((x) => x)),
      };
}
