import 'package:app_template/src/ui/widgets/bottom_navigation_bar_widget.dart';
import 'package:app_template/src/ui/widgets/coach_details_cupertino_tile.dart';
import 'package:app_template/src/ui/widgets/coach_profile_name_widget.dart';
import 'package:app_template/src/ui/widgets/coach_profile_rating_widget.dart';
import 'package:app_template/src/ui/widgets/heading_widget.dart';
import 'package:app_template/src/ui/widgets/home_group_tile.dart';
import 'package:app_template/src/ui/widgets/home_page_league_widget.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TeamMateDetailsPage extends StatefulWidget {
  @override
  _TeamMateDetailsPageState createState() => _TeamMateDetailsPageState();
}

class _TeamMateDetailsPageState extends State<TeamMateDetailsPage> {
  List<String> imgList = [
    "assets/icons/facility_bg_image.png",
    "assets/icons/facility_bg_image.png",
    "assets/icons/facility_bg_image.png"
  ];

  int _current = 0;
  double position;
  bool rating = false;
  int reviewCurrentIndex = 0;
  double reviewPosition;
  bool bookMarks = false;
  bool photos = false;

  @override
  Widget build(BuildContext context) {
    DotsDecorator(
        activeColor: Colors.red.withOpacity(.50),
        color: Colors.white.withOpacity(.50),
        shape: CircleBorder(),
        size: Size(10, 10));
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
      child: Scaffold(
          bottomNavigationBar: BottomNavigationWidget(),
          body: Container(
            height: screenHeight(context, dividedBy: 1),
            child: ListView(
              padding: EdgeInsets.zero,
              children: [
                Stack(
                  children: [
                    CarouselSlider(
                      options: CarouselOptions(
                          aspectRatio: screenWidth(context, dividedBy: 1) /
                              screenHeight(context, dividedBy: 2.4),
                          viewportFraction: 1,
                          onPageChanged: (index, reason) {
                            setState(() {
                              _current = index;
                              print(_current);
                            });
                          }),
                      items: imgList.map((i) {
                        return Builder(
                          builder: (
                            BuildContext context,
                          ) {
                            return CoachDetailsCupertinoTile(
                              imageName: i,
                              iconName: "assets/icons/notification_icon.svg",
                              icon1: true,
                              iconName1: "assets/icons/close_icon.svg",
                              sizedBoxWidth: 1.35,
                              coachCurrentIndex: _current,
                            );
                          },
                        );
                      }).toList(),
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 2.8),
                        ),
                        Container(
                            width: screenWidth(context, dividedBy: 1),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Constants.kitGradients[6],
                            ),
                            child: Column(children: [
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                        screenWidth(context, dividedBy: 30)),
                                child: Row(
                                  children: [
                                    CoachProfileNameWidget(
                                      coachName: "TeamMate Name",
                                      width: 1.3,
                                      image:
                                          "assets/icons/basket_ball_icon.svg",
                                      subHeading: "Soccer",
                                    ),
                                    Column(
                                      children: [
                                        SizedBox(
                                          height: screenHeight(context,
                                              dividedBy: 12),
                                        ),
                                        Row(
                                          children: [
                                            Icon(
                                              Icons.phone_outlined,
                                              size: screenWidth(context,
                                                  dividedBy: 20),
                                              color: Constants.kitGradients[1],
                                            ),
                                            SizedBox(
                                              width: screenWidth(context,
                                                  dividedBy: 30),
                                            ),
                                            Icon(
                                              Icons.mail_outline,
                                              color: Constants.kitGradients[1],
                                              size: screenWidth(context,
                                                  dividedBy: 20),
                                            )
                                          ],
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 60),
                              ),
                              Container(
                                color: Constants.kitGradients[5],
                                width: screenWidth(context, dividedBy: 1),
                                height: screenHeight(context, dividedBy: 9),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    CoachProfileRating(
                                      heading: "4.5",
                                      subHeading: "351  Ratings",
                                      rating: rating,
                                      iconName: Icons.star_rounded,
                                      ratingIcon: true,
                                      width: 4,
                                      divider: true,
                                      onPressed: () {
                                        setState(() {
                                          rating = !rating;
                                          print(rating);
                                        });
                                      },
                                    ),
                                    CoachProfileRating(
                                      heading: "137k",
                                      subHeading: "BookMarks",
                                      rating: bookMarks,
                                      ratingIcon: true,
                                      iconName: Icons.bookmark,
                                      width: 4,
                                      divider: true,
                                      onPressed: () {
                                        setState(() {
                                          bookMarks = !bookMarks;
                                        });
                                      },
                                    ),
                                    CoachProfileRating(
                                      heading: "4.5",
                                      subHeading: "351  Photos",
                                      iconName: Icons.photo_size_select_actual,
                                      rating: photos,
                                      ratingIcon: true,
                                      width: 4,
                                      onPressed: () {
                                        setState(() {
                                          photos = !photos;
                                        });
                                      },
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                        screenWidth(context, dividedBy: 30)),
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 30),
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          "ABOUT",
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontFamily: 'OswaldBold',
                                              color: Constants.kitGradients[1]),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 80),
                                    ),
                                    Text(
                                      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh"
                                      " euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim",
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontFamily: 'AvenirBook',
                                          color: Colors.white),
                                    ),
                                    ListView.builder(
                                        padding: EdgeInsets.zero,
                                        itemCount: 3,
                                        shrinkWrap: true,
                                        scrollDirection: Axis.vertical,
                                        itemBuilder:
                                            (BuildContext cntxt, int index) {
                                          return Column(
                                            children: [
                                              index == 0
                                                  ? HeadingWidget(
                                                      heading: "TEAMS",
                                                    )
                                                  : Container(),
                                              Column(
                                                children: [
                                                  HomePageLeagueWidget(
                                                      onPressed: () {},
                                                      heading:
                                                          "Plex Paul Adult League",
                                                      subHeading:
                                                          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit,",
                                                      imageName:
                                                          "assets/icons/basket_ball_icon.svg",
                                                      location: "Robinson",
                                                      rating: "4.7",
                                                      leagueNumbers: "73",
                                                      plusIcon: false),
                                                  SizedBox(
                                                    height: screenHeight(
                                                        context,
                                                        dividedBy: 40),
                                                  )
                                                ],
                                              ),
                                            ],
                                          );
                                        }),
                                    ListView.builder(
                                        scrollDirection: Axis.vertical,
                                        shrinkWrap: true,
                                        padding: EdgeInsets.zero,
                                        itemCount: 1,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Column(
                                            children: [
                                              index == 0
                                                  ? HeadingWidget(
                                                      heading: "Groups",
                                                    )
                                                  : Container(),
                                              HomeGroupTile(),
                                            ],
                                          );
                                        })
                                  ],
                                ),
                              ),
                            ])),
                      ],
                    )
                  ],
                ),
              ],
            ),
          )),
    );
  }
}
