import 'package:app_template/src/ui/screens/onBoardingAthlete19.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class OnBoardingAthlete17 extends StatefulWidget {
  @override
  _OnBoardingAthlete17State createState() => _OnBoardingAthlete17State();
}

class _OnBoardingAthlete17State extends State<OnBoardingAthlete17> {
  bool yes = false;
  bool no = false;
  bool mayBe = false;
  bool transparentYes = true;
  bool transparentNo = true;
  bool transparentMayBe = true;
  bool disabled = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 20)),
        child: Column(
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            TextProgressIndicator(
              heading: "DO YOU WANT TRAINING DONE AT YOUR HOME",
              descriptionText:
                  "Do you coaches to travel to your home to train your athletes? If not that's okay.",
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            OnBoardingProgressIndicator(
              width: 1.5,
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 5),
            ),
            BuildButton(
              title: "YES",
              transparent: transparentYes,
              onPressed: () {
                setState(() {
                  yes = true;
                  transparentYes = false;
                  no = false;
                  transparentNo = true;
                  mayBe = false;
                  transparentMayBe = true;
                  disabled = false;
                });
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            BuildButton(
              title: "NO",
              transparent: transparentNo,
              onPressed: () {
                setState(() {
                  no = true;
                  transparentNo = false;
                  yes = false;
                  transparentYes = true;
                  mayBe = false;
                  transparentMayBe = true;
                  disabled = false;
                });
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            BuildButton(
              title: "MAYBE",
              transparent: transparentMayBe,
              onPressed: () {
                setState(() {
                  mayBe = true;
                  transparentMayBe = false;
                  yes = false;
                  transparentYes = true;
                  no = false;
                  transparentNo = true;
                  disabled = false;
                });
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 3.9),
            ),
            BuildButton(
                title: "NEXT",
                disabled: disabled,
                onPressed: () {
                  push(context, OnBoardingAthlete19());
                })
          ],
        ),
      ),
    );
  }
}
