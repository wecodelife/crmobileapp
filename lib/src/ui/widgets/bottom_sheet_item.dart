import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class HomeBottomSheetItem extends StatefulWidget {
  final String label;
  final String title;
  final ValueChanged<bool> onClicked;
  HomeBottomSheetItem({this.label, this.title, this.onClicked});
  @override
  _HomeBottomSheetItemState createState() => _HomeBottomSheetItemState();
}

class _HomeBottomSheetItemState extends State<HomeBottomSheetItem> {
  bool check = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            // print("clicked");
            setState(() {
              check = !check;
            });
            widget.onClicked(check);
          },
          child: Container(
            color: Colors.transparent,
            width: screenWidth(context, dividedBy: 1),
            // height: screenHeight(context, dividedBy: 10),
            child: Row(
              children: [
                Spacer(
                  flex: 1,
                ),
                Text(
                  widget.label,
                  style: TextStyle(
                      fontFamily: "OpenSansRegular",
                      fontSize: 17,
                      color: Constants.kitGradients[0]),
                ),
                widget.title == "Ratings"
                    ? Icon(
                        Icons.star_rate_rounded,
                        size: 16,
                        color: Constants.kitGradients[1],
                      )
                    : Container(),
                Spacer(
                  flex: 20,
                ),
                check == true
                    ? Icon(
                        Icons.check,
                        size: 20,
                        color: Constants.kitGradients[1],
                      )
                    : Container(),
                Spacer(
                  flex: 1,
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Divider(
            thickness: 2,
            color: Constants.kitGradients[10],
          ),
        )
      ],
    );
  }
}
