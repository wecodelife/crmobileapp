import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ListTileSelectSports extends StatefulWidget {
  final ValueChanged onValueChanged;
  final String label;
  final String icon;
  final Function onPressed;

  ListTileSelectSports(
      {this.onValueChanged, this.icon, this.label, this.onPressed});

  @override
  _ListTileSelectSportsState createState() => _ListTileSelectSportsState();
}

class _ListTileSelectSportsState extends State<ListTileSelectSports> {
  bool onClick = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 60)),
      child: GestureDetector(
        onTap: () {
          setState(() {
            onClick = !onClick;
            widget.onValueChanged(onClick);
            // widget.onPressed();
          });
        },
        child: Container(
            height: screenHeight(context, dividedBy: 10),
            width: screenWidth(context, dividedBy: 5),
            child: Column(
              children: [
                Container(
                  height: screenWidth(context, dividedBy: 7),
                  width: screenWidth(context, dividedBy: 7),
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: onClick == true
                              ? Constants.kitGradients[1]
                              : Colors.transparent),
                      borderRadius: BorderRadius.circular(6)),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SvgPicture.string(
                      widget.icon,
                      width: screenWidth(context, dividedBy: 7),
                      height: screenWidth(context, dividedBy: 7),
                      color: onClick == true
                          ? Constants.kitGradients[1]
                          : Colors.white,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                Text(
                  widget.label,
                  style: TextStyle(
                    color: onClick == true
                        ? Theme.of(context).buttonColor
                        : Colors.white,
                    fontFamily: 'OpenSansRegular',
                  ),
                )
              ],
            )),
      ),
    );
  }
}
