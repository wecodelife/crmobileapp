import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OnBoardingAppBar extends StatefulWidget {
  @override
  _OnBoardingAppBarState createState() => _OnBoardingAppBarState();
}

class _OnBoardingAppBarState extends State<OnBoardingAppBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 8),
      color: Constants.kitGradients[3],
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: screenWidth(context, dividedBy: 6),
            height: screenHeight(context, dividedBy: 17),
            child: SvgPicture.asset("assets/icons/appbar_logo.svg"),
          ),
        ],
      ),
    );
  }
}
