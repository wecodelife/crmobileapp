import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify.dart';
import 'package:app_template/src/ui/screens/resert_password_screen.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

class ForgetPasswordScreen extends StatefulWidget {
  @override
  _OnBoarding5State createState() => _OnBoarding5State();
}

class _OnBoarding5State extends State<ForgetPasswordScreen> {
  bool loading = false;
  bool disabled = true;
  int id;
  TextEditingController emailTextEditingController =
      new TextEditingController();

  void emailValidator(String email) {
    final bool isValid = EmailValidator.validate(email);
    if (isValid == true) {
      print(1);
    } else {
      showToast("Please enter a valid email");
    }
  }

  Future<void> _sendResetPasswordCode() async {
    try {
      ResetPasswordResult res = await Amplify.Auth.resetPassword(
        username: emailTextEditingController.text,
      );
      setState(() {
        // isPasswordReset = res.isPasswordReset;
        print("password code sent");
        push(
            context,
            ResetPasswordScreen(
              username: emailTextEditingController.text,
            ));
      });
    } on AuthException catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: loading == false
          ? ListView(
              children: [
                Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: screenWidth(context, dividedBy: 20)),
                      child: Column(
                        children: [
                          SizedBox(
                            height: screenHeight(context, dividedBy: 20),
                          ),
                          TextProgressIndicator(
                            heading: "LET'S GET YOU BACK INTO YOUR ACCOUNT",
                            descriptionText:
                                "Enter the email address you used when creating your account and we will send a link to reset your password.",
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 5),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: screenWidth(context, dividedBy: 20)),
                      child: FormFeildUserDetails(
                        onValueChanged: (value) {
                          setState(() {
                            if (emailTextEditingController.text != "") {
                              setState(() {
                                disabled = false;
                              });
                            } else {
                              setState(() {
                                disabled = true;
                              });
                            }
                          });
                        },
                        textEditingController: emailTextEditingController,
                        labelText: "Email*",
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 3.5),
                    ),
                    BuildButton(
                      title: "CANCEL",
                      onPressed: () {},
                      transparent: true,
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 40),
                    ),
                    BuildButton(
                      title: "SEND",
                      onPressed: () {
                        emailValidator(emailTextEditingController.text);
                        _sendResetPasswordCode();
                      },
                      disabled: disabled,
                    )
                  ],
                ),
              ],
            )
          : Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }
}
