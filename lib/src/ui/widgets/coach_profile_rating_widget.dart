import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CoachProfileRating extends StatefulWidget {
  final String image;
  final String heading;
  final double width;
  final String subHeading;
  final IconData iconName;
  final Function onPressed;
  final bool rating;
  final bool ratingIcon;
  final bool divider;
  CoachProfileRating(
      {this.image,
      this.width,
      this.subHeading,
      this.heading,
      this.onPressed,
      this.rating,
      this.ratingIcon,
      this.divider,
      this.iconName});

  @override
  _CoachProfileRatingState createState() => _CoachProfileRatingState();
}

class _CoachProfileRatingState extends State<CoachProfileRating> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: widget.width),
      height: screenHeight(context, dividedBy: 12),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          widget.onPressed();
                        },
                        child: Container(
                            height: screenHeight(context, dividedBy: 40),
                            width: screenWidth(context, dividedBy: 20),
                            child: widget.ratingIcon == true
                                ? Icon(widget.iconName,
                                    color: widget.rating == true
                                        ? Constants.kitGradients[1]
                                        : Colors.white)
                                : SvgPicture.asset(
                                    widget.image,
                                    color: Colors.white,
                                  )),
                      ),
                      SizedBox(
                        width: 10,
                      )
                    ],
                  ),
                  Column(
                    children: [
                      widget.ratingIcon == true
                          ? SizedBox(
                              height: screenHeight(context, dividedBy: 70),
                            )
                          : Container(),
                      Text(
                        widget.heading,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                            fontFamily: 'OpenSSansRegular'),
                      ),
                    ],
                  ),
                ],
              ),
              Text(
                widget.subHeading,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontFamily: 'OpenSSansRegular'),
              ),
            ],
          ),
          Spacer(
            flex: 1,
          ),
          widget.divider == true
              ? Container(
                  height: screenHeight(context, dividedBy: 14),
                  width: 1,
                  color: Colors.white,
                )
              : Container(),
          Spacer(
            flex: 2,
          ),
        ],
      ),
    );
  }
}
