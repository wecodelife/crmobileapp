// To parse this JSON data, do
//
//     final getAthleteListResponse = getAthleteListResponseFromJson(jsonString);

import 'dart:convert';

GetAthleteListResponse getAthleteListResponseFromJson(String str) =>
    GetAthleteListResponse.fromJson(json.decode(str));

String getAthleteListResponseToJson(GetAthleteListResponse data) =>
    json.encode(data.toJson());

class GetAthleteListResponse {
  GetAthleteListResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final List<Datum> data;

  factory GetAthleteListResponse.fromJson(Map<String, dynamic> json) =>
      GetAthleteListResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.profPic,
    this.sports,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.name,
    this.gender,
    this.parent,
  });

  final int id;
  final ProfPic profPic;
  final List<Sport> sports;
  final bool isActive;
  final bool isDeleted;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String name;
  final int gender;
  final int parent;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        profPic: json["prof_pic"] == null
            ? null
            : ProfPic.fromJson(json["prof_pic"]),
        sports: json["sports"] == null
            ? null
            : List<Sport>.from(json["sports"].map((x) => Sport.fromJson(x))),
        isActive: json["is_active"] == null ? null : json["is_active"],
        isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        name: json["name"] == null ? null : json["name"],
        gender: json["gender"] == null ? null : json["gender"],
        parent: json["parent"] == null ? null : json["parent"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "prof_pic": profPic == null ? null : profPic.toJson(),
        "sports": sports == null
            ? null
            : List<dynamic>.from(sports.map((x) => x.toJson())),
        "is_active": isActive == null ? null : isActive,
        "is_deleted": isDeleted == null ? null : isDeleted,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "name": name == null ? null : name,
        "gender": gender == null ? null : gender,
        "parent": parent == null ? null : parent,
      };
}

class ProfPic {
  ProfPic({
    this.id,
    this.imageFile,
  });

  final int id;
  final String imageFile;

  factory ProfPic.fromJson(Map<String, dynamic> json) => ProfPic(
        id: json["id"] == null ? null : json["id"],
        imageFile: json["image_file"] == null ? null : json["image_file"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "image_file": imageFile == null ? null : imageFile,
      };
}

class Sport {
  Sport({
    this.id,
    this.name,
    this.icon,
    this.iconFile,
    this.description,
  });

  final int id;
  final String name;
  final String icon;
  final dynamic iconFile;
  final String description;

  factory Sport.fromJson(Map<String, dynamic> json) => Sport(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        icon: json["icon"] == null ? null : json["icon"],
        iconFile: json["iconFile"],
        description: json["description"] == null ? null : json["description"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "icon": icon == null ? null : icon,
        "iconFile": iconFile,
        "description": description == null ? null : description,
      };
}
