import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BuildAppBar extends StatefulWidget {
  final bool isSvg;
  final String appBarImage;
  final String title;
  final bool isLogo;
  BuildAppBar({this.isSvg, this.appBarImage, this.isLogo, this.title});
  @override
  _BuildAppBarState createState() => _BuildAppBarState();
}

class _BuildAppBarState extends State<BuildAppBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 8),
      color: Constants.kitGradients[3],
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          widget.isLogo == true
              ? Container(
                  width: screenWidth(context, dividedBy: 6),
                  height: screenHeight(context, dividedBy: 17),
                  child: widget.isSvg == true
                      ? SvgPicture.asset(widget.appBarImage)
                      : Image.asset(widget.appBarImage),
                )
              : Container(
                  width: screenWidth(context, dividedBy: 6),
                  height: screenHeight(context, dividedBy: 17),
                  child: Text(
                    widget.title,
                    style: TextStyle(
                        fontFamily: "Oswald",
                        fontSize: 17,
                        fontWeight: FontWeight.bold),
                  ),
                ),
        ],
      ),
    );
  }
}
