import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_sports_response_model.dart';
import 'package:app_template/src/ui/coach/screens/onboarding_coach_9.dart';
import 'package:app_template/src/ui/screens/on_boarding_parent_athelete_6.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/game_selection_tile.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class OnBoardingParentAthlete5 extends StatefulWidget {
  final String profession;
  OnBoardingParentAthlete5({this.profession});
  @override
  _OnBoardingParentAthlete5State createState() =>
      _OnBoardingParentAthlete5State();
}

class _OnBoardingParentAthlete5State extends State<OnBoardingParentAthlete5> {
  int counter = 0;
  bool disable = true;
  List<Datum> selectedSports = [];
  List<String> tileImages = [
    "assets/images/basketball_image.png",
    "assets/images/basketball_image.png",
    "assets/images/boxing_image.png",
    "assets/images/dancing_image.png",
    "assets/images/fishing_image.png",
    "assets/images/football_image.png",
    "assets/images/golf_image.png",
    "assets/images/hockey_image.png",
    "assets/images/martial_image.png",
    "assets/images/photo_image.png",
    "assets/images/soccer_image.png",
    "assets/images/speed_image.png",
    "assets/images/swimming_image.png",
    "assets/images/tennis_image.png",
    "assets/images/volleyball_image.png",
    "assets/images/watersports_image.png",
    "assets/images/wintersports_image.png"
  ];
  List<String> sportsNames = [
    "BASEBALL/SOFTBALL",
    "BASKETBALL",
    "BOXING",
    "DANCE/CHEER/GYMNASTICS",
    "FISHING",
    "FOOTBALL",
    "GOLF",
    "HOCKEY",
    "MARTIAL ARTS/SELF-EDEFENCE",
    "PHOTOS/VIDEOS",
    "SOCCER",
    "SPORTS/STRENGTH",
    "SWIMMING",
    "TENNIS",
    "VOLLEYBALL",
    "WATER SPORTS",
    "WINTER SPORTS"
  ];
  UserBloc userBloc = UserBloc();
  void incrementCounter(bool onClick) {
    if (onClick == true) {
      counter++;
      print(counter);
    } else {
      counter--;
    }
  }

  void enableSelectButton() {
    print(counter.toString() + "eee");
    if (counter >= 1)
      setState(() {
        disable = false;
      });
    else
      setState(() {
        disable = true;
      });
  }

  @override
  void initState() {
    // TODO: implement initState
    userBloc.getSports();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 20)),
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                TextProgressIndicator(
                  heading: widget.profession == "coach"
                      ? "WHAT SPORTS ARE YOU INTO?"
                      : "WHAT SPORTS DO YOU NEED TRAINING IN?",
                  descriptionText: widget.profession == "coach"
                      ? "What sports are you interested in helping out with?"
                      : "Select the sports you or any of your athletes will need training in.",
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                OnBoardingProgressIndicator(
                  width: 5,
                ),
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30)),
            child: StreamBuilder<GetSportsResponseModel>(
                stream: userBloc.getSportsResponse,
                builder: (context, snapshot) {
                  return snapshot.hasData
                      ? Container(
                          width: screenWidth(context, dividedBy: 1.06),
                          height: screenHeight(context, dividedBy: 1.6),
                          child: GridView.builder(
                              scrollDirection: Axis.vertical,
                              itemCount: snapshot.data.data.length,
                              shrinkWrap: true,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2,
                                      crossAxisSpacing:
                                          screenWidth(context, dividedBy: 30),
                                      childAspectRatio: screenWidth(context,
                                              dividedBy: 12) /
                                          screenHeight(context, dividedBy: 20),
                                      mainAxisSpacing:
                                          screenHeight(context, dividedBy: 40)),
                              itemBuilder: (BuildContext context, int index) {
                                return new GameSelectionTile(
                                  bgImage: tileImages[index],
                                  title: snapshot.data.data[index].name,
                                  selectedValue: (value) {
                                    print(value);
                                    setState(() {
                                      incrementCounter(value);
                                      enableSelectButton();
                                      if (selectedSports.contains(
                                          snapshot.data.data[index])) {
                                        selectedSports
                                            .remove(snapshot.data.data[index]);
                                      } else {
                                        selectedSports
                                            .add(snapshot.data.data[index]);
                                      }
                                    });
                                  },
                                );
                              }),
                        )
                      : Container(
                          width: screenWidth(context, dividedBy: 1.06),
                          height: screenHeight(context, dividedBy: 1.6),
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        );
                }),
          ),
          BuildButton(
            title: "NEXT",
            disabled: disable,
            onPressed: () {
              if (widget.profession == "coach") {
                push(
                    context,
                    OnBoardingCoach9(
                      selectedSports: selectedSports,
                    ));
              } else {
                push(context, OnBoardingParentAthlete6());
              }
            },
          ),
        ],
      ),
    );
  }
}
