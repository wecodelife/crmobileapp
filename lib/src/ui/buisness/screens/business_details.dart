import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/get_sports_response_model.dart';
import 'package:app_template/src/ui/buisness/screens/business_list.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/drop_down_cr_app.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/list_tile_select_sports.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class BusinessDetails extends StatefulWidget {
  @override
  _BusinessDetailsState createState() => _BusinessDetailsState();
}

class _BusinessDetailsState extends State<BusinessDetails> {
  TextEditingController businessNameTextEditingController =
      new TextEditingController();
  TextEditingController businessInfoTextEditingController =
      new TextEditingController();
  String dropDownValue;
  String dropDownSelectedValue;
  bool onClicked;
  int count = 0;
  List<int> selectedSports = [];
  UserBloc userBloc = UserBloc();
  List<String> sports = [
    "Basketball",
    "Volleyball",
    "Football",
    "Swimming",
    "Baseball",
    "Hockey",
    "Soccer",
    "Tennis"
  ];
  List<String> sportsIcon = [
    "assets/icons/basketball.svg",
    "assets/icons/volleyball.svg",
    "assets/icons/basket_ball_icon.svg",
    "assets/icons/swimming.svg",
    "assets/icons/baseball.svg",
    "assets/icons/hockey.svg",
    "assets/icons/soccer.svg",
    "assets/icons/tennis.svg"
  ];
  void incrementCounter() {
    count = count + 1;
  }

  void decrementCounter() {
    count = count - 1;
  }

  Future<String> addItems(int value) async {
    if (onClicked == true) {
      incrementCounter();
      setState(() {
        // selectedSports.add(snapshot
        //     .data.data[index].id);
        selectedSports.add(value);
      });
      return "fhfhgf" + selectedSports.toString();
    } else {
      decrementCounter();
      selectedSports.remove(value);
      return "gfh" + selectedSports.toString();
    }
  }

  @override
  void initState() {
    userBloc.getSports();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 12)),
        child: ListView(
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),
            TextProgressIndicator(
              heading: "LETS GET SOME BUSINESS DETAILS SET UP",
              descriptionText:
                  "Set up some details about your business and set your employee access.",
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),
            OnBoardingProgressIndicator(
              width: 3.5,
              progressIndicator: true,
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            FormFeildUserDetails(
              textEditingController: businessNameTextEditingController,
              labelText: "Business Name",
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            FormFeildUserDetails(
              textEditingController: businessInfoTextEditingController,
              labelText: "Business Info",
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            Text(
              "Employee Access",
              style: TextStyle(
                color: Constants.kitGradients[0],
                fontFamily: 'OpenSansRegular',
                fontSize: 16,
              ),
            ),
            DropDownCrApp(
              title: "Select",
              dropDownList: ["Yes", "No"],
              dropDownValue: dropDownValue,
              onClicked: (value) {
                setState(() {
                  dropDownValue = value;
                });
              },
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Text(
                  "Select your Business Sports Type",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'OpenSansRegular',
                      fontSize: 16),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            // Container(
            //   width: screenWidth(context, dividedBy: 5.5),
            //   height: screenHeight(context, dividedBy: 7),
            //   child: ListView.builder(
            //       itemCount: 5,
            //       shrinkWrap: true,
            //       scrollDirection: Axis.horizontal,
            //       itemBuilder: (BuildContext cntxt, int index) {
            //         return ListTileSelectSports(
            //             onValueChanged: (value) {},
            //             onPressed: () {
            //               push(context, BusinessList());
            //             },
            //             icon: sportsIcon[index],
            //             label: sports[index]);
            //       }),
            // ),
            Container(
              height: screenHeight(context, dividedBy: 5.5),
              width: screenWidth(context, dividedBy: 1.07),
              child: StreamBuilder<GetSportsResponseModel>(
                  stream: userBloc.getSportsResponse,
                  builder: (context, snapshot) {
                    return snapshot.hasData
                        ? ListView.builder(
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            itemCount: snapshot.data.data.length,
                            itemBuilder: (BuildContext context, int index) {
                              print(snapshot.data.data[index].name);
                              return ListTileSelectSports(
                                label: snapshot.data.data[index].name,
                                icon: snapshot.data.data[index].icon,
                                onValueChanged: (onClick) async {
                                  onClicked = onClick;
                                  await addItems(snapshot.data.data[index].id);

                                  print(selectedSports.toString());
                                },
                              );
                            },
                          )
                        : Container();
                  }),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 12),
            ),
            BuildButton(
              onPressed: () {
                push(
                    context,
                    BusinessList(
                      businessInfo: businessInfoTextEditingController.text,
                      businessName: businessNameTextEditingController.text,
                      employeeAccess: dropDownValue == "Yes" ? true : false,
                      selectedSports: selectedSports,
                    ));
              },
              title: "NEXT",
              disabled: false,
            )
          ],
        ),
      ),
    );
  }
}
