import 'package:app_template/src/ui/widgets/back_button.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SoccerFieldSelectionTile extends StatefulWidget {
  final String image;
  final String title;
  final String icon;
  final String subtitle;
  final String pricePerDay;
  final String pricePerHour;
  final Function onPressed;
  final bool hasIcon;
  SoccerFieldSelectionTile(
      {this.image,
      this.title,
      this.icon,
      this.subtitle,
      this.pricePerDay,
      this.pricePerHour,
      this.hasIcon,
      this.onPressed});
  @override
  _SoccerFieldSelectionTileState createState() =>
      _SoccerFieldSelectionTileState();
}

class _SoccerFieldSelectionTileState extends State<SoccerFieldSelectionTile> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.asset(widget.image),
      title: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 200)),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.title,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 13,
                      fontFamily: 'OpenSansSemiBold'),
                ),
                Spacer(
                  flex: 5,
                ),
                widget.hasIcon == false
                    ? Container()
                    : SvgPicture.asset(
                        widget.icon,
                        width: screenWidth(
                          context,
                          dividedBy: 20,
                        ),
                        height: screenHeight(context, dividedBy: 36),
                      ),
                Spacer(
                  flex: 20,
                ),
              ],
            ),
          ],
        ),
      ),
      subtitle: Column(
        children: [
          SizedBox(
            height: screenHeight(context, dividedBy: 100),
          ),
          Row(
            children: [
              Spacer(
                flex: 1,
              ),
              Text(
                widget.subtitle,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 13,
                    fontFamily: 'OpenSansSemiBold'),
              ),
              Spacer(
                flex: 50,
              ),
            ],
          ),
        ],
      ),
      trailing: Container(
        width: screenWidth(context, dividedBy: 3.3),
        child: Column(
          children: [
            Row(
              children: [
                Spacer(
                  flex: 10,
                ),
                Container(
                  child: Text(
                    "\u0024 " + widget.pricePerHour + "/hr  ",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontFamily: 'OpenSansSemiBold'),
                  ),
                ),
                Spacer(
                  flex: 1,
                ),
                Container(
                  child: Text(
                    "\u0024 " + widget.pricePerDay + "/day",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontFamily: 'OpenSansSemiBold'),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              children: [
                Spacer(
                  flex: 1,
                ),
                Container(
                  width: screenWidth(context, dividedBy: 5),
                  height: screenHeight(context, dividedBy: 25),
                  child: BackButtonCr(
                    title: "BookNow",
                    onPressed: () {
                      widget.onPressed();
                    },
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
