// To parse this JSON data, do
//
//     final bookZoomMeetingResponse = bookZoomMeetingResponseFromJson(jsonString);

import 'dart:convert';

BookZoomMeetingResponse bookZoomMeetingResponseFromJson(String str) =>
    BookZoomMeetingResponse.fromJson(json.decode(str));

String bookZoomMeetingResponseToJson(BookZoomMeetingResponse data) =>
    json.encode(data.toJson());

class BookZoomMeetingResponse {
  BookZoomMeetingResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final Data data;

  factory BookZoomMeetingResponse.fromJson(Map<String, dynamic> json) =>
      BookZoomMeetingResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
        "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    this.result,
    this.id,
    this.jsonrpc,
  });

  final Result result;
  final String id;
  final String jsonrpc;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        result: json["result"] == null ? null : Result.fromJson(json["result"]),
        id: json["id"] == null ? null : json["id"],
        jsonrpc: json["jsonrpc"] == null ? null : json["jsonrpc"],
      );

  Map<String, dynamic> toJson() => {
        "result": result == null ? null : result.toJson(),
        "id": id == null ? null : id,
        "jsonrpc": jsonrpc == null ? null : jsonrpc,
      };
}

class Result {
  Result({
    this.requireConfirm,
    this.bookings,
    this.invoice,
  });

  final bool requireConfirm;
  final List<Booking> bookings;
  final dynamic invoice;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        requireConfirm:
            json["require_confirm"] == null ? null : json["require_confirm"],
        bookings: json["bookings"] == null
            ? null
            : List<Booking>.from(
                json["bookings"].map((x) => Booking.fromJson(x))),
        invoice: json["invoice"],
      );

  Map<String, dynamic> toJson() => {
        "require_confirm": requireConfirm == null ? null : requireConfirm,
        "bookings": bookings == null
            ? null
            : List<dynamic>.from(bookings.map((x) => x.toJson())),
        "invoice": invoice,
      };
}

class Booking {
  Booking({
    this.id,
    this.eventId,
    this.unitId,
    this.clientId,
    this.clientHash,
    this.startDateTime,
    this.endDateTime,
    this.timeOffset,
    this.isConfirmed,
    this.requirePayment,
    this.code,
    this.hash,
  });

  final String id;
  final String eventId;
  final String unitId;
  final String clientId;
  final String clientHash;
  final DateTime startDateTime;
  final DateTime endDateTime;
  final String timeOffset;
  final String isConfirmed;
  final bool requirePayment;
  final String code;
  final String hash;

  factory Booking.fromJson(Map<String, dynamic> json) => Booking(
        id: json["id"] == null ? null : json["id"],
        eventId: json["event_id"] == null ? null : json["event_id"],
        unitId: json["unit_id"] == null ? null : json["unit_id"],
        clientId: json["client_id"] == null ? null : json["client_id"],
        clientHash: json["client_hash"] == null ? null : json["client_hash"],
        startDateTime: json["start_date_time"] == null
            ? null
            : DateTime.parse(json["start_date_time"]),
        endDateTime: json["end_date_time"] == null
            ? null
            : DateTime.parse(json["end_date_time"]),
        timeOffset: json["time_offset"] == null ? null : json["time_offset"],
        isConfirmed: json["is_confirmed"] == null ? null : json["is_confirmed"],
        requirePayment:
            json["require_payment"] == null ? null : json["require_payment"],
        code: json["code"] == null ? null : json["code"],
        hash: json["hash"] == null ? null : json["hash"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "event_id": eventId == null ? null : eventId,
        "unit_id": unitId == null ? null : unitId,
        "client_id": clientId == null ? null : clientId,
        "client_hash": clientHash == null ? null : clientHash,
        "start_date_time":
            startDateTime == null ? null : startDateTime.toIso8601String(),
        "end_date_time":
            endDateTime == null ? null : endDateTime.toIso8601String(),
        "time_offset": timeOffset == null ? null : timeOffset,
        "is_confirmed": isConfirmed == null ? null : isConfirmed,
        "require_payment": requirePayment == null ? null : requirePayment,
        "code": code == null ? null : code,
        "hash": hash == null ? null : hash,
      };
}
