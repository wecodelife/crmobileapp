import 'package:app_template/src/models/add_athlete_request_model.dart';
import 'package:app_template/src/models/add_authorized_contact_request_model.dart';
import 'package:app_template/src/models/add_banking_info_request.dart';
import 'package:app_template/src/models/add_business_banking_info_request.dart';
import 'package:app_template/src/models/add_coach_request.dart';
import 'package:app_template/src/models/add_emergency_contact_request_model.dart';
import 'package:app_template/src/models/add_facility_rate_request.dart';
import 'package:app_template/src/models/add_facility_request.dart';
import 'package:app_template/src/models/add_parent_request_model.dart';
import 'package:app_template/src/models/add_reference_request.dart';
import 'package:app_template/src/models/book_zoom_meeting_request.dart';
import 'package:app_template/src/models/get_timeslots_request.dart';
import 'package:app_template/src/models/login_request_model.dart';
import 'package:app_template/src/models/state.dart';
import 'package:app_template/src/models/update_sports_request.dart';
import 'package:app_template/src/models/update_user_type_request_model.dart';
import 'package:app_template/src/models/upload_image_request_model.dart';
import 'package:app_template/src/models/upload_video_request.dart';
import 'package:app_template/src/resources/api_providers/user_api_provider.dart';

/// Repository is an intermediary class between network and data
class Repository {
  final userApiProvider = UserApiProvider();

  Future<State> login({LoginRequest loginRequest}) =>
      UserApiProvider().loginCall(loginRequest);

  Future<State> getBasicUserDetails() =>
      UserApiProvider().getBasicUserDetails();

  Future<State> getUserInvitationType() =>
      UserApiProvider().getUserInvitationType();

  Future<State> updateUserType(
          UpdateUserTypeRequestModel updateUserTypeRequestModel) =>
      UserApiProvider().updateUserType(updateUserTypeRequestModel);

  Future<State> addParent(AddParentRequestModel addParentRequestModel) =>
      UserApiProvider().addParent(addParentRequestModel);

  Future<State> uploadImage(UploadImageRequest uploadImageRequest) =>
      UserApiProvider().uploadImage(uploadImageRequest);

  Future<State> getSports() => UserApiProvider().getSports();

  Future<State> addEmergencyContact(
          AddEmergencyContactRequestModel addEmergencyContactRequestModel) =>
      UserApiProvider().addEmergencyContact(addEmergencyContactRequestModel);

  Future<State> addAuthorizedContact(
          AddAuthorizedContactRequestModel addAuthorizedContactRequestModel) =>
      UserApiProvider().addAuthorizedContact(addAuthorizedContactRequestModel);

  Future<State> addAthlete(AddAthleteRequestModel addAthleteRequestModel) =>
      UserApiProvider().addAthlete(addAthleteRequestModel);

  Future<State> addFacility(AddFacilityRequest addFacilityRequest) =>
      UserApiProvider().addFacility(addFacilityRequest);

  Future<State> getFacility() => UserApiProvider().getFacility();

  Future<State> getFacilityType() => UserApiProvider().getFacilityType();

  Future<State> getAthleteList() => UserApiProvider().getAthleteList();

  Future<State> getProfile() => UserApiProvider().getProfile();

  Future<State> deleteAthlete(int id) => UserApiProvider().deleteAthlete(id);

  Future<State> updateSports(UpdateSportsRequest updateSportsRequest) =>
      UserApiProvider().updateSports(updateSportsRequest);

  Future<State> getAgeRange() => UserApiProvider().getAgeRange();

  Future<State> getSessionIncrement() =>
      UserApiProvider().getSessionIncrement();

  Future<State> uploadVideo(UploadVideoRequest uploadVideoRequest) =>
      UserApiProvider().uploadVideo(uploadVideoRequest);

  Future<State> getLevelOfEducation() =>
      UserApiProvider().getLevelOfEducation();

  Future<State> getRateByView() => UserApiProvider().getRateByView();

  Future<State> addCoach(AddCoachRequest addCoachRequest) =>
      UserApiProvider().addCoach(addCoachRequest);

  Future<State> getLevelOfPlay() => UserApiProvider().getLevelOfPlay();

  Future<State> addReference(AddReferenceRequest addReferenceRequest) =>
      UserApiProvider().addReference(addReferenceRequest);

  Future<State> getStates() => UserApiProvider().getStates();

  Future<State> addBankingInfo(AddBankingInfoRequest addBankingInfoRequest) =>
      UserApiProvider().addBankingInfo(addBankingInfoRequest);

  Future<State> getPaymentMethods() => UserApiProvider().getPaymentMethods();

  Future<State> getLeaseRates() => UserApiProvider().getLeaseRates();

  Future<State> getBanks() => UserApiProvider().getBanks();

  Future<State> addFacilityRate(
          AddFacilityRateRequest addFacilityRateRequest) =>
      UserApiProvider().addFacilityRate(addFacilityRateRequest);

  Future<State> addBusinessBankInfo(
          AddBusinessBankingInfo addBusinessBankingInfo) =>
      UserApiProvider().addBusinessBankInfo(addBusinessBankingInfo);

  Future<State> getTrainerList() => UserApiProvider().getTrainerList();

  Future<State> getTimeSlots(GetTimeSlotsRequest getTimeSlotsRequest) =>
      UserApiProvider().getTimeSlots(getTimeSlotsRequest);

  Future<State> bookZoomMeeting(
          BookZoomMeetingRequest bookZoomMeetingRequest) =>
      UserApiProvider().bookZoomMeeting(bookZoomMeetingRequest);

  Future<State> addOnBoardingStatus() =>
      UserApiProvider().addOnBoardingStatus();

  Future<State> getBillingAddress() => UserApiProvider().getBillingAddress();
}
