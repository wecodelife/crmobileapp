import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/ui/coach/widgets/coachBottomNavigationBar.dart';
import 'package:app_template/src/ui/screens/edit_profile_page.dart';
import 'package:app_template/src/ui/screens/profile_doc_1.dart';
import 'package:app_template/src/ui/screens/profile_money.dart';
import 'package:app_template/src/ui/widgets/profile_page_information_tile.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CoachProfilePage extends StatefulWidget {
  String sessionName;
  @override
  _CoachProfilePageState createState() => _CoachProfilePageState();
}

class _CoachProfilePageState extends State<CoachProfilePage> {
  UserBloc userBloc = new UserBloc();
  @override
  void initState() {
    userBloc.getProfile();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[6],
      bottomNavigationBar: CoachBottomNavigationWidget(),
      body: ListView(
        children: [
          Container(
            color: Colors.black,
            height: screenHeight(context, dividedBy: 4.3),
            width: screenWidth(context, dividedBy: 1),
            child: Column(
              children: [
                Spacer(
                  flex: 3,
                ),
                Text(
                  "PROFILE",
                  style: TextStyle(
                      color: Constants.kitGradients[1],
                      fontSize: 17,
                      fontFamily: 'OswaldBold'),
                ),
                Spacer(
                  flex: 1,
                ),
                Row(
                  children: [
                    Spacer(
                      flex: 2,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Container(
                        child: CircleAvatar(
                          radius: screenWidth(context, dividedBy: 10),
                          backgroundImage: NetworkImage(
                            "https://in.pinterest.com/pin/762375043151042725/",
                          ),
                        ),
                      ),
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Greth Smnith",
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                              fontFamily: 'OpenSansSemiBold'),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 150),
                        ),
                        Text(
                          "SanFransisco",
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontFamily: 'OpenSansRegular'),
                        ),
                      ],
                    ),
                    Spacer(
                      flex: 10,
                    )
                  ],
                ),
                Spacer(
                  flex: 1,
                ),
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30)),
            child: Container(
              height: screenHeight(context, dividedBy: 7),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Constants.kitGradients[5],
              ),
              child: Column(
                children: [
                  Spacer(
                    flex: 1,
                  ),
                  ProfilePageInformationTile(
                    icon: "assets/icons/profile_icon.svg",
                    title: "Information",
                    onPressed: () {
                      push(context, EditProfile());
                    },
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  ProfilePageInformationTile(
                    icon: "assets/icons/preferences_icon.svg",
                    title: "Preferences",
                    onPressed: () {
                      // push(context, UserList());
                    },
                  ),
                  Spacer(
                    flex: 1,
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30)),
            child: Container(
                height: screenHeight(context, dividedBy: 2.3),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Constants.kitGradients[5],
                ),
                child: Column(
                  children: [
                    Spacer(
                      flex: 2,
                    ),
                    ProfilePageInformationTile(
                      icon: "assets/icons/facilities_icon.svg",
                      title: "Facilities",
                      onPressed: () {
                        //push(context, ProfileMoney());
                      },
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    ProfilePageInformationTile(
                      icon: "assets/icons/marketplace_icon.svg",
                      title: "Marketplace",
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    ProfilePageInformationTile(
                      icon: "assets/icons/credentials_icon.svg",
                      title: "Credentials",
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    ProfilePageInformationTile(
                      icon: "assets/icons/money_icon.svg",
                      title: "Money",
                      onPressed: () {
                        push(context, ProfileMoney());
                      },
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    ProfilePageInformationTile(
                      icon: "assets/icons/invites_icon.svg",
                      title: "Invites",
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    ProfilePageInformationTile(
                      icon: "assets/icons/document_icon.svg",
                      title: "Documents",
                      onPressed: () {
                        push(context, ProfileDoc1());
                      },
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    ProfilePageInformationTile(
                      icon: "assets/icons/logout_icon.svg",
                      title: "Lost&Found",
                    ),
                    Spacer(
                      flex: 1,
                    ),
                  ],
                )),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30)),
            child: Container(
              height: screenHeight(context, dividedBy: 12),
              decoration: BoxDecoration(
                color: Constants.kitGradients[5],
                borderRadius: BorderRadius.circular(10),
              ),
              child: Center(
                child: ProfilePageInformationTile(
                  icon: "assets/icons/facebook.svg",
                  title: "LogOut",
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
