import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AmenitiesSelectionGridTile extends StatefulWidget {
  final String bgImage;
  final ValueChanged onValueChanged;
  final String title;
  AmenitiesSelectionGridTile({this.onValueChanged, this.bgImage, this.title});
  @override
  _AmenitiesSelectionGridTileState createState() =>
      _AmenitiesSelectionGridTileState();
}

class _AmenitiesSelectionGridTileState
    extends State<AmenitiesSelectionGridTile> {
  bool click;
  @override
  Widget build(BuildContext context) {
    return GridTile(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            SvgPicture.asset(
              widget.bgImage,
              height: screenHeight(context, dividedBy: 20),
              width: screenWidth(context, dividedBy: 10),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 200),
            ),
            Container(
              width: screenWidth(context, dividedBy: 3.1),
              child: Center(
                child: Text(
                  widget.title,
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'OpenSansBold',
                      fontSize: 17),
                  overflow: TextOverflow.visible,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
