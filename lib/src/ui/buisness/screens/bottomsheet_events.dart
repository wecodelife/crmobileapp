import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/event_date_fields.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/ui/widgets/time_slot_app_bar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class EventsBottomSheet extends StatefulWidget {
  Function onPressed;
  EventsBottomSheet({this.onPressed});
  @override
  _EventsBottomSheetState createState() => _EventsBottomSheetState();
}

class _EventsBottomSheetState extends State<EventsBottomSheet> {
  TextEditingController titleTextEditingController =
      new TextEditingController();
  TextEditingController contactTextEditingController =
      new TextEditingController();
  TextEditingController phoneTextEditingController =
      new TextEditingController();
  TextEditingController emailTextEditingController =
      new TextEditingController();
  TextEditingController locationTextEditingController =
      new TextEditingController();

  FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: screenWidth(context, dividedBy: 1),
        //height: screenHeight(context, dividedBy: 1),
        color: Constants.kitGradients[3],
        child: Column(
          children: [
            AppBarTimeSlot(
              pageTitle: "EVENT",
              pageNavigation: () {},
              rightIcon: true,
              icon: false,
              onTapRightIcon: () {
                pop(context);
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 100),
            ),
            Container(
              color: Constants.kitGradients[11],
              padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30),
              ),
              child: Column(
                children: [
                  FormFeildUserDetails(
                    textEditingController: titleTextEditingController,
                    labelText: "Title",
                  ),
                  FormFeildUserDetails(
                    textEditingController: titleTextEditingController,
                    labelText: "Contact",
                  ),
                  FormFeildUserDetails(
                    textEditingController: titleTextEditingController,
                    labelText: "Phone",
                  ),
                  FormFeildUserDetails(
                    textEditingController: titleTextEditingController,
                    labelText: "Email",
                  ),
                  FormFeildUserDetails(
                    textEditingController: titleTextEditingController,
                    labelText: "Location",
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            Container(
              color: Constants.kitGradients[11],
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 30),
                  vertical: screenHeight(context, dividedBy: 40)),
              child: Column(
                children: [
                  EventDateField(
                    label: "Date",
                    data: "12/11/2020",
                  ),
                  EventDateField(
                    label: "Start Time",
                    data: "5:30PM",
                  ),
                  EventDateField(
                    label: "End Time",
                    data: "6:30PM",
                  ),
                ],
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            BuildButton(
              title: "SUBMIT",
              disabled: false,
              onPressed: () {
                widget.onPressed();
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
          ],
        ),
      ),
    );
  }
}
