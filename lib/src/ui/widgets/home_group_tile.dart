import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeGroupTile extends StatefulWidget {
  @override
  _HomeGroupTileState createState() => _HomeGroupTileState();
}

class _HomeGroupTileState extends State<HomeGroupTile> {
  bool rated = false;
  @override
  Widget build(BuildContext context) {
    return Card(
      color: Constants.kitGradients[5],
      child: Container(
        width: screenWidth(context, dividedBy: 1),
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: 8.0,
          ),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 70),
                  ),
                  ClipRRect(
                    child: Container(
                      width: screenWidth(context, dividedBy: 4.5),
                      height: screenHeight(context, dividedBy: 7),
                      decoration: BoxDecoration(
                          color: Colors.red,
                          image: DecorationImage(
                              image: AssetImage("assets/images/dummy_bg.jpg"),
                              fit: BoxFit.fill)),
                      child: Image.network(
                        "https://image.shutterstock.com/image-photo/business-woman-drawing-global"
                        "-structure-260nw-1006041130.jpg",
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 60),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: screenWidth(context, dividedBy: 1.6),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                                width: screenWidth(context, dividedBy: 2),
                                child: Text(
                                  "Training Group Name",
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontFamily: 'OpenSansRegular',
                                      color: Colors.white),
                                )),
                            Spacer(
                              flex: 1,
                            ),
                            Container(
                                width: screenWidth(context, dividedBy: 19),
                                height: screenHeight(context, dividedBy: 40),
                                child: SvgPicture.asset(
                                    "assets/icons/arrow_button.svg"))
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 2.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.location_on_outlined,
                            size: 16,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: 2,
                          ),
                          Container(
                            width: screenWidth(context, dividedBy: 2),
                            child: Text("76A Eighth Avenue, New York",
                                style: TextStyle(
                                    fontSize: 13,
                                    fontFamily: 'OpenSansRegular',
                                    color: Colors.white),
                                overflow: TextOverflow.ellipsis),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 120),
                      ),
                      Row(
                        children: [
                          GestureDetector(
                              onTap: () {
                                rated = !rated;
                              },
                              child: rated == true
                                  ? Icon(
                                      Icons.star_rate_rounded,
                                      size: 16,
                                      color: Constants.kitGradients[1],
                                    )
                                  : Icon(
                                      Icons.star_rate_rounded,
                                      color: Constants.kitGradients[1],
                                      size: 18,
                                    )),
                          Container(
                            width: screenWidth(context, dividedBy: 18),
                            child: Text("4.7",
                                style: TextStyle(
                                    fontSize: 13,
                                    fontFamily: 'OpenSansRegular',
                                    color: Colors.white),
                                overflow: TextOverflow.ellipsis),
                          ),
                          SizedBox(
                            width: screenWidth(context, dividedBy: 100),
                          ),
                          Container(
                            width: 2,
                            height: 17,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: screenWidth(context, dividedBy: 80),
                          ),
                          rated == true
                              ? GestureDetector(
                                  onTap: () {
                                    rated = !rated;
                                  },
                                  child: Image.asset(
                                    "assets/icons/people_icon.png",
                                    color: Constants.kitGradients[0],
                                  ))
                              : Image.asset(
                                  "assets/icons/people_icon.png",
                                  color: Constants.kitGradients[1],
                                ),
                          SizedBox(
                            width: screenWidth(context, dividedBy: 50),
                          ),
                          Container(
                            width: screenWidth(context, dividedBy: 15),
                            child: Text("4.7",
                                style: TextStyle(
                                    fontSize: 13,
                                    fontFamily: 'OpenSansRegular',
                                    color: Colors.white),
                                overflow: TextOverflow.ellipsis),
                          ),
                          SizedBox(
                            width: screenWidth(context, dividedBy: 120),
                          ),
                          Container(
                            width: screenWidth(context, dividedBy: 9),
                            child: Text("30" + "Min",
                                style: TextStyle(
                                    fontSize: 13,
                                    fontFamily: 'OpenSansRegular',
                                    color: Colors.white),
                                overflow: TextOverflow.ellipsis),
                          ),
                          Icon(
                            Icons.circle,
                            size: 8,
                            color: Colors.red,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Container(
                            width: screenWidth(context, dividedBy: 9),
                            child: Text("0.4 " + "Km",
                                style: TextStyle(
                                    fontSize: 13,
                                    fontFamily: 'OpenSansRegular',
                                    color: Colors.white),
                                overflow: TextOverflow.ellipsis),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 120),
                      ),
                      Container(
                        width: screenWidth(context, dividedBy: 1.6),
                        child: Row(
                          children: [
                            SizedBox(
                              width: 5,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(2)),
                                  border: Border.all(
                                      color: Constants.kitGradients[1])),
                              width: screenWidth(context, dividedBy: 7),
                              child: Center(
                                child: Text("MONDAY 3PM",
                                    style: TextStyle(
                                        fontSize: 7,
                                        fontFamily: 'OpenSansRegular',
                                        color: Colors.white),
                                    overflow: TextOverflow.ellipsis),
                              ),
                            ),
                            Spacer(
                              flex: 1,
                            ),
                            Icon(
                              Icons.phone_outlined,
                              size: 20,
                              color: Constants.kitGradients[1],
                            ),
                            SizedBox(
                              width: screenWidth(context, dividedBy: 25),
                            ),
                            Icon(
                              Icons.mail_outline,
                              size: 20,
                              color: Constants.kitGradients[1],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 100),
              ),
              Row(
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 70),
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 2.3),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
