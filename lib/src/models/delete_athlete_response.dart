// To parse this JSON data, do
//
//     final deleteAthleteResponse = deleteAthleteResponseFromJson(jsonString);

import 'dart:convert';

DeleteAthleteResponse deleteAthleteResponseFromJson(String str) =>
    DeleteAthleteResponse.fromJson(json.decode(str));

String deleteAthleteResponseToJson(DeleteAthleteResponse data) =>
    json.encode(data.toJson());

class DeleteAthleteResponse {
  DeleteAthleteResponse({
    this.message,
    this.status,
  });

  final String message;
  final int status;

  factory DeleteAthleteResponse.fromJson(Map<String, dynamic> json) =>
      DeleteAthleteResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
      };
}
