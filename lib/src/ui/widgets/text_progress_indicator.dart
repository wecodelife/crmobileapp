import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class TextProgressIndicator extends StatefulWidget {
  final String descriptionText;
  final String heading;
  TextProgressIndicator({this.descriptionText, this.heading});
  @override
  _TextProgressIndicatorState createState() => _TextProgressIndicatorState();
}

class _TextProgressIndicatorState extends State<TextProgressIndicator> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: screenWidth(context, dividedBy: 1.2),
          child: Center(
            child: Text(
              widget.heading,
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'OswaldBold',
                fontSize: screenWidth(context, dividedBy: 27),
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 60),
        ),
        Container(
          width: screenWidth(context, dividedBy: 1.3),
          child: Text(
            widget.descriptionText,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSansSemiBold',
              fontSize: screenWidth(context, dividedBy: 30),
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }
}
