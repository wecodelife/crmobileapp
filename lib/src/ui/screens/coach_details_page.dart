import 'package:app_template/material/pickers/date_range_picker_dialog.dart';
import 'package:app_template/src/models/get_trainer_list_response.dart';
import 'package:app_template/src/ui/widgets/bottom_navigation_bar_widget.dart';
import 'package:app_template/src/ui/widgets/bottom_sheet_coach_detail_page.dart';
import 'package:app_template/src/ui/widgets/coach_details_cupertino_tile.dart';
import 'package:app_template/src/ui/widgets/coach_profile_name_widget.dart';
import 'package:app_template/src/ui/widgets/coach_profile_rating_widget.dart';
import 'package:app_template/src/ui/widgets/request_zoom_meeting_button.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

class CoachDetailsPage extends StatefulWidget {
  final String imageName;
  final String name;
  final List<TrainerSport> sports;
  CoachDetailsPage({this.imageName, this.name, this.sports});
  @override
  _CoachDetailsPageState createState() => _CoachDetailsPageState();
}

class _CoachDetailsPageState extends State<CoachDetailsPage> {
  List<String> imgList = [
    "assets/icons/facility_bg_image.png",
    "assets/icons/facility_bg_image.png",
    "assets/icons/facility_bg_image.png"
  ];
  int _current = 0;
  double position;
  bool rating = false;
  int reviewCurrentIndex = 0;
  double reviewPosition;
  bool calendar = false;
  bool slotSelection = false;
  bool timeSlots = false;
  bool showSummary = false;
  bool selected = false;
  int itemCount = 6;

  FToast fToast;
  Future<Null> selectDateRange(BuildContext context) async {
    DateTimeRange picked = await showDatesRangePicker(
        sports: widget.sports,
        pageValue: "Coach Details",
        context: context,
        imageName: widget.imageName,
        name: widget.name,
        // initialDateRange: DateTimeRange(
        //   start: DateTime.now().add(Duration(days: 0)),
        //   end: DateTime.now().add(Duration(days: 1)),
        // ),
        firstDate: DateTime.now(),
        lastDate: DateTime(DateTime.now().year + 5),
        helpText: "Select_Date_Range",
        cancelText: "CANCEL",
        confirmText: "OK",
        saveText: "SAVE",
        errorFormatText: "Invalid_format",
        errorInvalidText: "Out_of_range",
        errorInvalidRangeText: "Invalid_range",
        fieldStartHintText: "Start_Date",
        fieldEndLabelText: "End_Date");

    // if (picked != null) {
    //   state.didChange(picked);
    // }
  }

  @override
  void initState() {
    fToast = FToast();
    fToast.init(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DotsDecorator(
        activeColor: Colors.red.withOpacity(.50),
        color: Colors.white.withOpacity(.50),
        shape: CircleBorder(),
        size: Size(10, 10));
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      ),
      child: Scaffold(
          bottomNavigationBar: BottomNavigationWidget(),
          floatingActionButton: Padding(
            padding:
                EdgeInsets.only(bottom: screenHeight(context, dividedBy: 14)),
            child: RequestZoomMeeting(
              onPressed: () {
                selectDateRange(context);
                // displayBottomSheet(context);
              },
            ),
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          body: ListView(
            children: [
              Stack(
                children: [
                  // CarouselSlider(
                  //   options: CarouselOptions(
                  //       aspectRatio: screenWidth(context, dividedBy: 1) /
                  //           screenHeight(context, dividedBy: 2.4),
                  //       viewportFraction: 1,
                  //       onPageChanged: (index, reason) {
                  //         setState(() {
                  //           _current = index;
                  //           print(_current);
                  //         });
                  //       }),
                  //   items: imgList.map((i) {
                  //     return Builder(
                  //       builder: (
                  //         BuildContext context,
                  //       ) {
                  //         return Stack(
                  //           children: [
                  //             CoachDetailsCupertinoTile(
                  //               imageName: i,
                  //               iconName: "assets/icons/notification_icon.svg",
                  //               icon1: true,
                  //               iconName1: "assets/icons/close_icon.svg",
                  //               sizedBoxWidth: 1.35,
                  //               coachCurrentIndex: _current,
                  //             ),
                  //           ],
                  //         );
                  //       },
                  //     );
                  //   }).toList(),
                  // ),
                  CoachDetailsCupertinoTile(
                    imageName: widget.imageName,
                    iconName: "assets/icons/notification_icon.svg",
                    icon1: true,
                    iconName1: "assets/icons/close_icon.svg",
                    sizedBoxWidth: 1.35,
                    coachCurrentIndex: _current,
                  ),
                  Column(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 2.8),
                      ),
                      Container(
                          width: screenWidth(context, dividedBy: 1),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Constants.kitGradients[6],
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                        screenWidth(context, dividedBy: 30)),
                                child: Row(
                                  children: [
                                    CoachProfileNameWidget(
                                      sports: widget.sports,
                                      coachName: widget.name,
                                      // "Coach Name",
                                      width: 2,
                                      image:
                                          "assets/icons/basket_ball_icon.svg",
                                      subHeading: "Soccer",
                                    ),
                                    Column(
                                      children: [
                                        SizedBox(
                                          height: screenHeight(context,
                                              dividedBy: 12),
                                        ),
                                        Row(
                                          children: [
                                            SizedBox(
                                              width: screenWidth(context,
                                                  dividedBy: 4.9),
                                            ),
                                            Icon(
                                              Icons.phone_outlined,
                                              size: 25,
                                              color: Constants.kitGradients[1],
                                            ),
                                            SizedBox(
                                              width: screenWidth(context,
                                                  dividedBy: 30),
                                            ),
                                            Icon(
                                              Icons.mail_outline,
                                              color: Constants.kitGradients[1],
                                              size: 25,
                                            )
                                          ],
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 60),
                              ),
                              Container(
                                color: Constants.kitGradients[5],
                                width: screenWidth(context, dividedBy: 1),
                                height: screenHeight(context, dividedBy: 9),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    CoachProfileRating(
                                      heading: "4.5",
                                      subHeading: "351  Ratings",
                                      rating: rating,
                                      iconName: Icons.star_rounded,
                                      ratingIcon: true,
                                      width: 4,
                                      divider: true,
                                      onPressed: () {
                                        setState(() {
                                          rating = !rating;
                                          print(rating);
                                        });
                                      },
                                    ),
                                    CoachProfileRating(
                                      heading: "137k",
                                      subHeading: "BookMarks",
                                      rating: false,
                                      ratingIcon: true,
                                      iconName: Icons.bookmark_border,
                                      width: 4,
                                      divider: true,
                                      onPressed: () {
                                        setState(() {
                                          rating = !rating;
                                          print(rating);
                                        });
                                      },
                                    ),
                                    CoachProfileRating(
                                      heading: "4.5",
                                      subHeading: "351  Photos",
                                      iconName: Icons.photo_size_select_actual,
                                      rating: false,
                                      ratingIcon: true,
                                      width: 4,
                                      onPressed: () {},
                                    ),
                                  ],
                                ),
                              ),
                              // Padding(
                              //   padding: EdgeInsets.symmetric(
                              //       horizontal:
                              //           screenWidth(context, dividedBy: 20)),
                              //   child: Column(
                              //     children: [
                              //       SizedBox(
                              //         height:
                              //             screenHeight(context, dividedBy: 30),
                              //       ),
                              //       Row(
                              //         children: [
                              //           Text(
                              //             "EXPERIENCE",
                              //             style: TextStyle(
                              //                 fontSize: 20,
                              //                 fontFamily: 'OswaldBold',
                              //                 color: Constants.kitGradients[1]),
                              //           ),
                              //         ],
                              //       ),
                              //       SizedBox(
                              //         height:
                              //             screenHeight(context, dividedBy: 80),
                              //       ),
                              //       Text(
                              //         "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh"
                              //         " euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim",
                              //         style: TextStyle(
                              //             fontSize: 17,
                              //             fontFamily: 'AvenirBook',
                              //             color: Colors.white),
                              //       ),
                              //       SizedBox(
                              //         height:
                              //             screenHeight(context, dividedBy: 30),
                              //       ),
                              //       Row(
                              //         children: [
                              //           Text(
                              //             "ASSOCIATION",
                              //             style: TextStyle(
                              //                 fontSize: 20,
                              //                 fontFamily: 'OswaldBold',
                              //                 color: Constants.kitGradients[1]),
                              //           ),
                              //         ],
                              //       ),
                              //       SizedBox(
                              //         height:
                              //             screenHeight(context, dividedBy: 80),
                              //       ),
                              //       CoachProfileAssociation(
                              //         heading: "Plex Paul Adult League",
                              //         subHeading:
                              //             "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod",
                              //         imageName:
                              //             "assets/images/league_image.svg",
                              //       ),
                              //       HeadingWidget(
                              //         heading: "REVIEWS",
                              //       ),
                              //       CarouselSlider(
                              //         options: CarouselOptions(
                              //             aspectRatio: screenWidth(context,
                              //                     dividedBy: 1) /
                              //                 screenHeight(context,
                              //                     dividedBy: 2.4),
                              //             viewportFraction: 1,
                              //             onPageChanged: (index, reason) {
                              //               setState(() {
                              //                 reviewCurrentIndex = index;
                              //                 print(_current);
                              //               });
                              //             }),
                              //         items: imgList.map((i) {
                              //           return Builder(
                              //             builder: (
                              //               BuildContext context,
                              //             ) {
                              //               return Stack(
                              //                 children: [
                              //                   ReviewCupertinoTile(
                              //                       heading:
                              //                           "“Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt u"
                              //                           "t laoreet dolore magna aliquam erat volutpat. Ut wisi “",
                              //                       imageName:
                              //                           "https://i.pinimg.com/474x/bc/d4/ac/bcd4ac32cc7d3f98b5e54bde37d6b09e.jpg",
                              //                       radius: 20,
                              //                       textPadding: 70,
                              //                       userName: "DanSmith",
                              //                       fontSizeHeading: 15,
                              //                       fontSizePhoneNumber: 15,
                              //                       imagePadding: 100,
                              //                       phoneNumber: "6578908"),
                              //                   Positioned(
                              //                     top: screenHeight(context,
                              //                         dividedBy: 4),
                              //                     left: screenWidth(context,
                              //                         dividedBy: 2.5),
                              //                     child: Row(
                              //                       children: [
                              //                         DotsIndicator(
                              //                           dotsCount: 3,
                              //                           position:
                              //                               reviewCurrentIndex
                              //                                   .toDouble(),
                              //                           axis: Axis.horizontal,
                              //                           decorator: DotsDecorator(
                              //                               activeColor: Constants
                              //                                       .kitGradients[
                              //                                   1],
                              //                               color: Colors.white
                              //                                   .withOpacity(
                              //                                       .40),
                              //                               shape:
                              //                                   CircleBorder(),
                              //                               size: Size(10, 10)),
                              //                         ),
                              //                       ],
                              //                     ),
                              //                   )
                              //                 ],
                              //               );
                              //             },
                              //           );
                              //         }).toList(),
                              //       ),
                              //     ],
                              //   ),
                              // ),
                            ],
                          )),
                    ],
                  ),
                ],
              ),
            ],
          )),
    );
  }

  void displayBottomSheet(context) {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: true,
      context: context,
      backgroundColor: Constants.kitGradients[5],
      builder: (BuildContext context) {
        return BottomSheetCoachDetailPage();
      },
    );
  }

  _showToast() {
    Widget toast = Container(
        height: screenHeight(context, dividedBy: 2),
        width: screenWidth(context, dividedBy: 1.2),
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(30)),
        child: Column(
          children: [
            Container(
              height: screenHeight(context, dividedBy: 10),
              decoration: BoxDecoration(
                  color: Constants.kitGradients[1],
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: Center(
                  child: Icon(
                Icons.check_circle_outline,
                size: 40,
                color: Colors.white,
              )),
            ),
            Container(
              decoration: BoxDecoration(
                  color: Constants.kitGradients[5],
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(20),
                      bottomRight: Radius.circular(20))),
              height: screenHeight(context, dividedBy: 10),
              child: Center(
                child: Text(
                  "Appointment has been Created",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontFamily: 'OpenSansSemiBold'),
                ),
              ),
            )
          ],
        ));
    fToast.showToast(
      child: toast,
      gravity: ToastGravity.SNACKBAR,
      toastDuration: Duration(seconds: 2),
    );
  }
}
