import 'package:app_template/src/ui/screens/on_boarding_coach_8.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbar_manager/flutter_statusbar_manager.dart';

class OnBoarding4 extends StatefulWidget {
  @override
  _OnBoarding4State createState() => _OnBoarding4State();
}

class _OnBoarding4State extends State<OnBoarding4> {
  @override
  void initState() {
    statusBarColor();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: AppBar(
        leading: Container(),
        actions: [
          OnBoardingAppBar(),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: screenHeight(context, dividedBy: 4.0),
          ),
          GestureDetector(
            onTap: () {
              push(context, OnBoarding8());
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: screenWidth(context, dividedBy: 1.1),
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Constants.kitGradients[1], width: 1.0)),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 40),
                        ),
                        Text(
                          'GREAT, WELCOME TO COACH AND REFS!',
                          style: TextStyle(
                              fontFamily: 'OswaldBold',
                              fontSize: 14.0,
                              color: Theme.of(context).accentColor,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 50),
                        ),
                        Text(
                          'Lets get started by asking a few questions to',
                          overflow: TextOverflow.visible,
                          style: TextStyle(
                              fontFamily: 'OpenSansRegular',
                              fontSize: 14.0,
                              color: Theme.of(context).accentColor),
                        ),
                        Text(
                          'get to know you.',
                          overflow: TextOverflow.visible,
                          style: TextStyle(
                              fontFamily: 'OpenSansRegular',
                              fontSize: 14.0,
                              color: Theme.of(context).accentColor),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 40),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Future<void> statusBarColor() async {
    await FlutterStatusbarManager.setColor(Constants.kitGradients[3]);
    await FlutterStatusbarManager.setStyle(StatusBarStyle.LIGHT_CONTENT);
  }
}
