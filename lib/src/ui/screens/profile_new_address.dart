import 'package:app_template/src/ui/widgets/appbar_cr.dart';
import 'package:app_template/src/ui/widgets/bottom_navigation_bar_widget.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/check_box.dart';
import 'package:app_template/src/ui/widgets/drop_down_cr_app.dart';
import 'package:app_template/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfileNewAddress extends StatefulWidget {
  @override
  _ProfileNewAddressState createState() => _ProfileNewAddressState();
}

class _ProfileNewAddressState extends State<ProfileNewAddress> {
  TextEditingController nameTextEditingController = new TextEditingController();
  TextEditingController phoneNumberTextEditingController =
      new TextEditingController();
  TextEditingController addressTextEditingController =
      new TextEditingController();
  TextEditingController unitTextEditingController = new TextEditingController();
  TextEditingController cityTextEditingController = new TextEditingController();
  TextEditingController stateTextEditingController =
      new TextEditingController();
  TextEditingController zipTextEditingController = new TextEditingController();
  List<String> dropDownList = ["PA"];
  String dropDownValue;
  bool setDefault = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: Container(),
        actions: [
          AppBarCr(
            leftIcon: "assets/icons/back_button.svg",
            pageTitle: "NEW ADDRESS",
            noRightIcon: true,
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationWidget(),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 13)),
        child: Column(
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 80),
            ),
            FormFeildUserDetails(
              labelText: "Name",
              textEditingController: nameTextEditingController,
            ),
            FormFeildUserDetails(
              labelText: "PhoneNumber",
              textEditingController: phoneNumberTextEditingController,
            ),
            FormFeildUserDetails(
              labelText: "Address",
              textEditingController: addressTextEditingController,
            ),
            FormFeildUserDetails(
              labelText: "Apt,Suite,Unit,Building",
              textEditingController: unitTextEditingController,
            ),
            FormFeildUserDetails(
              labelText: "city",
              textEditingController: cityTextEditingController,
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 80),
            ),
            Row(
              children: [
                Text(
                  "State",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                      fontFamily: 'OpenSansRegular'),
                ),
              ],
            ),
            Container(
              width: screenWidth(context, dividedBy: 1.1),
              child: DropDownCrApp(
                title: "Select",
                onClicked: (value) {
                  setState(() {
                    dropDownValue = value;
                  });
                },
                dropDownList: dropDownList,
                dropDownValue: dropDownValue,
              ),
            ),
            FormFeildUserDetails(
              textEditingController: zipTextEditingController,
              labelText: "ZIP",
            ),
            Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                Row(
                  children: [
                    CheckBox(
                      onTap: () {
                        setState(() {
                          setDefault = !setDefault;
                        });
                      },
                      checked: setDefault,
                    ),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 20),
                    ),
                    Text(
                      "Make Default",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                          fontFamily: 'OpenSansSemiBold'),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 80),
            ),
            BuildButton(
              title: "Add Address",
              onPressed: () {},
              disabled: false,
            )
          ],
        ),
      ),
    );
  }
}
