import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ListTileAddSports extends StatefulWidget {
  final String image;
  final String label;
  final bool selected;
  final Function onPressed;
  ListTileAddSports({this.image, this.label, this.onPressed, this.selected});
  @override
  _ListTileAddSportsState createState() => _ListTileAddSportsState();
}

class _ListTileAddSportsState extends State<ListTileAddSports> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 60)),
      child: GestureDetector(
        onTap: () {
          widget.onPressed();
        },
        child: Container(
            height: screenHeight(context, dividedBy: 8),
            width: screenWidth(context, dividedBy: 5),
            color: Constants.kitGradients[1],
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SvgPicture.string(
                  widget.image,
                  color: Colors.white,
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                Text(
                  widget.label,
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'OpenSansRegular',
                  ),
                )
              ],
            )),
      ),
    );
  }
}
