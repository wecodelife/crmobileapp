import 'dart:io';

import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/upload_image_request_model.dart';
import 'package:app_template/src/models/upload_image_response_model.dart';
import 'package:app_template/src/ui/widgets/Gallery.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class GalleryScreen extends StatefulWidget {
  final ValueChanged<File> imageSelected;
  final String imageKey;

  GalleryScreen({this.imageSelected, this.imageKey});
  @override
  _GalleryScreenState createState() => _GalleryScreenState();
}

class _GalleryScreenState extends State<GalleryScreen> {
  Future addPhoto(UploadImageResponse event) async {
    await ObjectFactory()
        .appHive
        .putImagePath(key: widget.imageKey, value: event.data.imageUrl);
    await ObjectFactory().appHive.putImageId(
        key: "${widget.imageKey}id", value: event.data.id.toString());
  }

  File selectedImage;
  bool loading = false;
  UserBloc userBloc = UserBloc();
  @override
  void initState() {
    userBloc.uploadImageResponse.listen((event) async {
      setState(() {
        loading = false;
      });
      if (event.status == 200 || event.status == 201) {
        await addPhoto(event);
        // print("aada"+ObjectFactory().appHive.getImageId("${widget.imageKey}id"));
        // if(widget.imageKey=="parentImage")
        // push(
        //     context,
        //     OnBoardingParentAthlete6(
        //       imageId: event.data.id,
        //     ));
        // if(widget.imageKey.contains("athlete"))
        //   push(
        //       context,
        //       OnBoardingParentAthlete11(
        //         imageId: event.data.id,
        //         parentId: int.parse(ObjectFactory().appHive.getParentId()),
        //       ));
        pop(context);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: loading == true
          ? Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : Gallery(
              title: Container(
                height: 40,
                color: Colors.black.withOpacity(0.7),
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'CAMERA ROLL',
                          style: TextStyle(
                              color: Constants.kitGradients[1],
                              fontFamily: 'OswaldBold',
                              fontSize: 16),
                        )),
                  ],
                ),
              ),
              image: (value) {
                setState(() {
                  selectedImage = value;
                });
              },
              footer: Container(
                height: 80,
                color: Colors.black.withOpacity(0.7),
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                    child: Column(
                      //crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        BuildButton(
                            title: "NEXT",
                            onPressed: () {
                              setState(() {
                                loading = true;
                              });
                              userBloc.uploadImage(
                                  uploadImageRequest:
                                      UploadImageRequest(image: selectedImage));
                              // pop(context);
                            }),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }
}
