// To parse this JSON data, do
//
//     final addBusinessBankingInfoResponse = addBusinessBankingInfoResponseFromJson(jsonString);

import 'dart:convert';

AddBusinessBankingInfoResponse addBusinessBankingInfoResponseFromJson(
        String str) =>
    AddBusinessBankingInfoResponse.fromJson(json.decode(str));

String addBusinessBankingInfoResponseToJson(
        AddBusinessBankingInfoResponse data) =>
    json.encode(data.toJson());

class AddBusinessBankingInfoResponse {
  AddBusinessBankingInfoResponse({
    this.message,
    this.data,
    this.status,
  });

  final String message;
  final Data data;
  final int status;

  factory AddBusinessBankingInfoResponse.fromJson(Map<String, dynamic> json) =>
      AddBusinessBankingInfoResponse(
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "data": data == null ? null : data.toJson(),
        "status": status == null ? null : status,
      };
}

class Data {
  Data({
    this.id,
    this.facility,
    this.paymentMethods,
    this.bank,
    this.routingNumber,
    this.accountNumber,
  });

  final int id;
  final int facility;
  final List<int> paymentMethods;
  final int bank;
  final String routingNumber;
  final String accountNumber;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"] == null ? null : json["id"],
        facility: json["facility"] == null ? null : json["facility"],
        paymentMethods: json["payment_methods"] == null
            ? null
            : List<int>.from(json["payment_methods"].map((x) => x)),
        bank: json["bank"] == null ? null : json["bank"],
        routingNumber:
            json["routing_number"] == null ? null : json["routing_number"],
        accountNumber:
            json["account_number"] == null ? null : json["account_number"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "facility": facility == null ? null : facility,
        "payment_methods": paymentMethods == null
            ? null
            : List<dynamic>.from(paymentMethods.map((x) => x)),
        "bank": bank == null ? null : bank,
        "routing_number": routingNumber == null ? null : routingNumber,
        "account_number": accountNumber == null ? null : accountNumber,
      };
}
