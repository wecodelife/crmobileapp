// To parse this JSON data, do
//
//     final getFacilityResponse = getFacilityResponseFromJson(jsonString);

import 'dart:convert';

GetFacilityResponse getFacilityResponseFromJson(String str) =>
    GetFacilityResponse.fromJson(json.decode(str));

String getFacilityResponseToJson(GetFacilityResponse data) =>
    json.encode(data.toJson());

class GetFacilityResponse {
  GetFacilityResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final List<Datum> data;

  factory GetFacilityResponse.fromJson(Map<String, dynamic> json) =>
      GetFacilityResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.name,
    this.info,
    this.employeeAccess,
    this.address,
    this.facilityType,
    this.lat,
    this.lng,
    this.businessHoursFrom,
    this.businessHoursTo,
    this.images,
    this.sports,
    this.email,
    this.phone,
    this.availableSports,
    this.multipleCourtsRates,
  });

  final int id;
  final String name;
  final String info;
  final bool employeeAccess;
  final String address;
  final FacilityType facilityType;
  final dynamic lat;
  final dynamic lng;
  final String businessHoursFrom;
  final String businessHoursTo;
  final List<Images> images;
  final List<Sports> sports;
  final dynamic email;
  final dynamic phone;
  final List<Sports> availableSports;
  final bool multipleCourtsRates;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        info: json["info"] == null ? null : json["info"],
        employeeAccess:
            json["employee_access"] == null ? null : json["employee_access"],
        address: json["address"] == null ? null : json["address"],
        facilityType: json["facility_type"] == null
            ? null
            : FacilityType.fromJson(json["facility_type"]),
        lat: json["lat"],
        lng: json["lng"],
        businessHoursFrom: json["business_hours_from"] == null
            ? null
            : json["business_hours_from"],
        businessHoursTo: json["business_hours_to"] == null
            ? null
            : json["business_hours_to"],
        images: json["images"] == null
            ? null
            : List<Images>.from(json["images"].map((x) => Images.fromJson(x))),
        sports: json["sports"] == null
            ? null
            : List<Sports>.from(json["sports"].map((x) => Sports.fromJson(x))),
        email: json["email"],
        phone: json["phone"],
        availableSports: json["available_sports"] == null
            ? null
            : List<Sports>.from(
                json["available_sports"].map((x) => Sports.fromJson(x))),
        multipleCourtsRates: json["multiple_courts_rates"] == null
            ? null
            : json["multiple_courts_rates"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "info": info == null ? null : info,
        "employee_access": employeeAccess == null ? null : employeeAccess,
        "address": address == null ? null : address,
        "facility_type": facilityType == null ? null : facilityType.toJson(),
        "lat": lat,
        "lng": lng,
        "business_hours_from":
            businessHoursFrom == null ? null : businessHoursFrom,
        "business_hours_to": businessHoursTo == null ? null : businessHoursTo,
        "images": images == null
            ? null
            : List<dynamic>.from(images.map((x) => x.toJson())),
        "sports": sports == null
            ? null
            : List<dynamic>.from(sports.map((x) => x.toJson())),
        "email": email,
        "phone": phone,
        "available_sports": availableSports == null
            ? null
            : List<dynamic>.from(availableSports.map((x) => x.toJson())),
        "multiple_courts_rates":
            multipleCourtsRates == null ? null : multipleCourtsRates,
      };
}

class Sports {
  Sports({
    this.id,
    this.name,
    this.icon,
    this.iconFile,
    this.description,
  });

  final int id;
  final String name;
  final String icon;
  final dynamic iconFile;
  final String description;

  factory Sports.fromJson(Map<String, dynamic> json) => Sports(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        icon: json["icon"] == null ? null : json["icon"],
        iconFile: json["iconFile"],
        description: json["description"] == null ? null : json["description"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "icon": icon == null ? null : icon,
        "iconFile": iconFile,
        "description": description == null ? null : description,
      };
}

class FacilityType {
  FacilityType({
    this.id,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.name,
    this.description,
    this.order,
  });

  final int id;
  final bool isActive;
  final bool isDeleted;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String name;
  final String description;
  final int order;

  factory FacilityType.fromJson(Map<String, dynamic> json) => FacilityType(
        id: json["id"] == null ? null : json["id"],
        isActive: json["is_active"] == null ? null : json["is_active"],
        isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        name: json["name"] == null ? null : json["name"],
        description: json["description"] == null ? null : json["description"],
        order: json["order"] == null ? null : json["order"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "is_active": isActive == null ? null : isActive,
        "is_deleted": isDeleted == null ? null : isDeleted,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "name": name == null ? null : name,
        "description": description == null ? null : description,
        "order": order == null ? null : order,
      };
}

class Images {
  Images({
    this.id,
    this.imageUrl,
  });

  final int id;
  final String imageUrl;

  factory Images.fromJson(Map<String, dynamic> json) => Images(
        id: json["id"] == null ? null : json["id"],
        imageUrl: json["ImageUrl"] == null ? null : json["ImageUrl"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "ImageUrl": imageUrl == null ? null : imageUrl,
      };
}
