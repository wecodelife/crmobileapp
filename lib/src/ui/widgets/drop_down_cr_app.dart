import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DropDownCrApp extends StatefulWidget {
  List<String> dropDownList;
  String title;
  String dropDownValue;
  ValueChanged<String> onClicked;
  DropDownCrApp(
      {this.title, this.dropDownList, this.dropDownValue, this.onClicked});
  @override
  _DropDownCrAppState createState() => _DropDownCrAppState();
}

String dropdownValue;

class _DropDownCrAppState extends State<DropDownCrApp> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 10),
      width: screenWidth(context, dividedBy: 1.23),
      child: DropdownButton<String>(
        underline: Divider(
          thickness: 1.0,
          color: Constants.kitGradients[0],
        ),
        icon: Row(
          children: [
            Icon(
              Icons.arrow_drop_down,
              size: 30,
              color: Constants.kitGradients[0],
            ),
          ],
        ),
        iconSize: 20,
        dropdownColor: Colors.black,
        value:
            widget.dropDownValue == "" ? dropdownValue : widget.dropDownValue,
        style: TextStyle(fontSize: 16, color: Constants.kitGradients[0]),
        items: widget.dropDownList
            .map<DropdownMenuItem<String>>(
                (String value) => DropdownMenuItem<String>(
                    value: value,
                    child: Container(
                        width: screenWidth(context, dividedBy: 1.34),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              value,
                              style: TextStyle(fontFamily: 'OpenSansRegular'),
                            ),
                          ],
                        ))))
            .toList(),
        onChanged: (selectedValue) {
          setState(() {
            widget.dropDownValue = selectedValue;
            print(widget.dropDownValue);
          });
          widget.onClicked(widget.dropDownValue);
        },
        hint: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              widget.title,
              style: TextStyle(fontSize: 18, color: Constants.kitGradients[0]),
            ),
          ],
        ),
      ),
    );
  }
}
