import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BookingToast extends StatefulWidget {
  @override
  _BookingToastState createState() => _BookingToastState();
}

class _BookingToastState extends State<BookingToast> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      //height: screenHeight(context, dividedBy: 2.1),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Constants.kitGradients[3],
        //color:Colors.red,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: screenHeight(context, dividedBy: 6.5),
            width: screenWidth(context, dividedBy: 1),
            padding: EdgeInsets.symmetric(
              vertical: screenHeight(context, dividedBy: 30),
              horizontal: screenWidth(context, dividedBy: 30),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10)),
              color: Color(0xff3F4A40),
            ),
            child: SvgPicture.asset("assets/icons/circle_right_icon.svg"),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 35),
          ),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 30),
            ),
            child: Text(
              "The training session has been canceled and \$100 has been automatically withdrawn from your account.",
              style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'OpenSansRegular',
                  fontSize: 16),
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 15),
          ),
        ],
      ),
    );
  }
}
