// To parse this JSON data, do
//
//     final getFacilityTypeResponse = getFacilityTypeResponseFromJson(jsonString);

import 'dart:convert';

GetFacilityTypeResponse getFacilityTypeResponseFromJson(String str) =>
    GetFacilityTypeResponse.fromJson(json.decode(str));

String getFacilityTypeResponseToJson(GetFacilityTypeResponse data) =>
    json.encode(data.toJson());

class GetFacilityTypeResponse {
  GetFacilityTypeResponse({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final List<Datums> data;

  factory GetFacilityTypeResponse.fromJson(Map<String, dynamic> json) =>
      GetFacilityTypeResponse(
        message: json["message"] == null ? null : json["message"],
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null
            ? null
            : List<Datums>.from(json["data"].map((x) => Datums.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "status": status == null ? null : status,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datums {
  Datums({
    this.id,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.name,
    this.description,
    this.order,
  });

  final int id;
  final bool isActive;
  final bool isDeleted;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String name;
  final String description;
  final int order;

  factory Datums.fromJson(Map<String, dynamic> json) => Datums(
        id: json["id"] == null ? null : json["id"],
        isActive: json["is_active"] == null ? null : json["is_active"],
        isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        name: json["name"] == null ? null : json["name"],
        description: json["description"] == null ? null : json["description"],
        order: json["order"] == null ? null : json["order"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "is_active": isActive == null ? null : isActive,
        "is_deleted": isDeleted == null ? null : isDeleted,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "name": name == null ? null : name,
        "description": description == null ? null : description,
        "order": order == null ? null : order,
      };
}
