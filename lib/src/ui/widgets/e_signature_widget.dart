import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class ESignatureTile extends StatefulWidget {
  final String heading;
  final int index;
  final int count;
  final Function onPressed;
  ESignatureTile({this.onPressed, this.heading, this.count, this.index});
  @override
  _ESignatureTileState createState() => _ESignatureTileState();
}

class _ESignatureTileState extends State<ESignatureTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Container(
        width: screenWidth(context, dividedBy: 6),
        height: screenHeight(context, dividedBy: 20),
        child: Column(
          children: [
            Center(
              child: Text(
                widget.heading,
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'OpenSansSemibold',
                  fontSize: 16,
                ),
              ),
            ),
            Spacer(
              flex: 1,
            ),
            Divider(
              thickness: 4.0,
              color: widget.count == widget.index
                  ? Constants.kitGradients[1]
                  : Constants.kitGradients[5],
            )
          ],
        ),
      ),
    );
  }
}
