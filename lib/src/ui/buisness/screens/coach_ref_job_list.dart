import 'package:app_template/material/pickers/date_range_picker_dialog.dart';
import 'package:app_template/src/ui/screens/home_screen.dart';
import 'package:app_template/src/ui/widgets/bottom_navigation_bar_widget.dart';
import 'package:app_template/src/ui/widgets/button.dart';
import 'package:app_template/src/ui/widgets/coach_ref_job_list_tile.dart';
import 'package:app_template/src/ui/widgets/facility_soccer_filed_tile.dart';
import 'package:app_template/src/ui/widgets/home_fliter_tile.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class CoachRefJobListPage extends StatefulWidget {
  @override
  _CoachRefJobListPageState createState() => _CoachRefJobListPageState();
}

class _CoachRefJobListPageState extends State<CoachRefJobListPage> {
  List<String> dayType = ["Today", "Week", "Month"];
  List<bool> borderType = [false, true, true];
  List<String> filters = ["All Sports", "Time", "\$,\$\$"];
  List<String> titles = [
    "FootBall Field",
    "FootBall Field",
    "Soccer Field",
    "Soccer Field"
  ];
  List<String> subtitles = ["Field A", "Field C", "Field B", "Field D"];
  List<String> images = [
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png",
    "assets/images/soccer_field.png"
  ];
  List<String> icons = [
    " assets/icons/soccer_icon.svg",
    " assets/icons/volleyball.svg",
    " assets/icons/soccer_icon.svg",
    " assets/icons/volleyball.svg",
  ];

  int count = 1;
  int fcount = 1;
  List<bool> color = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[6],
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: SingleChildScrollView(
        child: Container(
          //height: screenHeight(context, dividedBy: 1),
          width: screenWidth(context, dividedBy: 1),
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 35)),
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),

                //Buttons
                Container(
                  //width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 25),
                  alignment: Alignment.center,
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: dayType.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        margin: EdgeInsets.symmetric(horizontal: 7),
                        child: CRButton(
                          buttonName: dayType[index],
                          buttonWidth: 100,
                          buttonBackgroundColor: Color(0xff3F4A40),
                          buttonBorder: borderType[index],
                          borderRadius: 0,
                          textColor: Colors.white,
                          onPressed: () {},
                        ),
                      );
                    },
                  ),
                ),

                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),

                //Filters
                Container(
                  //width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 15),
                  alignment: Alignment.center,
                  child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: dayType.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        // margin: EdgeInsets.symmetric(horizontal: 7),
                        child: HomeFilterTile(
                          label: filters[index],
                          colorChange: color[index],
                        ),
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),

                //Expansion Activities
                Column(
                  children: [
                    Row(
                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        fcount == 1
                            ? GestureDetector(
                                onTap: () {
                                  setState(() {
                                    fcount = 2;
                                  });
                                },
                                child: Icon(
                                  Icons.keyboard_arrow_down,
                                  size: 24,
                                  color: Colors.white,
                                ),
                              )
                            : GestureDetector(
                                onTap: () {
                                  setState(() {
                                    fcount = 1;
                                  });
                                },
                                child: Icon(
                                  Icons.keyboard_arrow_up,
                                  size: 24,
                                  color: Colors.white,
                                ),
                              ),
                        Container(
                          width: screenWidth(context, dividedBy: 3),
                          child: Text(
                            "Activities " + "(" + "1" ")",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'OpenSansSemiBold',
                                fontSize: screenWidth(context, dividedBy: 28)),
                          ),
                        ),
                        Container(
                          width: screenWidth(
                            context,
                            dividedBy: 2,
                          ),
                          height: 2,
                          color: Color(0xff3F4A40),
                        )
                      ],
                    ),
                  ],
                ),
                // SizedBox(
                //   height: screenHeight(context, dividedBy: 40),
                // ),
                fcount == 2
                    ? Container(
                        //flex: 2,
                        child: ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: 1,
                            itemBuilder: (BuildContext context, int index) {
                              return CoachRefJobListTile(
                                contact: "Greg Smith",
                                date: "12/10/2020",
                                startTime: "09:00AM",
                                sport: "Soccer",
                                duration: "5 Hours",
                                rate: "100",
                              );
                            }),
                      )
                    : Container(),
                Divider(
                  color: Color(0xff3F4A40),
                ),

                //Expansion Facilities

                Column(
                  children: [
                    Row(
                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        count == 1
                            ? GestureDetector(
                                onTap: () {
                                  setState(() {
                                    count = 2;
                                  });
                                },
                                child: Icon(
                                  Icons.keyboard_arrow_down,
                                  size: 24,
                                  color: Colors.white,
                                ),
                              )
                            : GestureDetector(
                                onTap: () {
                                  setState(() {
                                    count = 1;
                                  });
                                },
                                child: Icon(
                                  Icons.keyboard_arrow_up,
                                  size: 24,
                                  color: Colors.white,
                                ),
                              ),
                        Container(
                          width: screenWidth(context, dividedBy: 2.5),
                          child: Text(
                            "Facility Calendars " + "(" + "6" ")",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'OpenSansSemiBold',
                                fontSize: screenWidth(context, dividedBy: 28)),
                          ),
                        ),
                        Container(
                          width: screenWidth(
                            context,
                            dividedBy: 2.3,
                          ),
                          height: 2,
                          color: Color(0xff3F4A40),
                        )
                      ],
                    ),
                  ],
                ),
                count == 2
                    ? Container(
                        //flex: 4,
                        child: ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: 3,
                            itemBuilder: (BuildContext context, int index) {
                              return FacilitySoccerFieldTile(
                                title: titles[index],
                                image: images[index],
                                subtitle: subtitles[index],
                                icon: "assets/icons/soccer_icon.svg",
                                hasIcon: true,
                                hasButton: true,
                                hasPrice: false,
                                buttonTitle: "View Calendar",
                                onPressed: () {
                                  selectDateRange(context);
                                },
                              );
                            }),
                      )
                    : Container(),
                // SizedBox(
                //   height: screenHeight(context, dividedBy: 40),
                // ),
                Divider(
                  color: Color(0xff3F4A40),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<Null> selectDateRange(BuildContext context) async {
    DateTimeRange picked = await showDatesRangePicker(
        context: context,
        // initialDateRange: DateTimeRange(
        //   start: DateTime.now().add(Duration(days: 0)),
        //   end: DateTime.now().add(Duration(days: 1)),
        // ),

        firstDate: DateTime.now(),
        lastDate: DateTime(DateTime.now().year + 5),
        helpText: "Select_Date_Range",
        cancelText: "CANCEL",
        confirmText: "OK",
        saveText: "SAVE",
        bottomNavigationWidget: BottomNavigationWidget(
          coach: true,
          navigatePageName: HomePage(),
          sessionName: "coach",
          image: "assets/icons/group_image.png",
        ),
        errorFormatText: "Invalid_format",
        errorInvalidText: "Out_of_range",
        errorInvalidRangeText: "Invalid_range",
        fieldStartHintText: "Start_Date",
        sessionName: "business",
        fieldEndLabelText: "End_Date");

    // if (picked != null) {
    //   state.didChange(picked);
    // }
  }
}
