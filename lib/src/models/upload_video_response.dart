// To parse this JSON data, do
//
//     final uploadVideoResponse = uploadVideoResponseFromJson(jsonString);

import 'dart:convert';

UploadVideoResponse uploadVideoResponseFromJson(String str) => UploadVideoResponse.fromJson(json.decode(str));

String uploadVideoResponseToJson(UploadVideoResponse data) => json.encode(data.toJson());

class UploadVideoResponse {
  UploadVideoResponse({
    this.message,
    this.data,
    this.status,
  });

  final String message;
  final Data data;
  final int status;

  factory UploadVideoResponse.fromJson(Map<String, dynamic> json) => UploadVideoResponse(
    message: json["message"] == null ? null : json["message"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
    status: json["status"] == null ? null : json["status"],
  );

  Map<String, dynamic> toJson() => {
    "message": message == null ? null : message,
    "data": data == null ? null : data.toJson(),
    "status": status == null ? null : status,
  };
}

class Data {
  Data({
    this.id,
    this.videoUrl,
  });

  final int id;
  final String videoUrl;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"] == null ? null : json["id"],
    videoUrl: json["VideoURL"] == null ? null : json["VideoURL"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "VideoURL": videoUrl == null ? null : videoUrl,
  };
}
