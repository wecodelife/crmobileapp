import 'package:app_template/src/bloc/user_bloc.dart';
import 'package:app_template/src/models/update_user_type_request_model.dart';
import 'package:app_template/src/ui/buisness/screens/business_details.dart';
import 'package:app_template/src/ui/coach/screens/onboarding_coach%20_%202.dart';
import 'package:app_template/src/ui/screens/on_boarding_parent_athelete_2.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/designation_selection_box.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class OnBoarding5 extends StatefulWidget {
  int dropDownValue;
  OnBoarding5({this.dropDownValue});
  @override
  _OnBoarding5State createState() => _OnBoarding5State();
}

class _OnBoarding5State extends State<OnBoarding5> {
  bool coach = false;
  bool parent = false;
  bool businessman = false;
  bool allSelected = false;
  bool disabled = true;
  bool loading = false;
  int id;
  UserBloc userBloc = UserBloc();
  didUpdate() {
    if (coach == true ||
        parent == true ||
        businessman == true ||
        allSelected == true) {
      disabled = false;
    } else {
      disabled = true;
    }
  }

  @override
  void initState() {
    userBloc.updateUserTypeResponse.listen((event) {
      setState(() {
        loading = false;
      });
      if (event.status == 200) {
        if (parent == true) push(context, OnBoardingParentAthlete2());
        if (businessman == true) push(context, BusinessDetails());
        if (coach == true) push(context, OnBoardingCoach2());
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: loading == false
          ? Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      TextProgressIndicator(
                        heading: "WHO ARE YOU?",
                        descriptionText:
                            "This will help figure out what kind of an account to set up for you.",
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      OnBoardingProgressIndicator(
                        width: 10,
                      ),
                      DesignationSelectionBox(
                        descriptionText:
                            "Looking to coach/train Athletes or Referee/Umpire games, or maybe do both",
                        heading: "I AM A COACH/ REFEREE",
                        onPressed: () {
                          setState(() {
                            id = 1;
                            coach = !coach;
                            print(coach);
                            parent = false;
                            businessman = false;
                            allSelected = false;
                            didUpdate();
                          });
                        },
                        selected: coach,
                      ),
                      DesignationSelectionBox(
                        descriptionText:
                            "Looking to trade equipment, hire trainers, find leagues or help my kid progress in a sport.",
                        heading: "I AM A PARENT/ATHLETE",
                        onPressed: () {
                          setState(() {
                            id = 2;
                            parent = !parent;
                            coach = false;
                            businessman = false;
                            allSelected = false;
                            didUpdate();
                          });
                        },
                        selected: parent,
                      ),
                      DesignationSelectionBox(
                        descriptionText:
                            "A Club, Organization, or Facility looking to find members or rent out facilities.",
                        heading: "I AM A BUSINESSMAN",
                        onPressed: () {
                          setState(() {
                            id = 3;
                            businessman = !businessman;
                            parent = false;
                            coach = false;
                            allSelected = false;
                            didUpdate();
                          });
                        },
                        selected: businessman,
                      ),
                      DesignationSelectionBox(
                        descriptionText: "Perfect Blend of Work and Play",
                        heading: "I AM A COACH/REF & PARENT/ATHLETE",
                        onPressed: () {
                          setState(() {
                            id = 4;
                            allSelected = !allSelected;
                            parent = false;
                            businessman = false;
                            coach = false;
                            didUpdate();
                          });
                        },
                        selected: allSelected,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 40),
                ),
                BuildButton(
                  title: "NEXT",
                  onPressed: () {
                    setState(() {
                      loading = true;
                    });
                    // print("ssdj $id ${widget.dropDownValue.id.toString()}");
                    userBloc.updateUserType(
                        updateUserTypeRequestModel: UpdateUserTypeRequestModel(
                            userType: id,
                            invitationType: widget.dropDownValue.toString()
                            // widget.dropDownValue.id.toString()
                            ));
                  },
                )
              ],
            )
          : Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }
}
