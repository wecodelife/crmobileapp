import 'package:app_template/src/services/stripe_service.dart';
import 'package:app_template/src/ui/screens/on_boarding_athlete_finished_page.dart';
import 'package:app_template/src/ui/widgets/build_button.dart';
import 'package:app_template/src/ui/widgets/on_boarding_progress_indicator.dart';
import 'package:app_template/src/ui/widgets/onboarding4_appBar.dart';
import 'package:app_template/src/ui/widgets/text_progress_indicator.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class StripeCardPage extends StatefulWidget {
  @override
  _StripeCardPageState createState() => _StripeCardPageState();
}

class _StripeCardPageState extends State<StripeCardPage> {
  bool onClick = false;
  bool disabled = true;
  int count;
  @override
  void initState() {
    super.initState();
    StripeService.init();
  }

  payViaNewCard(BuildContext context) async {
    // ProgressDialog dialog = new ProgressDialog(context);
    // dialog.style(
    //     message: 'Please wait...'
    // );
    // await dialog.show();
    var response =
        await StripeService.payWithNewCard(amount: '15000', currency: 'INR');
    // await dialog.hide();
    showToast(response.message);
    // Scaffold.of(context).showSnackBar(SnackBar(
    //   content: Text(response.message),
    //   duration:
    //       new Duration(milliseconds: response.success == true ? 1200 : 3000),
    // ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        actions: [OnBoardingAppBar()],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 20)),
        child: Column(
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            TextProgressIndicator(
              heading: "LETS GET YOUR MONEY SETUP",
              descriptionText:
                  "Connect an account with any of the services below to be able to pay for items & coaches.",
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            OnBoardingProgressIndicator(
              width: 1.2,
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 6),
            ),
            GestureDetector(
              onTap: () {
                payViaNewCard(context);
              },
              child: Container(
                  height: screenHeight(context, dividedBy: 7),
                  width: screenWidth(context, dividedBy: 1.2),
                  decoration: BoxDecoration(
                    border: Border.all(color: Constants.kitGradients[1]),
                    color: Colors.transparent,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // SvgPicture.asset(logo[index]),
                      // SizedBox(
                      //   height: screenHeight(context, dividedBy: 50),
                      // ),
                      Text(
                        "Add a new card",
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                          fontFamily: 'OswaldSemiBold',
                        ),
                      )
                    ],
                  )),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                  height: screenHeight(context, dividedBy: 7),
                  width: screenWidth(context, dividedBy: 1.2),
                  decoration: BoxDecoration(
                    border: Border.all(color: Constants.kitGradients[1]),
                    color: Colors.transparent,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // SvgPicture.asset(logo[index]),
                      // SizedBox(
                      //   height: screenHeight(context, dividedBy: 50),
                      // ),
                      Text(
                        "Use Existing Card",
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                          fontFamily: 'OswaldSemiBold',
                        ),
                      )
                    ],
                  )),
            ),

            // SizedBox(
            //   height: screenHeight(context, dividedBy: 4),
            // ),
            SizedBox(
              height: screenHeight(context, dividedBy: 5),
            ),
            BuildButton(
              title: "NEXT",
              disabled: disabled,
              onPressed: () {
                push(context, OnBoardingAthleteFinished());
              },
            ),

            SizedBox(
              width: screenWidth(context, dividedBy: 30),
            )
          ],
        ),
      ),
    );
  }
}
